#include <string.h>
#include <gtk/gtk.h>
#include <unistd.h>
#include "gimpressionist.h"
#include "ppmtool.h"

GtkWidget *presetnameentry = NULL;
GtkWidget *presetlist = NULL;

char *factory_defaults = "<Factory defaults>";

void presetsrefresh(void)
{
  GtkWidget *list = presetlist;
  GtkWidget *tmpw;
  int n = g_list_length(GTK_LIST(list)->children);

  gtk_list_clear_items(GTK_LIST(list), 0, n);

  tmpw = gtk_list_item_new_with_label(factory_defaults);
  gtk_container_add(GTK_CONTAINER(list), tmpw);
  gtk_widget_show(tmpw);
  
  readdirintolist("Presets", list, NULL);
}

void applypreset(void)
{
  GList *h = GTK_LIST(presetlist)->selection;
  GtkWidget *tmpw = h->data;
  char *l;
  static char fname[200];
  FILE *f;

  gtk_label_get(GTK_LABEL(GTK_BIN(tmpw)->child), &l);

  if(!strcmp(l, factory_defaults)) {
    memcpy(&pcvals, &defaultpcvals, sizeof(pcvals));
    restorevals();
    return;
  }

  sprintf(fname, "Presets/%s", l);
  strcpy(fname, findfile(fname));

  f = fopen(fname, "rb");
  if(!f) {
    fprintf(stderr, "Error opening file \"%s\" for reading!%c\n", fname, 7);
    return;
  }
  fread(&pcvals, sizeof(pcvals), 1, f);
  fclose(f);

  restorevals();
}

void deletepreset(void)
{
  GList *h = GTK_LIST(presetlist)->selection;
  GtkWidget *tmpw = h->data;
  char *l;
  static char fname[200];

  gtk_label_get(GTK_LABEL(GTK_BIN(tmpw)->child), &l);

  sprintf(fname, "Presets/%s", l);
  strcpy(fname, findfile(fname));

  unlink(fname);
  presetsrefresh();
}

void savepreset(GtkWidget *wg, GtkWidget *p)
{
  char *l = GTK_ENTRY(p)->text;
  static char fname[200];
  FILE *f;

  sprintf(fname, "%s/.gimp/gimpressionist/Presets/%s", getenv("HOME"), l);

  f = fopen(fname, "wb");
  if(!f) {
    fprintf(stderr, "Error opening file \"%s\" for writing!%c\n", fname, 7);
    return;
  }

  fwrite(&pcvals, sizeof(pcvals), 1, f);
  fclose(f);
  presetsrefresh();
}

void create_presetpage(GtkNotebook *notebook)
{
  GtkWidget *box1, *thispage, *box2;
  GtkWidget *labelbox, *menubox;
  GtkWidget *scrolled_win, *list;
  GtkWidget *tmpw;
  char title[100];

  sprintf(title, "Presets");

  labelbox = gtk_hbox_new (FALSE, 0);
  tmpw = gtk_label_new(title);
  gtk_box_pack_start(GTK_BOX(labelbox), tmpw, FALSE, FALSE, 0);
  gtk_widget_show_all(labelbox);

  menubox = gtk_hbox_new (FALSE, 0);
  tmpw = gtk_label_new(title);
  gtk_box_pack_start(GTK_BOX(menubox), tmpw, FALSE, FALSE, 0);
  gtk_widget_show_all(menubox);


  presetlist = list = gtk_list_new ();

  thispage = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width (GTK_CONTAINER (thispage), 5);
  gtk_widget_show(thispage);

  box1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start(GTK_BOX(thispage), box1, FALSE, FALSE, 0);
  gtk_widget_show (box1);

  presetnameentry = tmpw = gtk_entry_new();
  gtk_box_pack_start (GTK_BOX (box1), tmpw, FALSE, FALSE, 0);
  gtk_widget_set_usize(tmpw, 150, -1);
  gtk_widget_show(tmpw);

  tmpw = gtk_button_new_with_label(" Save current ");
  gtk_box_pack_start(GTK_BOX(box1), tmpw,FALSE,FALSE,5);
  gtk_widget_show (tmpw);
  gtk_signal_connect (GTK_OBJECT(tmpw), "clicked",
		      GTK_SIGNAL_FUNC(savepreset),
		      presetnameentry);

  box1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start(GTK_BOX(thispage), box1, FALSE, FALSE, 0);
  gtk_widget_show (box1);

  scrolled_win = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
				  GTK_POLICY_AUTOMATIC, 
				  GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX (box1), scrolled_win, FALSE, FALSE, 0);
  gtk_widget_show (scrolled_win);
  gtk_widget_set_usize(scrolled_win, 150,150);

  /* list = gtk_list_new (); */ /* Moved up */
  gtk_list_set_selection_mode (GTK_LIST (list), GTK_SELECTION_BROWSE);
  gtk_container_add (GTK_CONTAINER (scrolled_win), list);
  gtk_widget_show (list);

  tmpw = gtk_list_item_new_with_label(factory_defaults);
  gtk_container_add(GTK_CONTAINER(list), tmpw);
  gtk_widget_show(tmpw);

  readdirintolist("Presets", list, NULL);

  box2 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start(GTK_BOX(box1), box2,FALSE,FALSE,5);
  gtk_widget_show (box2);
//  gtk_container_border_width (GTK_CONTAINER (box2), 5);

  tmpw = gtk_button_new_with_label(" Apply ");
  gtk_box_pack_start(GTK_BOX(box2), tmpw,FALSE,FALSE,0);
  gtk_widget_show (tmpw);
  gtk_signal_connect (GTK_OBJECT(tmpw), "clicked",
		      GTK_SIGNAL_FUNC(applypreset),
		      NULL);

  tmpw = gtk_button_new_with_label(" Delete ");
  gtk_box_pack_start(GTK_BOX(box2), tmpw, FALSE, FALSE,0);
  gtk_widget_show (tmpw);
  gtk_signal_connect (GTK_OBJECT(tmpw), "clicked",
		      GTK_SIGNAL_FUNC(deletepreset),
		      NULL);

  tmpw = gtk_button_new_with_label(" Refresh ");
  gtk_box_pack_start(GTK_BOX(box2), tmpw, FALSE, FALSE,0);
  gtk_widget_show (tmpw);
  gtk_signal_connect (GTK_OBJECT(tmpw), "clicked",
		      GTK_SIGNAL_FUNC(presetsrefresh),
		      NULL);

  gtk_notebook_append_page_menu (notebook, thispage, labelbox, menubox);
}
