#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gimpressionist.h"
#include "ppmtool.h"
#include <libgimp/gimp.h>


void prepbrush(struct ppm *p)
{
  int x, y;

  for(y = 0; y< p->height; y++) {
    for(x = 0; x < p->width; x++) {
      p->col[y][x].g = p->col[y][x].b = 0;
    }
  }

  for(y = 1; y< p->height; y++) {
    for(x = 1; x < p->width; x++) {
      int v = p->col[y][x].r - p->col[y-1][x-1].r;
      if(v < 0) v = 0;
      p->col[y][x].g = v;
    }
  }
}

void loadbrush(char *fn, struct ppm *p)
{
  loadppm(fn,p);
  prepbrush(p);
}

double sumbrush(struct ppm *p)
{
  int x, y;
  double sum = 0;
  for(y = 0; y< p->height; y++) {
    for(x = 0; x < p->width; x++) {
      sum += p->col[y][x].r;
    }
  }
  return sum;
}

int gethue(struct rgbcolor *rgb)
{
  double h, v, temp, diff;
  if((rgb->r == rgb->g) && (rgb->r == rgb->b)) /* Gray */
    return 0;
  v = (rgb->r > rgb->g ? rgb->r : rgb->g);     /* v = st<F8>rste verdi */
  if(rgb->b > v) v = rgb->b;
  temp = (rgb->r > rgb->g ? rgb->g : rgb->r ); /* temp = minste */
  if(rgb->b < temp) temp = rgb->b;
  diff = v - temp;

  if(v == rgb->r)
    h = ((double)rgb->g - rgb->b) / diff;
  else if(v == rgb->g)
    h = ((double)rgb->b - rgb->r) / diff + 2;
  else /* v == rgb->b */
    h = ((double)rgb->r - rgb->g) / diff + 4;
  if(h < 0) h += 6;
  return h * 255.0 / 6.0;
}

void repaint(struct ppm *p)
{
  int x, y;
  int tx = 0, ty = 0;
  struct ppm tmp = {0,0,NULL};
  int r, g, b, n, h, i;
  int numbrush, maxbrushwidth, maxbrushheight;
  struct rgbcolor back = {0,0,0};
  struct ppm *brushes;
  struct ppm *brush;
  int cx, cy, maxdist;
  double scale, relief, startangle, anglespan, density, bgamma;
  double edgedarken;
  double *brushsum;
  double thissum;
  int max_progress;
  struct ppm paperppm = {0,0,NULL};
  struct ppm dirmap = {0,0,NULL};
  int *xpos = NULL, *ypos = NULL;
  int step = 1;

  srand(time(NULL) + getpid());

  numbrush = pcvals.orientnum;
  startangle = pcvals.orientfirst;
  anglespan = pcvals.orientlast;

  relief = pcvals.brushrelief / 100.0;
  density = pcvals.brushdensity;

  if(pcvals.placetype == 1) density /= 5.0;

  bgamma = pcvals.brushgamma;

  edgedarken = 1.0 - pcvals.generaldarkedge;

  brushes = safemalloc(numbrush * sizeof(struct ppm));
  brushsum = safemalloc(numbrush * sizeof(double));

  brushes[0].col = NULL;
  loadppm(pcvals.selectedbrush, &brushes[0]);
  scale = pcvals.brushscale;
  scale = scale / brushes[0].width;

  if(bgamma != 1.0)
    ppmgamma(&brushes[0], 1.0/bgamma, 1,0,0);
  
  for(i = 1; i < numbrush; i++) {
    brushes[i].col = NULL;
    copyppm(&brushes[0], &brushes[i]);
  }
  for(i = 0; i < numbrush; i++) {
    double angstep = anglespan / numbrush;
    resize(&brushes[i], brushes[i].width * scale, brushes[i].height * scale);
    pad(&brushes[i], 25,25,25,25, &back);
    freerotate(&brushes[i], startangle + i * angstep);
    pad(&brushes[i], 1,1,1,1, &back);
    autocrop(&brushes[i],1);
    prepbrush(&brushes[i]);
    brushsum[i] = sumbrush(&brushes[i]);
  }

  brush = &brushes[0];
  thissum = brushsum[0];

  maxbrushwidth = maxbrushheight = 0;
  for(i = 0; i < numbrush; i++) {
    if(brushes[i].width > maxbrushwidth) maxbrushwidth = brushes[i].width;
    if(brushes[i].height > maxbrushheight) maxbrushheight = brushes[i].height;
  }

  if(pcvals.generalpaintedges)
    edgepad(p, maxbrushwidth, maxbrushwidth, maxbrushheight, maxbrushheight);

  if(pcvals.generalbgtype == 0) {
    struct rgbcolor tmpcol;
    newppm(&tmp, p->width, p->height);
    memcpy(&tmpcol, pcvals.color, 3);
    fill(&tmp, &tmpcol);
  } else if(pcvals.generalbgtype == 1) {
    copyppm(p, &tmp);
  } else {
    scale = pcvals.paperscale / 100.0;
    newppm(&tmp, p->width, p->height);
    loadppm(pcvals.selectedpaper, &paperppm);
    resize(&paperppm, paperppm.width * scale, paperppm.height * scale);
    for(x = 0; x < tmp.width; x++) {
      int rx = x % paperppm.width;
      for(y = 0; y < tmp.height; y++) {
	int ry = y % paperppm.height;
	memcpy(&tmp.col[y][x], &paperppm.col[ry][rx], 3);
      }
    }
  }

  cx = p->width / 2;
  cy = p->height / 2;
  maxdist = sqrt(cx*cx+cy*cy);

  if(pcvals.orienttype == 0) { /* Value */
    newppm(&dirmap, p->width, p->height);
    for(y = 0; y < dirmap.height; y++) {
      struct rgbcolor *dstrow = dirmap.col[y];
      struct rgbcolor *srcrow = p->col[y];
      for(x = 0; x < dirmap.width; x++) {
	dstrow[x].r = (srcrow[x].r + srcrow[x].g + srcrow[x].b) / 3;
      }
    }
  } else if(pcvals.orienttype == 1) { /* Radius */
    newppm(&dirmap, p->width, p->height);
    for(y = 0; y < dirmap.height; y++) {
      struct rgbcolor *dstrow = dirmap.col[y];
      double ysqr = (cy-y)*(cy-y);
      for(x = 0; x < dirmap.width; x++) {
	dstrow[x].r = sqrt((cx-x)*(cx-x)+ysqr) * 255 / maxdist;
      }
    }
  } else if(pcvals.orienttype == 3) { /* Radial */
    newppm(&dirmap, p->width, p->height);
    for(y = 0; y < dirmap.height; y++) {
      struct rgbcolor *dstrow = dirmap.col[y];
      for(x = 0; x < dirmap.width; x++) {
	dstrow[x].r = (M_PI + atan2(cy-y, cx-x)) * 255.0 / (M_PI*2);
      }
    }
  } else if(pcvals.orienttype == 4) { /* Flowing */
    newppm(&dirmap, p->width / 6 + 5, p->height / 6 + 5);
    mkgrayplasma(&dirmap, 15);
    blur(&dirmap, 2, 2);
    blur(&dirmap, 2, 2);
    resize(&dirmap, p->width, p->height);
    blur(&dirmap, 2, 2);
    if(pcvals.generalpaintedges)
      edgepad(&dirmap, maxbrushwidth, maxbrushheight,maxbrushwidth, maxbrushheight);
  } else if(pcvals.orienttype == 5) { /* Hue */
    newppm(&dirmap, p->width, p->height);
    for(y = 0; y < dirmap.height; y++) {
      struct rgbcolor *dstrow = dirmap.col[y];
      struct rgbcolor *srcrow = p->col[y];
      for(x = 0; x < dirmap.width; x++) {
	dstrow[x].r = gethue(&srcrow[x]);
      }
    }
  }
 

  if(pcvals.placetype == 0) {
    i = tmp.width * tmp.height / (maxbrushwidth * maxbrushheight);
    i *= density;
  } else if(pcvals.placetype == 1) {
    i = (tmp.width * density / maxbrushwidth) * (tmp.height * density / maxbrushheight);
    step = i;
    //fprintf(stderr, "step=%d i=%d\n", step, i);
  }

  max_progress = i;

  if(pcvals.placetype == 1) {
    int j;
    xpos = safemalloc(i * sizeof(int));
    ypos = safemalloc(i * sizeof(int));
    for(j = 0; j < i; j++) {
      xpos[j] = (j % (int)(tmp.width * density / maxbrushwidth)) * maxbrushwidth / density;
      ypos[j] = (j / (int)(tmp.width * density / maxbrushwidth)) * maxbrushheight / density;
    }
    for(j = 0; j < i; j++) {
      int a, b;
      a = rand()%i;
      b = xpos[j]; xpos[j] = xpos[a]; xpos[a] = b;
      b = ypos[j]; ypos[j] = ypos[a]; ypos[a] = b;
    }
  }

  saveppm(&tmp, "/tmp/shit.ppm");

  for(; i; i--) {
    if(pcvals.run) /* False if only preview */
      if(i % 20 == 0)
	gimp_progress_update(0.8 - 0.8*((double)i / max_progress));

    if(pcvals.placetype == 0) {
      tx = rand() % (tmp.width - maxbrushwidth);
      ty = rand() % (tmp.height - maxbrushheight);
    } else if(pcvals.placetype == 1) {
      tx = xpos[i-1];
      ty = ypos[i-1];
    }

    if((tx < 0) || (ty < 0) ||
       (tx + maxbrushwidth >= p->width) ||
       (ty + maxbrushheight >= p->height)) {
      //fprintf(stderr, "Internal Error; invalid coords: (%d,%d) i=%d\n", tx,ty,i);
      continue;
    }

    switch(pcvals.orienttype) {
    case 2: /* Random */
      n = rand()%numbrush;
      break;
    case 0: /* Value */
    case 1: /* Radius */
    case 3: /* Radial */
    case 4: /* Flowing */
    case 5: /* Hue */
      n = numbrush * dirmap.col[ty][tx].r / 255;
      break;
    default:
      fprintf(stderr, "Internal error; Unknown orientationtype\n");
      n = 0;
      break;
    }

    /* Should never happen, but hey... */
    if(n < 0) n = 0;
    else if(n >= numbrush) n = numbrush - 1;

    brush = &brushes[n];
    thissum = brushsum[n];

    /* Calculate color - avg. of in-brush pixels */
    r = g = b = 0;
    for(y = 0; y < brush->height; y++) {
      struct rgbcolor *row = p->col[ty+y];
      for(x = 0; x < brush->width; x++) {
	double v;
	if((h = brush->col[y][x].r)) {
	  v = h / 255.0;
	  r += row[tx+x].r * v;
	  g += row[tx+x].g * v;
	  b += row[tx+x].b * v;
	}
      }
    }
    r = r * 255.0 / thissum;
    g = g * 255.0 / thissum;
    b = b * 255.0 / thissum;

    /* Color = center pixel - Looks bad... */
    /* 
    r = p->col[ty+brush->height/2][tx+brush->width/2].r;
    g = p->col[ty+brush->height/2][tx+brush->width/2].g;
    b = p->col[ty+brush->height/2][tx+brush->width/2].b;
    */

    /* Apply brush */
    for(y = 0; y < brush->height; y++) {
      struct rgbcolor *row = tmp.col[ty+y];
      for(x = 0; x < brush->width; x++) {
	double v;
	h = brush->col[y][x].r;
	if(!h) continue;
	v = (1.0 - h / 255.0) * edgedarken;
	row[tx+x].r *= v;
	row[tx+x].g *= v;
	row[tx+x].b *= v;
	v = h / 255.0;
	row[tx+x].r += r * v;
	row[tx+x].g += g * v;
	row[tx+x].b += b * v;
      }
    }

    if(relief > 0.001) {
      for(y = 1; y < brush->height; y++) {
	struct rgbcolor *row = tmp.col[ty+y];
	for(x = 1; x < brush->width; x++) {
	  h = brush->col[y][x].g * relief;
	  if(h > 255) h = 255;
	  row[tx+x].r = (row[tx+x].r * (255-h) + 255 * h) / 255;
	  row[tx+x].g = (row[tx+x].g * (255-h) + 255 * h) / 255;
	  row[tx+x].b = (row[tx+x].b * (255-h) + 255 * h) / 255;
	}
      }
    }
  }
  for(i = 0; i < numbrush; i++) {
    killppm(&brushes[i]);
  }
  free(brushes);
  free(brushsum);

  if(pcvals.generalpaintedges)
    crop(&tmp, maxbrushwidth, maxbrushheight, tmp.width - maxbrushwidth, tmp.height - maxbrushheight);

  killppm(p);
  p->width = tmp.width;
  p->height = tmp.height;
  p->col = tmp.col;

  relief = pcvals.paperrelief / 100.0;
  if(relief > 0.001) {
    scale = pcvals.paperscale / 100.0;
    if(pcvals.paperinvert)
      relief *= -1.0;

    if(paperppm.col) {
      memcpy(&tmp, &paperppm, sizeof(struct ppm));
      paperppm.col = NULL;
    } else {
      tmp.col = NULL;
      loadppm(pcvals.selectedpaper, &tmp);
      resize(&tmp, tmp.width * scale, tmp.height * scale);
    }
    for(x = 0; x < p->width; x++) {
      double h, v;
      int px = x % tmp.width, py;
      for(y = 0; y < p->height; y++) {
	py = y % tmp.height;
	h = (tmp.col[py][px].r - (int)tmp.col[(py+1)%tmp.height][(px+1)%tmp.width].r) / -2.0 * relief;
	if(h <= 0.0) {
	  v = 1.0 + h/128.0;
	  if(v < 0.0) v = 0.0; else if(v > 1.0) v = 1.0;
	  p->col[y][x].r *= v;
	  p->col[y][x].g *= v;
	  p->col[y][x].b *= v;
	} else {
	  v = h/128.0;
	  if(v < 0.0) v = 0.0; else if(v > 1.0) v = 1.0;
	  p->col[y][x].r = p->col[y][x].r * (1.0-v) + 255 * v;
	  p->col[y][x].g = p->col[y][x].g * (1.0-v) + 255 * v;
	  p->col[y][x].b = p->col[y][x].b * (1.0-v) + 255 * v;
	}
      }
    }
    killppm(&tmp);
  }
  if(paperppm.col) killppm(&paperppm);
  if(dirmap.col) killppm(&dirmap);
  if(pcvals.run) /* False if only preview */
    gimp_progress_update(0.8);
}


