#include <string.h>
#include <gtk/gtk.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include "gimpressionist.h"
#include "ppmtool.h"
#include "libgimp/gimp.h"

GtkWidget *window = NULL;

struct ppm infile = {0,0,NULL};

char *findfile(char *fn)
{
  static char globalpath[200] = "";
  static char localpath[200] = "";
  static char file[200];
  struct stat st;

  if(!globalpath[0]) strcpy(globalpath, GLOBALDATADIR);
  if(!localpath[0]) sprintf(localpath, "%s/.gimp/gimpressionist", getenv("HOME"));
  sprintf(file, "%s/%s", globalpath, fn);
  if(!stat(file, &st)) return file;
  sprintf(file, "%s/%s", localpath, fn);
  if(!stat(file, &st)) return file;
  return NULL;
}

void storevals(void)
{
  pcvals.orientnum = GTK_ADJUSTMENT(orientnumadjust)->value;
  pcvals.orientfirst = GTK_ADJUSTMENT(orientfirstadjust)->value;
  pcvals.orientlast = GTK_ADJUSTMENT(orientlastadjust)->value;
  pcvals.brushrelief = GTK_ADJUSTMENT(brushreliefadjust)->value;
  pcvals.brushscale = GTK_ADJUSTMENT(brushscaleadjust)->value;
  pcvals.brushdensity = GTK_ADJUSTMENT(brushdensityadjust)->value;
  pcvals.brushgamma = GTK_ADJUSTMENT(brushgammaadjust)->value;
  pcvals.generaldarkedge = GTK_ADJUSTMENT(generaldarkedgeadjust)->value;
  pcvals.paperrelief = GTK_ADJUSTMENT(paperreliefadjust)->value;
  pcvals.paperscale = GTK_ADJUSTMENT(paperscaleadjust)->value;
  pcvals.paperinvert = GTK_TOGGLE_BUTTON(paperinvert)->active;
  pcvals.generalpaintedges = GTK_TOGGLE_BUTTON(generalpaintedges)->active;
}

void restorevals(void)
{

  reselect(brushlist, pcvals.selectedbrush);
  reselect(paperlist, pcvals.selectedpaper);

  gtk_adjustment_set_value(GTK_ADJUSTMENT(orientnumadjust), pcvals.orientnum);
  gtk_adjustment_set_value(GTK_ADJUSTMENT(orientfirstadjust), pcvals.orientfirst);
  gtk_adjustment_set_value(GTK_ADJUSTMENT(orientlastadjust), pcvals.orientlast);

  orientchange(NULL, (void *)pcvals.orienttype);
  placechange(NULL, (void *)pcvals.placetype);
  generalbgchange(NULL, (void *)pcvals.generalbgtype);

  gtk_adjustment_set_value(GTK_ADJUSTMENT(brushreliefadjust), pcvals.brushrelief);
  gtk_adjustment_set_value(GTK_ADJUSTMENT(brushscaleadjust), pcvals.brushscale);
  gtk_adjustment_set_value(GTK_ADJUSTMENT(brushdensityadjust), pcvals.brushdensity);
  gtk_adjustment_set_value(GTK_ADJUSTMENT(brushgammaadjust), pcvals.brushgamma);

  gtk_adjustment_set_value(GTK_ADJUSTMENT(generaldarkedgeadjust), pcvals.generaldarkedge);

  gtk_adjustment_set_value(GTK_ADJUSTMENT(paperreliefadjust), pcvals.paperrelief);
  gtk_adjustment_set_value(GTK_ADJUSTMENT(paperscaleadjust), pcvals.paperscale);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(paperinvert), pcvals.paperinvert);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(generalpaintedges), pcvals.generalpaintedges);

  drawcolor(NULL);
}

static void
dialog_close_callback(GtkWidget *widget, gpointer data)
{
  gtk_main_quit();
}

static void dialog_ok_callback(GtkWidget *widget, gpointer data)
{
  storevals();
  pcvals.run = 1;
  gtk_widget_destroy(GTK_WIDGET(data));
}

static void dialog_cancel_callback(GtkWidget *widget, gpointer data)
{
  pcvals.run = 0;
  gtk_widget_destroy(GTK_WIDGET(data));
}

void reselect(GtkWidget *list, char *fname)
{
  GList *h;
  GtkWidget *tmpw;
  char *tmps, *tmpfile;

  tmpfile = strrchr(fname, '/');
  if(tmpfile)
    fname = ++tmpfile;  

  for(;;) {
    h = GTK_LIST(list)->selection;
    if(!h) break;
    tmpw = h->data;
    if(!tmpw) break;
    gtk_list_unselect_child(GTK_LIST(list), tmpw);
  }
  
  h = GTK_LIST(list)->children;
  while(h) {
    tmpw = h->data;
    gtk_label_get(GTK_LABEL(GTK_BIN(tmpw)->child), &tmps);
    if(!strcmp(tmps, fname)) {
      gtk_list_select_child(GTK_LIST(list), tmpw);
      break;
    }
    h = g_list_next(h);
  }
}

void readdirintolist_real(char *subdir, GtkWidget *list, char *selected)
{
  char fpath[200];
  struct dirent *de;
  struct stat st;
  GtkWidget *selectedw = NULL, *tmpw;
  DIR *dir;
 
  if(selected) {
    if(!selected[0])
      selected = NULL;
    else {
      char *nsel;
      nsel = strrchr(selected, '/');
      if(nsel) selected = ++nsel;
    }
  }

  dir = opendir(subdir);

  if(!dir)
    return;

  for(;;) {
    if(!(de = readdir(dir))) break;
    sprintf(fpath, "%s/%s", subdir, de->d_name);
    stat(fpath, &st);
    if(!S_ISREG(st.st_mode)) continue;
    tmpw = gtk_list_item_new_with_label(de->d_name);
    if(selected)
      if(!strcmp(de->d_name, selected))
	selectedw = tmpw;
    gtk_container_add(GTK_CONTAINER(list), tmpw);
    gtk_widget_show(tmpw);
  }
  if(selectedw)
    gtk_list_select_child(GTK_LIST(list), selectedw);
  else
    gtk_list_select_item(GTK_LIST(list), 0);
}

void readdirintolist(char *subdir, GtkWidget *list, char *selected)
{
  char tmpdir[200];
  sprintf(tmpdir, "%s/.gimp/gimpressionist/%s", getenv("HOME"), subdir);
  readdirintolist_real(tmpdir, list, selected);
  sprintf(tmpdir, "%s/%s", GLOBALDATADIR, subdir);
  readdirintolist_real(tmpdir, list, selected);
}


void showabout(void)
{
  static GtkWidget *window = NULL;
  GtkWidget *tmpw, *tmpvbox, *tmphbox;
  GtkWidget *logobox, *tmpframe;
  int y;

  if(window) {
    gtk_widget_show(window);
    gdk_window_raise(window->window);
    return;
  }
  window = gtk_dialog_new();
  gtk_window_set_title(GTK_WINDOW(window), "The GIMPressionist!");
  gtk_window_position(GTK_WINDOW(window), GTK_WIN_POS_MOUSE);
  gtk_signal_connect(GTK_OBJECT(window), "destroy",
		     GTK_SIGNAL_FUNC (gtk_widget_destroyed),
		     &window);
  gtk_quit_add_destroy (1, GTK_OBJECT(window));
  gtk_signal_connect(GTK_OBJECT(window), "delete_event",
		     GTK_SIGNAL_FUNC (gtk_widget_hide_on_delete),
		     &window);
      
  tmpw = gtk_button_new_with_label("OK");
  GTK_WIDGET_SET_FLAGS(tmpw, GTK_CAN_DEFAULT);
  gtk_signal_connect_object (GTK_OBJECT(tmpw), "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_hide),
			     GTK_OBJECT(window));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->action_area),
		     tmpw, TRUE, TRUE, 0);
  gtk_widget_grab_default(tmpw);
  gtk_widget_show(tmpw);

  tmpvbox = gtk_vbox_new(FALSE, 5);
  gtk_container_border_width(GTK_CONTAINER(tmpvbox), 5);
  gtk_container_add(GTK_CONTAINER(GTK_DIALOG(window)->vbox), tmpvbox);
      
  tmphbox = gtk_hbox_new(TRUE, 5);
  gtk_box_pack_start(GTK_BOX(tmpvbox), tmphbox, TRUE, TRUE, 0);
  gtk_widget_show(tmphbox);
  
  logobox = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (tmphbox), logobox, FALSE, FALSE, 0);
  gtk_widget_show(logobox);
  
  tmpframe = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (tmpframe), GTK_SHADOW_IN);
  gtk_box_pack_start (GTK_BOX (logobox), tmpframe, TRUE, TRUE, 0);
  gtk_widget_show(tmpframe);
  
  tmpw = gtk_preview_new(GTK_PREVIEW_COLOR);
  gtk_preview_size (GTK_PREVIEW(tmpw), 177, 70);

  for(y = 0; y < 70; y++) {
    gtk_preview_draw_row (GTK_PREVIEW(tmpw), &logobuffer[y*177*3], 0, y, 177); 
  }
  gtk_widget_draw(tmpw, NULL);
  gtk_container_add(GTK_CONTAINER(tmpframe), tmpw);
  gtk_widget_show(tmpw);
      
  tmphbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(tmpvbox), tmphbox, TRUE, TRUE, 0);

  tmpw = gtk_label_new("\xa9 1998 Vidar Madsen\n"
		       "vidar@madsen.prosalg.no\n"
		       "http://www.prosalg.no/~vidar/gimpressionist/\n"
		       PLUG_IN_VERSION
		       );
  gtk_box_pack_start(GTK_BOX(tmphbox), tmpw, TRUE, FALSE, 0);
  gtk_widget_show(tmpw);
  
  gtk_widget_show(tmphbox);
  gtk_widget_show(tmpvbox);
  gtk_widget_show(window);
}

int create_dialog(void)
{
  GtkWidget *notebook;
  GtkWidget *tmpw, *box1, *box2;
  gint        argc;
  gchar     **argv;
  guchar     *color_cube;

  argc = 1;
  argv = g_new(char *, 1);
  argv[0] = "gimpressionist";

  gtk_init(&argc, &argv);
  gtk_rc_parse (gimp_gtkrc ());

  gdk_set_use_xshm(gimp_use_xshm());

  gtk_preview_set_gamma(gimp_gamma());
  gtk_preview_set_install_cmap(gimp_install_cmap());
  color_cube = gimp_color_cube();
  gtk_preview_set_color_cube(color_cube[0], color_cube[1], color_cube[2], color_cube[3]);

  gtk_widget_set_default_visual(gtk_preview_get_visual());
  gtk_widget_set_default_colormap(gtk_preview_get_cmap());

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      (GtkSignalFunc)dialog_close_callback,
		      NULL);

  gtk_window_set_title (GTK_WINDOW (window), "The GIMPressionist!");
  gtk_container_border_width (GTK_CONTAINER (window), 5);

  box1 = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), box1);
  gtk_widget_show (box1);

  notebook = gtk_notebook_new ();
  gtk_notebook_set_tab_pos (GTK_NOTEBOOK (notebook), GTK_POS_TOP);
  gtk_box_pack_start (GTK_BOX (box1), notebook, FALSE, FALSE, 5);
  gtk_container_border_width (GTK_CONTAINER (notebook), 0);
  gtk_widget_realize (notebook);
  gtk_widget_show(notebook);

  create_paperpage(GTK_NOTEBOOK (notebook));
  create_brushpage(GTK_NOTEBOOK (notebook));
  create_orientationpage(GTK_NOTEBOOK (notebook));
  create_placementpage(GTK_NOTEBOOK (notebook));
  create_generalpage(GTK_NOTEBOOK (notebook));
  create_presetpage(GTK_NOTEBOOK (notebook));
  create_previewpage(GTK_NOTEBOOK (notebook));

  box2 = gtk_hbox_new (TRUE, 0);
  //gtk_container_border_width (GTK_CONTAINER (box2), 5);
  gtk_box_pack_start (GTK_BOX (box1), box2, FALSE, FALSE, 0);
  gtk_widget_show (box2);

  tmpw = gtk_button_new_with_label(" OK ");
  gtk_signal_connect(GTK_OBJECT(tmpw), "clicked",
		     (GtkSignalFunc)dialog_ok_callback, window);
  gtk_box_pack_start (GTK_BOX (box2), tmpw, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS (tmpw, GTK_CAN_DEFAULT);
  gtk_widget_grab_default (tmpw);
  gtk_widget_show(tmpw);

  tmpw = gtk_button_new_with_label(" Cancel ");
  gtk_signal_connect(GTK_OBJECT(tmpw), "clicked",
		     (GtkSignalFunc)dialog_cancel_callback, window);
  gtk_box_pack_start (GTK_BOX (box2), tmpw, TRUE, TRUE, 0);
  gtk_widget_show(tmpw);

  tmpw = gtk_button_new_with_label(" About... ");
  gtk_signal_connect(GTK_OBJECT(tmpw), "clicked",
		     (GtkSignalFunc)showabout, window);
  gtk_box_pack_start (GTK_BOX (box2), tmpw, TRUE, TRUE, 0);
  gtk_widget_show(tmpw);

  gtk_widget_show(window);

  return 1;
}

int create_gimpressionist(void)
{
  pcvals.run = 0;

  create_dialog();
  gtk_main();
  gdk_flush();

  return pcvals.run;
}


