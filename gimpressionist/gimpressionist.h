#include <gtk/gtk.h>

#define PLUG_IN_NAME "plug_in_gimpressionist"
#define PLUG_IN_VERSION "v0.1g, April 1998"

#ifndef GLOBALDATADIR
#define GLOBALDATADIR "/usr/local/share/gimp/gimpressionist"
#endif

#define PREVIEWSIZE 150


/* Type declaration and definitions */

typedef struct {
  int orientnum;
  double orientfirst;
  double orientlast;
  int orienttype;
  double brushrelief;
  double brushscale;
  double brushdensity;
  double brushgamma;
  int generalbgtype;
  double generaldarkedge;
  double paperrelief;
  double paperscale;
  int paperinvert;
  int run;
  char selectedbrush[100];
  char selectedpaper[100];
  guchar color[3];
  int generalpaintedges;
  int placetype;
} gimpressionist_vals_t;

/* Globals */

extern unsigned char logobuffer[];

extern gimpressionist_vals_t pcvals;
extern gimpressionist_vals_t defaultpcvals;
extern char *path;
extern struct ppm infile;
extern GtkWidget *window;

//extern char *selectedbrush;
extern GtkWidget *brushlist;
extern GtkObject *brushscaleadjust;
extern GtkObject *brushreliefadjust;
extern GtkObject *brushdensityadjust;
extern GtkObject *brushgammaadjust;

//extern char *selectedpaper;
extern GtkWidget *paperlist;
extern GtkObject *paperscaleadjust;
extern GtkObject *paperreliefadjust;
extern GtkWidget *paperinvert;

extern GtkObject *orientnumadjust;
extern GtkObject *orientfirstadjust;
extern GtkObject *orientlastadjust;
extern int orientationtype;

extern GtkObject *generaldarkedgeadjust;
extern int generalbgtype;
extern GtkWidget *generalpaintedges;

extern struct ppm infile;


/* Prototypes */

int create_dialog(void);

void create_paperpage(GtkNotebook *);
void create_brushpage(GtkNotebook *);
void create_orientationpage(GtkNotebook *);
void create_generalpage(GtkNotebook *);
void create_presetpage(GtkNotebook *);
void create_previewpage(GtkNotebook *);
void create_placementpage(GtkNotebook *);

void grabarea(void);
void storevals(void);
void restorevals(void);
char *findfile(char *);

void reselect(GtkWidget *list, char *fname);
void readdirintolist(char *subdir, GtkWidget *list, char *selected);
void drawcolor(GtkWidget *w);
void orientchange(GtkWidget *wg, void *d);
void placechange(GtkWidget *wg, void *d);
void generalbgchange(GtkWidget *wg, void *d);


