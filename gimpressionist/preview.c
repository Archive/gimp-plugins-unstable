#include <string.h>
#include <gtk/gtk.h>
#include <unistd.h>
#include "gimpressionist.h"
#include "ppmtool.h"

GtkWidget *previewprev = NULL;

void updatepreviewprev(GtkWidget *wg, void *d)
{
  int i, j;
  char buf[PREVIEWSIZE*3];
  static struct ppm p = {0,0,NULL};
  static struct ppm backup = {0,0,NULL};

  if(!infile.col && d) grabarea();

  if(!infile.col && !d) {
    memset(buf, 0, PREVIEWSIZE*3);
    for(i = 0; i < PREVIEWSIZE; i++) {
      gtk_preview_draw_row (GTK_PREVIEW(previewprev), buf, 0, i, PREVIEWSIZE);
    }
  } else {
    if(!backup.col) {
      copyppm(&infile, &backup);
      if((backup.width != PREVIEWSIZE) || (backup.height != PREVIEWSIZE))
	resize_fast(&backup, PREVIEWSIZE, PREVIEWSIZE);
    }
    if(!p.col)
      copyppm(&backup, &p);
    if(d) {
      storevals();

      repaint(&p);
    }
    for(i = 0; i < PREVIEWSIZE; i++) {
      memset(buf,0,PREVIEWSIZE*3);
      for(j = 0; j < p.width; j++)
	gtk_preview_draw_row(GTK_PREVIEW(previewprev), (guchar *)p.col[i],
			     0, i, PREVIEWSIZE);
    }
    killppm(&p);
  }
  gtk_widget_draw (previewprev, NULL);
}

void create_previewpage(GtkNotebook *notebook)
{
  GtkWidget *box1, *thispage;
  GtkWidget *labelbox, *menubox;
  GtkWidget *tmpw;
  char title[100];

  sprintf(title, "Preview");

  labelbox = gtk_hbox_new (FALSE, 0);
  tmpw = gtk_label_new(title);
  gtk_box_pack_start(GTK_BOX(labelbox), tmpw, FALSE, FALSE, 0);
  gtk_widget_show_all(labelbox);

  menubox = gtk_hbox_new (FALSE, 0);
  tmpw = gtk_label_new(title);
  gtk_box_pack_start(GTK_BOX(menubox), tmpw, FALSE, FALSE, 0);
  gtk_widget_show_all(menubox);

  thispage = gtk_hbox_new(FALSE, 0);
  gtk_container_border_width (GTK_CONTAINER (thispage), 5);
  gtk_widget_show(thispage);

  box1 = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start(GTK_BOX(thispage), box1, FALSE, FALSE, 0);
  gtk_widget_show (box1);

  previewprev = tmpw = gtk_preview_new (GTK_PREVIEW_COLOR);
  gtk_preview_size(GTK_PREVIEW (tmpw), PREVIEWSIZE, PREVIEWSIZE);
  gtk_box_pack_start(GTK_BOX (box1), tmpw, FALSE, FALSE, 5);
  gtk_widget_show(tmpw);

  tmpw = gtk_button_new_with_label(" Update ");
  gtk_signal_connect(GTK_OBJECT(tmpw), "clicked",
                     (GtkSignalFunc)updatepreviewprev, (void *)1);
  gtk_box_pack_start (GTK_BOX (box1), tmpw, FALSE, FALSE, 0);
  gtk_widget_show(tmpw);

  updatepreviewprev(NULL, 0);

  gtk_notebook_append_page_menu (notebook, thispage, labelbox, menubox);
}
