/* icon.c	                                  */
/* This is a File input and output filter for     */
/* Gimp. It loads and saves images in windows(TM) */
/* icon bitmap format.                            */
/* Some parts are taken from the BMP plugin by    */
/* Alexander Schulz				  */
/*						  */
/* Lance Dillon					  */
/* riffraff@gte.net				  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgimp/gimp.h>
#include <gtk/gtk.h>
#include "icon.h"


char *prog_name="icon";
char *filename;
int interactive_icon;

/* Declare some local functions.
 */
static void   query      (void);
static void   run        (char    *name,
                          int      nparams,
                          GParam  *param,
                          int     *nreturn_vals,
                          GParam **return_vals);
/*static gint32 load_image(char *filename);
static gint save_image(char *filename, gint32 image_ID, 
		       gint32 drawable_ID); */


GPlugInInfo PLUG_IN_INFO =
{
  NULL,    /* init_proc */
  NULL,    /* quit_proc */
  query,   /* query_proc */
  run,     /* run_proc */
};

MAIN ()

static void
query (void) {
  static GParamDef load_args[] = {
    { PARAM_INT32, "run_mode", "Interactive, non-interactive" },
    { PARAM_STRING, "filename", "The name of the file to load" },
    { PARAM_STRING, "raw_filename", "The name entered" },
  };
  static GParamDef load_return_vals[] = {
    { PARAM_IMAGE, "image", "Output image" },
  };
  static int nload_args = sizeof (load_args) / sizeof (load_args[0]);
  static int nload_return_vals = sizeof (load_return_vals) / sizeof (load_return_vals[0]);

  static GParamDef save_args[] = {
    { PARAM_INT32, "run_mode", "Interactive, non-interactive" },
    { PARAM_IMAGE, "image", "Input image" },
    { PARAM_DRAWABLE, "drawable", "Drawable to save" },
    { PARAM_STRING, "filename", "The name of the file to save the image in" },
    { PARAM_STRING, "raw_filename", "The name entered" },
  };
  static int nsave_args = sizeof (save_args) / sizeof (save_args[0]);
  
  gimp_install_procedure ("file_icon_load",
                          "Loads files of Windows ICO file format",
                          "Loads files of Windows ICO file format",
                          "Lance Dillon",
                          "Lance DIllon",
                          "1997",
                          "<Load>/ICO",
                          NULL,
                          PROC_PLUG_IN,
                          nload_args, nload_return_vals,
                          load_args, load_return_vals);

  gimp_install_procedure ("file_icon_save",
                          "Saves files in Windows ICO file format",
                          "Saves files in Windows ICO file format",
                          "Lance Dillon",
                          "Lance Dillon",
                          "1997",
                          "<Save>/ICO",
                          "INDEXED*",
                          PROC_PLUG_IN,
                          nsave_args, 0,
                          save_args, NULL);

/*  gimp_register_magic_load_handler ("file_icon_load", "ico", "", "0,string,BM"); */
/*  gimp_register_magic_load_handler ("file_icon_load", "ico", "", ""); */
  gimp_register_load_handler ("file_icon_load", "ico", "");
  gimp_register_save_handler ("file_icon_save", "ico", "");
}

static void
run (char    *name,
     int      nparams,
     GParam  *param,
     int     *nreturn_vals,
     GParam **return_vals) {
  static GParam values[2];
  GStatusType status = STATUS_SUCCESS;
  GRunModeType run_mode;
  gint32 image_ID;
  
  run_mode = param[0].data.d_int32;

  *nreturn_vals = 1;
  *return_vals = values;
  values[0].type = PARAM_STATUS;
  values[0].data.d_status = STATUS_CALLING_ERROR;

  if (!strcmp(name, "file_icon_load")) {
    image_ID = ReadICON(param[1].data.d_string);

    if (image_ID != -1) {
      *nreturn_vals = 2;
      values[0].data.d_status = STATUS_SUCCESS;
      values[1].type = PARAM_IMAGE;
      values[1].data.d_image = image_ID;
    } else {
      values[0].data.d_status = STATUS_EXECUTION_ERROR;
    }
  } else if (!strcmp(name, "file_icon_save")) {
    switch (run_mode) {
      case RUN_INTERACTIVE:
	interactive_icon = TRUE;
	break;

      case RUN_NONINTERACTIVE:
	/*  Make sure all the arguments are there!  */
	interactive_icon = FALSE;
	if (nparams != 5)
	  status = STATUS_CALLING_ERROR;
	break;

      case RUN_WITH_LAST_VALS:
	interactive_icon = FALSE;
	break;

      default:
	break;
    }

    *nreturn_vals = 1;
    if (WriteICON(param[3].data.d_string, param[1].data.d_int32, param[2].data.d_int32)) {
      values[0].data.d_status = STATUS_SUCCESS;
    } else
      values[0].data.d_status = STATUS_EXECUTION_ERROR;
  }
}

