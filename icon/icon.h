#include <libgimp/gimp.h>

#define MAXCOLORS       256

#define BitSet(byte, bit)  (((byte) & (bit)) == (bit))

/*extern gint32 ReadICON(char *);
extern gint WriteICON(char *,gint32,gint32);
extern gint ReadColorMap(FILE *, unsigned char[256][3], int, int, int *);
extern gint32 ReadImage(FILE *, int, int, unsigned char[256][3], int, int, int, int, int);
extern void WriteColorMap(FILE *, int *, int *, int *, int);
extern void WriteImage(FILE *,char *,int,int,int,int,int,int); */

gint32 ReadICON(char *);
gint WriteICON(char *,gint32,gint32);

#ifdef DEBUG
int main(int,char **);
#endif

extern int interactive_icon;

typedef struct {
  short idReserved;  /* always set to 0 */
  short idType;      /* always set to 1 */
  short idCount;     /* number of icon images */
  /* immediately followed by idCount TIconDirEntries */
} ICO_Header;	     

typedef struct {
  unsigned char bWidth;    	    /* Width */
  unsigned char bHeight;	    /* Height */
  unsigned char bColorCount;	    /* # of colors used, see below */
  unsigned char bReserved;	    /* not used, 0 */
  unsigned short wPlanes;        /* not used, 0 */
  unsigned short wBitCount;      /* not used, 0 */
  unsigned long dwBytesInRes;   /* total number of bytes in image */
  unsigned long dwImageOffset;  /* location of image from the beginning of file */
} ICO_TIconDirEntry;

/* 
   for a 32x32 icon, 16 colors, first three fields above would be:
   bWidth=32
   bHeight=32
   bColorCount=16
 */

typedef struct {
  long biSize;	    /* sizeof(TBitmapInfoHeader) */
  long biWidth;	    /* width of bitmap */
  long biHeight;	    /* height of bitmap, see notes */
  short biPlanes;	    /* planes, always 1 */
  short biBitCount;	    /* number of color bits */
  long biCompression;    /* compression used, 0 */
  long biSizeImage;	    /* size of the pixel data, see notes */
  long biXPelsPerMeter;  /* not used, 0 */
  long biYPelsPerMeter;  /* not used, 0 */
  long biClrUsed;	    /* # of colors used, set to 0 */
  long biClrImportant;   /* important colors, set to 0 */
} ICO_TBitmapInfoHeader;

/* 
   biHeight=2*TIconDirEntry.bHeight;
   biSizeImage=ANDmask + XORmask;

   XORmask=(TIconDirEntry.bWidth * TIconDirEntry.bHeight * biBitCount)/8;
   ANDmask=(TIconDirEntry.bWidth * TIconDirEntry.bHeight)/8;
 */

typedef struct {
  unsigned char rgbBlue;	/* blue component of color */
  unsigned char rgbGreen;	/* green component of color */
  unsigned char rgbRed;	/* red component of color */
  unsigned char rgbReserved; /* reserved, 0 */
} ICO_TRGBQuad;


typedef struct {
  ICO_TBitmapInfoHeader icHeader; /* image header info */
  ICO_TRGBQuad *icColors;	  /* image palette */
  unsigned char *icXOR;		  /* image xor mask */
  unsigned char *icAND;		  /* image and mask */
  int xormasklen;
  int andmasklen;
} ICO_IconImage;

/* 
   has TIconDirEntry.bColorCount palette entries

   sizeof palette is sizeof(TRGBQuad)*TIconDirEntry.bColorCount bytes
 */

/*
  stored bottom up, XOR mask first, then AND mask:

  32x32 16 color, 32 nybbles (4 bits for 16 colors)=last row
  next 32 nybbles=last-1 row
  next 23 nybbles=last-2 row

 */


