/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GTK_PANNER_H__
#define __GTK_PANNER_H__


#include <gtk/gtkwidget.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


enum
{
  GTK_TRAVERSE_LEFT   = 1 << 0,
  GTK_TRAVERSE_RIGHT  = 1 << 1,
  GTK_TRAVERSE_TOP    = 1 << 2,  
  GTK_TRAVERSE_BOTTOM = 1 << 3
};


#define GTK_PANNER(obj)          GTK_CHECK_CAST (obj, gtk_panner_get_type (), GtkPanner)
#define GTK_PANNER_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_panner_get_type (), GtkPannerClass)
#define GTK_IS_PANNER(obj)       GTK_CHECK_TYPE (obj, gtk_panner_get_type ())


typedef struct _GtkPanner       GtkPanner;
typedef struct _GtkPannerClass  GtkPannerClass;

struct _GtkPanner
{
  GtkWidget widget;

  GdkRectangle canvas_area;
  GdkRectangle slider_area;

  gint canvas_width;
  gint canvas_height;
  gint slider_x;
  gint slider_y;
  gint slider_width;
  gint slider_height;

  guint do_slide : 1;
  guint do_resize : 1;

  guint allow_traverse_left : 1;     /* XXX: Is this good name? */
  guint allow_traverse_right : 1;    /* XXX: Is this good name? */
  guint allow_traverse_top : 1;      /* XXX: Is this good name? */
  guint allow_traverse_bottom : 1;   /* XXX: Is this good name? */

  guint is_sliding : 1;
  guint is_resizing : 1;
  gint pressed_point_x;
  gint pressed_point_y;

  gint old_slider_x;
  gint old_slider_y;
  gint old_slider_width;
  gint old_slider_height;


  void (* paint_canvas) (GtkPanner *panner, GdkRectangle *area,
			 gpointer user_data);
  void (* paint_slider) (GtkPanner *panner, GdkRectangle *area,
			 gpointer user_data);
  gpointer paint_canvas_user_data;
  gpointer paint_slider_user_data;
};

struct _GtkPannerClass
{
  GtkWidgetClass parent_class;

  void (* canvas_resize_notify) (GtkPanner *panner);
  void (* slider_move_notify) (GtkPanner *panner);
  void (* slider_resize_notify) (GtkPanner *panner);
  void (* slider_motion_notify) (GtkPanner *panner);
};


guint      gtk_panner_get_type (void);
GtkWidget* gtk_panner_new      (void);

void       gtk_panner_set_canvas_size (GtkPanner *panner,
				       gint width, gint height);
void       gtk_panner_set_slider_size (GtkPanner *panner,
				       gint width, gint height);
void       gtk_panner_set_slider_position (GtkPanner *panner,
					   gint x, gint y);

void       gtk_panner_set_traverse_perm (GtkPanner *panner,
					 gint permission);
void       gtk_panner_unset_traverse_perm (GtkPanner *panner,
					   gint permission);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_PANNER_H__ */
