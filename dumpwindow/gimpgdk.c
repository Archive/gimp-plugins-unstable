/*
 *
 */


#include "gimpgdk.h"


static void gimp_image_set_cmap_with_gdk_colormap (gint32 image_id,
						   GdkColormap *gdk_colormap,
						   gint colormap_size);
static void gdrawable_from_gdk_image__indexed (gint32 gdrawable_id,
					       GdkImage *gdk_image);
static void gdrawable_from_gdk_image__rgb (gint32 gdrawable_id,
					   GdkImage *gdk_image);
static void pixel_to_rgb__packed (GdkVisual *gdk_visual,
				  guint32 pixel,
				  guchar *red_ret,
				  guchar *green_ret,
				  guchar *blue_ret);
static void pixel_to_rgb__indexed (GdkVisual *gdk_visual,
				   GdkColormap *gdk_colormap,
				   guint32 pixel,
				   guchar *red_ret,
				   guchar *green_ret,
				   guchar *blue_ret);


gint32
gimp_image_new_from_gdk_image (GdkImage *gdk_image,
			       GdkColormap *gdk_colormap)
{
  GImageType image_type;
  gint32 image_id;
  GDrawableType drawable_type;
  gint32 layer_id;

  gint width;
  gint height;
  gint colormap_size;

#if 0
  g_print("%d x %d  depth: %d  bpp: %d  bpl: %d\n",
	  gdk_image->width, gdk_image->height,
	  gdk_image->depth, gdk_image->bpp, gdk_image->bpl);
#endif

  width = gdk_image->width;
  height = gdk_image->height;
  colormap_size = gdk_image->visual->colormap_size;

  switch (gdk_image->visual->type)
    {
    case GDK_VISUAL_STATIC_GRAY:
    case GDK_VISUAL_GRAYSCALE:
    case GDK_VISUAL_STATIC_COLOR:
    case GDK_VISUAL_PSEUDO_COLOR:
	image_type = INDEXED;
	drawable_type = INDEXED_IMAGE;
	break;

    case GDK_VISUAL_TRUE_COLOR:
    case GDK_VISUAL_DIRECT_COLOR:
	image_type = RGB;
	drawable_type = RGB_IMAGE;
	break;

    default:
	g_assert_not_reached();
	break;
    }

  image_id = gimp_image_new (width, height, image_type);
  layer_id = gimp_layer_new (image_id,
			     "screen shot",
			     width, height,
			     drawable_type,
			     100.0,
			     NORMAL_MODE);
  gimp_image_add_layer (image_id, layer_id, 0);

  switch (image_type)
    {
    case INDEXED:
	gdrawable_from_gdk_image__indexed (layer_id, gdk_image);
	gimp_image_set_cmap_with_gdk_colormap (image_id,
					       gdk_colormap, colormap_size);
	break;
    case RGB:
	gdrawable_from_gdk_image__rgb (layer_id, gdk_image);
	break;
    default:
	g_assert_not_reached();
	break;
    }

  return image_id;
}

static void
gimp_image_set_cmap_with_gdk_colormap (gint32 image_id,
				       GdkColormap *gdk_colormap,
				       gint colormap_size)
{
  guchar *buf;
  gint i;

  buf = g_new (guchar, 3 * colormap_size);
  for (i = 0; i < colormap_size; ++i)
    {
      buf[i*3 + 0] = gdk_colormap->colors[i].red;
      buf[i*3 + 1] = gdk_colormap->colors[i].green;
      buf[i*3 + 2] = gdk_colormap->colors[i].blue;
    }
  gimp_image_set_cmap (image_id, buf, colormap_size);
  g_free (buf);
}

static void
gdrawable_from_gdk_image__indexed (gint32 gdrawable_id,
				   GdkImage *gdk_image)
{
  gint width;
  gint height;

  GDrawable *gdrawable;
  GPixelRgn pixel_rgn;
  guint32 pixel;
  guchar *buf;
  gint i, j;

  width = gdk_image->width;
  height = gdk_image->height;

  gdrawable = gimp_drawable_get (gdrawable_id);

  buf = g_new (guchar, width);
  gimp_pixel_rgn_init(&pixel_rgn, gdrawable, 0, 0, width, height, TRUE, FALSE);
  for (j = 0; j < height; ++j)
    {
      for (i = 0; i < width; ++i)
	{
	  pixel = gdk_image_get_pixel (gdk_image, i, j);

	  buf[i] = pixel;
	}
      gimp_pixel_rgn_set_row (&pixel_rgn, buf, 0, j, width);
    }
  g_free (buf);

  gimp_drawable_flush (gdrawable);
  gimp_drawable_update (gdrawable_id, 0, 0, width, height);
}

static void
gdrawable_from_gdk_image__rgb (gint32 gdrawable_id,
			       GdkImage *gdk_image)
{
  gint width;
  gint height;

  GDrawable *gdrawable;
  GPixelRgn pixel_rgn;
  guint32 pixel;
  guchar red, green, blue;
  guchar *buf;
  gint i, j;

  width = gdk_image->width;
  height = gdk_image->height;

  gdrawable = gimp_drawable_get (gdrawable_id);

  gimp_progress_init ("screen shot");

#if 0
  buf = g_new (guchar, 3 * width);
  gimp_pixel_rgn_init(&pixel_rgn, gdrawable, 0, 0, width, height, TRUE, FALSE);

  for (j = 0; j < height; ++j)
    {
      for (i = 0; i < width; ++i)
	{
	  pixel = gdk_image_get_pixel (gdk_image, i, j);
	  pixel_to_rgb__packed (gdk_image->visual, pixel,
				&red, &green, &blue);

	  buf[i*3 + 0] = red;
	  buf[i*3 + 1] = green;
	  buf[i*3 + 2] = blue;
	}
      gimp_pixel_rgn_set_row (&pixel_rgn, buf, 0, j, width);
      gimp_progress_update ((gdouble)j / (height-1));
    }
  g_free (buf);
#else
  buf = g_new (guchar, 3 * width * height);
  gimp_pixel_rgn_init(&pixel_rgn, gdrawable, 0, 0, width, height, TRUE, FALSE);

  for (j = 0; j < height; ++j)
    {
      for (i = 0; i < width; ++i)
	{
	  pixel = gdk_image_get_pixel (gdk_image, i, j);
	  pixel_to_rgb__packed (gdk_image->visual, pixel,
				&red, &green, &blue);

	  buf[j*width*3 + i*3 + 0] = red;
	  buf[j*width*3 + i*3 + 1] = green;
	  buf[j*width*3 + i*3 + 2] = blue;
	}
      if ((j % (width/10))== (width/10 - 1))
	  gimp_progress_update ((gdouble)j / (height-1));
    }
  gimp_progress_update (1.0);
  gimp_pixel_rgn_set_rect (&pixel_rgn, buf, 0, 0, width, height);
  g_free (buf);
#endif


  gimp_drawable_flush (gdrawable);
  gimp_drawable_update (gdrawable_id, 0, 0, width, height);
}

/***/

#define MAX_RED 255
#define MAX_GREEN 255
#define MAX_BLUE 255

void
pixel_to_rgb (GdkVisual *gdk_visual,
	      GdkColormap *gdk_colormap,
	      guint32 pixel,
	      guint8 *red_ret,
	      guint8 *green_ret,
	      guint8 *blue_ret)
{
  switch (gdk_visual->type)
    {
    case GDK_VISUAL_TRUE_COLOR:
    case GDK_VISUAL_DIRECT_COLOR:
      pixel_to_rgb__packed (gdk_visual,
			    pixel, red_ret, green_ret, blue_ret);
      break;

    case GDK_VISUAL_STATIC_GRAY:
    case GDK_VISUAL_GRAYSCALE:
    case GDK_VISUAL_STATIC_COLOR:
    case GDK_VISUAL_PSEUDO_COLOR:
      pixel_to_rgb__indexed (gdk_visual, gdk_colormap,
			     pixel, red_ret, green_ret, blue_ret);
      break;
      
    default:
      g_assert_not_reached();
      break;
    }
}

static void
pixel_to_rgb__packed (GdkVisual *gdk_visual,
		      guint32 pixel,
		      guchar *red_ret,
		      guchar *green_ret,
		      guchar *blue_ret)
{
  guint32 orig_red;
  guint32 orig_green;
  guint32 orig_blue;

  guchar red;
  guchar green;
  guchar blue;

  guint32 red_mask;
  guint32 green_mask;
  guint32 blue_mask;
  gint red_shift;
  gint green_shift;
  gint blue_shift;


  red_mask = gdk_visual->red_mask;
  green_mask = gdk_visual->green_mask;
  blue_mask = gdk_visual->blue_mask;
  red_shift = gdk_visual->red_shift;
  green_shift = gdk_visual->green_shift;
  blue_shift = gdk_visual->blue_shift;

  orig_red = (pixel & red_mask) >> red_shift;
  orig_green = (pixel & green_mask) >> green_shift;
  orig_blue = (pixel & blue_mask) >> blue_shift;

  if (0)
    {
      red = orig_red;
      green = orig_green;
      blue = orig_blue;
    }
  else
    {
      red = (MAX_RED * orig_red) / (red_mask >> red_shift);
      green = (MAX_GREEN * orig_green) / (green_mask >> green_shift);
      blue = (MAX_BLUE * orig_blue) / (blue_mask >> blue_shift);
    }

  *red_ret = red;
  *green_ret = green;
  *blue_ret = blue;
}

static void
pixel_to_rgb__indexed (GdkVisual *gdk_visual,
		       GdkColormap *gdk_colormap,
		       guint32 pixel,
		       guchar *red_ret,
		       guchar *green_ret,
		       guchar *blue_ret)
{
  guchar red;
  guchar green;
  guchar blue;

  red = gdk_colormap->colors[pixel].red;
  green = gdk_colormap->colors[pixel].green;
  blue = gdk_colormap->colors[pixel].blue;

  *red_ret = red;
  *green_ret = green;
  *blue_ret = blue;
}
