#include <stdio.h>
#include <signal.h>

#include <gtk/gtk.h>
#include "gtkrootwindow.h"
#include "gtkpanner.h"
#include "gimpgdk.h"

#include <gdk/gdkx.h>

struct DumpWindowValues {
  gchar *display_name;
  gint32 window_XID;
  gint x;
  gint y;
  gint width;
  gint height;
};

static void dumpwindow_main (struct DumpWindowValues *dumpwindow_values,
			     gint run_mode);
static gint dumpwindow_dialog (gint *x_ret, gint *y_ret,
			       gint *width_ret, gint *height_ret);
static void fake_init_gtk (gchar *display_name);
static GdkImage* gdk_image_get_from_root_window (gint x, gint y,
						 gint width, gint height);
static void gtk_entry_set_text_with_gint (GtkEntry *entry, gint num);

/***/

#include "libgimp/gimp.h"


static void	query	(void);
static void	run	(gchar   *name,
			 gint    nparams,
			 GParam  *param,
			 gint    *nreturn_vals,
			 GParam  **return_vals);

static void     dumpwindow_values_set_with_gparams
                        (struct DumpWindowValues *dumpwindow_values,
			 gint nparams,
			 GParam *param);

#define PLUGIN_PROCEDURE_NAME "extension_dump_window"

GPlugInInfo PLUG_IN_INFO =
{
  NULL,    /* init_proc */
  NULL,    /* quit_proc */
  query,   /* query_proc */
  run,     /* run_proc */
};

/***/

MAIN ();

/***/

static void
query()
{
  static GParamDef args[]=
  {
      { PARAM_INT32, "run_mode", "Interactive, non-interactive" },
      { PARAM_STRING, "display", "Display Name" },
      { PARAM_INT32, "window_id", "Window XID (unused, yet)" },
      { PARAM_INT32, "x", "X" },
      { PARAM_INT32, "y", "Y" },
      { PARAM_INT32, "width", "Width" },
      { PARAM_INT32, "height", "Height" },
  };
  static gint nargs = sizeof (args) / sizeof (args[0]);

  static GParamDef *return_vals = NULL;
  static gint nreturn_vals = 0;

  gimp_install_procedure (PLUGIN_PROCEDURE_NAME,
			  "Dump Window",
			  "Dump Window",
			  "AOSASA Shigeru",
			  "AOSASA Shigeru",
			  "1997-1998",
			  "<Toolbox>/Xtns/Dump Window",
			  "RGB*, GRAY*, INDEXED*",
			  PROC_EXTENSION,
			  nargs, nreturn_vals,
			  args, return_vals);
}

static void
run (gchar   *name,
     gint    nparams,
     GParam  *param,
     gint    *nreturn_vals,
     GParam  **return_vals)
{
    struct DumpWindowValues  dumpwindow_values;

    GStatusType status;
    gint run_mode;

    static GParam values[1];


    dumpwindow_values.display_name = NULL;
    dumpwindow_values.x = 0;
    dumpwindow_values.y = 0;
    dumpwindow_values.width = 200;
    dumpwindow_values.height = 150;


    run_mode = param[0].data.d_int32;

    switch (run_mode) {
    case RUN_INTERACTIVE:
	gimp_get_data(PLUGIN_PROCEDURE_NAME, &dumpwindow_values);
	dumpwindow_main(&dumpwindow_values, run_mode);
	gimp_set_data(PLUGIN_PROCEDURE_NAME, &dumpwindow_values,
		      sizeof(struct DumpWindowValues));
	break;
    case RUN_NONINTERACTIVE:
	dumpwindow_values_set_with_gparams (&dumpwindow_values,
					    nparams, param);
	dumpwindow_main (&dumpwindow_values, run_mode);
	break;
    case RUN_WITH_LAST_VALS:
	gimp_get_data(PLUGIN_PROCEDURE_NAME, &dumpwindow_values);
	dumpwindow_main (&dumpwindow_values, run_mode);
	break;
    default:
	break;
    }


    if (1) {
	status = STATUS_SUCCESS;
    } else {
	status = STATUS_SUCCESS;
    }

    
    values[0].type = PARAM_STATUS;
    values[0].data.d_status = status;
    
    *nreturn_vals = 1;
    *return_vals = values;
}

static void
dumpwindow_values_set_with_gparams (struct DumpWindowValues *dumpwindow_values,
				    gint nparams,
				    GParam *param)
{
  gchar *display_name;
  gint32 window_XID;
  gint x;
  gint y;
  gint width;
  gint height;

  display_name = param[1].data.d_string;
  window_XID = param[2].data.d_int32;
  x = param[3].data.d_int32;
  y = param[4].data.d_int32;
  width = param[5].data.d_int32;
  height = param[6].data.d_int32;

  dumpwindow_values->display_name = display_name;
  dumpwindow_values->window_XID = window_XID;
  dumpwindow_values->x = x;
  dumpwindow_values->y = y;
  dumpwindow_values->width = width;
  dumpwindow_values->height = height;
}


/***/
/***/

static void
dumpwindow_main (struct DumpWindowValues *dumpwindow_values,
		 gint run_mode)
{
  gint32 image_id;

  GdkImage *gdk_image;
  GdkColormap *gdk_colormap;
  gchar *display_name;
  gint32 window_XID;
  gint r;
  gint x, y;
  gint width, height;


  display_name = dumpwindow_values->display_name;
  window_XID = dumpwindow_values->window_XID;
  x = dumpwindow_values->x;
  y = dumpwindow_values->y;
  width = dumpwindow_values->width;
  height = dumpwindow_values->height;


  switch (run_mode)
    {
    case RUN_INTERACTIVE:
	fake_init_gtk (NULL);
	r = dumpwindow_dialog (&x, &y, &width, &height);
	break;

    case RUN_NONINTERACTIVE:
	fake_init_gtk (display_name);
	r = TRUE;
	break;

    case RUN_WITH_LAST_VALS:
	/* XXX: */
	fake_init_gtk (NULL);
	r = TRUE;
	break;

    default:
	break;
    }


        printf("bumpmap: waiting... (pid %d)\n", getpid());
        kill(getpid(), SIGSTOP);

  if (r == TRUE)
    {
      gdk_image = gdk_image_get_from_root_window (x, y, width, height);
      gdk_colormap = gdk_colormap_of_root_window ();

      image_id = gimp_image_new_from_gdk_image (gdk_image, gdk_colormap);

      if (run_mode != RUN_NONINTERACTIVE)
	{
	  gimp_display_new (image_id);
	  gimp_displays_flush ();
	}

      gdk_image_destroy (gdk_image);
    }
}

/***/

struct XXX_Data {
  GtkPanner *panner;
  guint8 *pixel_data;
  GtkPreview *preview_canvas;
  GtkPreview *preview_slider;
  GtkRootWindow *root_window;

  GtkEntry  *entry_display;
  GtkEntry  *entry_window_id;
  GtkEntry  *entry_x;
  GtkEntry  *entry_y;
  GtkEntry  *entry_width;
  GtkEntry  *entry_height;

  gint update_entry;

  gint x;
  gint y;
  gint width;
  gint height;
};

static void
change_position (GtkPanner *panner, gpointer data)
{
  struct XXX_Data *xxx_data;
  gint x, y;

  xxx_data = (struct XXX_Data *)data;

  if (xxx_data->update_entry == TRUE)
    {
      x = panner->slider_x * 4;
      y = panner->slider_y * 4;

      gtk_entry_set_text_with_gint (GTK_ENTRY (xxx_data->entry_x), x);
      gtk_entry_set_text_with_gint (GTK_ENTRY (xxx_data->entry_y), y);
    }
}

static void
change_size (GtkPanner *panner, gpointer data)
{
  struct XXX_Data *xxx_data;
  gint width, height;

  xxx_data = (struct XXX_Data *)data;

  if (xxx_data->update_entry == TRUE)
    {
      width = panner->slider_width * 4;
      height = panner->slider_height * 4;

      gtk_entry_set_text_with_gint (GTK_ENTRY (xxx_data->entry_width),
				    width);
      gtk_entry_set_text_with_gint (GTK_ENTRY (xxx_data->entry_height),
				    height);
    }
}

static void
change_x (GtkEntry *entry, gpointer data)
{
  gchar *text;
  struct XXX_Data *xxx_data;
  gint x;

  xxx_data = (struct XXX_Data *)data;

  text = gtk_entry_get_text (entry);
  x = atol (text);

  xxx_data->x = x;

  xxx_data->update_entry = FALSE;
  gtk_panner_set_slider_position (xxx_data->panner,
				  xxx_data->x/4, xxx_data->y/4);
  xxx_data->update_entry = TRUE;
}

static void
change_y (GtkEntry *entry, gpointer data)
{
  gchar *text;
  struct XXX_Data *xxx_data;
  gint y;

  xxx_data = (struct XXX_Data *)data;

  text = gtk_entry_get_text (entry);
  y = atol (text);

  xxx_data->y = y;

  xxx_data->update_entry = FALSE;
  gtk_panner_set_slider_position (xxx_data->panner,
				  xxx_data->x/4, xxx_data->y/4);
  xxx_data->update_entry = TRUE;
}

static void
change_width (GtkEntry *entry, gpointer data)
{
  gchar *text;
  struct XXX_Data *xxx_data;
  gint width;

  xxx_data = (struct XXX_Data *)data;

  text = gtk_entry_get_text (entry);
  width = atol (text);

  xxx_data->width = width;

  xxx_data->update_entry = FALSE;
  gtk_panner_set_slider_size (xxx_data->panner,
			      xxx_data->width/4, xxx_data->height/4);
  xxx_data->update_entry = TRUE;
}

static void
change_height (GtkEntry *entry, gpointer data)
{
  gchar *text;
  struct XXX_Data *xxx_data;
  gint height;

  xxx_data = (struct XXX_Data *)data;

  text = gtk_entry_get_text (entry);
  height = atol (text);

  xxx_data->height = height;

  xxx_data->update_entry = FALSE;
  gtk_panner_set_slider_size (xxx_data->panner,
			      xxx_data->width/4, xxx_data->height/4);
  xxx_data->update_entry = TRUE;
}

static void
init_preview (struct XXX_Data *xxx_data)
{
  gint root_width, root_height;
  gint i, j;
  guint8 *buf;
  guint8 red, green, blue;
  guint32 pixel;
  GdkImage *root_image;
  GdkColormap *root_colormap;

  root_width = GTK_WIDGET (xxx_data->root_window)->allocation.width;
  root_height = GTK_WIDGET (xxx_data->root_window)->allocation.height;

  if (xxx_data->pixel_data == NULL)
    {
      xxx_data->pixel_data
	  = g_new (guint8, (root_width/4 * root_height/4 * 3));
    }
     
  if (xxx_data->preview_canvas == NULL)
    {
      xxx_data->preview_canvas
	  = GTK_PREVIEW (gtk_preview_new (GTK_PREVIEW_COLOR));
      gtk_preview_size (xxx_data->preview_canvas, root_width/4, root_height/4);
    }

  if (xxx_data->preview_slider == NULL)
    {
      xxx_data->preview_slider
	  = GTK_PREVIEW (gtk_preview_new (GTK_PREVIEW_COLOR));
      gtk_preview_size (xxx_data->preview_slider, root_width/4, root_height/4);
    }

  root_image = gdk_image_get_from_root_window (0,0, root_width, root_height);
  root_colormap = gdk_colormap_of_root_window ();
  
  for (j = 0; j < root_height/4; ++j)
    {
      for (i = 0; i < root_width/4; ++i)
	{
	  pixel = gdk_image_get_pixel (root_image, i*4, j*4);
	  pixel_to_rgb (root_image->visual, root_colormap,
			pixel, &red, &green, &blue);

	  xxx_data->pixel_data[j*(root_width/4)*3 + i*3 + 0] = red;
	  xxx_data->pixel_data[j*(root_width/4)*3 + i*3 + 1] = green;
	  xxx_data->pixel_data[j*(root_width/4)*3 + i*3 + 2] = blue;
	}
    }

  gdk_image_destroy (root_image);

  buf = g_new (guint8, root_width/4 * 3);
  for (j = 0; j < root_height/4; ++j)
    {
      for (i = 0; i < root_width/4; ++i)
	{
	  buf[i*3 + 0] = xxx_data->pixel_data[j*(root_width/4)*3+i*3+0] >> 2;
	  buf[i*3 + 1] = xxx_data->pixel_data[j*(root_width/4)*3+i*3+1] >> 2;
	  buf[i*3 + 2] = xxx_data->pixel_data[j*(root_width/4)*3+i*3+2] >> 2;
	}
      gtk_preview_draw_row (xxx_data->preview_canvas,
			    buf, 0, j, root_width/4);
    }
  for (j = 0; j < root_height/4; ++j)
    {
      for (i = 0; i < root_width/4; ++i)
	{
	  buf[i*3 + 0] = xxx_data->pixel_data[j*(root_width/4)*3+i*3+0];
	  buf[i*3 + 1] = xxx_data->pixel_data[j*(root_width/4)*3+i*3+1];
	  buf[i*3 + 2] = xxx_data->pixel_data[j*(root_width/4)*3+i*3+2];
	}
      gtk_preview_draw_row (xxx_data->preview_slider,
			    buf, 0, j, root_width/4);
    }
  g_free (buf);

  gtk_widget_queue_draw (GTK_WIDGET (xxx_data->panner));
}

#if 0
static void
root_expose (GtkWidget *widget, GdkEvent *event, gpointer data)
{
  struct XXX_Data *xxx_data;
    
  xxx_data = (struct XXX_Data *)data;
}
#endif

static void
dumpwindow_dialog_cancel (GtkButton *button,
			  gpointer data)
{
  *((gint *)data) = TRUE;

  gtk_main_quit();
}

static void
paint_canvas (GtkPanner *panner,
	      GdkRectangle *area,
	      gpointer user_data)
{
  struct XXX_Data *xxx_data;

  xxx_data = (struct XXX_Data *)user_data;
    
  if (0)
    {
      gdk_window_clear_area (GTK_WIDGET (panner)->window,
			     area->x, area->y, area->width, area->height);
      gdk_draw_rectangle (GTK_WIDGET (panner)->window,
			  GTK_WIDGET (panner)->style->black_gc,
			  FALSE,
			  panner->canvas_area.x,
			  panner->canvas_area.y,
			  panner->canvas_area.width - 1,
			  panner->canvas_area.height - 1);
    }
  else
    {
      if (xxx_data->preview_canvas)
	{
	  gtk_preview_put (xxx_data->preview_canvas,
			   GTK_WIDGET (panner)->window,
			   GTK_WIDGET (panner)->style->black_gc,
			   0, 0,
			   panner->canvas_area.x,
			   panner->canvas_area.y,
			   panner->canvas_area.width,
			   panner->canvas_area.height);
	}
    }
}

static void
paint_slider (GtkPanner *panner,
	      GdkRectangle *area,
	      gpointer user_data)
{
  struct XXX_Data *xxx_data;

  xxx_data = (struct XXX_Data *)user_data;

  if (xxx_data->preview_slider)
    {
      gtk_preview_put (xxx_data->preview_slider,
		       GTK_WIDGET (panner)->window,
		       GTK_WIDGET (panner)->style->black_gc,
		       panner->slider_area.x - panner->canvas_area.x,
		       panner->slider_area.y - panner->canvas_area.y,
		       panner->slider_area.x,
		       panner->slider_area.y,
		       panner->slider_area.width,
		       panner->slider_area.height);
    }
}

static gint
dumpwindow_dialog (gint *x_ret, gint *y_ret,
		   gint *width_ret, gint *height_ret)
{
  struct XXX_Data xxx_data;
  gint is_canceled;
    
  GtkWidget *dialog;

  GtkWidget *button_ok;
  GtkWidget *button_cancel;

  GtkWidget *vbox;
  GtkWidget *label_display;
  GtkWidget *label_window_id;
  GtkWidget *label_x;
  GtkWidget *label_y;
  GtkWidget *label_width;
  GtkWidget *label_height;
  GtkWidget *entry_display;
  GtkWidget *entry_window_id;
  GtkWidget *entry_x;
  GtkWidget *entry_y;
  GtkWidget *entry_width;
  GtkWidget *entry_height;

  GtkWidget *hbox;
  GtkWidget *frame;
  GtkWidget *panner;
  GtkWidget *table;
  GtkWidget *root_window;

  gchar buf[32];

  xxx_data.panner = NULL;
  xxx_data.pixel_data = NULL;
  xxx_data.preview_canvas = NULL;
  xxx_data.preview_slider = NULL;
  xxx_data.root_window = NULL;
  xxx_data.entry_display = NULL;
  xxx_data.entry_window_id = NULL;
  xxx_data.entry_x = NULL;
  xxx_data.entry_y = NULL;
  xxx_data.entry_width = NULL;
  xxx_data.entry_height = NULL;
  xxx_data.update_entry = TRUE;

  xxx_data.x = *x_ret;
  xxx_data.y = *y_ret;
  xxx_data.width = *width_ret;
  xxx_data.height = *height_ret;


  dialog = gtk_dialog_new ();

  button_ok = gtk_button_new_with_label ("Ok");
  gtk_signal_connect (GTK_OBJECT (button_ok),
		      "clicked", GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
  gtk_box_pack_start_defaults (GTK_BOX (GTK_DIALOG (dialog)->action_area),
			       button_ok);

  button_cancel = gtk_button_new_with_label ("Cancel");
  is_canceled = FALSE;
  gtk_signal_connect (GTK_OBJECT (button_cancel),
		      "clicked",
		      GTK_SIGNAL_FUNC (dumpwindow_dialog_cancel),
		      &is_canceled);
  gtk_box_pack_start_defaults (GTK_BOX (GTK_DIALOG (dialog)->action_area),
			       button_cancel);


  vbox = gtk_vbox_new (0, 10);
  gtk_box_pack_start_defaults (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox);


  root_window = gtk_root_window_widget ();
  
  hbox = gtk_hbox_new (FALSE, 0);
  frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (frame), 10);
  panner = gtk_panner_new ();
  gtk_panner_set_canvas_size (GTK_PANNER (panner),
			      root_window->allocation.width / 4,
			      root_window->allocation.height / 4);
  gtk_panner_set_slider_position (GTK_PANNER (panner),
				  *x_ret / 4, *y_ret / 4);
  gtk_panner_set_slider_size (GTK_PANNER (panner),
			      *width_ret / 4, *height_ret / 4);

  GTK_PANNER (panner)->paint_canvas = paint_canvas;
  GTK_PANNER (panner)->paint_canvas_user_data = &xxx_data;
  GTK_PANNER (panner)->paint_slider = paint_slider;
  GTK_PANNER (panner)->paint_slider_user_data = &xxx_data;
  gtk_container_add (GTK_CONTAINER (frame), panner);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 5);

  table = gtk_table_new (6, 2, FALSE);

  label_display = gtk_label_new ("Display Name:");
  label_window_id = gtk_label_new ("Window XID:");
  label_x = gtk_label_new ("X:");
  label_y = gtk_label_new ("Y:");
  label_width = gtk_label_new ("Width:");
  label_height = gtk_label_new ("Height:");

  entry_display = gtk_entry_new ();
  entry_window_id = gtk_entry_new ();
  entry_x = gtk_entry_new ();
  entry_y = gtk_entry_new ();
  entry_width = gtk_entry_new ();
  entry_height = gtk_entry_new ();

  gtk_table_attach (GTK_TABLE (table), label_display, 0, 1, 0, 1,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_table_attach (GTK_TABLE (table), label_window_id, 0, 1, 1, 2,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_table_attach (GTK_TABLE (table), label_x, 0, 1, 2, 3,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_table_attach (GTK_TABLE (table), label_y, 0, 1, 3, 4,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_table_attach (GTK_TABLE (table), label_width, 0, 1, 4, 5,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_table_attach (GTK_TABLE (table), label_height, 0, 1, 5, 6,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

  gtk_table_attach (GTK_TABLE (table), entry_display, 1, 2, 0, 1,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_table_attach (GTK_TABLE (table), entry_window_id, 1, 2, 1, 2,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_table_attach (GTK_TABLE (table), entry_x, 1, 2, 2, 3,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_table_attach (GTK_TABLE (table), entry_y, 1, 2, 3, 4,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_table_attach (GTK_TABLE (table), entry_width, 1, 2, 4, 5,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_table_attach (GTK_TABLE (table), entry_height, 1, 2, 5, 6,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

  gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);
  gtk_widget_show_all (table);

  gtk_entry_set_text (GTK_ENTRY (entry_display), gdk_get_display());
  gtk_entry_set_editable   (GTK_ENTRY (entry_display), FALSE);
  sprintf(buf, "0x%x", (unsigned int)GDK_ROOT_WINDOW ());	/* XXX: GDKX */
  gtk_entry_set_text (GTK_ENTRY (entry_window_id), buf);
  gtk_entry_set_editable   (GTK_ENTRY (entry_window_id), FALSE);
  gtk_entry_set_text_with_gint (GTK_ENTRY (entry_x), *x_ret);
  gtk_entry_set_text_with_gint (GTK_ENTRY (entry_y), *y_ret);
  gtk_entry_set_text_with_gint (GTK_ENTRY (entry_width), *width_ret);
  gtk_entry_set_text_with_gint (GTK_ENTRY (entry_height), *height_ret);


  xxx_data.panner = GTK_PANNER (panner);
  xxx_data.root_window = GTK_ROOT_WINDOW (root_window);
  xxx_data.entry_display = GTK_ENTRY (entry_display);
  xxx_data.entry_window_id = GTK_ENTRY (entry_window_id);
  xxx_data.entry_x = GTK_ENTRY (entry_x);
  xxx_data.entry_y = GTK_ENTRY (entry_y);
  xxx_data.entry_width = GTK_ENTRY (entry_width);
  xxx_data.entry_height = GTK_ENTRY (entry_height);

  gtk_signal_connect (GTK_OBJECT (panner),
		      "slider_move_notify",
		      GTK_SIGNAL_FUNC (change_position), &xxx_data);
  gtk_signal_connect (GTK_OBJECT (panner),
		      "slider_resize_notify",
		      GTK_SIGNAL_FUNC (change_size), &xxx_data);
  gtk_signal_connect (GTK_OBJECT (entry_x),
		      "changed", GTK_SIGNAL_FUNC (change_x), &xxx_data);
  gtk_signal_connect (GTK_OBJECT (entry_y),
		      "changed", GTK_SIGNAL_FUNC (change_y), &xxx_data);
  gtk_signal_connect (GTK_OBJECT (entry_width),
		      "changed", GTK_SIGNAL_FUNC (change_width), &xxx_data);
  gtk_signal_connect (GTK_OBJECT (entry_height),
		      "changed", GTK_SIGNAL_FUNC (change_height), &xxx_data);

#if 0
  gtk_root_window_set_events (GTK_ROOT_WINDOW (root_window),
			      GDK_EXPOSURE_MASK);
  gtk_signal_connect (GTK_OBJECT (root_window),
		      "expose_event",
		      GTK_SIGNAL_FUNC (root_expose), &xxx_data);
#endif
  init_preview (&xxx_data);



  gtk_widget_show_all (dialog);

  gtk_main ();

  gtk_widget_hide_all (dialog);
  gtk_widget_unmap (dialog);
  gtk_widget_unrealize (dialog);
  gdk_flush();

  *x_ret = xxx_data.x;
  *y_ret = xxx_data.y;
  *width_ret = xxx_data.width;
  *height_ret = xxx_data.height;

  if (is_canceled != FALSE)
    { return FALSE; }
  else
    { return TRUE; }
}

/***/
/***/

static void
fake_init_gtk (char *display_name)
{
  int fake_argc;
  char **fake_argv;
  int i;

  if (display_name == NULL)
    {
      fake_argc = 1;
      fake_argv = g_new(gchar *, fake_argc);
      fake_argv[0] = g_strdup(PLUGIN_PROCEDURE_NAME);
    }
  else
    {
      fake_argc = 3;
      fake_argv = g_new(gchar *, fake_argc);
      fake_argv[0] = g_strdup(PLUGIN_PROCEDURE_NAME);
      fake_argv[1] = g_strdup("--display");
      fake_argv[2] = g_strdup(display_name);
    }

  gtk_init(&fake_argc, &fake_argv);
  gtk_rc_parse(gimp_gtkrc());

  for (i = 0; i < fake_argc; ++i)
    { g_free(fake_argv[i]); }
  g_free(fake_argv);
}

static GdkImage*
gdk_image_get_from_root_window (gint x, gint y, gint width, gint height)
{
  GdkWindow *gdk_window;
  GdkImage *gdk_image;

  gdk_window = gdk_window_of_root_window ();
  gdk_image = gdk_image_get (gdk_window, x, y, width, height);

  return gdk_image;
}

static void
gtk_entry_set_text_with_gint (GtkEntry *entry, gint num)
{
  guchar buf[32];
    
  g_return_if_fail (entry != NULL);
  g_return_if_fail (GTK_IS_ENTRY (entry));

  
  g_snprintf(buf, 32, "%d", num);
  
  gtk_entry_set_text (entry, buf);
}
