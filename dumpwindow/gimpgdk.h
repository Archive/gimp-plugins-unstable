
#include <libgimp/gimp.h>
#include <gdk/gdk.h>

gint32 gimp_image_new_from_gdk_image (GdkImage *gdk_image,
				      GdkColormap *gdk_colormap);

void pixel_to_rgb (GdkVisual *gdk_visual, GdkColormap *gdk_colormap,
		   guint32 pixel,
		   guint8 *red_ret, guint8 *green_ret, guint8 *blue_ret);
