/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gtk/gtksignal.h>
#include "gtkpanner.h"


enum {
  CANVAS_RESIZE_NOTIFY,
  SLIDER_MOVE_NOTIFY,
  SLIDER_RESIZE_NOTIFY,
  SLIDER_MOTION_NOTIFY,
  LAST_SIGNAL
};


static void gtk_panner_class_init    (GtkPannerClass *klass);
static void gtk_panner_init          (GtkPanner      *panner);
static void gtk_panner_realize           (GtkWidget      *widget);
static void gtk_panner_size_request      (GtkWidget      *widget,
					  GtkRequisition *requisition);
static void gtk_panner_size_allocate     (GtkWidget      *widget,
					  GtkAllocation  *allocation);
static gint gtk_panner_expose            (GtkWidget      *widget,
					  GdkEventExpose *event);
static gint gtk_panner_button_press      (GtkWidget      *widget,
					  GdkEventButton *event);
static gint gtk_panner_button_release    (GtkWidget      *widget,
					  GdkEventButton *event);
static gint gtk_panner_motion_notify     (GtkWidget      *widget,
					  GdkEventMotion *event);
static void gtk_panner_canvas_resize_notify         (GtkPanner *panner);
static void gtk_panner_slider_move_notify           (GtkPanner *panner);
static void gtk_panner_slider_resize_notify         (GtkPanner *panner);
static void gtk_panner_slider_motion_notify         (GtkPanner *panner);
#ifdef DEBUG_GTK_PANNER
static void gtk_real_panner_canvas_resize_notify    (GtkPanner *panner);
static void gtk_real_panner_slider_move_notify      (GtkPanner *panner);
static void gtk_real_panner_slider_resize_notify    (GtkPanner *panner);
static void gtk_real_panner_slider_motion_notify    (GtkPanner *panner);
#endif

static void gtk_panner_paint_canvas            (GtkPanner      *panner,
						GdkRectangle   *area);
static void gtk_panner_paint_slider            (GtkPanner      *panner,
						GdkRectangle   *area);
static void gtk_panner_default_paint_canvas    (GtkPanner      *panner,
						GdkRectangle   *area,
						gpointer       user_data);
static void gtk_panner_default_paint_slider    (GtkPanner      *panner,
						GdkRectangle   *area,
						gpointer       user_data);


static gint panner_signals[LAST_SIGNAL] = { 0 };


guint
gtk_panner_get_type ()
{
  static guint panner_type = 0;

  if (!panner_type)
    {
      GtkTypeInfo panner_info =
      {
	"GtkPanner",
	sizeof (GtkPanner),
	sizeof (GtkPannerClass),
	(GtkClassInitFunc) gtk_panner_class_init,
	(GtkObjectInitFunc) gtk_panner_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL,
      };

      panner_type = gtk_type_unique (gtk_widget_get_type (), &panner_info);
    }

  return panner_type;
}

static void
gtk_panner_class_init (GtkPannerClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;

  panner_signals[CANVAS_RESIZE_NOTIFY] =
    gtk_signal_new ("canvas_resize_notify",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkPannerClass, canvas_resize_notify),
                    gtk_signal_default_marshaller,
                    GTK_TYPE_NONE, 0);
  panner_signals[SLIDER_MOVE_NOTIFY] =
    gtk_signal_new ("slider_move_notify",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkPannerClass, slider_move_notify),
                    gtk_signal_default_marshaller,
                    GTK_TYPE_NONE, 0);
  panner_signals[SLIDER_RESIZE_NOTIFY] =
    gtk_signal_new ("slider_resize_notify",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkPannerClass, slider_resize_notify),
                    gtk_signal_default_marshaller,
                    GTK_TYPE_NONE, 0);
  panner_signals[SLIDER_MOTION_NOTIFY] =
    gtk_signal_new ("slider_motion_notify",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkPannerClass, slider_motion_notify),
                    gtk_signal_default_marshaller,
                    GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, panner_signals, LAST_SIGNAL);

  widget_class->size_request = gtk_panner_size_request;
  widget_class->size_allocate = gtk_panner_size_allocate;
  widget_class->realize = gtk_panner_realize;
  widget_class->expose_event = gtk_panner_expose;
  widget_class->button_press_event = gtk_panner_button_press;
  widget_class->button_release_event = gtk_panner_button_release;
  widget_class->motion_notify_event = gtk_panner_motion_notify;

#ifdef DEBUG_GTK_PANNER
  class->canvas_resize_notify = gtk_real_panner_canvas_resize_notify;
  class->slider_move_notify = gtk_real_panner_slider_move_notify;
  class->slider_resize_notify = gtk_real_panner_slider_resize_notify;
  class->slider_motion_notify = gtk_real_panner_slider_motion_notify;
#else
  class->canvas_resize_notify = NULL;
  class->slider_move_notify = NULL;
  class->slider_resize_notify = NULL;
  class->slider_motion_notify = NULL;
#endif  
}

static void
gtk_panner_init (GtkPanner *panner)
{
  GTK_WIDGET_SET_FLAGS (panner, GTK_BASIC);

  panner->canvas_area.x = 10;
  panner->canvas_area.y = 10;
  panner->canvas_area.width = 100;
  panner->canvas_area.height = 100;
  panner->slider_area.x = panner->canvas_area.x + 25;
  panner->slider_area.y = panner->canvas_area.y + 25;
  panner->slider_area.width = 50;
  panner->slider_area.height = 50;

  panner->canvas_width = 100;
  panner->canvas_height = 100;
  panner->slider_x = 25;
  panner->slider_y = 25;
  panner->slider_width = 50;
  panner->slider_height = 50;

  panner->is_sliding = 0;
  panner->is_resizing = 0;
  panner->pressed_point_x = 0;
  panner->pressed_point_y = 0;

  panner->do_slide = 1;
  panner->do_resize = 1;

  panner->allow_traverse_left = 0;
  panner->allow_traverse_right = 0;
  panner->allow_traverse_top = 0;
  panner->allow_traverse_bottom = 0;

  panner->old_slider_x = 0;
  panner->old_slider_y = 0;
  panner->old_slider_width = 0;
  panner->old_slider_height = 0;


  panner->paint_canvas = gtk_panner_default_paint_canvas;
  panner->paint_slider = gtk_panner_default_paint_slider;
  panner->paint_canvas_user_data = NULL;
  panner->paint_slider_user_data = NULL;
}


GtkWidget*
gtk_panner_new ()
{
  return GTK_WIDGET (gtk_type_new (gtk_panner_get_type ()));
}

void
gtk_panner_set_canvas_size (GtkPanner *panner,
			    gint width,
			    gint height)
{
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));

  if ((panner->canvas_width != width) ||
      (panner->canvas_height != height))
    {
      panner->canvas_area.width = width;
      panner->canvas_area.height = height;

      panner->canvas_width = width;
      panner->canvas_height = height;

      gtk_panner_canvas_resize_notify (panner);
      gtk_widget_queue_resize (GTK_WIDGET (panner));
    }
}

void
gtk_panner_set_slider_size (GtkPanner *panner,
			    gint width,
			    gint height)
{
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));

  if ((panner->slider_width != width) ||
      (panner->slider_height != height))
    {
      panner->slider_area.width = width;
      panner->slider_area.height = height;

      panner->slider_width = width;
      panner->slider_height = height;

      gtk_panner_slider_resize_notify (panner);
      gtk_widget_queue_draw (GTK_WIDGET (panner));
    }
}

void
gtk_panner_set_slider_position (GtkPanner *panner,
				gint x,
				gint y)
{
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));

  if ((panner->slider_x != x) ||
      (panner->slider_y != y))
    {
      panner->slider_area.x = panner->canvas_area.x + x;
      panner->slider_area.y = panner->canvas_area.y + y;
  
      panner->slider_x = x;
      panner->slider_y = y;

      gtk_panner_slider_move_notify (panner);
      gtk_widget_queue_draw (GTK_WIDGET (panner));
    }
}

void
gtk_panner_set_traverse_perm (GtkPanner *panner,
			      gint permission)
{
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));

  if (permission & GTK_TRAVERSE_LEFT)
    {
      panner->allow_traverse_left = 1;
    }
  if (permission & GTK_TRAVERSE_RIGHT)
    {
      panner->allow_traverse_right = 1;
    }
  if (permission & GTK_TRAVERSE_TOP)
    {
      panner->allow_traverse_top = 1;
    }
  if (permission & GTK_TRAVERSE_BOTTOM)
    {
      panner->allow_traverse_bottom = 1;
    }
}

void
gtk_panner_unset_traverse_perm (GtkPanner *panner,
				gint permission)
{
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));

  if (permission & GTK_TRAVERSE_LEFT)
    {
      panner->allow_traverse_left = 0;
    }
  if (permission & GTK_TRAVERSE_RIGHT)
    {
      panner->allow_traverse_right = 0;
    }
  if (permission & GTK_TRAVERSE_TOP)
    {
      panner->allow_traverse_top = 0;
    }
  if (permission & GTK_TRAVERSE_BOTTOM)
    {
      panner->allow_traverse_bottom = 0;
    }
}


static void
gtk_panner_size_request (GtkWidget *widget,
			 GtkRequisition *requisition)
{
  GtkPanner *panner;
  gint req_width;
  gint req_height;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PANNER (widget));

  panner = GTK_PANNER (widget);
  req_width = panner->canvas_area.width
      + panner->canvas_area.x * 2; /* XXX */
  req_height = panner->canvas_area.height
      + panner->canvas_area.y * 2; /* XXX */

  requisition->width = req_width;
  requisition->height = req_height;
}

static void
gtk_panner_size_allocate (GtkWidget *widget,
			  GtkAllocation *allocation)
{
  GtkPanner *panner;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PANNER (widget));

  panner = GTK_PANNER (widget);

  if (GTK_WIDGET_REALIZED (widget))
    gdk_window_move_resize (widget->window,
                            allocation->x, 
                            allocation->y,
                            widget->requisition.width, 
                            widget->requisition.height);

  widget->allocation.width = widget->requisition.width;
  widget->allocation.height = widget->requisition.height;
}

static void
gtk_panner_realize (GtkWidget *widget)
{
  GtkPanner *panner;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PANNER (widget));

  panner = GTK_PANNER (widget);
  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget);
  attributes.event_mask |= (GDK_EXPOSURE_MASK |
                            GDK_BUTTON_PRESS_MASK |
                            GDK_BUTTON_RELEASE_MASK |
			    GDK_BUTTON_MOTION_MASK |
                            GDK_POINTER_MOTION_HINT_MASK |
                            GDK_ENTER_NOTIFY_MASK |
                            GDK_LEAVE_NOTIFY_MASK);

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
				   &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, panner);

  widget->style = gtk_style_attach (widget->style, widget->window);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_ACTIVE);
}

static gint
gtk_panner_expose (GtkWidget      *widget,
		   GdkEventExpose *event)
{
  GtkPanner *panner;
  GdkRectangle area;
  gint r;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_PANNER (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  if (GTK_WIDGET_DRAWABLE (widget))
    {
      panner = GTK_PANNER (widget);

      r = gdk_rectangle_intersect (&panner->canvas_area, &event->area, &area);
      if (r == TRUE)
	{ gtk_panner_paint_canvas (panner, &area); }

      r = gdk_rectangle_intersect (&panner->slider_area, &event->area, &area);
      if (r == TRUE)
	{ gtk_panner_paint_slider (panner, &area); }
    }

  return FALSE;
}

static gint
gtk_panner_button_press (GtkWidget *widget,
			 GdkEventButton *event)
{
  GtkPanner *panner;
  gint pressed_button;
  gint pressed_point_x;
  gint pressed_point_y;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_PANNER (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);


  panner = GTK_PANNER (widget);
  pressed_button = event->button;
  pressed_point_x = event->x;
  pressed_point_y = event->y;

  if ((panner->do_slide == 1) &&
      (panner->is_sliding == 0) &&
      (panner->is_resizing == 0) &&
      (pressed_point_x >= panner->slider_area.x) &&
      (pressed_point_x <= (panner->slider_area.x +
			   panner->slider_area.width)) &&
      (pressed_point_y >= panner->slider_area.y) &&
      (pressed_point_y <= (panner->slider_area.y +
			   panner->slider_area.height)) &&
      (pressed_button == 1))
    {
      panner->is_sliding = 1;
      panner->pressed_point_x = pressed_point_x;
      panner->pressed_point_y = pressed_point_y;

      panner->old_slider_x = panner->slider_x;
      panner->old_slider_y = panner->slider_y;
    }

  if ((panner->do_resize == 1) &&
      (panner->is_sliding == 0) &&
      (panner->is_resizing == 0) &&
      (pressed_button == 2))
    {
      panner->is_resizing = 1;
      panner->pressed_point_x = pressed_point_x;
      panner->pressed_point_y = pressed_point_y;

      panner->old_slider_x = panner->slider_x;
      panner->old_slider_y = panner->slider_y;
      panner->old_slider_width = panner->slider_width;
      panner->old_slider_height = panner->slider_height;
    }

  return FALSE;
}

static gint
gtk_panner_button_release (GtkWidget *widget,
			   GdkEventButton *event)
{
  GtkPanner *panner;
  gint released_button;
  gint released_point_x;
  gint released_point_y;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_PANNER (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);


  panner = GTK_PANNER (widget);
  released_button = event->button;
  released_point_x = event->x;
  released_point_y = event->y;

  if ((panner->is_sliding == 1) &&
      (panner->is_resizing == 0) &&
      (released_button == 1))
    {
      panner->is_sliding = 0;
      panner->pressed_point_x = 0;
      panner->pressed_point_y = 0;
      panner->old_slider_x = 0;
      panner->old_slider_y = 0;
    }

  if ((panner->is_sliding == 0) &&
      (panner->is_resizing == 1) &&
      (released_button == 2))
    {
      panner->is_resizing = 0;
      panner->pressed_point_x = 0;
      panner->pressed_point_y = 0;
      panner->old_slider_x = 0;
      panner->old_slider_y = 0;
      panner->old_slider_width = 0;
      panner->old_slider_height = 0;
    }

  return FALSE;
}

static gint
gtk_panner_motion_notify (GtkWidget *widget,
			  GdkEventMotion *event)
{
  GtkPanner *panner;
  gint pointer_point_x;
  gint pointer_point_y;
  GdkModifierType modifiers;
  gint tmp1_slider_left;
  gint tmp1_slider_right;
  gint tmp1_slider_top;
  gint tmp1_slider_bottom;
  gint tmp2_slider_left;
  gint tmp2_slider_right;
  gint tmp2_slider_top;
  gint tmp2_slider_bottom;
  gint new_slider_x;
  gint new_slider_y;
  gint new_slider_width;
  gint new_slider_height;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_PANNER (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);


  panner = GTK_PANNER (widget);
  if (event->is_hint)
    {
      gdk_window_get_pointer (GTK_WIDGET (panner)->window,
			      &pointer_point_x,
			      &pointer_point_y,
			      &modifiers);
    }
  else
    {
      pointer_point_x = event->x;
      pointer_point_y = event->y;
      modifiers = 0;
    }

  if ((panner->is_sliding == 1) &&
      ((modifiers & GDK_BUTTON1_MASK)))
    {
      tmp1_slider_left = panner->old_slider_x
	  + (pointer_point_x - panner->pressed_point_x);
      tmp1_slider_top = panner->old_slider_y
	  + (pointer_point_y - panner->pressed_point_y);
      tmp1_slider_right = tmp1_slider_left + panner->slider_width - 1;
      tmp1_slider_bottom = tmp1_slider_top + panner->slider_height - 1;

      tmp2_slider_left = tmp1_slider_left;
      tmp2_slider_right = tmp1_slider_right;
      tmp2_slider_top = tmp1_slider_top;
      tmp2_slider_bottom = tmp1_slider_bottom;

      if ((panner->allow_traverse_left == 0) &&
	  (tmp1_slider_left < 0))
	{
	  tmp2_slider_left = 0;
	}
      if ((panner->allow_traverse_right == 0) &&
	  (tmp1_slider_right > (panner->canvas_width - 1)))
	{
	  tmp2_slider_right = panner->canvas_width - 1;
	}

      if ((panner->allow_traverse_top == 0) &&
	  (tmp1_slider_top < 0))
	{
	  tmp2_slider_top = 0;
	}
      if ((panner->allow_traverse_bottom == 0) &&
	  (tmp1_slider_bottom > (panner->canvas_height - 1)))
	{
	  tmp2_slider_bottom = panner->canvas_height - 1;
	}


      if (tmp2_slider_right != tmp1_slider_right)
	{
#if 0
	  /* no treat for bigger slider than canvas */
	  new_slider_x
	      = tmp2_slider_left + (tmp2_slider_right - tmp1_slider_right);
#else
	  /* XXX: enough? */
	  new_slider_x
	      = tmp2_slider_left - ABS(tmp2_slider_right - tmp1_slider_right);
#endif
	}
      else
	{
	  new_slider_x = tmp2_slider_left;
	}

      if (tmp2_slider_bottom != tmp1_slider_bottom)
	{
#if 0
	  /* no treat for bigger slider than canvas */
	  new_slider_y
	      = tmp2_slider_top + (tmp2_slider_bottom - tmp1_slider_bottom);
#else
	  /* XXX: enough? */
	  new_slider_y
	      = tmp2_slider_top - ABS(tmp2_slider_bottom - tmp1_slider_bottom);
#endif
	}
      else
	{
	  new_slider_y = tmp2_slider_top;
	}

      if ((new_slider_x != panner->slider_x) ||
	  (new_slider_y != panner->slider_y))
	{
	  gtk_panner_set_slider_position (panner, new_slider_x, new_slider_y);
	  gtk_panner_slider_motion_notify (panner);
	}
    }
#if 1
  if ((panner->is_resizing == 1) &&
      ((modifiers & GDK_BUTTON2_MASK)))
    {
      tmp1_slider_left = MIN (pointer_point_x, panner->pressed_point_x);
      tmp1_slider_right = MAX (pointer_point_x, panner->pressed_point_x);
      tmp1_slider_top = MIN (pointer_point_y, panner->pressed_point_y);
      tmp1_slider_bottom = MAX (pointer_point_y, panner->pressed_point_y);

      if (panner->allow_traverse_left == 1)
	{ tmp2_slider_left = tmp1_slider_left; }
      else
	{ tmp2_slider_left = MAX (tmp1_slider_left, panner->canvas_area.x); }

      if (panner->allow_traverse_right == 1)
	{ tmp2_slider_right = tmp1_slider_right; }
      else
	{
	  tmp2_slider_right = MIN (tmp1_slider_right,
				   (panner->canvas_area.x
				    + panner->canvas_area.width));
	}

      if (panner->allow_traverse_top == 1)
	{ tmp2_slider_top = tmp1_slider_top; }
      else
	{ tmp2_slider_top = MAX (tmp1_slider_top, panner->canvas_area.y); }

      if (panner->allow_traverse_bottom == 1)
	{ tmp2_slider_bottom = tmp1_slider_bottom; }
      else
	{
	  tmp2_slider_bottom = MIN (tmp1_slider_bottom,
				    (panner->canvas_area.y
				     + panner->canvas_area.height));
	}

      new_slider_x = tmp2_slider_left - panner->canvas_area.x;
      new_slider_y = tmp2_slider_top - panner->canvas_area.y;
      new_slider_width = tmp2_slider_right - tmp2_slider_left;
      new_slider_height = tmp2_slider_bottom - tmp2_slider_top;

      if ((new_slider_x != panner->slider_x) ||
	  (new_slider_y != panner->slider_y))
	{
	  gtk_panner_set_slider_position (panner, new_slider_x, new_slider_y);
	}
      if ((new_slider_width != panner->slider_width) ||
	  (new_slider_height != panner->slider_height))
	{
	  gtk_panner_set_slider_size (panner,
				      new_slider_width, new_slider_height);
	}
    }
#endif

  return FALSE;
}

static void
gtk_panner_canvas_resize_notify (GtkPanner *panner)
{
  gtk_signal_emit(GTK_OBJECT (panner),
                  panner_signals[CANVAS_RESIZE_NOTIFY]);
}

static void
gtk_panner_slider_move_notify (GtkPanner *panner)
{
  gtk_signal_emit(GTK_OBJECT (panner),
                  panner_signals[SLIDER_MOVE_NOTIFY]);
}

static void
gtk_panner_slider_resize_notify (GtkPanner *panner)
{
  gtk_signal_emit(GTK_OBJECT (panner),
                  panner_signals[SLIDER_RESIZE_NOTIFY]);
}

static void
gtk_panner_slider_motion_notify (GtkPanner *panner)
{
  gtk_signal_emit(GTK_OBJECT (panner),
                  panner_signals[SLIDER_MOTION_NOTIFY]);
}


#ifdef DEBUG_GTK_PANNER
static void
gtk_real_panner_canvas_resize_notify (GtkPanner *panner)
{
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));

  g_print ("canvas resized\n");
}

static void
gtk_real_panner_slider_move_notify (GtkPanner *panner)
{
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));

  g_print ("slider moved\n");
}

static void
gtk_real_panner_slider_resize_notify (GtkPanner *panner)
{
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));

  g_print ("slider resized\n");
}

static void
gtk_real_panner_slider_motion_notify (GtkPanner *panner)
{
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));

#if 1
  g_print ("slider: %dx%d (%d,%d)-(%d,%d)  canvas: %dx%d (%d,%d)-(%d,%d)\n",
	   panner->slider_width, panner->slider_height,
	   panner->slider_x, panner->slider_y,
	   (panner->slider_x + panner->slider_width - 1),
	   (panner->slider_y + panner->slider_height - 1),
	   panner->canvas_width, panner->canvas_height,
	   0, 0,
	   (0 + panner->canvas_width - 1),
	   (0 + panner->canvas_height - 1));
#endif
}
#endif


static void
gtk_panner_paint_canvas (GtkPanner *panner,
			 GdkRectangle *area)
{
  GtkPannerClass *panner_class;
    
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));


  panner_class = GTK_PANNER_CLASS (GTK_OBJECT (panner)->klass);

  if (panner->paint_canvas)
    {
      (* panner->paint_canvas) (panner, area, panner->paint_canvas_user_data);
    }
}

static void
gtk_panner_paint_slider (GtkPanner *panner,
			 GdkRectangle *area)
{
  GtkPannerClass *panner_class;
    
  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));


  panner_class = GTK_PANNER_CLASS (GTK_OBJECT (panner)->klass);

  if (panner->paint_slider)
    {
      (* panner->paint_slider) (panner, area, panner->paint_slider_user_data);
    }
}


static void
gtk_panner_default_paint_canvas (GtkPanner *panner,
				 GdkRectangle *area,
				 gpointer user_data)
{
  GtkWidget *widget;

  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));


  widget = GTK_WIDGET (panner);

  gdk_window_clear_area (widget->window,
			 area->x, area->y, area->width, area->height);
}

static void
gtk_panner_default_paint_slider (GtkPanner *panner,
				 GdkRectangle *area,
				 gpointer user_data)
{
  GtkWidget *widget;
  gint slider_x;
  gint slider_y;
  gint slider_width;
  gint slider_height;

  gint r;
  GdkRectangle clip_area;
  

  g_return_if_fail (panner != NULL);
  g_return_if_fail (GTK_IS_PANNER (panner));


  widget = GTK_WIDGET (panner);
  slider_x = panner->slider_area.x;
  slider_y = panner->slider_area.y;
  slider_width = panner->slider_area.width;
  slider_height = panner->slider_area.height;


  /* XXX */
  r = gdk_rectangle_intersect (&panner->canvas_area, &panner->slider_area,
				&clip_area);

  if (r == TRUE)
    {
      gdk_gc_set_clip_rectangle (widget->style->black_gc,
				 &clip_area);
      gdk_gc_set_clip_rectangle (widget->style->bg_gc[GTK_STATE_PRELIGHT],
				 &clip_area);
      gdk_gc_set_clip_rectangle (widget->style->light_gc[GTK_STATE_PRELIGHT],
				 &clip_area);
      gdk_gc_set_clip_rectangle (widget->style->dark_gc[GTK_STATE_PRELIGHT],
				 &clip_area);

      gdk_draw_rectangle (widget->window,
			  widget->style->bg_gc[GTK_STATE_PRELIGHT], TRUE,
			  slider_x, slider_y, slider_width, slider_height);

      gtk_draw_shadow (widget->style,
		       widget->window,
		       GTK_STATE_PRELIGHT, GTK_SHADOW_OUT,
		       slider_x, slider_y, slider_width, slider_height);

      gdk_gc_set_clip_rectangle (widget->style->black_gc,
				 NULL);
      gdk_gc_set_clip_rectangle (widget->style->bg_gc[GTK_STATE_PRELIGHT],
				 NULL);
      gdk_gc_set_clip_rectangle (widget->style->light_gc[GTK_STATE_PRELIGHT],
				 NULL);
      gdk_gc_set_clip_rectangle (widget->style->dark_gc[GTK_STATE_PRELIGHT],
				 NULL);
    }
}
