/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GTK_ROOT_WINDOW_H__
#define __GTK_ROOT_WINDOW_H__


#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_ROOT_WINDOW(obj)          GTK_CHECK_CAST (obj, gtk_root_window_get_type (), GtkRootWindow)
#define GTK_ROOT_WINDOW_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_root_window_get_type (), GtkRootWindowClass)
#define GTK_IS_ROOT_WINDOW(obj)       GTK_CHECK_TYPE (obj, gtk_root_window_get_type ())


typedef struct _GtkRootWindow       GtkRootWindow;
typedef struct _GtkRootWindowClass  GtkRootWindowClass;

struct _GtkRootWindow
{
  GtkWidget widget;
};

struct _GtkRootWindowClass
{
  GtkWidgetClass parent_class;
};


guint      gtk_root_window_get_type    (void);

GtkWidget* gtk_root_window_widget        (void);
void       gtk_root_window_set_events    (GtkRootWindow *root_window,
					  gint events);


GdkWindow* gdk_window_of_root_window        (void);
GdkColormap* gdk_colormap_of_root_window    (void);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_LABEL_H__ */
