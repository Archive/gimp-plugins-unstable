/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gdk/gdkx.h>
#include "gtkrootwindow.h"


static void       gtk_root_window_class_init (GtkRootWindowClass *klass);
static void       gtk_root_window_init       (GtkRootWindow      *root_window);
static GtkWidget* gtk_root_window_new        (void);


static GdkWindow *static__gdk_root_gdk_window = NULL;
static GtkWidget *static__gtk_root_window = NULL;


guint
gtk_root_window_get_type ()
{
  static guint root_window_type = 0;

  if (!root_window_type)
    {
      GtkTypeInfo root_window_info =
      {
	"GtkRootWindow",
	sizeof (GtkRootWindow),
	sizeof (GtkRootWindowClass),
	(GtkClassInitFunc) gtk_root_window_class_init,
	(GtkObjectInitFunc) gtk_root_window_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL,
      };

      root_window_type = gtk_type_unique (gtk_widget_get_type (), &root_window_info);
    }
  
  return root_window_type;
}

static void
gtk_root_window_class_init (GtkRootWindowClass *class)
{
  GtkWidgetClass *widget_class;

  widget_class = (GtkWidgetClass*) class;
}

static void
gtk_root_window_init (GtkRootWindow *gtk_root_window)
{
  gint width;
  gint height;

  
  GTK_WIDGET_UNSET_FLAGS (gtk_root_window,
			  GTK_NO_WINDOW);
  GTK_WIDGET_SET_FLAGS (gtk_root_window,
			(GTK_REALIZED | GTK_MAPPED | GTK_VISIBLE));

  GTK_WIDGET(gtk_root_window)->window = gdk_window_of_root_window ();
  gdk_window_set_user_data (GTK_WIDGET (gtk_root_window)->window,
			    gtk_root_window);

  gdk_window_get_size (GTK_WIDGET (gtk_root_window)->window, &width, &height);

  GTK_WIDGET (gtk_root_window)->requisition.width = width;
  GTK_WIDGET (gtk_root_window)->requisition.height = height;
  GTK_WIDGET (gtk_root_window)->allocation.x = 0;
  GTK_WIDGET (gtk_root_window)->allocation.y = 0;
  GTK_WIDGET (gtk_root_window)->allocation.width = width;
  GTK_WIDGET (gtk_root_window)->allocation.height = height;
}

static GtkWidget *
gtk_root_window_new ()
{
  GtkWidget *gtk_root_window;

  gtk_root_window = gtk_type_new (gtk_root_window_get_type ());

  return GTK_WIDGET(gtk_root_window);
}

GtkWidget *
gtk_root_window_widget ()
{
  if (static__gtk_root_window == NULL) {
      static__gtk_root_window = gtk_root_window_new();
  }

  return static__gtk_root_window;
}

void
gtk_root_window_set_events (GtkRootWindow *gtk_root_window, gint events)
{
  g_return_if_fail (gtk_root_window != NULL);
  g_return_if_fail (GTK_IS_ROOT_WINDOW (gtk_root_window));

  /* call gtk_widget_set_events to set object data; "event_mask" */
  /* deceive gtk_widget_set_events()'s GTK_REALIZE flag check */
  GTK_WIDGET_UNSET_FLAGS (GTK_WIDGET (gtk_root_window), GTK_REALIZED);
  gtk_widget_set_events (GTK_WIDGET (gtk_root_window), events);
  GTK_WIDGET_SET_FLAGS (GTK_WIDGET (gtk_root_window), GTK_REALIZED);

  /* set root window's event mask */
  /* XXX: need care for previous event mask? */
  gdk_window_set_events (GTK_WIDGET (gtk_root_window)->window, events);
}

/***/

GdkWindow *
gdk_window_of_root_window (void)
{
  if (static__gdk_root_gdk_window == NULL)
    {
      static__gdk_root_gdk_window = gdk_window_foreign_new(GDK_ROOT_WINDOW());
    }

  return static__gdk_root_gdk_window;
}

GdkColormap *
gdk_colormap_of_root_window (void)
{
  GdkColormap *gdk_colormap;

  /* Is this right ? */
  gdk_colormap = gdk_colormap_get_system ();
    
  return gdk_colormap;
}
