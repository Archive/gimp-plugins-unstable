/* acconfig.h
   This file is in the public domain.

   Descriptive text for the C preprocessor macros that
   the distributed Autoconf macros can define.
   No software package will use all of them; autoheader copies the ones
   your configure.in uses into your configuration header file templates.

   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  Although this order
   can split up related entries, it makes it easier to check whether
   a given entry is in the file.

   Leave the following blank line there!!  Autoheader needs it.  */


/* These get defined to the version numbers in configure.in */
#undef GIMP_MAJOR_VERSION_NUMBER
#undef GIMP_MINOR_VERSION_NUMBER
#undef GIMP_MICRO_VERSION_NUMBER

#undef HAVE_DIRENT_H
#undef HAVE_DOPRNT
#undef HAVE_IPC_H
#undef HAVE_NDIR_H
#undef HAVE_SHM_H
#undef HAVE_SYS_DIR_H
#undef HAVE_SYS_NDIR_H
#undef HAVE_SYS_SELECT_H
#undef HAVE_SYS_TIME_H
#undef HAVE_UNISTD_H
#undef HAVE_VPRINTF
#undef HAVE_XSHM_H

#undef IPC_RMID_DEFERRED_RELEASE

#undef NO_FD_SET

#undef RETSIGTYPE
#undef TIME_WITH_SYS_TIME

/* #undef PACKAGE */
/* #undef VERSION */


/* Leave that blank line there!!  Autoheader needs it.
   If you're adding to this file, keep in mind:
   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  */
