/*
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This is a plug-in for the GIMP.
 *
 * onroot:-
 * Dump image onto root window.
 *
 * Copyright (C) 1998 Andy Thomas  alt@picnic.demon.co.uk
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 */


/* Change log:-
 * Third version. Added freehand mode. v0.99.2
 * Second version. Improved centre placing v0.99.1
 * First version. relased v 0.99
 */

#include <stdio.h>
#include <stdlib.h>
#include "gtk/gtk.h"
#include "gdk/gdk.h"
#include "gdk/gdkkeysyms.h"
#include "libgimp/gimp.h"
#include <X11/X.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>





/***** Magic numbers *****/

#define PREVIEW_SIZE 128 
#define SCALE_WIDTH  100

#define PREVIEW_MASK   GDK_BUTTON_PRESS_MASK | \
		       GDK_BUTTON_RELEASE_MASK | \
		       GDK_POINTER_MOTION_MASK | \
		       GDK_POINTER_MOTION_HINT_MASK | \
		       GDK_KEY_PRESS_MASK | \
		       GDK_KEY_RELEASE_MASK | \
		       GDK_BUTTON_MOTION_MASK

#define CURSOR_SEL_BOUNDS 3   /* Boundary in center preview where cursor activated */

/* Even more stuff from Quartics plugins */
#define CHECK_SIZE  8
#define CHECK_DARK  ((int) (1.0 / 3.0 * 255))
#define CHECK_LIGHT ((int) (2.0 / 3.0 * 255))


typedef struct {
  GtkWidget *preview;
  guchar     preview_row[PREVIEW_SIZE * 4];
  gint run;
  guchar * pv_cache;
  guchar * pv_working;
  gint pv_imgsz;
  gint img_bpp;
} RootwinInterface;


static RootwinInterface tint =
{
  NULL,  /* Preview */
  {'4','u'},    /* Preview_row */
  FALSE, /* run */
  NULL,
  NULL,
  0,
  4      /* bpp of drawable */
};

typedef enum {
  FULLSIZE=0, /* Image full screen */
  MAXASP=1,   /* Max aspect ratio on screen */
  TILED=2,    /* Normal tiled */
  INTTILE=3,  /* Interger tiled */
  CENTRE=4    /* Cneter of screen */
} WinTypeTo;

/*WinTypeTo wintype = TILED; Default to normal tiled */
gint rootw_width; 
gint rootw_height;
gint rootw_destx;
gint rootw_desty;
GDrawable *rootw_drawable;
gint32 onroot_image;
gint32 saved_newimage_ID = -1;
gdouble magfactor = 1.0;
guchar bg_red = 0, bg_green = 0, bg_blue = 0;
guchar last_bg_red = 0, last_bg_green = 0, last_bg_blue = 0;
static GtkWidget *colsel_window = NULL;

GDrawable *rootwindrawable;

static void      query  (void);
static void      run    (gchar    *name,
			 gint      nparams,
			 GParam   *param,
			 gint     *nreturn_vals,
			 GParam  **return_vals);

static gint      rootwin_dialog (void);
static void      rootwin_close_callback (GtkWidget *widget, gpointer   data);
static void      rootwin_ok_callback (GtkWidget *widget, gpointer   data);
static void      dialog_update_preview(void);
static void      rootwin_toggle_update(GtkWidget *widget,gpointer);
static void	 cache_preview(void);
static gint      onroot_preview_events ( GtkWidget *widget,GdkEvent *event );
static void      do_centre_preview();
static void      do_window();
static void      do_centre_init();
static void      onroot_init();
static void      centre_check_bounds();
static void      centre_check_bounds2();
static void      mag_image(GtkWidget *widget,gint enlarge);

GPlugInInfo PLUG_IN_INFO =
{
  NULL,    /* init_proc */
  NULL,    /* quit_proc */
  query,   /* query_proc */
  run,     /* run_proc */
};

typedef struct {
  guchar color[3];
} COLORVALS; 

/* Variables set in dialog box */
typedef struct data {
  WinTypeTo wintype; /* Mode to draw into root window */
  gint xpos; /* top left x in centre mode */
  gint ypos; /* top left y in centre mode */
  gint width; /* width image in centre mode */
  gint height; /* height image in centre mode */
  COLORVALS bg_colours;
} RootwinVals;

/* Values when first invoked */
static RootwinVals rootwinvals =
{
  TILED,
  0,
  0,
  0,
  0,
  {{0,0,0}}, /* Black by default */
};

/* Stuff for the preview bit */
static gint   sel_x1, sel_y1, sel_x2, sel_y2;
static gint   sel_width, sel_height;
static gint   preview_width, preview_height;
static gint   has_alpha;
static gint    isgrey;
static gint   screen_width;
static gint   screen_height;
/* Stuff for centre placement */
gint presel_width;
gint presel_height;
gint start_row;
gint start_col;

MAIN ()

static void
query ()
{
  static GParamDef args[] =
  {
    { PARAM_INT32, "run_mode", "Interactive, non-interactive" },
    { PARAM_IMAGE, "image", "Input image (unused)" },
    { PARAM_DRAWABLE, "drawable", "Input drawable" },
    { PARAM_INT32, "rootmode", "dump to root window using mode ( FULLSIZE(0),MAXASPECT(1),TILED(2),INTTILE(3),CENTRE(4)"},
    { PARAM_INT32,"Xpos", "Top left corner X position (Free hand mode only default centres image)"},
    { PARAM_INT32,"Ypos", "Top left corner Y position (Free hand mode only default centres image)"},
    { PARAM_INT32,"Width","Width of image (Free hand mode only default image width)"},
    { PARAM_INT32,"Height","Height of image (Free hand mode only default image height)"},
    { PARAM_COLOR, "color", "Color of backgound (Free hand mode only default black)"},
  };
  static GParamDef *return_vals = NULL;
  static int nargs = sizeof (args) / sizeof (args[0]);
  static int nreturn_vals = 0;

  gimp_install_procedure ("plug_in_root_window",
			  "Dumps the image into the root window",
			  "More here later",
			  "Andy Thomas",
			  "Andy Thomas",
			  "1998",
			  "<Image>/File/Onroot",
			  "RGB*, GRAY*",
			  PROC_PLUG_IN,
			  nargs, nreturn_vals,
			  args, return_vals);
}

static void
run    (gchar    *name,
	gint      nparams,
	GParam   *param,
	gint     *nreturn_vals,
	GParam  **return_vals)
{
  static GParam values[1];
  GDrawable *drawable;
  GRunModeType run_mode;
  GStatusType status = STATUS_SUCCESS;

  run_mode = param[0].data.d_int32;

  *nreturn_vals = 1;
  *return_vals = values;

  values[0].type = PARAM_STATUS;
  values[0].data.d_status = status;

  onroot_image = param[1].data.d_image;

  rootwindrawable = 
    drawable = 
    gimp_drawable_get (param[2].data.d_drawable);

  gimp_drawable_mask_bounds(drawable->id, &sel_x1, &sel_y1, &sel_x2, &sel_y2);

  sel_width  = sel_x2 - sel_x1;
  sel_height = sel_y2 - sel_y1;

  /* will fail badly if no X display  */
  onroot_init();
  
  switch (run_mode)
    {
    case RUN_INTERACTIVE:
    case RUN_WITH_LAST_VALS:
      {
	gint real_presel_width = MAX((sel_width*preview_width)/screen_width,1);
	gint real_presel_height = MAX((sel_height*preview_height)/screen_height,1);
	
	gimp_get_data ("plug_in_root_window", &rootwinvals);
	start_col = (rootwinvals.xpos*preview_width)/screen_width;
	start_row = (rootwinvals.ypos*preview_height)/screen_height;
	/* Calc row where image appears */
	presel_width = (real_presel_width*rootwinvals.width)/sel_width;
	presel_height = (real_presel_height*rootwinvals.height)/sel_height;
	
	centre_check_bounds();

	bg_red = rootwinvals.bg_colours.color[0];
	bg_green = rootwinvals.bg_colours.color[1];
	bg_blue = rootwinvals.bg_colours.color[2];

	if(run_mode == RUN_INTERACTIVE)
	{
	  if (!rootwin_dialog())
	  {
	    gimp_drawable_detach (drawable);
	    return;
	  }
	}
	break;
      }

    case RUN_NONINTERACTIVE:
      if (nparams != 9)
      {
	status = STATUS_CALLING_ERROR;
      }
      else
      {
	gint real_presel_width = MAX((sel_width*preview_width)/screen_width,1);
	gint real_presel_height = MAX((sel_height*preview_height)/screen_height,1);
	
	rootwinvals.wintype = param[3].data.d_int32;
	rootwinvals.xpos = param[4].data.d_int32;
	rootwinvals.ypos = param[5].data.d_int32;
	presel_width = (real_presel_width*param[6].data.d_int32)/sel_width;
	presel_height = (real_presel_height*param[7].data.d_int32)/sel_height;
	centre_check_bounds();

	bg_red = param[8].data.d_color.red;
	bg_green = param[8].data.d_color.green;
	bg_blue = param[8].data.d_color.blue;                                     
      }
      break;
    default:
      break;
    }


  if (gimp_drawable_color (drawable->id) || gimp_drawable_gray (drawable->id))
    {
      do_window();

      if (run_mode != RUN_NONINTERACTIVE)
	gimp_displays_flush ();

      if (run_mode == RUN_INTERACTIVE)
      {
	gint real_presel_width = MAX((sel_width*preview_width)/screen_width,1);
	gint real_presel_height = MAX((sel_height*preview_height)/screen_height,1);

	rootwinvals.width = (presel_width*sel_width)/real_presel_width;
	rootwinvals.height = (presel_height*sel_height)/real_presel_height;

	rootwinvals.bg_colours.color[0] = bg_red;
	rootwinvals.bg_colours.color[1] = bg_green;
	rootwinvals.bg_colours.color[2] = bg_blue;

	gimp_set_data ("plug_in_root_window", &rootwinvals, sizeof (RootwinVals));
      }
    }
  else
    {
      status = STATUS_EXECUTION_ERROR;
    }

  values[0].data.d_status = status;

  gimp_drawable_detach (drawable);
}

static void onroot_init()
{
  gchar **argv;
  gint argc;
  int           pwidth, pheight;

  argc = 1;
  argv = g_new (gchar *, 1);
  argv[0] = g_strdup ("rootwin");

  gtk_init (&argc, &argv);

  screen_width = gdk_screen_width();
  screen_height = gdk_screen_height();

  /* Calculate preview size */
  
  if (screen_width > screen_height) {
    pwidth  = MIN(screen_width, PREVIEW_SIZE);
    pheight = screen_height * pwidth / screen_width;
  } else {
    pheight = MIN(screen_height, PREVIEW_SIZE);
    pwidth  = screen_width * pheight / screen_height;
  }
  
  preview_width  = MAX(pwidth, 2);  /* Min size is 2 */
  preview_height = MAX(pheight, 2); 

  
}

/* Build the dialog up. This was the hard part! */
static gint
rootwin_dialog ()
{
  GtkWidget *dlg;
  GtkWidget *button;
  GtkWidget *frame;
  GtkWidget *xframe;
  GtkWidget *table;
  GtkWidget *toggle_vbox;
  GtkWidget *toggle;

  GSList *orientation_group = NULL;
  /*
  guchar     *color_cube;
  */

  /* Get the stuff for the preview window...*/
  /*gtk_preview_set_gamma(gimp_gamma());
   * gtk_preview_set_install_cmap(gimp_install_cmap());

  color_cube = gimp_color_cube();
  gtk_preview_set_color_cube(color_cube[0], color_cube[1], color_cube[2], color_cube[3]);
  
  gtk_widget_set_default_visual(gtk_preview_get_visual());
  gtk_widget_set_default_colormap(gtk_preview_get_cmap());
  */
  cache_preview(); /* Get the preview image and store it also set has_alpha */


  dlg = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (dlg), "To Root Window");
  gtk_window_position (GTK_WINDOW (dlg), GTK_WIN_POS_MOUSE);
  gtk_signal_connect (GTK_OBJECT (dlg), "destroy",
		      (GtkSignalFunc) rootwin_close_callback,
		      NULL);

  /*  Action area  */
  button = gtk_button_new_with_label ("OK");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      (GtkSignalFunc) rootwin_ok_callback,
                      dlg);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->action_area), button, TRUE, TRUE, 0);
  gtk_widget_grab_default (button);
  gtk_widget_show (button);

  button = gtk_button_new_with_label ("Cancel");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_signal_connect_object(GTK_OBJECT (button), "clicked",
			     (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT (dlg));
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->action_area), button, TRUE, TRUE, 0);
  gtk_widget_show (button);

  /* Start building the frame for the preview area */

  frame = gtk_frame_new ("preview");
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_container_border_width (GTK_CONTAINER (frame), 10);
  table = gtk_table_new (5, 5, FALSE); 
  gtk_container_border_width (GTK_CONTAINER (table), 10); 
  gtk_container_add (GTK_CONTAINER (frame), table); 
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), frame, TRUE, TRUE, 0);
  tint.preview = gtk_preview_new(GTK_PREVIEW_COLOR);
  gtk_preview_size(GTK_PREVIEW(tint.preview), preview_width, preview_height);

  gtk_widget_set_events( GTK_WIDGET(tint.preview), PREVIEW_MASK);
  GTK_WIDGET_SET_FLAGS (GTK_WIDGET(tint.preview), GTK_CAN_FOCUS);
  gtk_signal_connect( GTK_OBJECT(tint.preview), "event",
		      (GtkSignalFunc) onroot_preview_events,
		      NULL);
  xframe = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (xframe), GTK_SHADOW_IN);
  gtk_container_add(GTK_CONTAINER(xframe), tint.preview);
  gtk_widget_show(xframe);
  gtk_table_attach(GTK_TABLE(table), xframe, 0, 1, 0, 2, GTK_EXPAND , GTK_EXPAND, 0, 0);
  gtk_widget_show(frame); 
  gtk_widget_show(table); 
  
  frame = gtk_frame_new ("Parameter Settings");
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_container_border_width (GTK_CONTAINER (frame), 10);
  table = gtk_table_new (5, 5, FALSE);
  gtk_container_border_width (GTK_CONTAINER (table), 10);

  toggle_vbox = gtk_hbox_new(FALSE, 0);

  gtk_box_pack_start (GTK_BOX(toggle_vbox), table, TRUE, TRUE, 0);
  
  gtk_container_add (GTK_CONTAINER (frame), toggle_vbox);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), frame, TRUE, TRUE, 0);

  /* Now the type toggles */

  /* TILED */
  toggle = gtk_radio_button_new_with_label (orientation_group,"Tiled");
  if(rootwinvals.wintype == TILED)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(toggle),TRUE);

  orientation_group = gtk_radio_button_group (GTK_RADIO_BUTTON (toggle));
  gtk_signal_connect (GTK_OBJECT (toggle), "toggled",
		      (GtkSignalFunc) rootwin_toggle_update,
		      (gpointer)TILED);
  gtk_widget_show (toggle);  
  gtk_container_add (GTK_CONTAINER (toggle_vbox), toggle);

  /* FULLSIZE */

  toggle = gtk_radio_button_new_with_label (orientation_group,"Full size");
  if(rootwinvals.wintype == FULLSIZE)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(toggle),TRUE);

  orientation_group = gtk_radio_button_group (GTK_RADIO_BUTTON (toggle));
  gtk_signal_connect (GTK_OBJECT (toggle), "toggled",
		      (GtkSignalFunc) rootwin_toggle_update,
		      (gpointer)FULLSIZE);
  gtk_widget_show (toggle);  

  gtk_container_add (GTK_CONTAINER (toggle_vbox), toggle);

  /* MAXASP */
  toggle = gtk_radio_button_new_with_label (orientation_group,"Max aspect");
  if(rootwinvals.wintype == MAXASP)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(toggle),TRUE);

  orientation_group = gtk_radio_button_group (GTK_RADIO_BUTTON (toggle));
  gtk_signal_connect (GTK_OBJECT (toggle), "toggled",
		      (GtkSignalFunc) rootwin_toggle_update,
		      (gpointer)MAXASP);
  gtk_widget_show (toggle);  

  gtk_container_add (GTK_CONTAINER (toggle_vbox), toggle);

  /* INTTILE */
  toggle = gtk_radio_button_new_with_label (orientation_group,"Integer tiled");
  if(rootwinvals.wintype == INTTILE)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(toggle),TRUE);

  orientation_group = gtk_radio_button_group (GTK_RADIO_BUTTON (toggle));
  gtk_signal_connect (GTK_OBJECT (toggle), "toggled",
		      (GtkSignalFunc) rootwin_toggle_update,
		      (gpointer)INTTILE);
  gtk_widget_show (toggle);  

  gtk_container_add (GTK_CONTAINER (toggle_vbox), toggle);

  /* CENTRE */
  toggle = gtk_radio_button_new_with_label (orientation_group,"Free Hand");
  if(rootwinvals.wintype == CENTRE)
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(toggle),TRUE);

  orientation_group = gtk_radio_button_group (GTK_RADIO_BUTTON (toggle));
  gtk_signal_connect (GTK_OBJECT (toggle), "toggled",
		      (GtkSignalFunc) rootwin_toggle_update,
		      (gpointer)CENTRE);
  gtk_widget_show (toggle);  

  gtk_container_add (GTK_CONTAINER (toggle_vbox), toggle);

  gtk_widget_show(toggle_vbox);
  gtk_widget_show(frame); 
  gtk_widget_show(table); 
  gtk_widget_show(tint.preview);

  gtk_widget_show (dlg);
  dialog_update_preview();

  gdk_window_set_cursor(GTK_WIDGET(tint.preview)->window,
			gdk_cursor_new(GDK_CROSSHAIR));
  gtk_widget_grab_focus (GTK_WIDGET(tint.preview));
  gtk_main ();
  gdk_flush ();

  return tint.run;
}

static void
rootwin_close_callback (GtkWidget *widget,
			 gpointer   data)
{
  gtk_main_quit ();
}

static void
rootwin_ok_callback (GtkWidget *widget,
		      gpointer   data)
{
  tint.run = TRUE;
  gtk_widget_destroy (GTK_WIDGET (data));
  if(colsel_window != NULL)
    gtk_widget_destroy (GTK_WIDGET (colsel_window));
}


static void
rootwin_toggle_update(GtkWidget *widget,
                      gpointer   data)
{
  WinTypeTo type = (WinTypeTo)data;
  
  if (GTK_TOGGLE_BUTTON (widget)->active)
  {
    rootwinvals.wintype = type;
    do_centre_init(); /*Just in csae we are doing the centre type */
    dialog_update_preview();
  }
}


/* Cache the preview image - updates are a lot faster. */
/* The preview_cache will contain the small image */

static void
cache_preview()
{
  GPixelRgn src_rgn;
  int y,x;
  guchar *src_rows;
  guchar *p;

  gimp_pixel_rgn_init(&src_rgn,rootwindrawable,sel_x1,sel_y1,sel_width,sel_height,FALSE,FALSE);

  src_rows = g_new(guchar ,sel_width*4); 
  p = tint.pv_cache = g_new(guchar ,preview_width*preview_height*4);
  tint.pv_working =  g_new(guchar ,preview_width*preview_height*4);
  tint.pv_imgsz = preview_width*preview_height*4;

  tint.img_bpp = gimp_drawable_bpp(rootwindrawable->id);   

  has_alpha = gimp_drawable_has_alpha(rootwindrawable->id);

  if(tint.img_bpp < 3)
    {
      tint.img_bpp = 3 + has_alpha;
    }

  switch ( gimp_drawable_type (rootwindrawable->id) )
    {
    case GRAYA_IMAGE:
    case GRAY_IMAGE:
      isgrey = 1;
    default:
      ;
    }

  for (y = 0; y < preview_height; y++) {
  
    gimp_pixel_rgn_get_row(&src_rgn,
			   src_rows,
			   sel_x1,
			   sel_y1 + (y*sel_height)/preview_height,
			   sel_width);
      
    for (x = 0; x < (preview_width); x ++) {
      /* Get the pixels of each col */
      int i;
      for (i = 0 ; i < 3; i++ )
	p[x*tint.img_bpp+i] = src_rows[((x*sel_width)/preview_width)*src_rgn.bpp +((isgrey)?0:i)]; 
      if(has_alpha)
	p[x*tint.img_bpp+3] = src_rows[((x*sel_width)/preview_width)*src_rgn.bpp + ((isgrey)?1:3)];
    }
    p += (preview_width*tint.img_bpp);
  }
  g_free(src_rows);
}


#define STEP 100

static void 
fill_preview_widget(GtkWidget *pv)
{
  gint y;
  GPixelRgn src_rgn;
  guchar *src_rows;
  guchar *p;

  p = g_new(guchar ,rootw_width*4); 

  gimp_pixel_rgn_init(&src_rgn,rootw_drawable,sel_x1,sel_y1,rootw_width,rootw_height,FALSE,FALSE);

  src_rows = g_new(guchar ,(MAX(rootw_width,rootw_height))*4*STEP); 

  for (y = 0; y < rootw_height; y += STEP) 
  {
    int rr;
    int step;
    
    if((y + STEP) > rootw_height)
      step = rootw_height - y;
    else
      step = STEP;
    
    gimp_pixel_rgn_get_rect(&src_rgn,
			    src_rows,
			    sel_x1,
			    sel_y1 + y,
			    rootw_width,
			    step);
    
    /* Dump area into preview */
    for(rr = 0 ; rr < step; rr++)
    {
      int x;
      for (x = 0; x < (rootw_width); x ++) 
      {
	/* Get the pixels of each col */
	int i;
	for (i = 0 ; i < 3; i++ )
	  p[x*3+i] = src_rows[((rr*rootw_width + x)*src_rgn.bpp) +((isgrey)?0:i)]; 
	/*
	  if(has_alpha)
	  p[x*tint.img_bpp+3] = src_rows[((rr*rootw_width + x)*src_rgn.bpp) + ((isgrey)?1:3)];
	  */
      }
      gtk_preview_draw_row(GTK_PREVIEW(pv), p, 0, y+rr, rootw_width);
    }
  }
  g_free(src_rows);
}

static gint32
gimp_image_active_drawable (gint32 image_ID)
{
  GParam *return_vals;
  int nreturn_vals;
  gint32 drawable_ID;

  return_vals = gimp_run_procedure ("gimp_image_active_drawable",
				    &nreturn_vals,
				    PARAM_IMAGE, image_ID,
				    PARAM_END);

  drawable_ID = -1;
  if (return_vals[0].data.d_status == STATUS_SUCCESS)
    drawable_ID = return_vals[1].data.d_drawable;

  gimp_destroy_params (return_vals, nreturn_vals);

  return drawable_ID;
}


static gint32
gimp_image_duplicate (gint32 image_ID)
{
  GParam *return_vals;
  int nreturn_vals;

  return_vals = gimp_run_procedure ("gimp_channel_ops_duplicate",
				    &nreturn_vals,
				    PARAM_IMAGE,image_ID,
				    PARAM_END);

  saved_newimage_ID = -1;
  if (return_vals[0].data.d_status == STATUS_SUCCESS)
    saved_newimage_ID = return_vals[1].data.d_image;
  else
    printf("Failed to dup image\n");

  gimp_destroy_params (return_vals, nreturn_vals);

  return saved_newimage_ID;
}

static void
gimp_image_scale (gint32 image_ID,
		  guint  new_width,
		  guint  new_height)
{
  GParam *return_vals;
  int nreturn_vals;

  return_vals = gimp_run_procedure ("gimp_image_scale",
				    &nreturn_vals,
				    PARAM_IMAGE, image_ID,
				    PARAM_INT32, new_width,
				    PARAM_INT32, new_height,
				    PARAM_END);

  gimp_destroy_params (return_vals, nreturn_vals);
}

static int 
newresizedimage(gint new_width,gint new_height)
{
  gint32 newimage_ID;
  gint32 newdrawable_ID;

  newimage_ID = gimp_image_duplicate(onroot_image);
  if(newimage_ID > 0)
  {
    /* Don't want gimp cache filled up */
    gimp_image_disable_undo(newimage_ID);

    newdrawable_ID = gimp_image_active_drawable(newimage_ID);
    
    if(newdrawable_ID < 0 )
    {
      printf("Failed to get drawable\n");
    }
    else
    {
      /* OK here we go make image full screen size */
      gimp_image_scale(newimage_ID,new_width,new_height);
      rootw_width = new_width;
      rootw_height = new_height;
      rootw_drawable =  gimp_drawable_get(newdrawable_ID);
      if(rootw_drawable)
	return 0; /* Setup OK for this option */
      else
	printf("Failed to get active drawable\n");
    }
  }
  else
  {
    printf("Failed to dup to new layer\n");
  }
  return -1;
}

/* Return -1 on failure 0 if OK */
static int 
fullsize()
{
  return(newresizedimage(screen_width,screen_height));
}

static int
maxasp()
{
  gint new_width;
  gint new_height;

  /* work out width/height is max
   * work out mutli ratio ignore if ratio is 1.0
   * copy image 
   * mutliply width/height
   * resize image
   * set up to do image
   */

  new_height = (sel_height*screen_width)/sel_width;
  new_width = screen_width;

  if(new_height > screen_height)
  {
    new_width = (sel_width*screen_height)/sel_height;
    new_height = screen_height;
  }

  return(newresizedimage(new_width,new_height));
}

static int
inttile()
{
  gint width_ratio = screen_width/sel_width + 1;
  gint height_ratio = screen_height/sel_height + 1;

  return(newresizedimage((screen_width+width_ratio)/width_ratio,
			 (screen_height+height_ratio)/height_ratio));
}

static int
centre()
{
  gint real_presel_width = MAX((sel_width*preview_width)/screen_width,1);
  gint real_presel_height = MAX((sel_height*preview_height)/screen_height,1);

  rootw_width = (presel_width*sel_width)/real_presel_width;
  rootw_height = (presel_height*sel_height)/real_presel_height;
  rootw_drawable = rootwindrawable;
  rootw_destx = rootwinvals.xpos;
  rootw_desty = rootwinvals.ypos;

  if(real_presel_width == presel_width && real_presel_height == presel_height)
    return 0;
  else
    return(newresizedimage(rootw_width,rootw_height)); 
}

/* Set the image that we are going to dump */
/* Things set :-
 * rootw_width
 * rootw_height
 * rootw_drawable
 */

static void 
setup_image()
{
  /* destination pixmap x,y defaults to 0,0 */

  rootw_destx = 0;
  rootw_desty = 0;

  switch(rootwinvals.wintype)
  {
  case FULLSIZE:
    if(fullsize() == 0)
      return;
    /* Failed for some reason */
    break;
  case MAXASP:
    if(maxasp() == 0)
      return;
    break;
  case INTTILE:
    if(inttile() == 0)
      return;
  case CENTRE:
    if(centre() == 0)
      return;
  case TILED:
  default:
  }
  rootw_width = sel_width;
  rootw_height = sel_height;
  rootw_drawable = rootwindrawable;
}

static void
free_gimp_data()
{
  /* Delete tmp images */
  if(saved_newimage_ID != -1)
  {
    gimp_image_enable_undo(saved_newimage_ID);
    gimp_image_delete(saved_newimage_ID);
  }
}

GdkPixmap *pixmap; /* Pixmap to dump onto root */

static void
do_window()
{
  extern GdkWindow gdk_root_parent;
  extern void *gdk_display;
  extern Window gdk_root_window;
  static Pixmap tmpPix = (Pixmap) NULL;   

  GdkGC * gc; /* Using this Gc */

  GtkWidget * pv = gtk_preview_new(GTK_PREVIEW_COLOR);

  Atom prop, type;
  unsigned char *data = NULL;
  int format;
  unsigned long length, after;

  setup_image();

  /* This is the size of the real image */
  /* Clip to size of screen?*/
  gtk_preview_size(GTK_PREVIEW(pv), rootw_width,rootw_height);

  gc = gdk_gc_new(&gdk_root_parent);

  /* The wad of Xmem for the pixmap */ 
  if(rootwinvals.wintype == CENTRE)
  {
    GdkColor *col = (GdkColor *)malloc(sizeof(GdkColor));
    pixmap = gdk_pixmap_new(&gdk_root_parent,screen_width,screen_height,-1);

    col->red = bg_red * (65535/255);
    col->green = bg_green * (65535/255);
    col->blue = bg_blue * (65535/255);

    col->pixel = (gulong)(bg_red*65536 + bg_green*256 + bg_blue);

    gdk_color_alloc(gtk_widget_get_colormap(GTK_WIDGET(pv)), col);

    gdk_gc_set_foreground(gc,col);
    gdk_draw_rectangle(pixmap,gc,TRUE,0,0,screen_width,screen_height);
  }
  else
  {
    pixmap = gdk_pixmap_new(&gdk_root_parent,rootw_width,rootw_height,-1);
  }

  /* Draw into the pixmap */

  fill_preview_widget(pv);

  gtk_preview_put(GTK_PREVIEW(pv),pixmap,gc,0,0,rootw_destx,rootw_desty,rootw_width,rootw_height);

  /*gdk_draw_rectangle(pixmap,gc,0,0,0,80,80);*/

  gdk_window_set_back_pixmap(&gdk_root_parent,pixmap,FALSE);
  gdk_window_clear(&gdk_root_parent);


  /* HACK Hopefully frees up allocated colormap cells*/

  tmpPix = XCreatePixmap(gdk_display,gdk_root_window , 1, 1, 1);  
  prop = XInternAtom(gdk_display, "_XSETROOT_ID", FALSE);
  
  (void)XGetWindowProperty(gdk_display, gdk_root_window, prop, 0L, 1L, TRUE, AnyPropertyType,
			   &type, &format, &length, &after, &data);
  if ((type == XA_PIXMAP) && (format == 32) && (length == 1) && (after == 0))
    XKillClient(gdk_display, *((Pixmap *)data));

  XChangeProperty(gdk_display, gdk_root_window, prop, XA_PIXMAP, 32, PropModeReplace,
		  (unsigned char *) &tmpPix, 1);
  XSetCloseDownMode(gdk_display, RetainPermanent);

  if (data) 
    XFree((char *) data);  

  free_gimp_data();
}


/* Given a row then srink it down a bit */
static void
do_inttiles_preview_row(guchar *dest_row, 
	    guchar *src_rows,
	    gint width,
	    gint dh,
	    gint height,
	    gint bpp)
{
  gint x;
  gint i;
  gint px,py;

  gint xnumtiles = screen_width/sel_width + 1;
  gint ynumtiles = screen_height/sel_height + 1;

  for (x = 0; x < width; x++) 
    {
      py = (dh*ynumtiles)%height;
      
      px = (x*xnumtiles)%width; 

      for (i = 0 ; i < bpp; i++ )
	dest_row[x*tint.img_bpp+i] = 
	  src_rows[(px + (py*width))*bpp+i]; 
    }
}

/* Given a row then srink it down a bit */
static void
do_tiles_preview_row(guchar *dest_row, 
	    guchar *src_rows,
	    gint width,
	    gint dh,
	    gint height,
	    gint bpp)
{
  gint x;
  gint i;
  gdouble px,py;
  gdouble xnumtiles = (1.0*screen_width/sel_width);
  gdouble ynumtiles = (1.0*screen_height/sel_height);

  for (x = 0; x < width; x++) 
    {
      py = ((gint)(dh*ynumtiles))%height;
      
      px = ((gint)(x*xnumtiles))%width; 

      for (i = 0 ; i < bpp; i++ )
	dest_row[x*tint.img_bpp+i] = 
	  src_rows[((gint)(px + (py*width))*bpp)+i]; 
    }
}

/* Given a row then srink it down a bit */
static void
do_maxasp_preview_row(guchar *dest_row, 
	    guchar *src_rows,
	    gint width,
	    gint dh,
	    gint height,
	    gint bpp)
{
  gint x;
  gint i;
  gdouble px,py;
  gdouble xnumtiles;
  gdouble ynumtiles;

  gint new_width;
  gint new_height;

  new_height = (sel_height*screen_width)/sel_width;
  new_width = screen_width;
  if(new_height > screen_height)
  {
    new_width = (sel_width*screen_height)/sel_height;
    new_height = screen_height;
  }

  xnumtiles = 1/(1.0*new_width/screen_width);
  ynumtiles = 1/(1.0*new_height/screen_height);

  for (x = 0; x < width; x++) 
    {
      py = ((gint)(dh*ynumtiles))%height;
      
      px = ((gint)(x*xnumtiles))%width; 

      for (i = 0 ; i < bpp; i++ )
	dest_row[x*tint.img_bpp+i] = 
	  src_rows[((gint)(px + (py*width))*bpp)+i]; 
    }
}


static void
do_tiled_preview(int inttiled)
{
  int     y;
  guchar *p;
  gint check,check_0,check_1;  

  p = tint.pv_working;  

  for (y = 0; y < preview_height; y++) {
    
    if ((y / CHECK_SIZE) & 1) {
      check_0 = CHECK_DARK;
      check_1 = CHECK_LIGHT;
    } else {
      check_0 = CHECK_LIGHT;
      check_1 = CHECK_DARK;
    }

    if(inttiled == 0)
    {
      do_tiles_preview_row(tint.preview_row,
			   tint.pv_working,
			   preview_width,
			   y,
			   preview_height,
			   tint.img_bpp);
    }
    else if(inttiled == 1)
    {
      do_inttiles_preview_row(tint.preview_row,
			      tint.pv_working,
			      preview_width,
			      y,
			      preview_height,
			      tint.img_bpp);
    }
    else if(inttiled == 2)
    {
      do_maxasp_preview_row(tint.preview_row,
			    tint.pv_working,
			    preview_width,
			    y,
			    preview_height,
			    tint.img_bpp);
    }

    /*memcpy(tint.preview_row,p,sizeof(tint.preview_row));*/

    if(tint.img_bpp > 3)
      {
	int i,j;
	for (i = 0, j = 0 ; i < sizeof(tint.preview_row); i += 4, j += 3 )
	  {
	    gint alphaval;
	    if (((i/4) / CHECK_SIZE) & 1)
	      check = check_0;
	    else
	      check = check_1;
	    
	    alphaval = tint.preview_row[i + 3];
	    
	    tint.preview_row[j] = 
	      check + (((tint.preview_row[i] - check)*alphaval)/255);
	    tint.preview_row[j + 1] = 
	      check + (((tint.preview_row[i + 1] - check)*alphaval)/255);
	    tint.preview_row[j + 2] = 
	      check + (((tint.preview_row[i + 2] - check)*alphaval)/255);
	  }
      }
    
    gtk_preview_draw_row(GTK_PREVIEW(tint.preview), tint.preview_row, 0, y, preview_width);
	
    p += preview_width*tint.img_bpp;
  } 
}

static void centre_check_bounds2()
{
  if((start_col + presel_width) > preview_width)
    start_col = preview_width - presel_width;
  
  if(start_col < 0)
  {
    start_col = 0;

    if(presel_width > preview_width)
      presel_width = preview_width;
  }
  
  if((start_row + presel_height) > preview_height)
    start_row = preview_height - presel_height;
  
  if(start_row < 0)
  {
    start_row = 0;

    if(presel_height > preview_height)
      presel_height = preview_height;
  }
}


static void centre_check_bounds()
{
  if(presel_width < 0 || presel_width > preview_width)
  {
    presel_width = MAX((sel_width*preview_width)/screen_width,1);
  }

  if(presel_height < 0 || presel_height > preview_height)
  {
    presel_height = MAX((sel_height*preview_height)/screen_height,1);
  }

  centre_check_bounds2();
}

typedef enum { CTYPE_1,
	       CTYPE_2,
	       CTYPE_3,
	       CTYPE_4,
	       CTYPE_5,
	       CTYPE_6,
	       CTYPE_7,
	       CTYPE_8,
	       CTYPE_9,
	       CNOTYPE} CURTYPE;

CURTYPE lastctype = CNOTYPE;

static GdkCursor *preview_cursor1;  
static GdkCursor *preview_cursor2;  
static GdkCursor *preview_cursor3;  
static GdkCursor *preview_cursor4;  
static GdkCursor *preview_cursor5;  
static GdkCursor *preview_cursor6;  
static GdkCursor *preview_cursor7;  
static GdkCursor *preview_cursor8;  
static GdkCursor *preview_cursor9;  
 
static void setupcursors()
{
  GdkCursorType ctype1 = GDK_CROSSHAIR;
  GdkCursorType ctype2 = GDK_RIGHT_SIDE;
  GdkCursorType ctype3 = GDK_LEFT_SIDE;
  GdkCursorType ctype4 = GDK_TOP_SIDE;
  GdkCursorType ctype5 = GDK_BOTTOM_SIDE;
  GdkCursorType ctype6 = GDK_BOTTOM_LEFT_CORNER;
  GdkCursorType ctype7 = GDK_BOTTOM_RIGHT_CORNER;
  GdkCursorType ctype8 = GDK_TOP_RIGHT_CORNER;
  GdkCursorType ctype9 = GDK_TOP_LEFT_CORNER;

  if(!preview_cursor1)
    preview_cursor1 = gdk_cursor_new(ctype1);
  else
    return; /* assume all setup if first is */

  if(!preview_cursor2)
    preview_cursor2 = gdk_cursor_new(ctype2);

  if(!preview_cursor3)
    preview_cursor3 = gdk_cursor_new(ctype3);

  if(!preview_cursor4)
    preview_cursor4 = gdk_cursor_new(ctype4);

  if(!preview_cursor5)
    preview_cursor5 = gdk_cursor_new(ctype5);

  if(!preview_cursor6)
    preview_cursor6 = gdk_cursor_new(ctype6);

  if(!preview_cursor7)
    preview_cursor7 = gdk_cursor_new(ctype7);

  if(!preview_cursor8)
    preview_cursor8 = gdk_cursor_new(ctype8);

  if(!preview_cursor9)
    preview_cursor9 = gdk_cursor_new(ctype9);
}

static int inselregion(gint x, gint y)
{
  gint checkx;
  gint checky;

  checkx = start_col - CURSOR_SEL_BOUNDS;
  
  if(checkx < 0)
    checkx = 0;
  
  checky = start_row - CURSOR_SEL_BOUNDS;
  
  if(checky < 0)
    checky = 0;
  
  if((x - checkx) > 0 && 
     (x - checkx) < (presel_width +  CURSOR_SEL_BOUNDS) &&
     (y - checky) > 0 &&
     (y - checky) < (presel_height + CURSOR_SEL_BOUNDS))
    return 1;

  return 0;
}

static void updatecursor(GtkWidget *widget,gint x, gint y)
{
  /* If with 3 pixels of edge then change cursor 
   * else its a normal cursor
   */
  CURTYPE thisctype = CTYPE_1;

  GdkCursor *cursor_type = NULL;

  setupcursors(); /* set all cursors if if not already */

  if(inselregion(x,y))
  {
    if(abs(x - start_col) <= CURSOR_SEL_BOUNDS)
    {
      thisctype = CTYPE_3;
    }
    
    if (abs(x - (start_col+presel_width)) <= CURSOR_SEL_BOUNDS)
    {
      thisctype = CTYPE_2;
    }
    
    if (abs(y - start_row) <=  CURSOR_SEL_BOUNDS)
    {
      switch(thisctype)
      {
      case CTYPE_3:
	thisctype = CTYPE_9;
	break;
      case CTYPE_2:
	thisctype = CTYPE_8;
	break;
      default:
	thisctype = CTYPE_4;
	break;
      }
    }
    
    if (abs(y - (start_row+presel_height)) <=  CURSOR_SEL_BOUNDS)
    {
      switch(thisctype)
      {
      case CTYPE_3:
      thisctype = CTYPE_6;
      break;
      case CTYPE_2:
	thisctype = CTYPE_7;
	break;
      default:
	thisctype = CTYPE_5;
	break;
      }
    }
  }
  else
    thisctype = CTYPE_1;

  if(thisctype == lastctype)
    return; /* No change */

  lastctype = thisctype;

  switch(thisctype)
  {
  case CTYPE_1:
    cursor_type = preview_cursor1;
    break;
  case CTYPE_2:
    cursor_type = preview_cursor2;
    break;
  case CTYPE_3:
    cursor_type = preview_cursor3;
    break;
  case CTYPE_4:
    cursor_type = preview_cursor4;
    break;
  case CTYPE_5:
    cursor_type = preview_cursor5;
    break;
  case CTYPE_6:
    cursor_type = preview_cursor6;
    break;
  case CTYPE_7:
    cursor_type = preview_cursor7;
    break;
  case CTYPE_8:
    cursor_type = preview_cursor8;
    break;
  case CTYPE_9:
    cursor_type = preview_cursor9;
    break;
  default:
    /* ignore it */;
    return;
  }

  gdk_window_set_cursor(GTK_WIDGET(widget)->window,
			cursor_type);

}

/* 0 if not done expand else 1 if done expand */

static int doexpand(GtkWidget *widget,gint x, gint y)
{
  gint retval = 0;

  if(lastctype == CTYPE_1 || lastctype == CNOTYPE)
    return 0; 

  retval = 1;

  switch(lastctype)
  {
  case CTYPE_1:
    break;
  case CTYPE_2:
    presel_width = (x - start_col);
    break;
  case CTYPE_3:
    presel_width += (start_col - x);
    start_col = x;
    break;
  case CTYPE_4:
    presel_height += (start_row - y);
    start_row = y;
    break;
  case CTYPE_5:
    presel_height = (y - start_row);
    break;
  case CTYPE_6:
    presel_width += (start_col - x);
    presel_height = (y - start_row);
    start_col = x;
    break;
  case CTYPE_7:
    presel_height = (y - start_row);
    presel_width = (x - start_col);
    break;
  case CTYPE_8:
    presel_width = (x - start_col);
    presel_height += (start_row - y);
    start_row = y;
    break;
  case CTYPE_9:
    presel_width += (start_col - x);
    start_col = x;
    presel_height += (start_row - y);
    start_row = y;
    break;
  default:
    retval = 0;
    /* ignore it */;
  }


  presel_width = MAX(presel_width,1);
  presel_height = MAX(presel_height,1);

  return retval;
}

void
color_selection_cancel (GtkWidget               *w,
                    GtkColorSelectionDialog *cs)
{
  bg_red = last_bg_red;
  bg_green = last_bg_green;
  bg_blue = last_bg_blue;
  dialog_update_preview();
  gtk_widget_destroy(GTK_WIDGET(cs));
}

void
color_selection_ok (GtkWidget               *w,
                    GtkColorSelectionDialog *cs)
{
  GtkColorSelection *colorsel;
  gdouble color[4];

  colorsel=GTK_COLOR_SELECTION(cs->colorsel);

  gtk_color_selection_get_color(colorsel,color);
  gtk_color_selection_set_color(colorsel,color);
  gtk_widget_destroy(GTK_WIDGET(cs));
  bg_red = (guchar)(255*color[0]);
  bg_green = (guchar)(255*color[1]);
  bg_blue = (guchar)(255*color[2]);
  dialog_update_preview();
}

void
color_selection_changed (GtkWidget *w,
                         GtkColorSelectionDialog *cs)
{
  GtkColorSelection *colorsel;
  gdouble color[4];

  colorsel=GTK_COLOR_SELECTION(cs->colorsel);
  gtk_color_selection_get_color(colorsel,color);
  bg_red = (guchar)(255*color[0]);
  bg_green = (guchar)(255*color[1]);
  bg_blue = (guchar)(255*color[2]);
  dialog_update_preview();
}

static void selcolour()
{
  gdouble color[4];
  
  if (!colsel_window)
  {
    colsel_window = gtk_color_selection_dialog_new ("color selection dialog");
    
    gtk_color_selection_set_opacity (
      GTK_COLOR_SELECTION (GTK_COLOR_SELECTION_DIALOG (colsel_window)->colorsel),
				     FALSE);
    
    gtk_color_selection_set_update_policy(
        GTK_COLOR_SELECTION (GTK_COLOR_SELECTION_DIALOG (colsel_window)->colorsel),
	GTK_UPDATE_CONTINUOUS);

    color[0] = ((gdouble)bg_red)/255;
    color[1] = ((gdouble)bg_green)/255;
    color[2] = ((gdouble)bg_blue)/255;
    

    last_bg_red = bg_red;
    last_bg_green = bg_green;
    last_bg_blue = bg_blue;

    gtk_color_selection_set_color(
      GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG (colsel_window)->colorsel),
      color);

    gtk_window_position (GTK_WINDOW (colsel_window), GTK_WIN_POS_MOUSE);

    gtk_signal_connect (GTK_OBJECT (colsel_window), "destroy",
			GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			&colsel_window);

    gtk_signal_connect (
	GTK_OBJECT (GTK_COLOR_SELECTION_DIALOG (colsel_window)->colorsel),
	"color_changed",
	GTK_SIGNAL_FUNC(color_selection_changed),
	colsel_window);

    gtk_signal_connect (
	GTK_OBJECT (GTK_COLOR_SELECTION_DIALOG (colsel_window)->ok_button),
	"clicked",
	GTK_SIGNAL_FUNC(color_selection_ok),
	colsel_window);
    
    gtk_signal_connect_object (
        GTK_OBJECT (GTK_COLOR_SELECTION_DIALOG (colsel_window)->cancel_button),
	"clicked",
	GTK_SIGNAL_FUNC(color_selection_cancel),
	GTK_OBJECT (colsel_window));


    gtk_widget_show (colsel_window);
  }
}

/* preview stuff */
static gint
onroot_preview_events ( GtkWidget *widget,
			     GdkEvent *event )
{
  static int have_start = 0;
  static GdkPoint point;
  gint x,y;
  GdkEventKey *kevent;

  if(rootwinvals.wintype != CENTRE)
    return FALSE;

  gtk_widget_get_pointer(widget, &x, &y);

  switch (event->type)
    {
    case GDK_BUTTON_PRESS:
      if(!inselregion(x,y))
      {
	selcolour();
	break;
      }
	
      point.x = x;
      point.y = y;
      have_start = 1;

      break;
    case GDK_BUTTON_RELEASE:
      have_start = 0;
      break;

    case GDK_MOTION_NOTIFY:

      if(x < 0)
	x = 0;
      else if(x > preview_width)
	x = preview_width;
      
      if(y < 0)
	y = 0;
      else if(y > preview_height)
	y = preview_height;

      if(!have_start)
      {
	updatecursor(widget,x,y);
	break;
      }

      if(doexpand(widget,x,y) == 0)
      {
	start_col -= (point.x - x);
	start_row -= (point.y - y);
	point.x = x;
	point.y = y;
      }

      centre_check_bounds();

      do_centre_preview();
      gtk_widget_draw(widget, NULL);
      rootwinvals.xpos = (start_col * screen_width)/preview_width;
      rootwinvals.ypos = (start_row * screen_height)/preview_height;
      break;
    case GDK_KEY_PRESS:
    case GDK_KEY_RELEASE:
      kevent = (GdkEventKey *) event;
      switch (kevent->keyval)
      {     
      case GDK_plus:
      case GDK_KP_Add:
      case GDK_equal:
	mag_image(widget,1);
	break;
      case GDK_minus:
      case GDK_KP_Subtract:
      case GDK_bar:
	mag_image(widget,0);
	break;
      default:
	break;
      }
      break;
    default:
      break;
    }
  return FALSE;
}


/* Given a row then srink it down a bit */
static void
do_centre_preview_row(guchar *dest_row, 
	    guchar *src_rows,
	    gint width,
	    gint dh,
	    gint height,
	    gint start_col,
	    gint bpp)
{
  gint x;
  gint i;
  gint px,py;

  gdouble real_presel_width = (1.0*sel_width*preview_width)/screen_width;
  gdouble real_presel_height = (1.0*sel_height*preview_height)/screen_height;

  gdouble mod_width = (presel_width/(1.0*real_presel_width))*sel_width;
  gdouble mod_height = (presel_height/(1.0*real_presel_height))*sel_height;
  gdouble xnumtiles = (1.0*screen_width/(mod_width));
  gdouble ynumtiles = (1.0*screen_height/(mod_height));

  for (x = start_col; x < start_col+width; x++) 
    {
      py = ((gint)(dh*ynumtiles))%preview_height;
      
      px = ((gint)((x-start_col)*xnumtiles))%preview_width; 

      for (i = 0 ; i < bpp; i++ )
	dest_row[x*tint.img_bpp+i] = 
	  src_rows[(px + (py*preview_width))*bpp+i]; 
    }
}

static void
do_centre_init()
{
  /* Calc row where image appears */
  presel_width = MAX((sel_width*preview_width)/screen_width,1);
  presel_height = MAX((sel_height*preview_height)/screen_height,1);

  start_col = (preview_width - presel_width)/2;
  start_row = (preview_height - presel_height)/2;

  rootwinvals.xpos = (start_col * screen_width)/preview_width;
  rootwinvals.ypos = (start_row * screen_height)/preview_height;
  magfactor = 1.0;
  gtk_widget_grab_focus (GTK_WIDGET(tint.preview));
}

static void 
mag_image(GtkWidget *widget,gint enlarge)
{
  gint last_presel_width;
  gint last_presel_height;
  gdouble last_magfactor = magfactor;

  /*
    if(last_enlarge != enlarge)
    magfactor = 1.0;
    
    last_enlarge = enlarge;
    */

    
  if(enlarge)
  {
    magfactor = 1.1;
    if(magfactor > 8.0)
    {
      magfactor = 8.0;
      return;
    }
  }
  else
  {
    magfactor = 0.9;
    if(magfactor < 0.1)
    {
      magfactor = 0.1;
      return;
    }
  }

  last_presel_width = presel_width;
  last_presel_height = presel_height;

  /*
    presel_width = magfactor*MAX((sel_width*preview_width)/screen_width,1);
    presel_height = magfactor*MAX((sel_height*preview_height)/screen_height,1);
    */

  presel_width *= magfactor;
  presel_height *= magfactor;

  if(presel_width < 10 ||
     presel_height < 10)
  {
    presel_width = last_presel_width;
    presel_height = last_presel_height;
  }

  start_col += (last_presel_width - presel_width)/2;
  start_row += (last_presel_height - presel_height)/2;

  centre_check_bounds2();

  if(last_presel_width == presel_width &&
     last_presel_height == presel_height)
  {
    /* Abort this call */
    if((enlarge && magfactor <= last_magfactor) ||
       (!enlarge && magfactor >= last_magfactor))
       magfactor = last_magfactor;
    return;
  }

  do_centre_preview();
  gtk_widget_draw(widget, NULL);
  rootwinvals.xpos = (start_col * screen_width)/preview_width;
  rootwinvals.ypos = (start_row * screen_height)/preview_height;
}

static void
clear_preview_line()
{
  gint loop;

  for(loop = 0; loop < (sizeof(tint.preview_row)/4)*3; loop +=3)
  {
    tint.preview_row[loop] = bg_red;
    tint.preview_row[loop + 1] = bg_green;
    tint.preview_row[loop + 2] = bg_blue;
    /*tint.preview_row[loop + 3] = 0;*/
  }
}


static void 
do_centre_preview()
{
  gint      y;
  guchar *p;
  gint check,check_0,check_1;  
  int i,j;
  
  p = tint.pv_working;  

  /* Clear out preview to BG colour? */
  for (y = 0; y < preview_height; y++) {
    clear_preview_line();
    gtk_preview_draw_row(GTK_PREVIEW(tint.preview), tint.preview_row, 0, y, preview_width);
  }

  for (y = start_row; y < start_row+presel_height; y++) {
    
    if ((y / CHECK_SIZE) & 1) {
      check_0 = CHECK_DARK;
      check_1 = CHECK_LIGHT;
    } else {
      check_0 = CHECK_LIGHT;
      check_1 = CHECK_DARK;
    }

    /* Clear out again !! */
    clear_preview_line();
    /*memset(tint.preview_row,0,sizeof(tint.preview_row));*/
    do_centre_preview_row(tint.preview_row,
			  tint.pv_working,
			  presel_width,
			  y-start_row,
			  presel_height,
			  start_col,
			  tint.img_bpp);

    if(tint.img_bpp > 3)
      {
	for (i = 4*start_col, j = 3*start_col ; i < 4*(start_col+presel_width); i += 4, j += 3 )
	  {
	    gint alphaval;
	    if (((i/4) / CHECK_SIZE) & 1)
	      check = check_0;
	    else
	      check = check_1;
	    
	    alphaval = tint.preview_row[i + 3];
	    
	    tint.preview_row[j] = 
	      check + (((tint.preview_row[i] - check)*alphaval)/255);
	    tint.preview_row[j + 1] = 
	      check + (((tint.preview_row[i + 1] - check)*alphaval)/255);
	    tint.preview_row[j + 2] = 
	      check + (((tint.preview_row[i + 2] - check)*alphaval)/255);
	  }
	/* Clear out alpha stuff */
	memset(tint.preview_row+j,0,sizeof(tint.preview_row)-j);
      }
    
    gtk_preview_draw_row(GTK_PREVIEW(tint.preview), tint.preview_row, 0, y, preview_width);
	
    p += preview_width*tint.img_bpp;
  } 
}


static void 
do_maxsize_preview()
{
  int     y;
  guchar *p;
  gint check,check_0,check_1;  

  p = tint.pv_working;  

  for (y = 0; y < preview_height; y++) {
    
    if ((y / CHECK_SIZE) & 1) {
      check_0 = CHECK_DARK;
      check_1 = CHECK_LIGHT;
    } else {
      check_0 = CHECK_LIGHT;
      check_1 = CHECK_DARK;
    }

    memcpy(tint.preview_row,p,sizeof(tint.preview_row));

    if(tint.img_bpp > 3)
      {
	int i,j;
	for (i = 0, j = 0 ; i < sizeof(tint.preview_row); i += 4, j += 3 )
	  {
	    gint alphaval;
	    if (((i/4) / CHECK_SIZE) & 1)
	      check = check_0;
	    else
	      check = check_1;
	    
	    alphaval = tint.preview_row[i + 3];
	    
	    tint.preview_row[j] = 
	      check + (((tint.preview_row[i] - check)*alphaval)/255);
	    tint.preview_row[j + 1] = 
	      check + (((tint.preview_row[i + 1] - check)*alphaval)/255);
	    tint.preview_row[j + 2] = 
	      check + (((tint.preview_row[i + 2] - check)*alphaval)/255);
	  }
      }
    
    gtk_preview_draw_row(GTK_PREVIEW(tint.preview), tint.preview_row, 0, y, preview_width);
	
    p += preview_width*tint.img_bpp;
  } 
}

static void
dialog_update_preview(void)
{
  memcpy(tint.pv_working,tint.pv_cache,tint.pv_imgsz);

  switch(rootwinvals.wintype)
  {
  case FULLSIZE:
    do_maxsize_preview();
    break;
  case MAXASP:
    do_tiled_preview(2);
    break;
  case INTTILE:
    do_tiled_preview(1);
    break;
  case CENTRE:
    do_centre_preview();
    break;
  case TILED:
    do_tiled_preview(0);
    break;
  default:
  }
  
  gtk_widget_draw(tint.preview, NULL);
  gdk_flush();
}








