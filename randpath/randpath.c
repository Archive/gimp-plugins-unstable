/*  randpath.c
 *  by Heinz Sollich <heinz@ventoux.in-berlin.de>
 *  04/1998
 *
 *  draws randompath figures
 *
 */

/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "libgimp/gimp.h"
#include "gtk/gtk.h"

/* Declare local functions. */
static void query(void);
static void run(char *name,
                int nparams,
                GParam * param,
                int *nreturn_vals,
                GParam ** return_vals);
static gint dialog();

static void doit(GDrawable * drawable);

GPlugInInfo PLUG_IN_INFO =
{
  NULL,                         /* init_proc */
  NULL,                         /* quit_proc */
  query,                        /* query_proc */
  run,                          /* run_proc */
};

gint bytes;
gint width, height;
gint sx1, sy1, sx2, sy2 , sw, sh;
int run_flag = 0;

typedef struct {
  gint steps;
  gint repeats;
  gint walk;
  gint color;
} config;

config my_config =
{
  1,                              /* steps x 1000 */
  20,                             /* repeats */
  0,                              /* walk */
  2,                              /* color */
};


MAIN()

static void query()
{
  static GParamDef args[] =
  {
    {PARAM_INT32, "run_mode", "Interactive, non-interactive"},
    {PARAM_IMAGE, "image", "Input image (unused)"},
    {PARAM_DRAWABLE, "drawable", "Input drawable"},
    {PARAM_INT32, "steps", "Steps"},
    {PARAM_INT32, "repeats", "Repeats"},
    {PARAM_INT32, "walk", "walk"},
    {PARAM_INT32, "color", "Color"},
  };
  static GParamDef *return_vals = NULL;
  static int nargs = sizeof(args) / sizeof(args[0]);
  static int nreturn_vals = 0;

  gimp_install_procedure("plug_in_randompath",
			 "paints random path figures in different ways",
			 "retains tileability",
			 "Heinz Sollich <heinz@ventoux.in-berlin.de>",
			 "Heinz Sollich",
			 "1998",
			 "<Image>/Filters/Render/RandomPath",
			 "RGB*",
			 PROC_PLUG_IN,
			 nargs, nreturn_vals,
			 args, return_vals);
} /* query() */

static void run(char *name, int n_params, GParam * param, int *nreturn_vals,
                GParam ** return_vals)
{
  static GParam values[1];
  GDrawable *drawable;
  GRunModeType run_mode;
  GStatusType status = STATUS_SUCCESS;

  *nreturn_vals = 1;
  *return_vals = values;

  run_mode = param[0].data.d_int32;

  if (run_mode == RUN_NONINTERACTIVE) {
    if (n_params != 7) {
      status = STATUS_CALLING_ERROR;
    } else {
      my_config.steps   = param[3].data.d_int32;
      my_config.repeats = param[4].data.d_int32;
      my_config.walk    = param[5].data.d_int32;
      my_config.color   = param[6].data.d_int32;
    }
  } else {
    /*  Possibly retrieve data  */
    gimp_get_data("plug_in_myrandpath", &my_config);

    if (run_mode == RUN_INTERACTIVE) {
      if (!dialog()) {
        /* The dialog was closed, or something similarly evil happened. */
        status = STATUS_EXECUTION_ERROR;
      }
    }
  }

  if (status == STATUS_SUCCESS) {
    /*  Get the specified drawable  */
    drawable = gimp_drawable_get(param[2].data.d_drawable);

    /*  Make sure that the drawable is RGB color  */
    if ( gimp_drawable_color(drawable->id) ) {
      gimp_progress_init("Drawing RandomPath...");

      gimp_tile_cache_ntiles(2 * (drawable->width / gimp_tile_width() + 1));

      srand(time(NULL));
      doit(drawable);

      if (run_mode != RUN_NONINTERACTIVE)
              gimp_displays_flush();

      if (run_mode == RUN_INTERACTIVE)
        gimp_set_data("plug_in_myrandpath", &my_config, sizeof(my_config));
    } else { 
      status = STATUS_EXECUTION_ERROR;
    }
    gimp_drawable_detach(drawable);
  }

  values[0].type = PARAM_STATUS;
  values[0].data.d_status = status;
} /* run() */

static void get_colors(GDrawable *drawable, guchar *fg, int u) 
{
  GParam *return_vals;
  gint nreturn_vals;
  char *command[2] = { "gimp_palette_get_foreground" ,
		       "gimp_palette_get_background" };

  switch ( gimp_drawable_type(drawable->id) )
    {
    case RGBA_IMAGE:   /* ASSUMPTION: Assuming the user wants entire */
      fg[3] = 255;                 /* area to be fully opaque.       */
    case RGB_IMAGE:

      return_vals = gimp_run_procedure(command[u],
				       &nreturn_vals,
				       PARAM_END);

      if (return_vals[0].data.d_status == STATUS_SUCCESS)
	{
	  fg[0] = return_vals[1].data.d_color.red;
	  fg[1] = return_vals[1].data.d_color.green;
	  fg[2] = return_vals[1].data.d_color.blue;
	}
      else
	{
	  fg[0] = u * 255;
	  fg[1] = u * 255;
	  fg[2] = u * 255;
	}
      break;
    case GRAYA_IMAGE:
    case GRAY_IMAGE:       
    case INDEXEDA_IMAGE:
    case INDEXED_IMAGE:
    default:
      break;
    }
} /* get_colors */

static void doit(GDrawable * drawable)
{
  GPixelRgn srcPR, destPR;
  int x, y, dir, /*ndir,*/ i, reps=0;
  long steps, isteps;
  int delta_x[11] = {  0,  1,  1,  1,  0, -1, -1, -1,  0,  1,  1 };
  int delta_y[11] = { -1, -1,  0,  1,  1,  1,  0, -1, -1, -1,  0 };
  guchar *tmp;
  guchar fg[4];
  int walk = my_config.walk + 8;

  /* color values */
  bytes = drawable->bpp;
  switch (my_config.color) {
  case 1:
  case 0:
    get_colors(drawable, fg, my_config.color);
  default:
    break;
  }
  fg[3] = 255;

  /* input area */
  width = drawable->width;
  height = drawable->height;
  gimp_drawable_mask_bounds(drawable->id, &sx1, &sy1, &sx2, &sy2);
  sw = sx2 - sx1; sh = sy2 - sy1;

  /*  initialize the pixel regions  */
  gimp_pixel_rgn_init(&srcPR, drawable, 0, 0, width, height, FALSE, FALSE);
  gimp_pixel_rgn_init(&destPR, drawable, 0, 0, width, height, TRUE, TRUE);

  /* temp buffer */
  tmp = (guchar *) malloc(sw * sh * bytes);
  if (tmp == NULL) {
    return;
  }
  gimp_pixel_rgn_get_rect(&srcPR, tmp, sx1, sy1, sw, sh);

  /* number of steps */
  steps = my_config.steps * 1000;

 loop:
  if (my_config.color > 1)
    for (i=0; i<3; i++)
      fg[i] = 255.0 * rand() / (RAND_MAX + 1.0);
  /* startpoint */
  x = sx1 + (double) sw * rand() / (RAND_MAX + 1.0);
  y = sy1 + (double) sh * rand() / (RAND_MAX + 1.0);
  dir = (float) walk * rand() / (RAND_MAX + 1.0);
  for (isteps=0; isteps<steps; isteps++) {
    /*    while ( (ndir = 10.0 * rand() / (RAND_MAX + 1.0)) == ((dir + 5) % 10) ) ;*/
    dir = (float) walk * rand() / (RAND_MAX + 1.0) /*ndir*/;
    x += delta_x[dir];
    x = x < 0? x + width : x;
    x = x >= width? x - width : x;
    y += delta_y[dir];
    y = y < 0? y + height : y;
    y = y >= height? y - height : y;
    /* (x,y) in selected area ? */
    if ( (x >= sx1) && (x < sx2) && (y >= sy1) && (y < sy2) ) {
      for (i=0; i<bytes; i++) {
	tmp[(x - sx1 + (y - sy1) * sw) * bytes + i] = fg[i];
      }
    }
  }
  gimp_progress_update((double) reps / (double) my_config.repeats);
  if (++reps < my_config.repeats)
    goto loop;

  gimp_pixel_rgn_set_rect(&destPR, tmp, sx1, sy1, sw, sh);
  free(tmp);

  /* update the altered region */
  gimp_drawable_flush(drawable);
  gimp_drawable_merge_shadow(drawable->id, TRUE);
  gimp_drawable_update(drawable->id, sx1, sy1, sw, sh);
} /* doit */

/***************************************************
 * GUI stuff
 */

static void close_callback(GtkWidget * widget, gpointer data)
{
  gtk_main_quit();
}

static void ok_callback(GtkWidget *widget, gpointer data)
{
  run_flag = 1;
  gtk_widget_destroy(GTK_WIDGET(data));
}

static void scale_callback(GtkAdjustment *adjustment, gpointer data)
{
  gint *dptr = (gint*) data;
  *dptr = (gint) adjustment->value;
}

static void toggle_walk_callback(GtkWidget *widget, gint32 value)
{
  if (GTK_TOGGLE_BUTTON (widget)->active)
    my_config.walk = value;
}

static void toggle_color_callback(GtkWidget *widget, gint32 value)
{
  if (GTK_TOGGLE_BUTTON (widget)->active)
    my_config.color = value;
}

static gint dialog()
{
  GtkWidget *dlg;
  GtkWidget *abox, *bbox, *cbox;
  GtkWidget *aframe, *bframe, *cframe;
  GtkWidget *button, *label, *scale;
  GtkObject *adjustment;
  gchar **argv;
  gint argc;

  argc = 1;
  argv = g_new(gchar *, 1);
  argv[0] = g_strdup("myrandpath");

  gtk_init(&argc, &argv);
  gtk_rc_parse (gimp_gtkrc ());

  dlg = gtk_dialog_new();
  gtk_window_set_title(GTK_WINDOW(dlg), "Random Path");
  gtk_window_position(GTK_WINDOW(dlg), GTK_WIN_POS_CENTER /*NONE,MOUSE*/);
  gtk_signal_connect(GTK_OBJECT(dlg), "destroy",
                     (GtkSignalFunc) close_callback, NULL);

  /*  Action area  */
  button = gtk_button_new_with_label("OK");
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     (GtkSignalFunc) ok_callback,
                     dlg);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dlg)->action_area),
		     button, TRUE, TRUE, 0);
  gtk_widget_grab_default(button);
  gtk_widget_show(button);

  button = gtk_button_new_with_label("Cancel");
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
                            (GtkSignalFunc) gtk_widget_destroy,
                            GTK_OBJECT(dlg));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dlg)->action_area),
		     button, TRUE, TRUE, 0);
  gtk_widget_show(button);

  /* Parameter settings */
  aframe = gtk_frame_new("Plug-In Options");
  gtk_frame_set_shadow_type(GTK_FRAME (aframe), GTK_SHADOW_ETCHED_OUT);
  gtk_container_border_width(GTK_CONTAINER (aframe), 5);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG (dlg)->vbox),
		     aframe, FALSE, FALSE, 0);

  abox = gtk_vbox_new(FALSE, 5); 
  gtk_container_border_width(GTK_CONTAINER (abox), 5);
  gtk_container_add(GTK_CONTAINER (aframe), abox);

  /* steps & repeats */
  bframe = gtk_frame_new("Walking Parameters");
  gtk_frame_set_shadow_type(GTK_FRAME (bframe), GTK_SHADOW_ETCHED_IN);
  gtk_box_pack_start(GTK_BOX (abox),
		     bframe, FALSE, FALSE, 0);
  
  bbox = gtk_vbox_new(FALSE, 5);
  gtk_container_border_width(GTK_CONTAINER (bbox), 5);
  gtk_container_add(GTK_CONTAINER (bframe), bbox);

  /* scale for steps */
  label=gtk_label_new("Steps (x1000):");
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_box_pack_start(GTK_BOX(bbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);

  adjustment = gtk_adjustment_new (my_config.steps, 1, 101, 1, 1, 1);
  gtk_signal_connect (adjustment, "value_changed",
		     (GtkSignalFunc) scale_callback,
		     &(my_config.steps));
  scale= gtk_hscale_new(GTK_ADJUSTMENT (adjustment));
  gtk_widget_set_usize(GTK_WIDGET (scale), 150, 30);
  gtk_range_set_update_policy(GTK_RANGE (scale), GTK_UPDATE_DELAYED);
  gtk_scale_set_digits(GTK_SCALE (scale), 0);
  gtk_scale_set_draw_value(GTK_SCALE (scale), TRUE);
  gtk_box_pack_start(GTK_BOX (bbox), scale, FALSE, FALSE,0);
  gtk_widget_show(scale );

  /* scale for repeats */
  label=gtk_label_new("Repeats:");
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_box_pack_start(GTK_BOX(bbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);

  adjustment = gtk_adjustment_new(my_config.repeats, 1, 51, 1, 1, 1);
  gtk_signal_connect(adjustment, "value_changed",
		     (GtkSignalFunc) scale_callback,
		     &(my_config.repeats));
  scale= gtk_hscale_new(GTK_ADJUSTMENT (adjustment));
  gtk_widget_set_usize(GTK_WIDGET (scale), 150, 30);
  gtk_range_set_update_policy(GTK_RANGE (scale), GTK_UPDATE_DELAYED);
  gtk_scale_set_digits(GTK_SCALE (scale), 0);
  gtk_scale_set_draw_value (GTK_SCALE (scale), TRUE);
  gtk_box_pack_start(GTK_BOX (bbox), scale, FALSE, FALSE,0);
  gtk_widget_show(scale );

  /* radiobuttons for walk */
  label=gtk_label_new("Preferences:");
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_box_pack_start(GTK_BOX(bbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  {
    int   i;
    char * name[4]= {"null", "S-N", "SSW-NNE", "SW-NE"};

    button = NULL;
    for (i=0; i<4; i++)
      {
	button = gtk_radio_button_new_with_label (
           (button==NULL) ? NULL :
	      gtk_radio_button_group (GTK_RADIO_BUTTON (button)), name[i]);
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button), 
				    (my_config.walk==i));

	gtk_signal_connect (GTK_OBJECT (button), "toggled",
			    (GtkSignalFunc) toggle_walk_callback,
			    (gpointer) i);

	gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
      }
  }
  gtk_widget_show(bbox);
  gtk_widget_show(bframe);

  cframe = gtk_frame_new ("Which Color?");
  gtk_frame_set_shadow_type (GTK_FRAME (cframe), GTK_SHADOW_ETCHED_IN);
  gtk_box_pack_start (GTK_BOX (abox),
		     cframe, FALSE, FALSE, 0);

  cbox= gtk_vbox_new (FALSE, 5);
  gtk_container_border_width (GTK_CONTAINER (cbox), 5);
  gtk_container_add (GTK_CONTAINER (cframe), cbox);
  
  /* radio buttons for color */
  {
    int   i;
    char * name[3]= {"Foreground", "Background", "Random"};

    button = NULL;
    for (i=0; i<3; i++)
      {
	button = gtk_radio_button_new_with_label (
           (button==NULL) ? NULL :
	      gtk_radio_button_group (GTK_RADIO_BUTTON (button)), name[i]);
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button), 
				    (my_config.color==i));

	gtk_signal_connect (GTK_OBJECT (button), "toggled",
			    (GtkSignalFunc) toggle_color_callback,
			    (gpointer) i);

	gtk_box_pack_start (GTK_BOX (cbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
      }
  }
  gtk_widget_show(cbox);
  gtk_widget_show(cframe);

  gtk_widget_show(abox);
  gtk_widget_show(aframe);

  gtk_widget_show(dlg);

  gtk_main();
  gdk_flush();

  return run_flag;
}
