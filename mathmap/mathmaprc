{
  Distorts = {
    wave = "origValXY(x+sin(y*10+t*360)*3,y+sin(x*10+t*360)*3)";
    square = "origValXY(sign(x)*x^2/50,sign(y)*y^2/50)";
    slice = "q=t*360;\norigValXY(x+5*sign(cos(9*y+q)),y+5*sign(cos(9*x+q)))";
    mercator = "origValXY(x*cos(90/Y*y+t*360),y)";
    pond = "origValRA(r+sin(r*20+t*360)*3,a)";
    "enhanced pond" = "origValRA(r+(sin(500000/(r+100)+t*360)*7),a)";
    "sea 1" = "origValXY(x+10*sin(t*360+2000*Y*(y+Y+60)^-1),y)";
    "sea 2" = "origValXY(x,y+5*sin(t*360+2000*Y*(y+Y+60)^-1))";
    "sea 3" = "s=sin(t*360+2000*Y*(y+Y+60)^-1);\norigValXY(x+10*s,y+5*s)";
    "twirl 90" = "origValRA(r,a+(r/R-1)*(t-0.5)*360)";
    sphere = "p=r/(X*2);\norigValRA(r*(1-inintv(p,-0.5,0.5))+X/90*asin(inintv(p,-0.5,0.5)*r/X),a)";
    jitter = "origValRA(r,a+(a+t*8)%8-4)";
    "circular slice" = "origValRA(r,a+(r+t*6)%6-3)";
    fisheye = "origValRA(r^(2-t)/R^(1-t),a)";
    "center shake" = "origValXY(x+max(0,cos(x*2))*5*max(0,cos(y*2))*cos(y*10),y+max(0,cos(x*2))*5*max(0,cos(y*2))*cos(x*10))";
  };
  Blur = {
    mosaic = "origValXY(x-x%5,y-y%5)";
    "radial mosaic" = "origValRA(r-r%5,a+2.5-a%5)";
  };
  Render = {
    spiral = "p=origValXY(x,y);\nq=sin((r-a*0.1)*10+t*360)*0.5+0.5;\nrgbColor(q*red(p),q*green(p),q*blue(p))";
    "alpha spiral" = "p=origValXY(x,y);\nrgbaColor(red(p),green(p),blue(p),sin((r-a*0.1)*10+t*360)*0.5+0.5)";
    darts = "p=origValXY(x,y);\np=if inintv((a-9)%36,0,18)\n    then\n        p\n    else\n        rgbColor(1-red(p),1-green(p),1-blue(p))\n    end;\nif inintv(r%80,68,80)\nthen\n    p\nelse\n    rgbColor(1-red(p),1-green(p),1-blue(p))\nend";
    "sine wave" = "grayColor(sin(r*10+t*360)*0.5+0.5)";
    grid = "grayColor(if (x%20)*(y%20) then 1 else 0 end)";
    moire1 = "q=t*360;\nrgbColor(abs(sin(15*r+q)+sin(15*a+q))*0.5,abs(sin(17*r+q)+sin(17*a+q))*0.5,abs(sin(19*r+q)+sin(19*a+q))*0.5)";
    moire2 = "grayColor(sin(x*y+t*360)*0.5+0.5)";
    mandelbrot = "tx=1.5*x/X-0.5;\nty=1.5*y/X-0;\niter=0;\nxr=0;\nxi=0;\nxrsq=0;\nxisq=0;\nwhile xrsq+xisq<4 && iter<31\ndo\n    xrsq=xr*xr;\n    xisq=xi*xi;\n    xi=2*xr*xi+ty;\n    xr=xrsq-xisq+tx;\n    iter=iter+1\nend;\ngradient(iter/32)";
  };
  Colors = {
    desaturate = "p=origValXY(x,y);\ngrayaColor(gray(p),alpha(p))";
    "gamma correction" = "p=origValXY(x,y);rgbaColor(curve(red(p)),curve(green(p)),curve(blue(p)),alpha(p))";
    colorify = "gradient((gray(origValXY(x,y))+t)%1)";
  };
  Noise = {scatter = "origValXY(x+rand(-30,30)*t,y+rand(-30,30)*t)";};
  Map = {tile = "origValXY((x+X)*2%W-X,(y+Y)*2%H-Y)";};
}
