#ifndef __SCANNER_H__
#define __SCANNER_H__

void scanFromString (char *string);
void endScanningFromString (void);

#endif
