#ifndef LIGHTINGPIXMAPSH
#define LIGHTINGPIXMAPSH

#include "amb1.xpm"
#include "amb2.xpm"
#include "diffint1.xpm"
#include "diffint2.xpm"
#include "diffref1.xpm"
#include "diffref2.xpm"
#include "specref1.xpm"
#include "specref2.xpm"
#include "high1.xpm"
#include "high2.xpm"

extern char *amb1_xpm[];
extern char *amb2_xpm[];
extern char *diffint1_xpm[];
extern char *diffint2_xpm[];
extern char *diffref1_xpm[];
extern char *diffref2_xpm[];
extern char *specref1_xpm[];
extern char *specref2_xpm[];
extern char *high1_xpm[];
extern char *high2_xpm[];

#endif
