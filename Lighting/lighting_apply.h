#ifndef LIGHTINGAPPLYH
#define LIGHTINGAPPLYH

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <libgimp/gimp.h>
#include <gck/gck.h>

#include "lighting_main.h"
#include "lighting_image.h"

extern void init_compute  (void);
extern void compute_image (void);

#endif
