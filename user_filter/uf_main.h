/*
 * Written 1997 Jens Ch. Restemeier <jchrr@hrz.uni-bielefeld.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifndef _UF_MAIN_H
#define _UF_MAIN_H

#define PLUG_IN_VERSION "0.4"
#define PREV_MAX 128

typedef struct {
	s_filter_data data;
	char last_filename[255];
} s_plug_in_data;
extern s_plug_in_data plug_in_data;

extern int sel_x1,sel_y1,sel_x2,sel_y2;
extern int img_height, img_width, img_bpp, img_has_alpha;

extern unsigned char *prev_data;
extern int prev_x1, prev_y1, prev_x2, prev_y2, prev_size, prev_scale;

extern int tile_width, tile_height;
extern int read_tile_col, read_tile_row;
extern GTile *read_tile;

extern GDrawable *drawable;

int dialog_create();
void load_file(char *s);
void save_file(char *s);

void apply_to_drawable();
void apply_to_preview(GtkWidget *preview);
void build_preview();

#endif