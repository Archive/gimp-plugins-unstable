#include <gtk/gtk.h>
#include "gimp_warn_dialog.h"

/*
 * Warn-dialog from Fractal-explorer.
 * What about putting it into GCK or Megawidget ?
 */

/**********************************************************************
 FUNCTION: ok_warn_window
 *********************************************************************/

/* From testgtk */
static void
ok_warn_window(GtkWidget * widget,
	       gpointer data)
{
    gtk_widget_destroy(GTK_WIDGET(data));
}

/**********************************************************************
 FUNCTION: create_warn_dialog
 *********************************************************************/

void 
create_warn_dialog(gchar * msg)
{
    GtkWidget          *window = NULL;
    GtkWidget          *label;
    GtkWidget          *button;

    window = gtk_dialog_new();

    gtk_window_set_title(GTK_WINDOW(window), "Warning");
    gtk_container_border_width(GTK_CONTAINER(window), 0);

    button = gtk_button_new_with_label("OK");
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
		       (GtkSignalFunc) ok_warn_window,
		       window);

    GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->action_area), button, TRUE, TRUE, 0);
    gtk_widget_grab_default(button);
    gtk_widget_show(button);
    label = gtk_label_new(msg);
    gtk_misc_set_padding(GTK_MISC(label), 10, 10);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->vbox), label, TRUE, TRUE, 0);
    gtk_widget_show(label);
    gtk_widget_show(window);
    gtk_main();
    gdk_flush();
}
