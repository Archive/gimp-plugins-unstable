/*
 * Written 1997 Jens Ch. Restemeier <jchrr@hrz.uni-bielefeld.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include "uf_parser.h"
#include "uf_parser.tab.h"
#include "uf_tool.h"
#include "uf_file.h"

#define ATC(x) uf_to_c(f, a->nodes[x])

void uf_to_c(FILE *f, s_uf_tree *a)
{
	if (a==NULL) return;
	switch (a->op_type) {
		case v_int:	fprintf(f, "%i", a->value); break;
		case '+':	fprintf(f, "("); ATC(0); fprintf(f, "+"); ATC(1); fprintf(f, ")"); break;
		case '-':	fprintf(f, "("); ATC(0); fprintf(f, "-"); ATC(1); fprintf(f, ")"); break;
		case '*':	fprintf(f, "("); ATC(0); fprintf(f, "*"); ATC(1); fprintf(f, ")"); break;
		case t_and:	fprintf(f, "("); ATC(0); fprintf(f, "&&"); ATC(1); fprintf(f, ")"); break;
		case t_or:	fprintf(f, "("); ATC(0); fprintf(f, "||"); ATC(1); fprintf(f, ")"); break;
		case t_eq:	fprintf(f, "("); ATC(0); fprintf(f, "=="); ATC(1); fprintf(f, ")"); break;
		case t_neq:	fprintf(f, "("); ATC(0); fprintf(f, "!="); ATC(1); fprintf(f, ")"); break;
		case '<':	fprintf(f, "("); ATC(0); fprintf(f, "<"); ATC(1); fprintf(f, ")"); break;
		case t_le:	fprintf(f, "("); ATC(0); fprintf(f, "<="); ATC(1); fprintf(f, ")"); break;
		case '>':	fprintf(f, "("); ATC(0); fprintf(f, ">"); ATC(1); fprintf(f, ")"); break;
		case t_be:	fprintf(f, "("); ATC(0); fprintf(f, ">="); ATC(1); fprintf(f, ")"); break;
		case '&':	fprintf(f, "("); ATC(0); fprintf(f, "&"); ATC(1); fprintf(f, ")"); break;
		case '|':	fprintf(f, "("); ATC(0); fprintf(f, "|"); ATC(1); fprintf(f, ")"); break;
		case '^':	fprintf(f, "("); ATC(0); fprintf(f, "^"); ATC(1); fprintf(f, ")"); break;
		case t_shl:	fprintf(f, "("); ATC(0); fprintf(f, "<<"); ATC(1); fprintf(f, ")"); break;
		case t_shr:	fprintf(f, "("); ATC(0); fprintf(f, ">>"); ATC(1); fprintf(f, ")");  break;
		case '~':	fprintf(f, "("); fprintf(f, "~"); ATC(0); fprintf(f, ")"); break; 
		case 'r':	fprintf(f, "e->r"); break;
		case 'g':	fprintf(f, "e->g"); break;
		case 'b':	fprintf(f, "e->b"); break;
		case 'a':	fprintf(f, "e->a"); break;
		case 'c':	fprintf(f, "e->c"); break;
		case 'i':	fprintf(f, "e->i"); break;
		case 'u':	fprintf(f, "e->u"); break;
		case 'v':	fprintf(f, "e->v"); break;
		case 'x':	fprintf(f, "e->x"); break;
		case 'y':	fprintf(f, "e->y"); break;
		case 'z':	fprintf(f, "e->z"); break;
		case 'X':	fprintf(f, "e->X"); break;
		case 'Y':	fprintf(f, "e->Y"); break;
		case 'Z':	fprintf(f, "e->Z"); break;
		case 'd':	fprintf(f, "e->d"); break;
		case 'D':	fprintf(f, "1024"); break;
		case 'm':	fprintf(f, "e->m"); break;
		case 'M':	fprintf(f, "e->M"); break;
		case t_mmin:	fprintf(f, "0"); break;
		case t_dmin:	fprintf(f, "0"); break;
		case '!':	fprintf(f, "!"); ATC(0); break;
		case t_sin:	fprintf(f, "p_sin(e,"); ATC(0); break;
		case t_cos:	fprintf(f, "p_cos(e,"); ATC(0); break;
		case t_tan:	fprintf(f, "p_tan(e,"); ATC(0); break;
		case t_sqr:	fprintf(f, "p_sqr(e,"); ATC(0); break;
		case t_ctl:	fprintf(f, "p_ctl(e,"); ATC(0); break;
		case t_get:	fprintf(f, "p_get(e,"); ATC(0); break;
		case t_abs:	fprintf(f, "p_abs(e,"); ATC(0); break;
		case t_c2d:	fprintf(f, "p_c2d(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case t_c2m:	fprintf(f, "p_c2m(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case t_r2x:	fprintf(f, "p_r2x(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case t_r2y:	fprintf(f, "p_r2y(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case '/':	fprintf(f, "p_div(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case '%':	fprintf(f, "p_mod(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case t_put:	fprintf(f, "p_put(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case t_add:	fprintf(f, "p_add(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ",");  ATC(2); fprintf(f, ")");  break;
		case t_mix:	fprintf(f, "p_mix(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ",");  ATC(2); fprintf(f, ",");  ATC(3); fprintf(f, ")");  break;
		case t_sub:	fprintf(f, "p_sub(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ",");  ATC(2); fprintf(f, ")");  break;
		case t_min:	fprintf(f, "p_min(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case t_max:	fprintf(f, "p_max(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case t_rnd:	fprintf(f, "p_rnd(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case t_dif:	fprintf(f, "p_dif(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ")");  break;
		case t_val:	fprintf(f, "p_val(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ",");  ATC(2); fprintf(f, ")");  break;
		case t_src:	fprintf(f, "p_src(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ",");  ATC(2); fprintf(f, ")");  break;
		case t_rad:	fprintf(f, "p_rad(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ",");  ATC(2); fprintf(f, ")");  break;
		case '?':   	ATC(0); fprintf(f, "?"); ATC(1); fprintf(f, ":"); ATC(2); break;
		case t_scl:	fprintf(f, "p_scl(e,"); ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ",");  ATC(2); fprintf(f, ",");  ATC(3); fprintf(f, ",");  ATC(4); fprintf(f, ")");  break;
		case t_cnv:	fprintf(f, "p_cnv(e,"); 
			ATC(0); fprintf(f, ",");  ATC(1); fprintf(f, ",");  ATC(2); fprintf(f, ",");  
			ATC(3); fprintf(f, ",");  ATC(4); fprintf(f, ",");  ATC(5); fprintf(f, ",");  
			ATC(6); fprintf(f, ",");  ATC(7); fprintf(f, ",");  ATC(8); fprintf(f, ",");  
			ATC(9); fprintf(f, ")");  break;
		case t_comma: fprintf(f, "p_comma(e,"); ATC(0); fprintf(f, ","); ATC(1); fprintf(f, ")"); break;
		default: fprintf(f, "\n/* error %i ! */\n", a->op_type); break;
	}
}

s_filter_data data;
s_uf_tree *function;

void main(int argc, char **argv)
{
	if (argc==2) {
		default_filter(&data);

		strcpy(data.title, argv[1]);
		load_filter(argv[1], &data);
	
		write_as_c(&data, stderr);
	}
}
