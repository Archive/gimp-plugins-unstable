;;; Commentary:
;;;  This is a sample file for %guash-mapper function.
;;;  Modify or delete freely!

;;; First, try:
;;; %gm-thumbnail, %gm-add-copyright, %gm-to-jpg
;;; These functions were generated from its mother(meta) functions.

;; 1st sample is automatic thumnail&html generator
;; This is an example of use of two extra invocations.

(define (%gm-thumbnail-m thumbnail-tail directory html-file)
  ;; make a closure
  (let ((url-infos '())
	;; parameters: modify them as you like.
	(url-file-header "")	; "<ul>"
	(url-file-trailer "")	; "</ul>"
	(url-header "")	; "<li>"
	(url-trailer ""))
    (lambda (img drw path-strs)
      (if (number? img)
	  (let* ((width (car (gimp-image-width img)))
		 (height (car (gimp-image-height img)))
		 (thumbnail-size 64)
		 (thumbnail-width 1)
		 (thumbnail-height 1)
		 (original-name "")
		 (thumbnail-name "")
		 (new-name ""))
	    (if (null? directory)
		(set! directory (nth 1 path-strs)))
	    (if (< width height)
		(begin
		  (set! thumbnail-height thumbnail-size)
		  (set! thumbnail-width (* (/ width height) thumbnail-size)))
		(begin
		  (set! thumbnail-width thumbnail-size)
		  (set! thumbnail-height (* (/ height width) thumbnail-size))))
	    (gimp-image-scale img thumbnail-width thumbnail-height)
	    (set! original-name (string-append (nth 2 path-strs) "."
					       (nth 3 path-strs)))
	    (set! thumbnail-name (string-append (nth 2 path-strs)
						thumbnail-tail
						(nth 3 path-strs)))
	    (set! new-name (string-append directory "/" thumbnail-name))
	    (gimp-file-save 0 img drw new-name new-name)
	    (gimp-image-clean-all img)
	    (set! url-infos
		  (cons (string-append url-header
				       "<a href=\"" original-name
				       "\"><img src=\"" thumbnail-name
				       "\" alt=\"" original-name
				       "\"></a>"
				       url-trailer "\n")
			url-infos)))
	  ;; hook
	  (if (eq? drw #f)
	      ;; initialization
	      (begin		
		;; for 2nd invocation
		(set! url-infos '()))
	      ;; finalization
	      (let ((html (fopen (string-append directory "/" html-file) "w")))
		(if (string? url-file-header)
		    (fwrite url-file-header html))
		(for-each (lambda (str) (fwrite str html)) url-infos)
		(if (string? url-file-trailer)
		    (fwrite url-file-trailer html))
		(fclose html)))))))

;; run with the following settings:
;; Display the file		=> NO
;; Overwrite the file		=> NO
;; Sort files alphabetically	=> YES
(define %gm-thumbnail
  (%gm-thumbnail-m  "-small." #f ; or "/tmp"
		    "thumbnail-index.html"))

;; 2nd mother is add a string to image
(define (%gm-add-string str)
  (lambda (img drw path-strs)
    (if (number? img)
	(let ((not-in-use #t))		; for future extension
	  (gimp-text img -1 2 2 str 1 TRUE 14 PIXELS
		     "*" "courier" "*" "*" "*" "*")
	  ;;(gimp-image-flatten img)
	  ))))

(define %gm-add-copyright (%gm-add-string "Copyright (c) Shuji Narazaki"))

;; file  converter
(define (%gm-save-as extension indexed?)
  (lambda (img drw path-strs)
    (if (number? img)
	(let ((new-name (string-append (nth 1 path-strs) ; directory
				       "/"
				       (nth 2 path-strs) ; name 
				       "."
				       extension)))
	  ;; check image type if need!
	  ;; not implemented...
	  (gimp-file-save 0 img drw new-name new-name)))))

;; try them!
;; run with the following settings:
;; Display the file		=> NO
;; Overwrite the file		=> NO
;; Sort files alphabetically	=> NO
(define %gm-to-jpg (%gm-save-as "jpg" #f))
(define %gm-to-gif (%gm-save-as "gif" #t))

;; gather selected images into a new multi-layered image
;; run with the following settings:
;; Display the file		=> NO
;; Overwrite the file		=> NO
;; Sort files alphabetically	=> NO
(define %gm-collect
  (let* ((init-size 256)
	 (max-width init-size)
	 (max-height init-size)
	 (new-image #f)
	 (bg-layer #f)
	 (top-layer #f))
    (lambda (img drw path-strs)
      (if (number? img)
	  (let* ((width (car (gimp-drawable-width drw)))
		 (height (car (gimp-drawable-height drw)))
		 (new-layer
		  (car (gimp-layer-new new-image width height RGBA_IMAGE 
				       (string-append (nth 2 path-strs) "."
						      (nth 3 path-strs))
				       100 NORMAL))))
	    ;; flatten image
	    (set! drw (car (gimp-image-flatten img)))
	    ;; copy drawable
	    (gimp-edit-copy img drw)
	    ;; add to new-image
	    (gimp-image-add-layer new-image new-layer -1)
	    (set! top-layer (car (gimp-edit-paste new-image new-layer 0)))
	    (gimp-floating-sel-anchor top-layer)
	    (if (< max-width width)
		(set! max-width width))
	    (if (< max-height height)
		(set! max-height height))
	    )
	  ;; hook
	  (if (eq? drw #f)
	      ;; initialization
	      (begin
		(set! new-image (car (gimp-image-new init-size init-size RGB)))
		(set! bg-layer (car (gimp-layer-new new-image 
						    init-size init-size
						    RGBA_IMAGE "BG" 100
						    NORMAL)))
		(set! top-layer bg-layer)
		(gimp-image-add-layer new-image bg-layer 0)
		(gimp-display-new new-image)
		)
	      ;; finalization
	      (begin
		(gimp-image-resize new-image max-width max-height 0 0)
		(gimp-image-remove-layer new-image bg-layer)
		(gimp-displays-flush))
	      )))))

;;; guash-mapper-sample.scm ends here
