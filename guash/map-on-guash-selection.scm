;;; map-on-guash-selection.scm -*-scheme-*-
;;; Author: Shuji Narazaki <narazaki@inetq.or.jp>
;;; Time-stamp: <1998/05/12 21:27:34 narazaki@InetQ.or.jp>
;;; Version: 0.6
;;; Commentary:
;;;  This script is a part of guash, and is invoked from guash mainly.
;;;  YOU MUST define guash-selection-file out of this script.
;;;  EDIT ./guash-selection.scm

;;; Code:

(if (not (symbol-bound? '%gm-blur ; 'script-fu-map-on-guash-selection-procedure is too long for text-box :-< 
			(the-environment)))
    (define %guash-mapper ; script-fu-map-on-guash-selection-procedure
      ;; arguments: image-id, drawable-id, file-list
      ;; file-list is: (fullpath directory file-name-w/o-suffix suffix)
      ;; Namely, this function is inovoked like:
      ;;  (%guash-mapper 1 1 ("/usr/tmp/foo.jpg" "/usr/tmp" "foo" "jpg"))
      ;;  Warning: period between foo and jpg is removed there: ^
      ;;
      ;; The procedure is invoked twice more out of the iteration on selection.
      ;; Fisrt invoked as initialization step with args: #f #f selection-list
      ;; And invoked as finalization with #f #t selection-list after the loop
      ;; Note: #t is equal to 1 in script-fu !!!!!!!!
      (lambda (img drw file)
	 (if (number? img)
	     (plug-in-gauss-rle 1 img drw 5.0 TRUE TRUE)))))
(if (not (symbol-bound? 'script-fu-map-on-guash-selection-display?
			(the-environment)))
    (define script-fu-map-on-guash-selection-display? FALSE))
(if (not (symbol-bound? 'script-fu-map-on-guash-selection-overwrite?
			(the-environment)))
    (define script-fu-map-on-guash-selection-overwrite? FALSE))
(if (not (symbol-bound? 'script-fu-map-on-guash-selection-sort?
			(the-environment)))
    (define script-fu-map-on-guash-selection-sort? FALSE))

(define (script-fu-map-on-guash-selection func display? overwrite? sort?)
  (define (sort l small? accessor)	; copied from my guile lib.
    (define (divide obj l)
      (define (divide-aux obj l smalls larges)
	(cond ((null? l) (cons smalls larges))
	      ((small? (accessor (car l)) (accessor obj))
	       (divide-aux obj (cdr l) (cons (car l) smalls) larges))
	      ('else (divide-aux obj (cdr l) smalls (cons (car l) larges)))))
      (divide-aux obj l () ()))
    (define (quick-sort l)
      (if (null? l)
	  ()
	  (let* ((obj (let ((tmp (car l))) (set! l (cdr l)) tmp))
		 (tmp (divide obj l))
		 (append! append))	; Oh boy!
	    (append! (quick-sort (car tmp))
		     (append! (list obj) (quick-sort (cdr tmp)))))))
    (quick-sort l))
  (let ((image #f)
	(drawable #f)
	(image-files #f)
	(image-file #f)
	(interactive? 1))
    ;; for 0.99.15 that crashes after refresh twice
    (if (symbol-bound? 'guash-selection-file (the-environment))
	(load guash-selection-file)
	(set! %gaush-selection '()))
    (set! image-files
	  (if (eq? sort? TRUE)
	      (sort %guash-selection (lambda (x y) (< (strcmp x y) 0)) car)
	      %guash-selection))
    ;; First call initializer with #f #f
    (func #f #f image-files)
    (while (not (null? image-files))
      (set! image-file (car (car image-files)))
      (set! image (car (gimp-file-load interactive? image-file image-file)))
      (gimp-image-clean-all image)
      (set! drawable (car (gimp-image-get-active-layer image)))
      (if (= drawable -1)
	  (set! drawable (car (nth 1 (gimp-image-get-layers image)))))
      (if (eq? display? TRUE) (gimp-display-new image))
      (func image drawable (car image-files))
      (if (eq? overwrite? TRUE)
	  (gimp-file-save interactive? image drawable image-file image-file))
      (if (not (eq? display? TRUE))
	  (begin
	    ;; delete quietly even if the image modified
	    (gimp-image-clean-all image)
	    (gimp-image-delete image)))
      (set! image-files (cdr image-files)))
    ;; Lastly call finalizer with #f #t
    (func #f #t image-files)
    ;; (set! script-fu-map-on-guash-selection-procedure func)
    (set! %guash-mapper func)
    (set! script-fu-map-on-guash-selection-display? display?)
    (set! script-fu-map-on-guash-selection-overwrite? overwrite?)
    (set! script-fu-map-on-guash-selection-sort? sort?)
    (set! %guash-selection '())		; for fail safe
    (gimp-displays-flush)))

(script-fu-register
 "script-fu-map-on-guash-selection"
 "<Toolbox>/Xtns/Script-Fu/Misc/Map on guash selection"
 "Map a procedure on the selected image files by guash"
 "Shuji Narazaki <narazaki@inetq.or.jp>"
 "Shuji Narazaki"
 "1997"
 ""
 SF-VALUE "Procedure" ;; "script-fu-map-on-guash-selection-procedure"
 "%gm-blur"
 SF-TOGGLE "Display the file" script-fu-map-on-guash-selection-display?
 SF-TOGGLE "Overwrite the file" script-fu-map-on-guash-selection-overwrite?
 SF-TOGGLE "Sort files alphabetically" script-fu-map-on-guash-selection-sort?
)
;;; map-on-guash-selection.scm ends here
