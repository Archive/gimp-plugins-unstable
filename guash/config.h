/* 
#define DEBUG 1
*/

/* 
 * ARCHITECTURE-DEPENDED CONSTANTS
 */

#define GCHAR	gchar
#define GINDEX	glong		/* gint is also acceptable on 32bit arch. */

/* Does your machine have <unistd.h> ???
 * Sorry, I don't know how to determine automatically... */
#define HAVE_UNISTD_H	1

/* 
 * CUSTOMIZABLE CONSTANTS
 */

/* Font for filenames in thumbnail panel
 * NOTE: select a font that your machine has! Use xfontsel or gimp text-tool. */
#define THUMBNAIL_FONT_FOUNDARY		"*"		/* "adobe", "*" */
#define THUMBNAIL_FONT_FAMILY		"helvetica" /* "helvetica", "courier" */
#define THUMBNAIL_FONT_WEIGHT		"medium"	/* "medium", "*" */
#define THUMBNAIL_FONT_SLANT		"r"	/* "r", "*" */
#define THUMBNAIL_FONT_SIZE		10 	/* 10 12 */
#define THUMBNAIL_FONT_ANTIALIAS	0 	/* 0:NO 1:YES */

/* You can set inihibit suffixes dynamically by guash-inhibit-suffix in
   gimprc */
#define GUASH_INHIBIT_SUFFIX_TABLE_DEFAULT \
	"core:.ps.gz:.ps:.tar:.tar.gz:.tgz"

/* You can set default command dynamically by guash-mapping-command in
   gimprc */
#define MAPPING_COMMAND_DEFAULT \
	"xv -root -maxpect -quit {}"

/* You can set history size dynamically by guash-directory-history-length in
   gimprc
   If your machine has small (physical) memory, try smaller value.
 */
#define DIRECTORY_CACHE_TABLE_MAX_SIZE	16

#ifndef CONFIG_H
#define CONFIG_H
#endif
/* config.h ends here */
