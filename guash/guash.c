/* The GIMP -- an image manipulation program
 * Copyright (C) 1995-1998 Spencer Kimball and Peter Mattis
 *
 * guash.c -- This is a plug-in for the GIMP 1.0
 * Time-stamp: <1998/06/01 13:02:43 narazaki@gimp.org>
 * Guash: Gimp Users' Another SHell (pronounced like 'gouache')
 * Copyright (C) 1997-1998 Shuji Narazaki <narazaki@gimp.org>
 * guash uses internally for loading xv's thumbnail:
 * xvpict (Copyright (C) 1997 Marco Lamberto <ml568366@silab.dsi.unimi.it>)
 * (slighly modified xvpict is embedded into guash for reducing communication
 * overhead)
 * Version: 1.0.2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Comment:
 * Thanks:
 *  Marco Lamberto <ml568366@silab.dsi.unimi.it>
 *  Vincent Hascoet <hascoet@lep.research.philips.com>
 *  J"urgen Koslowski <koslowj@iti.cs.tu-bs.de>
 *  meo@netads.com (Miles O'Neal)
 *  Casper Maarbjerg <casper@tv1.dk>
 *
 */

#include "config.h"
#include "guash-banner.h"
#include "guash-directory-icon.h"

#include "gtk/gtk.h"
#include "gdk/gdkkeysyms.h"
#include "libgimp/gimp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <pwd.h>
#include <errno.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

/* For debug */
#ifdef DEBUG
# define DPRINT		printf ("Debug: "), printf
# define DEBUGBLOCK	printf ("debug: "), printf
# define DEBUGEND	printf ("done.\n")
#else
# define DPRINT		if (0) printf
# define DEBUGBLOCK	if (0) printf
# define DEBUGEND	;
#endif

/* CONSTANTS */
#define	PLUG_IN_NAME		"extension_guash"
#define SHORT_NAME		"Guash"
#define MENU_PATH		"<Toolbox>/Xtns/Guash"
#define GUASH_SELECTION_SCM	"#guash-selection.scm"
#define GUASH_DND_SIGNATURE	"Guash copy files:\n"
#define THUMBNAIL_DIR		"/.xvpics"

#define INTERFACE		guash_interface
#define	DIALOG			guash_dialog
#define VALS			guash_vals

#ifdef THUMBNAIL_FORMAT_IS_XCF
#undef THUMBNAIL_SUFFIX		".xcf"
#endif

#define USE_DND			1
#define DELETE_CORE		1
#define STRICT_MM		1

/* VISUAL CONSTANTS */
#define NCOL_OF_THUMBNAIL_MIN	4
#define NROW_OF_THUMBNAIL_MIN	1
#define NCOL_OF_THUMBNAIL_DEFAULT	5
#define NROW_OF_THUMBNAIL_DEFAULT	3
#define DIRECTORY_LABEL_MAXLEN	55
#define THUMBNAIL_INFO_MAXLEN	55
#define JUMP_BUTTON_WIDTH	40
#define	SCROLLBAR_WIDTH		15
#define THUMBNAIL_WIDTH		80	/* Max width of XV thumbnail */
#define THUMBNAIL_HEIGHT	60	/* Max height of XV thumbnail */
#define THUMBNAIL_TT_SEPARATOR	6
#define THUMBNAIL_THEIGHT	(THUMBNAIL_FONT_SIZE + THUMBNAIL_TT_SEPARATOR)
#define THUMBNAIL_SEPARATOR	5

/* OTHER CONSTANTS */
#define NEW_DIRECTORY_MODE	00777
#define NEW_FILE_MODE		00644
#define INITIALIZATION_SLEEP_PERIOD	1500
#define STARTUP_SLEEP_PERIOD		500
#define EVENT_MASK	GDK_EXPOSURE_MASK | \
     			GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK | \
			GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK
#define	LINE_BUF_SIZE	1024

#define INDEX_TO_X(i)	((i % ncol_of_thumbnail) \
			 * (THUMBNAIL_WIDTH + THUMBNAIL_SEPARATOR) \
			 + THUMBNAIL_SEPARATOR)
#define INDEX_TO_Y(i)	((i / ncol_of_thumbnail) \
			 * (THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT + THUMBNAIL_SEPARATOR) \
			 + THUMBNAIL_SEPARATOR)
#define VALID_POS_P(x,y)	((y < \
				  (THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT + THUMBNAIL_SEPARATOR) \
				  * nrow_of_thumbnail) \
				 && \
				 ((x < \
				   (THUMBNAIL_WIDTH + THUMBNAIL_SEPARATOR) \
				   * ncol_of_thumbnail)))
#define POS_TO_INDEX(x,y)	(cwd_cache->display_page * nthumbnails_in_page \
				 + (y - THUMBNAIL_SEPARATOR) \
				 / (THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT + THUMBNAIL_SEPARATOR) \
				 * ncol_of_thumbnail \
				 + x / (THUMBNAIL_WIDTH + THUMBNAIL_SEPARATOR))
#define COL2WIDTH(col)	(col * (THUMBNAIL_WIDTH + THUMBNAIL_SEPARATOR) + THUMBNAIL_SEPARATOR)
#define ROW2HEIGHT(row)	(row * (THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT + THUMBNAIL_SEPARATOR) + THUMBNAIL_SEPARATOR)
#define WIDTH2COL(w)	(w - THUMBNAIL_SEPARATOR) / (THUMBNAIL_WIDTH + THUMBNAIL_SEPARATOR)
#define HEIGHT2ROW(h)	(h - THUMBNAIL_SEPARATOR) / (THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT + THUMBNAIL_SEPARATOR)

/* gtkWrapper */
/* gtkW is the abbreviation of gtk Wrapper */
#define GTKW_ENTRY_BUFFER_SIZE	3
#define GTKW_ENTRY_WIDTH	20
#define GTKW_SCALE_WIDTH	200
#define	GTKW_BORDER_WIDTH	0

/* gtkW type */
typedef struct
{
  GtkWidget	*widget;
  gpointer	value;
  void	 	(*updater)();
} gtkW_widget_table;
gtkW_widget_table	widget_pointer[1];

/* gtkW global variables */
gint	gtkW_border_width = GTKW_BORDER_WIDTH;
gint	gtkW_border_height = 0;
gint	gtkW_homogeneous_layout	= FALSE;
gint	gtkW_frame_shadow_type = GTK_SHADOW_ETCHED_IN;
gint	gtkW_align_x = GTK_FILL;
gint	gtkW_align_y = GTK_FILL;

/* gtkW callback */
static void	gtkW_close_callback (GtkWidget *widget, gpointer data);
static void	gtkW_message_dialog (gint gtk_was_initialized, GCHAR *message);
static GtkWidget	*gtkW_message_dialog_new (GCHAR *name);
static gint	gtkW_confirmor_dialog (gint gtk_was_initialized, GCHAR *message,
				       gint default_value);
static GtkWidget 	*gtkW_confirmor_new (GCHAR *name, gint default_value,
					     gtkW_widget_table *table);
static void	gtkW_confirmor_yes (GtkWidget *widget, gpointer data);
static void	gtkW_confirmor_no (GtkWidget *widget, gpointer data);
static GtkWidget	*gtkW_frame_new (GtkWidget *parent, GCHAR *name);
static GtkWidget	*gtkW_hbox_new (GtkWidget *parent, gint expand, gint fill);
static GtkWidget	*gtkW_table_new (GtkWidget *parent, gint col, gint row);
static void	gtkW_iscroll_entry_change_value (gtkW_widget_table *wtable);
static void	gtkW_iscroll_update (GtkAdjustment *adjustment, gpointer data);
static void	gtkW_ivscroll_entry_new (GtkWidget **scroll,
					 GtkWidget **entry,
					 GtkSignalFunc	scale_update,
					 GtkSignalFunc	entry_update,
					 gint		*value,
					 gdouble	min,
					 gdouble	max,
					 gdouble	step,
					 gpointer	widget_entry);
static void	gtkW_ientry_update (GtkWidget *widget, gpointer data);
static GtkWidget	*gtkW_table_add_label (GtkWidget	*table,
					       GCHAR	*text,
					       gint	x0,
					       gint	x1,
					       gint	y,
					       gint	flush_left);
static void	gtkW_query_box (GCHAR *title, GCHAR *message,
				GCHAR *initial, GCHAR *data);
static void	gtkW_preview_force_to_update (GtkWidget *widget);
static GtkWidget 	*gtkW_query_string_box_new (char        *title,
						   char        *message,
						   char        *initial,
						   gpointer     data);
static void	gtkW_query_box_cancel_callback (GtkWidget *, gpointer);
static void	gtkW_query_box_ok_callback (GtkWidget *, gpointer);
static gint	gtkW_query_box_delete_callback (GtkWidget *, GdkEvent *, gpointer);
static gint	gtkW_parse_gimprc_gint (GCHAR *name, gint default_value);
static GCHAR	*gtkW_parse_gimprc_string (GCHAR *name, GCHAR *buffer);
/* end of GtkW */

/* define enums */
/* for VAL.sort_kind (SORT_BY_NAME, SORT_BY_DATE } */
#define SORT_BY_NAME	0
#define SORT_BY_DATE	1
/* for file_kind { NOT_EXIST, REGFILE, DIRECTORY, SLNKFILE, SLNKDIR } */
#define NOT_EXIST	0
#define REGFILE		2
#define DIRECTORY	4
#define SLNKFILE	8
#define SLNKDIR		16
/* for directory_cache.savable { FALSE, TRUE, UNKNOWN } */
#define UNKNOWN		2
/* for selection_kind { FALSE, IMAGE, DIRECTORY } */
#define	IMAGE		1
/* for sensives { SELECTION, SCROLL, HIDDEN } */
#define SELECTION	1
#define SCROLL		2
#define HIDDEN		4
/* for directory_cache_set_to_cwd() ( FASLE, TRUE, GC ) */
#define	GC		(TRUE + TRUE)

typedef struct
{
  gint	sort_type;
  GCHAR last_dir_name[LINE_BUF_SIZE];
  GCHAR mapping_command[LINE_BUF_SIZE];
} VALS;

typedef struct
{
  gint  deleted;
  gint	gc_flag;		/* 1 if it is a garbage */
  gint	directory_p;
  gint	width;
  gint	height;
  guchar	*data;
  GCHAR	name[256];
  GCHAR	info[256];
} cache_entry;

typedef struct
{
  GCHAR		name[LINE_BUF_SIZE];
#ifdef STRICT_MM
  gint		birth_index;
#endif
  gint		filed;
  gint		savable;
  gint		purged;	/* TRUE if move/copy/delete was executed */
  cache_entry	*image;
  cache_entry	*dir;
  gint		ndir;
  gint		nimage;
  gint		max_ndir;
  gint		max_nimage;
  gint		display_page;
} directory_cache;

typedef struct
{
  GCHAR	key;
  guint	mod;
} Gtk_binding;

#define NKEYSTYLES	2
typedef struct
{
  GCHAR		*label;
  void		(* command) (GtkWidget *, gpointer);
  gint		sensitive;
  GtkWidget	*widget;
  Gtk_binding	binding[NKEYSTYLES];
} image_command_table;

typedef struct
{
  GCHAR	*action;
  GCHAR	*condition;
  GCHAR	*behavior;
  gint	flush_left;
} help_table;

VALS		VAL;
gchar		*directory_icon = NULL;
GHashTable	*directory_cache_table;
gint		directory_cache_max_size = 500;
#ifdef STRICT_MM
gint		directory_cache_table_max_size = DIRECTORY_CACHE_TABLE_MAX_SIZE;
gint		directory_cache_table_size = 0;
#endif
GCHAR		**inhibit_suffix_table = NULL;
gint		num_inhibit_suffix = -1;
directory_cache *cwd_cache = NULL;
GtkWidget	*dlg = NULL;
GtkWidget	*cwd_label = NULL;
GtkWidget	*thumbnail_panel = NULL;
GtkWidget	*file_property = NULL;
GtkWidget	*thumbnail_panel_root_menu = NULL;
GtkWidget	*thumbnail_panel_selection_menu = NULL;
GtkWidget	*thumbnail_panel_hidden_menu = NULL;
gint		ncol_of_thumbnail_default = NCOL_OF_THUMBNAIL_DEFAULT;
gint		nrow_of_thumbnail_default = NROW_OF_THUMBNAIL_DEFAULT;
gint		ncol_of_thumbnail = 5;
gint		nrow_of_thumbnail = 3;
gint		nthumbnails_in_page = 15;
gint		thumbnail_panel_last_index = -1;
gint		thumbnail_panel_page1 = 1;
gint		thumbnail_panel_width;
gint		thumbnail_panel_height;
gint		thumbnail_panel_resized = FALSE;
gint32		text_image = -1;
gint32		text_bg = -1;
GList		*selection_list;
gint		selection_kind;
GCHAR		thumbnail_panel_info_default[256];
GCHAR		thumbnail_panel_directory_str[256];
GCHAR		fileselector_last_pathname[LINE_BUF_SIZE];

gint	binding_style = 0;
gint	use_confirmor = TRUE;
gint	hidden_features = FALSE;

#define NSCROLLER	2
GtkWidget	*widget_for_scroll[NSCROLLER];
#ifdef USE_DND
gchar		*possible_dnd_types[] = {"text/plain"};
#define	NSELECTION_BUTTON	1
GtkWidget	*widget_for_selecion[NSELECTION_BUTTON];
#endif

#define HEADER_PIXEL(data,pixel) \
  pixel[0] = (((data[0] - 33) << 2) | ((data[1] - 33) >> 4)); \
  pixel[1] = ((((data[1] - 33) & 0xF) << 4) | ((data[2] - 33) >> 2)); \
  pixel[2] = ((((data[2] - 33) & 0x3) << 6) | ((data[3] - 33))); \
  data += 4;

static void	query	(void);
static void	run	(char	*name,
			 int	nparams,
			 GParam	*param,
			 int	*nreturn_vals,
			 GParam **return_vals);
static gint	DIALOG ();
static gint	timer_initialize_thumbnail_panel (gpointer data);
static gint	about_dialog ();
static gint	cache_get_image (GCHAR *name, gint f_kind);
static gint	cache_get_image_from_file (GCHAR *name, GCHAR *suffix,
					   gint f_kind);
static void	cache_set_to_symlink_color ();
static void	cache_set_to_normal_color ();
static void	cache_render_text (GCHAR *str, cache_entry *cache);
static gint	cache_save_to_file (GCHAR *file_name, gint32 i_id);
static gint	cache_open_image_file (cache_entry *cache);
static GCHAR *	file_get_canonical_name (GCHAR *name);
static gint	file_get_last_slash_index (GCHAR *pathname);
static gint	file_get_last_period_index (GCHAR *pathname);
static GCHAR *	file_get_parent_directory (GCHAR *pathname);
static GCHAR *	file_get_filename (GCHAR *pathname);
static GCHAR *	file_build_cache_directory_name (GCHAR *filename);
static GCHAR *	file_build_cache_file_name (GCHAR *filename);
static gint	file_match_inhibit_suffix (GCHAR *pathname);
static gint	os_file_alphasort (GCHAR **a, GCHAR **b);
static gint	os_file_mtimesort (GCHAR **a, GCHAR **b);
typedef int	(*scandir_selector_function) (GCHAR *);
static gint	os_scandir_selector (GCHAR *filename);
static gint	os_scandir (GCHAR *dir_name,
			    GCHAR ***filename_list,
			    scandir_selector_function selector,
			    gint sort_type);
typedef	int	(*sort_compare_function)(const void *, const void *);
static gint	os_copy_file (GCHAR *filename, GCHAR *newname);
static gint	os_file_kind (GCHAR *filename, gint shrink);
static gint	os_file_size (GCHAR *filename);
static gint	os_mkdir (GCHAR *pathname);
static gint	os_make_directory (GCHAR *pathname, gint mode);
static gint	os_rename_file (GCHAR *filename, GCHAR *newname);
static gint	file_confirm_operation (GCHAR *operation_phrase);
static gint	cache_file_is_valid (GCHAR *file_name, GCHAR *suffix);
static gint	image_file_copy (GCHAR *filename, GCHAR *newname);
static gint	image_file_delete (GCHAR *filename);
static gint	image_file_move (GCHAR *filename, GCHAR *newname);
static void	fileselector_set_last_value (GCHAR *pathname);
directory_cache	*directory_cache_set_to_cwd (gint purge);
#ifdef STRICT_MM
static void	directory_cache_table_recycler (gpointer key,
						gpointer value,
						gpointer user_data);
#endif
directory_cache	*directory_cache_new (GCHAR *name);
static gint	directory_cache_npage ();
cache_entry *	directory_cache_get_nth (GINDEX index);
static gint	directory_cache_add_directory (GCHAR *name, gint f_kind);
static gint	directory_cache_add_image (GCHAR *name, gint i_id, gint f_kind);
static gint	directory_cache_force_to_purge (GCHAR *name);
static gint	directory_cache_make_cache_directory (directory_cache *dentry);
static void	directory_cache_create_history_menu_foreach (gpointer key,
							     gpointer value,
							     gpointer user_data);
GtkWidget *	directory_cache_create_histroy_menu ();
GtkWidget *	directory_cache_create_parents_menu ();
static gint	directory_cache_delete_invalid_cache_files (gint force);
GtkWidget *	directory_cache_create_parents_menu ();
static gint	directory_cache_garbage_collect ();
static gint	directory_cache_update ();
static void	guash_parse_gimprc ();
static void	guash_build_inhibit_suffix_table (GCHAR *buffer);
static void	guash_assure_scratch_buffer ();
static void	guash_unref_scratch_buffer ();
static GList	*selection_reset ();
static GList	*selection_add (GINDEX index);
static GList	*selection_delete (GINDEX index);
static gint	selection_is_active ();
static gint	selection_length ();
static gint	selection_member_p (GINDEX index);
static void	selection_map_script ();
static void	selection_map_unix_command ();
static GCHAR	*generate_unix_command (GCHAR *template, GCHAR *directory, GCHAR *filename);
static void	selection_open_files ();
static gint	selection_reverse_member (GINDEX index);
static void	selection_open_files ();
static void	selection_copy_files_to (GCHAR *pathname);
static void	selection_delete_files ();
static void	selection_move_files_to (GCHAR *pathname);
static gint	thumbnail_panel_show_cache (cache_entry *cache);
static void	thumbnail_panel_banner ();
static void	thumbnail_panel_clear ();
static void	thumbnail_panel_set_directory_info ();
static void	thumbnail_panel_set_info (GCHAR *name);
static void	thumbnail_panel_set_info_default ();
static void	thumbnail_panel_draw_frame (GINDEX index, guchar val);
static void	thumbnail_panel_draw_selection_frame ();
static void	thumbnail_panel_clear_selection_frame ();
static void	thumbnail_panel_size_request (GtkWidget      *widget,
					      GtkRequisition *requisition);
static void	thumbnail_panel_size_allocate (GtkWidget     *widget,
					       GtkAllocation *allocation);
static gint	thumbnail_panel_update ();
static void	thumbnail_panel_update_sensitive_menu ();
static void	thumbnail_panel_update_selection_buttons ();
static void	thumbnail_panel_update_scroller ();
#ifdef GTK_HAVE_ACCEL_GROUP
static void	thumbnail_panel_create_menu_for (GtkWidget *menu,
						 image_command_table *commands,
						 gint ncommand,
						 GtkAccelGroup *accelerator_table);
#else
static void	thumbnail_panel_create_menu_for (GtkWidget *menu,
						 image_command_table *commands,
						 gint ncommand,
						 GtkAcceleratorTable *accelerator_table);
#endif
static void	thumbnail_panel_create_menu ();
static gint	thumbnail_panel_move_focus (gint offset);
static void	menu_change_directory_callback (GtkWidget *widget, gpointer data);
static void	directory_jump_callback (GtkWidget *widget, gpointer data);
static void	parent_directory_callback (GtkWidget *widget, gpointer data);
static void	forward_callback (GtkWidget *widget, gpointer data);
static void	select_and_forward_callback (GtkWidget *widget, gpointer data);
static void	backward_callback (GtkWidget *widget, gpointer data);
static void	next_callback (GtkWidget *widget, gpointer data);
static void	prev_callback (GtkWidget *widget, gpointer data);
static void	next_page_callback (GtkWidget *widget, gpointer data);
static void	prev_page_callback (GtkWidget *widget, gpointer data);
static void	open_callback (GtkWidget *widget, gpointer data);
static void	update_callback (GtkWidget *widget, gpointer data);
static void	toggle_sort_mode_callback (GtkWidget *widget, gpointer data);
static void	purge_thumbnail_file_callback (GtkWidget *widget, gpointer data);
static void	help_callback (GtkWidget *widget, gpointer data);
static void	fileselector_for_copy_callback (GtkWidget *widget, gpointer client_data);
static void	fileselector_for_move_callback (GtkWidget *widget, gpointer client_data);
static void	fileselector_for_chdir_callback (GtkWidget *widget, gpointer client_data);
static void	fileselector_for_mkdir_callback (GtkWidget *widget, gpointer client_data);
static void	copy_callback (GtkWidget *widget, gpointer data);
static void	move_callback (GtkWidget *widget, gpointer data);
static void	delete_callback (GtkWidget *widget, gpointer data);
static void	select_all_callback (GtkWidget *widget, gpointer data);
static void	select_none_callback (GtkWidget *widget, gpointer data);
static void	chdir_callback (GtkWidget *widget, gpointer data);
static void	mkdir_callback (GtkWidget *widget, gpointer data);
static void	selection_map_script_callback (GtkWidget *widget, gpointer data);
static void	selection_map_unix_command_callback (GtkWidget *widget, gpointer data);
static gint	preview_event_handler (GtkWidget *widget, GdkEvent *event);
static gint	cursor_event_handler (GtkWidget *widget, GdkEvent *event);
#ifdef USE_DND
static void	dnd_drag_button_callback (GtkWidget *widget, GdkEvent *event);
static void	dnd_drag_request_callback (GtkWidget *button, GdkEvent *event);
static void	dnd_drop (GtkWidget *button, GdkEvent *event);
static gint	dnd_copy_files_to (GCHAR *buffer, GCHAR *pathname, GCHAR *dir, gint cwd_p);
#endif
static gint	save_xvpict_image (char *filename, gint32 image_ID, gint32 drawable_ID);
static gint32	load_xvpict_image (char *filename);

#define MENU_SEPARATOR   { NULL, NULL, 0, NULL, \
  				{ { 0, 0 }, \
				  { 0, 0 } \
			  } }
image_command_table
image_root_commands [] =
{
  { "Next page", next_page_callback, SCROLL, NULL,
    { { ' ', 0 },
      { ' ', 0 }
    } },
  { "Prev page", prev_page_callback, SCROLL, NULL,
    { { 'B', 0 },
      { 'B', 0 }
    } },
  MENU_SEPARATOR,
  { "Selection All", select_all_callback, FALSE, NULL,
    { { 'A', GDK_CONTROL_MASK },
      { 'A', GDK_CONTROL_MASK }
    } },
  { "Selection None", select_none_callback, FALSE, NULL,
    { { 'A', GDK_CONTROL_MASK|GDK_SHIFT_MASK },
      { 'A', GDK_CONTROL_MASK|GDK_SHIFT_MASK }
    } },
  MENU_SEPARATOR,
  { "Make new directory", fileselector_for_mkdir_callback, FALSE, NULL,
    { { 'D', GDK_SHIFT_MASK },
      { '+', GDK_SHIFT_MASK }
    } },
  { "Change directory", fileselector_for_chdir_callback, FALSE, NULL,
    { { 'C', 0 },
      { 'D', GDK_CONTROL_MASK }
    } },
  { "Change to parent directory", parent_directory_callback, FALSE, NULL,
    { { '^', GDK_SHIFT_MASK },
      { '^', GDK_SHIFT_MASK }
    } },
  { "Update", update_callback, FALSE, NULL,
    { { 'R', 0 },
      { 'G', 0 }
    } },
  { "Change sort mode", toggle_sort_mode_callback, FALSE, NULL,
    { { 'S', 0 },
      { 'S', 0 }
    } },
  { "Remove all thumbnail files", purge_thumbnail_file_callback, FALSE, NULL,
    { { 'R', GDK_MOD1_MASK },
      { 'P', GDK_MOD1_MASK }
    } },
  MENU_SEPARATOR,
  { "Help", help_callback, FALSE, NULL,
    { { '?', 0 },
      { 'H', GDK_CONTROL_MASK }
    } },
  { "Quit", gtkW_close_callback, FALSE, NULL,
    { { 'Q', 0 },
      { 'Q', GDK_CONTROL_MASK }
    } }
};

image_command_table
image_selection_commands [] =
{
  { "Open", open_callback, SELECTION, NULL,
    { { 'O', GDK_CONTROL_MASK },
      { 'O', GDK_CONTROL_MASK }
    } },
  { "Selection None", select_none_callback, FALSE, NULL,
    { { 0, 0 },
      { 0, 0 }
    } },
  MENU_SEPARATOR,
  { "Copy to...", fileselector_for_copy_callback, SELECTION, NULL,
    { { 'C', GDK_CONTROL_MASK },
      { 'C', GDK_CONTROL_MASK }
    } },
  { "Move to...", fileselector_for_move_callback, SELECTION, NULL,
    { { 'V', GDK_CONTROL_MASK },
      { 'R', GDK_CONTROL_MASK }
    } },
  { "Map script-fu on...", selection_map_script_callback, SELECTION, NULL,
    { { 'X', 0 },
      { '!', GDK_SHIFT_MASK }
    } },
  { "Map unix command on...", selection_map_unix_command_callback, SELECTION, NULL,
    { { 'X', GDK_SHIFT_MASK },
      { '|', GDK_SHIFT_MASK }
    } },
  MENU_SEPARATOR,
  { "Delete selected files", delete_callback, SELECTION, NULL,
    { { 'X', GDK_CONTROL_MASK },
      { 'D', GDK_MOD1_MASK }
    } }
};

image_command_table
image_hidden_commands [] =
{
  { "Forward image", forward_callback, HIDDEN, NULL,
    { { 'F', 0 },
      { 'F', GDK_CONTROL_MASK }
    } },
  { "Backward image", backward_callback, HIDDEN, NULL,
    { { 'B', 0 },
      { 'B', GDK_CONTROL_MASK }
    } },
  { "Down image", next_callback, HIDDEN, NULL,
    { { 'D', 0 },
      { 'N', GDK_CONTROL_MASK }
    } },
  { "Up image", prev_callback, HIDDEN, NULL,
    { { 'U', 0 },
      { 'P', GDK_CONTROL_MASK }
    } },
  MENU_SEPARATOR,
  { "Select then go forward", select_and_forward_callback, HIDDEN, NULL,
    { { ' ', GDK_CONTROL_MASK },
      { ' ', GDK_CONTROL_MASK }
    } }
};

help_table
help_document [] =
{
  { NULL, "Gimp Users' Another SHell\nVersion 1.0.2\nJune 1 1998", NULL, FALSE},
  { NULL,
    NULL, FALSE},
  { NULL, NULL, NULL, FALSE },
  { "ACTION", "CONDITION",
    "COMMAND", FALSE },
  { "Left-click", "on unselected image file",
    "Select only the image file", TRUE },
  { "Left-click", "on selected image file",
    "Open the selected image files", TRUE },
  { "Left-click", "on directory",
    "Jump to the directory", TRUE },
  { "Shift + Left-click", "on image file",
    "Add/delete the file to/from selection", TRUE },
  { "Control + Left-click", "on directory & active selection",
    "Copy selected files to the directory", TRUE },
  { "Shift + Left-click", "on directory & active selection",
    "Move selected files to the directory", TRUE },
  { "Middle-click", NULL,
    "Go to the next page of panel", TRUE },
  { "Shift + Middle-click", NULL,
    "Go to the pevious page of panel", TRUE },
  { "Right-click", "no selection",
    "Open root menu", TRUE },
  { "Right-click", "active selection",
    "Open selection menu", TRUE },
  { "Shift + Right-click", "",
    "Open root menu", TRUE },
  { NULL, NULL, NULL, FALSE },
  { NULL, "written by Shuji Narazaki <narazaki@gimp.org>", NULL, FALSE},
};

GPlugInInfo PLUG_IN_INFO =
{
  NULL,				/* init_proc  */
  NULL,				/* quit_proc */
  query,			/* query_proc */
  run,				/* run_proc */
};

MAIN ()

static void
query ()
{
  static GParamDef args [] =
  {
    { PARAM_INT32, "run_mode", "Interactive, non-interactive"},
    { PARAM_STRING, "directory_name", "directory name used as the default directory" },
  };
  static GParamDef *return_vals = NULL;
  static int nargs = sizeof (args) / sizeof (args[0]);
  static int nreturn_vals = 0;

  gimp_install_procedure (PLUG_IN_NAME,
			  "Thumbnail-based directory browser",
			  "Thumbnail-based directory browser",
			  "Shuji Narazaki <narazaki@gimp.org>",
			  "Shuji Narazaki",
			  "1997, 1998",
			  MENU_PATH,
			  "",
			  PROC_EXTENSION,
			  nargs, nreturn_vals,
			  args, return_vals);
}

static void
run (char	*name,
     int	nparams,
     GParam	*param,
     int	*nreturn_vals,
     GParam	**return_vals)
{
  GParam	*values;
  GStatusType	status = STATUS_EXECUTION_ERROR;
  GRunModeType	run_mode;

  values = g_new (GParam, 1);
  run_mode = param[0].data.d_int32;

  *nreturn_vals = 1;
  *return_vals = values;

  values[0].type = PARAM_STATUS;
  values[0].data.d_status = status;

  DPRINT ("guash is just started.");
  switch (run_mode)
    {
    case RUN_INTERACTIVE:
    case RUN_WITH_LAST_VALS:
      VAL.sort_type = SORT_BY_NAME;
      VAL.mapping_command[0] = VAL.last_dir_name[0] = 0;
      gimp_get_data (PLUG_IN_NAME, &VAL);

      directory_cache_table = g_hash_table_new ((GHashFunc) g_str_hash,
						(GCompareFunc) g_str_equal);
      selection_list = g_list_alloc ();
      if (0 < strlen (VAL.last_dir_name))
	chdir (VAL.last_dir_name);

      {
	gchar	*val = directory_icon_data;
	gint	x, y;

	directory_icon =
	  (gchar *) g_malloc (directory_icon_width * directory_icon_height * 3);

	for (y = 0; y < directory_icon_height ; y++)
	  for (x = 0; x < directory_icon_width; x++)
	    {				/*  don't delete this blaket! */
	      HEADER_PIXEL (val,
			    (directory_icon + (y * directory_icon_width + x) * 3));
	    }
      }
      DPRINT ("open dialog\n");
      DIALOG ();
      gimp_displays_flush();
      gimp_set_data (PLUG_IN_NAME, &VAL, sizeof (VALS));
      status = STATUS_SUCCESS;
      break;
    case RUN_NONINTERACTIVE:
      break;
    }

  guash_unref_scratch_buffer ();
  if (0 < text_image)
    gimp_image_delete (text_image);
  g_hash_table_destroy (directory_cache_table);
  g_list_free (selection_list);
  g_free (directory_icon);
  g_free (inhibit_suffix_table);

  values[0].type = PARAM_STATUS;
  values[0].data.d_status = status;
}

/* guash classes */

static void
guash_parse_gimprc ()
{
  GCHAR buf[LINE_BUF_SIZE];

  DEBUGBLOCK ("guash_parse_gimprc...");

  gtkW_parse_gimprc_string ("guash-confirmor", buf);
  if ((strcmp (buf, "no") == 0) ||
      (strcmp (buf, "No") == 0) ||
      (strcmp (buf, "NO") == 0))
    use_confirmor = 0;
  else
    use_confirmor = 1;

  gtkW_parse_gimprc_string ("guash-command", buf);
  if ((strcmp (buf, "all") == 0) ||
      (strcmp (buf, "All") == 0) ||
      (strcmp (buf, "ALL") == 0))
    hidden_features = 1;
  else
    hidden_features = 0;

  gtkW_parse_gimprc_string ("guash-keybindings", buf);
  if (((strcmp (buf, "emacs") == 0) ||
       (strcmp (buf, "Emacs") == 0) ||
       (strcmp (buf, "EMACS") == 0)))
    binding_style = 1;
  else
    binding_style = 0;

  gtkW_parse_gimprc_string ("guash-inhibit-suffix", buf);
  if (*buf == 0)
    strcpy (buf, GUASH_INHIBIT_SUFFIX_TABLE_DEFAULT);
  guash_build_inhibit_suffix_table (buf);

  gtkW_parse_gimprc_string ("guash-mapping-command", buf);
  if (*buf == 0)
    strcpy (buf, MAPPING_COMMAND_DEFAULT);
  if ((VAL.mapping_command == NULL) || strlen (VAL.mapping_command) == 0)
    strcpy (VAL.mapping_command, buf);

  {
    gint	tmp = 0;

    tmp = gtkW_parse_gimprc_gint ("guash-max-images-in-dir",
				  directory_cache_max_size);
    if (0 < tmp)
      directory_cache_max_size = CLAMP (tmp, 1, 2000);
    else
      directory_cache_max_size = -1;
  }
#ifdef STRICT_MM
  {
    gint	tmp = 0;

    tmp = gtkW_parse_gimprc_gint ("guash-directory-history-length",
				  directory_cache_table_max_size);
    if (0 < tmp)
      directory_cache_table_max_size = CLAMP (tmp, 8, 100);
    else
      directory_cache_table_max_size = -1;
  }
#endif

  DEBUGEND;
}

static void
guash_build_inhibit_suffix_table (GCHAR *buffer)
{
  GCHAR	*ptr = buffer;
  GCHAR	**chank;
  gint	chank_index = 0;
  gint	chank_size = 4;

  DEBUGBLOCK ("guash_build_inhibit_suffix_table...");

  chank = g_malloc (chank_size * sizeof (GCHAR *));

  while (*ptr != 0)
    {
      gint	index = 0;
      GCHAR	*new_str;

      while ((*(ptr + index) != ':') && (*(ptr + index) != 0))
	index++;

      if (0 < index)
	{
	  new_str = g_malloc (index + 1); /*  end of str */
	  memcpy (new_str, ptr, index);
	  new_str[index] = 0;

	  if (chank_size <= chank_index)
	    {
	      chank_size *= 2;
	      chank = g_realloc ((gpointer) chank,
				 chank_size * sizeof (GCHAR *));
	    }
	  chank[chank_index++] = new_str;
	}
      if (*(ptr + index) == 0)
	break;
      else
	ptr += index + 1;
    }

  inhibit_suffix_table = chank;
  num_inhibit_suffix = chank_index;

  DEBUGEND;
}

static void
guash_assure_scratch_buffer ()
{
  if (text_image < 0)
    {
      text_image = gimp_image_new (THUMBNAIL_WIDTH, THUMBNAIL_THEIGHT, RGB);
      gimp_image_disable_undo (text_image);
    }

  if (text_bg < 0)
    {
      text_bg = gimp_layer_new (text_image, "Background",
				THUMBNAIL_WIDTH, THUMBNAIL_THEIGHT,
				RGB_IMAGE, 100, NORMAL_MODE);
      gimp_image_add_layer (text_image, text_bg, 0);
    }
}

static void
guash_unref_scratch_buffer ()
{
  if (0 < text_image)
    {
      gimp_image_delete (text_image);
      text_bg = text_image = -1;
    }
}

static GList *
selection_reset ()
{
  if (! cwd_cache)
    return NULL;
  thumbnail_panel_clear_selection_frame ();
  g_list_free (selection_list);
  selection_list = NULL;
  thumbnail_panel_update_selection_buttons ();
  gtk_widget_queue_draw (thumbnail_panel);
  selection_kind = FALSE;

  return selection_list;
}

static GList *
selection_add (GINDEX index)
{
  switch (selection_kind)
    {
    case FALSE:
      selection_kind = IMAGE;
      break;
    case IMAGE:
      break;
    default:
      selection_reset ();
      selection_kind = IMAGE;
      break;
    }

  if (! selection_member_p (index))
    {
      selection_list = g_list_append (selection_list, (gpointer) index);
      thumbnail_panel_draw_selection_frame ();
      thumbnail_panel_update_selection_buttons ();
    }
  return selection_list;
}

static GList *
selection_delete (GINDEX index)
{
  switch (selection_kind)
    {
    case FALSE:
      selection_kind = IMAGE;
      break;
    case IMAGE:
      break;
    default:
      selection_reset ();
      selection_kind = IMAGE;
      break;
    }

  if (selection_member_p (index))
    {
      thumbnail_panel_clear_selection_frame ();
      selection_list = g_list_remove (selection_list, (gpointer) index);
      if (selection_is_active ())
	thumbnail_panel_draw_selection_frame ();
      else
	thumbnail_panel_set_info (NULL);
      thumbnail_panel_update_selection_buttons ();
    }
  return selection_list;
}

static gint
selection_is_active ()
{
  return (selection_list != NULL);
}

static gint
selection_length ()
{
  return g_list_length (selection_list);
}

static gint
selection_member_p (GINDEX index)
{
  return (g_list_find (selection_list, (gpointer) index) != NULL);
}

static gint
selection_reverse_member (GINDEX index)
{
  gint	flag = selection_member_p (index);

  switch (selection_kind)
    {
    case FALSE:
      selection_kind = IMAGE;
      break;
    case IMAGE:
      break;
    default:
      selection_reset ();
      selection_kind = IMAGE;
      break;
    }

  if (flag)
    selection_delete (index);
  else
    selection_add (index);
  return (! flag);
}

static void
selection_map_script ()
{
  gint		retvals;
  FILE		*file;
  GString	*script_file = NULL;
  GList		*l;
  GCHAR		buf[LINE_BUF_SIZE];

  if (getenv ("HOME") != NULL)
    {
      script_file = g_string_new (g_strdup (getenv ("HOME")));
      g_string_append (script_file, "/.gimp/scripts/");
    }
  else
    {
      struct passwd *pwd_ptr;

      /* the loser doesn't have $HOME set */
      setpwent ();

      pwd_ptr = getpwuid (getuid ());
      if(pwd_ptr)
	{
	  script_file = g_string_new (g_strdup (pwd_ptr->pw_dir));
	  g_string_append (script_file, "/.gimp/scripts/");
	}
    }
  if ((script_file == NULL) || (os_file_kind (script_file->str, TRUE) != DIRECTORY))
    {
      gtkW_message_dialog (TRUE, "Abort execution: you don't have ~/.gimp/scripts.");
      return;
    }

  thumbnail_panel_set_info ("Map script-fu: wait a moment...");

  g_string_append (script_file, GUASH_SELECTION_SCM);

  if ((file = fopen (script_file->str, "w")) == NULL)
    {
      sprintf (buf, " Failed to save to %s ", script_file->str);
      gtkW_message_dialog (TRUE, buf); /* info is too short to display filename */
      g_string_free (script_file, TRUE);
    }
  fprintf (file, ";; This file was generated by guash automatically.\n");
  fprintf (file, ";; You can delete me without trouble.\n\n");
  fprintf (file, "(define %%guash-selection\n");
  fprintf (file, "  '(\n");
  l = selection_list;
  while (l)
    {
      GINDEX		selected_id = (gint) l->data;
      cache_entry	*selected;
      gint		ppos, i;

      selected = directory_cache_get_nth (selected_id);
      fprintf (file,
	       "    (\"%s/%s\" \"%s\" \"",
	       cwd_cache->name, selected->name, /* full path */
	       cwd_cache->name /* directory that does not end with slash */
	       );

      ppos = file_get_last_period_index (selected->name);

      for (i = 0; i < ppos; i++)
	fprintf (file, "%c", selected->name[i]);

      fprintf (file, "\" \"");

      for (i = ppos + 1; i < strlen (selected->name); i++)
	fprintf (file, "%c", selected->name[i]);

      fprintf (file, "\")\n");

      l = l->next;
    }
  fprintf (file, "    ))\n");
  fclose (file);

  /* script_fu_refresh is a temp_proc which returns NULL always */
  gimp_run_procedure ("script_fu_refresh",
		      &retvals,
		      PARAM_INT32, RUN_NONINTERACTIVE,
		      PARAM_END);

  thumbnail_panel_set_info ("Map script-fu: start launchar...");

  /* And all script-fus return NULL always */
  gimp_run_procedure ("script-fu-map-on-guash-selection",
		      &retvals,
		      PARAM_INT32, RUN_INTERACTIVE,
		      PARAM_STRING, "",
		      PARAM_INT32, TRUE,
		      PARAM_INT32, FALSE,
		      PARAM_INT32, TRUE,
		      PARAM_END);
  g_string_free (script_file, TRUE);

  thumbnail_panel_set_info ("Map script-fu: finished");
}

static void
selection_map_unix_command ()
{
  GList	*l;
  GCHAR	template[LINE_BUF_SIZE];

  gtkW_query_box ("Guash: mapping unix command on selection",
		  "Type command to map on the selected image files. \n\
 \"{}\" in the command string is replaced to each filename without directory. \n\
 \"%%\" in the command string is replaced to the directory part of each file. \n\
 - SAMPLES -\n\
 rm .xvpics/{} \n\
 uuencode {} {} > /tmp/{}.uu; chmod go-r /tmp/{}.uu",
		  VAL.mapping_command, template);
  if (strlen (template) == 0)
    return;
  l = selection_list;
  while (l)
    {
      GCHAR		*command;
      GINDEX		selected_id = (gint) l->data;
      cache_entry	*selected;

      selected = directory_cache_get_nth (selected_id);
      command = generate_unix_command (template, cwd_cache->name, selected->name);
      if (command)
	{
	  system (command);
	  g_free (command);
	}
      l = l->next;
    }
  strcpy (VAL.mapping_command, template);
}

static GCHAR *
generate_unix_command (GCHAR *template, GCHAR *directory, GCHAR *filename)
{
  GCHAR	*new = NULL, *tmp;
  gint	index = -1;
  gint	directory_p = FALSE;
  gint	i = 0;

  for (tmp = template; *tmp; tmp++, i++)
    {
      if ((*tmp == '{') && (*(tmp + 1) == '}'))
	{
	  directory_p = FALSE;
	  index = i;
	  break;
	}
      if ((*tmp == '%') && (*(tmp + 1) == '%'))
	{
	  directory_p = TRUE;
	  index = i;
	  break;
	}
    }
  if (-1 < index)
    {
      if (directory_p)
	new = g_malloc (strlen (template) + strlen (directory) + 1);
      else
	new = g_malloc (strlen (template)
			+ strlen (directory) + strlen (filename) + 1);
      i = index;
      memcpy (new, template, i);
      if (directory_p)
	{
	  memcpy (new + i, directory, strlen (directory));
	  i += strlen (directory);
	  /* new[i++] = '/'; */
	}
      else
	{
	  memcpy (new + i, filename, strlen (filename));
	  i += strlen (filename);
	}
      memcpy (new + i, template + index + 2, strlen (template) - index - 2);
      i += strlen (template) - index - 2;
      new[i] = 0;

      tmp = generate_unix_command (new, directory, filename);
      g_free (new);
      return tmp;
    }
  else
    {
      return g_strdup (template);
    }
}

static void
selection_open_files ()
{
  GList	*list = selection_list;
  cache_entry	*selected;

  if (selection_kind != IMAGE)
    return;

  while (list)
    {
      GINDEX	selected_id = (gint) list->data;

      selected = directory_cache_get_nth (selected_id);
      cache_open_image_file (selected);

      list = list->next;
    }
}

static void
selection_copy_files_to (GCHAR *pathname)
{
  GCHAR	info[256], first_file[LINE_BUF_SIZE];
  gint	kind = NOT_EXIST;
  gint	success = 0;
  GList	*list = selection_list;

  if (selection_kind != IMAGE)
    return;

  while (list)
    {
      GINDEX	selected_id = (gint) list->data;

      kind = os_file_kind (pathname, TRUE);

      if ((kind == NOT_EXIST) || (kind == DIRECTORY))
	{
	  cache_entry	*selected;
	  GString	*name, *new;

	  selected = directory_cache_get_nth (selected_id);
	  name = g_string_new (cwd_cache->name);
	  g_string_append (name, "/");
	  g_string_append (name, selected->name);
	  new = g_string_new (pathname);
	  if (kind == DIRECTORY)
	    {
	      if (pathname [strlen (pathname) - 1] != '/')
		g_string_append (new, "/");
	      g_string_append (new, selected->name);
	    }
	  if (os_file_kind (new->str, TRUE) != NOT_EXIST)
	    {
	      sprintf (info, "%s already exists.", new->str);
	      gtkW_message_dialog (TRUE, info);
	    }
	  else if (image_file_copy (name->str, new->str) == TRUE)
	    {
	      if (success++ == 0)
		strcpy (first_file, selected->name);
	    }

	  g_string_free (name, TRUE);
	  g_string_free (new, TRUE);
	}
      else if (kind == REGFILE)
	{
	  sprintf (info, "%s already exists.", pathname);
	  gtkW_message_dialog (TRUE, info);
	}
      list = list->next;
    }
  if ((0 < success) && (kind == DIRECTORY))
    directory_cache_force_to_purge (pathname);

  selection_reset ();

  if (0 < success)
    directory_cache_set_to_cwd (TRUE);	/* renaming in a dir requires TRUE */

  if (0 < success)
    {
      GCHAR	buffer[2 * LINE_BUF_SIZE];

      if (success == 1)
	sprintf (buffer, "%s was copied to %s.", first_file, pathname);
      else
	sprintf (buffer, "%d files were copied to %s.", success, pathname);
      thumbnail_panel_set_info (buffer);
    }
  else
    thumbnail_panel_set_info ("Failed to copy.");
}

static void
selection_delete_files ()
{
  GList		*list = selection_list;
  GCHAR		first_file[LINE_BUF_SIZE];
  gint		success = 0;

  if (selection_kind != IMAGE)
    return;

  while (list)
    {
      GINDEX		selected_id = (gint) list->data;
      cache_entry	*selected;
      GString		*name;

      selected = directory_cache_get_nth (selected_id);
      name = g_string_new (g_strdup (cwd_cache->name));
      g_string_append (name, "/");
      g_string_append (name, selected->name);
      if (unlink (name->str) == 0)
	{
	  GCHAR *cache_name;

	  cache_name = file_build_cache_file_name (name->str);
	  unlink (cache_name);
	  g_free (cache_name);
	  if (success++ == 0)
	    strcpy (first_file, selected->name);
	}
      g_string_free (name, TRUE);
      selected->deleted = TRUE;

      list = list->next;
    }
  selection_reset ();
  directory_cache_set_to_cwd (GC);

  if (0 < success)
    {
      GCHAR	buffer[2 * LINE_BUF_SIZE];

      if (success == 1)
	sprintf (buffer, "%s was deleted.", first_file);
      else
	sprintf (buffer, "%d files were deleted.", success);
      thumbnail_panel_set_info (buffer);
    }
  else
    thumbnail_panel_set_info ("Failed to delete (permission problem?).");
}

static void
selection_move_files_to (GCHAR *pathname)
{
  GCHAR	info[256], first_file[LINE_BUF_SIZE];
  gint	kind = NOT_EXIST;
  gint	success = 0;
  GList	*list = selection_list;

  if (selection_kind != IMAGE)
    return;
  kind = os_file_kind (pathname, TRUE);
  if ((1 < selection_length ()) && (kind != DIRECTORY))
    {
      gtkW_message_dialog (TRUE, "The destination should be a directory");
      return;
    }

  while (list)
    {
      GINDEX	selected_id = (gint) list->data;

      kind = os_file_kind (pathname, TRUE); /* might be overwrited */
      if ((kind == NOT_EXIST) || (kind == DIRECTORY))
	{
	  cache_entry	*selected;
	  GString	*name, *new;

	  selected = directory_cache_get_nth (selected_id);
	  name = g_string_new (g_strdup (cwd_cache->name));
	  g_string_append (name, "/");
	  g_string_append (name, selected->name);
	  new = g_string_new (g_strdup (pathname));

	  if (kind == DIRECTORY)
	    {
	      if (pathname [strlen (pathname) - 1] != '/')
		g_string_append_c (new, '/');
	      g_string_append (new, selected->name);
	    }
	  if (image_file_move (name->str, new->str))
	    {
	      selected->deleted = TRUE;
	      if (success++ == 0)
		strcpy (first_file, selected->name);
	    }
	  g_string_free (name, TRUE);
	  g_string_free (new, TRUE);
	}
      else if (kind == REGFILE)
	{
	  sprintf (info, "%s already exists.", pathname);
	  gtkW_message_dialog (TRUE, info);
	}
      list = list->next;
    }
  selection_reset ();

  if (0 < success)
    {
      if ((kind == DIRECTORY) && (strcmp (cwd_cache->name, pathname) != 0))
	{
	  directory_cache_force_to_purge (pathname);
	  directory_cache_set_to_cwd (GC);
	}
      else
	directory_cache_set_to_cwd (TRUE);
    }

  if (0 < success)
    {
      GCHAR	buffer[2 * LINE_BUF_SIZE];

      if (success == 1)
	sprintf (buffer, "%s was moved to %s.", first_file, pathname);
      else
	sprintf (buffer, "%d files were moved to %s.", success, pathname);
      thumbnail_panel_set_info (buffer);
    }
  else
    thumbnail_panel_set_info ("Failed to move (permission problem?).");
}

static gint
thumbnail_panel_show_cache (cache_entry *cache)
{
  gint	y;
  gint	sx, sy;
  gint	ox, oy;

  thumbnail_panel_last_index++;
  if (nthumbnails_in_page <= thumbnail_panel_last_index)
    return FALSE;
  sx = INDEX_TO_X (thumbnail_panel_last_index);
  sy = INDEX_TO_Y (thumbnail_panel_last_index);
  ox = CLAMP ((THUMBNAIL_WIDTH - cache->width) / 2, 0, thumbnail_panel_width);

  if (cache->directory_p)
    {
      ox = (THUMBNAIL_WIDTH - directory_icon_width) / 2;
      oy = (THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT - directory_icon_height) / 2;
      /* reder from directory_icon */
      for (y = 0; y < THUMBNAIL_HEIGHT; y++)
	gtk_preview_draw_row (GTK_PREVIEW (thumbnail_panel),
			      directory_icon + (y * directory_icon_width * 3),
			      sx + ox, sy + y + oy,
			      directory_icon_width);
      /* render its name */
      ox = CLAMP ((THUMBNAIL_WIDTH - cache->width) / 2, 0, thumbnail_panel_width);
      oy = THUMBNAIL_HEIGHT;
      for (y = 0; y < THUMBNAIL_THEIGHT; y++)
	gtk_preview_draw_row (GTK_PREVIEW (thumbnail_panel),
			      cache->data + (y * cache->width * 3),
			      sx + ox, sy + y + oy,
			      CLAMP (cache->width, 0, THUMBNAIL_WIDTH));
    }
  else
    {
      oy = (THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT - cache->height) / 2;
      for (y = 0; y < cache->height; y++)
	gtk_preview_draw_row (GTK_PREVIEW (thumbnail_panel),
			      cache->data + (y * cache->width * 3),
			      sx + ox, sy + y + oy,
			      /* CLAMP (cache->width, 0, THUMBNAIL_WIDTH) */
			      cache->width);
    }
  {
    GdkRectangle current_row;

    current_row.x = sx;
    current_row.y = sy;
    current_row.width = THUMBNAIL_WIDTH;
    current_row.height = THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT;
    gtk_widget_draw (thumbnail_panel, &current_row);
  }
  return TRUE;
}

static void
thumbnail_panel_banner ()
{
  guchar	data[3] = { 255, 255, 255 };
  gchar		*val = banner_data;
  guchar	*banner;
  gint		x, y;
  gint		sx, sy;
  gint		width, hight;

  DEBUGBLOCK ("thumbnail_panel_banner...");

  for (y = 0; y < thumbnail_panel_height; y++)
    for (x = 0; x < thumbnail_panel_width; x++)
      gtk_preview_draw_row (GTK_PREVIEW (thumbnail_panel), data, x, y, 1);

  banner = g_malloc (banner_height * banner_width * 3);
  for (y = 0; y < banner_height ; y++)
    for (x = 0; x < banner_width; x++)
      {				/*  don't delete this blaket! */
	HEADER_PIXEL (val, (banner + (y * banner_width + x) * 3));
      }
  sx = MAX (0, ((thumbnail_panel_width - banner_width) / 2));
  sy = MAX (0, ((thumbnail_panel_height - banner_height) * 2 / 3));
  width = MIN (thumbnail_panel_width, banner_width);
  hight = MIN (thumbnail_panel_height, banner_height);
  for (y = 0; y < hight; y++)
    gtk_preview_draw_row (GTK_PREVIEW (thumbnail_panel),
			  banner + (y * banner_width * 3),
			  sx,  sy + y,
			  width);
  g_free (banner);

  DEBUGEND;
}

static void
thumbnail_panel_clear ()
{
  gint	x, y;
  guchar	data[3] = { 255, 255, 255 };

  for (y = 0; y < thumbnail_panel_height; y++)
    for (x = 0; x < thumbnail_panel_width; x++)
      gtk_preview_draw_row (GTK_PREVIEW (thumbnail_panel), data, x, y, 1);
  gtk_widget_draw (thumbnail_panel, NULL);
}

static void
thumbnail_panel_set_directory_info ()
{
  GCHAR	title[LINE_BUF_SIZE];
  GCHAR	*tmp = NULL;
  gint	maxlen = DIRECTORY_LABEL_MAXLEN;
  gint	from = 0;

  tmp = file_get_filename (cwd_cache->name);
  sprintf (title, "%s: %s", SHORT_NAME, tmp);
  gtk_window_set_title (GTK_WINDOW (dlg), title);
  g_free (tmp);

  thumbnail_panel_page1 = cwd_cache->display_page + 1;

  if (1 < directory_cache_npage ())
    maxlen -= 7;
  if (maxlen < strlen (cwd_cache->name))
    from = strlen (cwd_cache->name) - maxlen;

  if (1 < directory_cache_npage ())
    sprintf (thumbnail_panel_directory_str,
	     "%s%s (%d/%d by %s) ",
	     (from == 0) ? " " : "*",
	     cwd_cache->name + from,
	     cwd_cache->display_page + 1,
	     directory_cache_npage (),
	     ((VAL.sort_type == SORT_BY_NAME) ? "name" : "date"));
  else
    sprintf (thumbnail_panel_directory_str,
	     "%s%s (by %s)",
	     (from == 0) ? " " : "*",
	     cwd_cache->name + from,
	     ((VAL.sort_type == SORT_BY_NAME) ? "name" : "date"));
  gtk_label_set (GTK_LABEL (cwd_label), thumbnail_panel_directory_str);
  gtk_label_set_justify (GTK_LABEL (cwd_label), GTK_JUSTIFY_FILL);
}

static void
thumbnail_panel_set_info (GCHAR *name)
{
  if (name == NULL)
    gtk_label_set (GTK_LABEL (file_property), thumbnail_panel_info_default);
  else
    {
      if (THUMBNAIL_INFO_MAXLEN < strlen (name))
	{
	  GCHAR	*str = NULL;

	  str = (GCHAR *) g_malloc (THUMBNAIL_INFO_MAXLEN + 1);
	  memcpy (str, name, THUMBNAIL_INFO_MAXLEN);
	  str[THUMBNAIL_INFO_MAXLEN] = 0;
	  gtk_label_set (GTK_LABEL (file_property), str);
	  g_free (str);
	}
      else
	gtk_label_set (GTK_LABEL (file_property), name);
    }
  gtk_label_set_justify (GTK_LABEL (file_property), GTK_JUSTIFY_FILL);
  /* Since info field should be updated immediately, I use gtk_widget_draw ()
     instead of gtk_widget_queue_draw () here. */
  gtk_widget_draw (file_property, NULL);
  gdk_flush ();
}

static void
thumbnail_panel_set_info_default ()
{
  if (! cwd_cache)
    strcpy (thumbnail_panel_info_default, "");

  if (1 == cwd_cache->ndir)
    sprintf (thumbnail_panel_info_default, "%d %s",
	     cwd_cache->nimage,
	     ((1 < cwd_cache->nimage) ? "images" : "image"));
  else
    sprintf (thumbnail_panel_info_default, "%d %s and %d %s",
	     cwd_cache->nimage,
	     ((1 < cwd_cache->nimage) ? "images" : "image"),
	     (cwd_cache->ndir - 1),
	     ((2 < cwd_cache->ndir) ? "subdirectories" : "subdirectory"));
}

static void
thumbnail_panel_draw_frame (GINDEX index, guchar val)
{
  gint	x, y;
  gint	xi, yi;
  guchar data[3];
  gint	border = 2;

  index -= cwd_cache->display_page * nthumbnails_in_page;
  x = INDEX_TO_X (index);
  y = INDEX_TO_Y (index);
  data[0] = data[1] = data[2] = val;
  data[0] = 255;

  /* draw top */
  for (yi = y - border; yi < y; yi++)
    for (xi = x - border; xi < x + THUMBNAIL_WIDTH + border; xi++)
      gtk_preview_draw_row (GTK_PREVIEW (thumbnail_panel), data, xi, yi, 1);

  /* draw left */
  for (yi = y - border;
       yi < y + THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT + border;
       yi++)
    for (xi = x - border; xi < x; xi++)
      gtk_preview_draw_row (GTK_PREVIEW (thumbnail_panel), data, xi, yi, 1);
  /* draw right */
  for (yi = y - border;
       yi < y + THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT + border;
       yi++)
    for (xi = x + THUMBNAIL_WIDTH; xi < x + THUMBNAIL_WIDTH + border; xi++)
      gtk_preview_draw_row (GTK_PREVIEW (thumbnail_panel), data, xi, yi, 1);
  /* draw bottom */
  for (yi = y + THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT;
       yi < y + THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT + border;
       yi++)
    for (xi = x - border; xi < x + THUMBNAIL_WIDTH + border; xi++)
      gtk_preview_draw_row (GTK_PREVIEW (thumbnail_panel), data, xi, yi, 1);
}

static void
thumbnail_panel_draw_selection_frame ()
{
  GList	*tmp = selection_list;
  GINDEX	id;

  while (tmp)
    {
      id = (GINDEX) tmp->data;
      if ((cwd_cache->display_page * nthumbnails_in_page <= id)
	  && (id < (cwd_cache->display_page + 1) * nthumbnails_in_page))
	thumbnail_panel_draw_frame (id, 0);
      tmp = tmp->next;
    }
  gtk_widget_queue_draw (thumbnail_panel);
}

static void
thumbnail_panel_clear_selection_frame ()
{
  GList	*tmp = selection_list;
  GINDEX	id;

  while (tmp)
    {
      id = (GINDEX) tmp->data;
      if ((cwd_cache->display_page * nthumbnails_in_page <= id)
	  && (id < (cwd_cache->display_page + 1) * nthumbnails_in_page))
	thumbnail_panel_draw_frame (id, 255);
      tmp = tmp->next;
    }
  gtk_widget_queue_draw (thumbnail_panel);
}

static void
thumbnail_panel_size_request (GtkWidget      *widget,
			 GtkRequisition *requisition)
{
  gint	ncol, nrow;

  DPRINT ("checking invocation condition of thumbnail_panel_size_request");

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PREVIEW (widget));
  g_return_if_fail (requisition != NULL);

  DEBUGBLOCK ("thumbnail_panel_size_request...");

  if (! cwd_cache)
    {
      ncol = ncol_of_thumbnail_default;
      nrow = nrow_of_thumbnail_default;
    }
  else
    {
      ncol = NCOL_OF_THUMBNAIL_MIN;
      nrow = NROW_OF_THUMBNAIL_MIN;
    }
  requisition->width = COL2WIDTH (ncol);
  requisition->height = ROW2HEIGHT (nrow);

  DEBUGEND;
}

static void
thumbnail_panel_size_allocate (GtkWidget     *widget,
			       GtkAllocation *allocation)
{
  DPRINT ("checking invocation condition of thumbnail_panel_size_allocate");

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PREVIEW (widget));
  g_return_if_fail (allocation != NULL);

  DEBUGBLOCK ("thumbnail_panel_size_allocate...");

  widget->allocation = *allocation;

  if ((thumbnail_panel_width != widget->allocation.width)
      || (thumbnail_panel_height != widget->allocation.height))
    thumbnail_panel_resized = TRUE;

  thumbnail_panel_width = widget->allocation.width;
  thumbnail_panel_height = widget->allocation.height;

  ncol_of_thumbnail = WIDTH2COL (thumbnail_panel_width);
  nrow_of_thumbnail = HEIGHT2ROW (thumbnail_panel_height);
  nthumbnails_in_page = ncol_of_thumbnail * nrow_of_thumbnail;

  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
			      widget->allocation.x,
			      widget->allocation.y,
			      thumbnail_panel_width,
			      thumbnail_panel_height);

      if (cwd_cache)
	if (((GtkAdjustment *) widget_pointer[0].widget)->upper != directory_cache_npage () + 1)
	  {
	    thumbnail_panel_page1 = directory_cache_npage () + 1;
	    ((GtkAdjustment *) widget_pointer[0].widget)->upper
	      = directory_cache_npage () + 1;
	  }
    }
  gtk_preview_set_expand (GTK_PREVIEW (thumbnail_panel), TRUE);
  gtk_preview_size (GTK_PREVIEW (thumbnail_panel),
		    thumbnail_panel_width,
		    thumbnail_panel_height);

  DEBUGEND;
}

/* this function does not check the current size of thumbnail_panel */
static gint
thumbnail_panel_update ()
{
  GINDEX	index, max;

  if (! cwd_cache)
    return FALSE;

  /* from 0.99.5, this function must also check the page number, since resize
     changes the number of pages. */
  cwd_cache->display_page = CLAMP (cwd_cache->display_page,
				   0,
				   (directory_cache_npage () - 1));
  thumbnail_panel_page1 = cwd_cache->display_page + 1;
  thumbnail_panel_set_directory_info ();
  thumbnail_panel_clear ();
  gtkW_preview_force_to_update (thumbnail_panel);

  max = MIN ((1 + cwd_cache->display_page)*nthumbnails_in_page,
	     cwd_cache->ndir + cwd_cache->nimage);

  thumbnail_panel_last_index = -1;
  for (index = cwd_cache->display_page * nthumbnails_in_page; index < max; index++)
    thumbnail_panel_show_cache (directory_cache_get_nth (index));

  thumbnail_panel_set_info_default ();
  if (selection_is_active ())
    thumbnail_panel_draw_selection_frame ();
  else
    thumbnail_panel_set_info (NULL);
  thumbnail_panel_update_sensitive_menu ();
  gdk_flush ();
  return TRUE;
}

static void
thumbnail_panel_update_sensitive_menu ()
{
  thumbnail_panel_update_selection_buttons ();
  thumbnail_panel_update_scroller ();
}

static void
thumbnail_panel_update_selection_buttons ()
{
  gint	ncommand =
    sizeof (image_selection_commands) / sizeof (image_selection_commands[0]);
  gint	flag = selection_is_active ();
  gint	i;

  for (i = 0; i < ncommand; i++)
    if (image_selection_commands[i].sensitive & SELECTION)
      gtk_widget_set_sensitive (image_selection_commands[i].widget, flag);
  for (i = 0 ; i < NSELECTION_BUTTON; i++)
    gtk_widget_set_sensitive (widget_for_selecion[i], flag);

  gtk_widget_dnd_drag_set (widget_for_selecion[0], flag, possible_dnd_types, 1);
}

static void
thumbnail_panel_update_scroller ()
{
  gint	ncommand = sizeof (image_root_commands) / sizeof (image_root_commands[0]);
  gint	i;
  gint	flags[2];

  if (! cwd_cache)
    return;
  if ((widget_pointer[0].widget == NULL) || (widget_pointer[0].updater == NULL))
    return;

  flags[0] = flags[1] = TRUE;	/* 0 for back(prev), 1 for next */
  if (cwd_cache->display_page == 0)
    flags[0] = FALSE;
  if (directory_cache_npage () <= 1 + cwd_cache->display_page)
    flags[1] = FALSE;

  {
    GCHAR buffer[GTKW_ENTRY_BUFFER_SIZE];

    sprintf (buffer, "%d", thumbnail_panel_page1);
    gtk_entry_set_text (GTK_ENTRY (widget_for_scroll[1]), buffer);
  }

  ((GtkAdjustment *) widget_pointer[0].widget)->value = thumbnail_panel_page1;
  if (((GtkAdjustment *) widget_pointer[0].widget)->upper != directory_cache_npage () + 1)
    {
      ((GtkAdjustment *) widget_pointer[0].widget)->upper
	= directory_cache_npage () + 1;
      /* From 0.99.5 changes of thumbnail_panel_page1 propagates here. */
      gtk_signal_emit_by_name (GTK_OBJECT ((GtkAdjustment *) widget_pointer[0].widget), "changed");
    }
    /* The following statement is still required to update scroll-bar. */
  (widget_pointer[0].updater) (widget_pointer);

  for (i = 0; i < NSCROLLER; i++)
    {
      gtk_widget_set_sensitive (widget_for_scroll[i],
				(1 < directory_cache_npage ()));
      gtk_widget_queue_draw (widget_for_scroll[i]);
    }
  {
    gint	j = 1;

    for (i = 0; i < ncommand; i++)
      if (image_root_commands[i].sensitive & SCROLL)
	{
	  gtk_widget_set_sensitive (image_root_commands[i].widget, flags[j]);
	  j = 0;
	}
  }
}

#ifdef GTK_HAVE_ACCEL_GROUP
static void
thumbnail_panel_create_menu_for (GtkWidget *menu,
				 image_command_table *commands,
				 gint ncommand,
				 GtkAccelGroup *accelerator_table)
#else
static void
thumbnail_panel_create_menu_for (GtkWidget *menu,
				 image_command_table *commands,
				 gint ncommand,
				 GtkAcceleratorTable *accelerator_table)
#endif
{
  GtkWidget	*menu_item;
  gint	i;

#ifdef GTK_HAVE_ACCEL_GROUP
  gtk_menu_set_accel_group (GTK_MENU (menu), accelerator_table);
#else
  gtk_menu_set_accelerator_table (GTK_MENU (menu), accelerator_table);
#endif
  for (i = 0; i < ncommand; i++)
    {
      if ((commands[i].sensitive == HIDDEN) && (! hidden_features))
	continue;
      if (commands[i].label)
	menu_item = gtk_menu_item_new_with_label (commands[i].label);
      else
	menu_item = gtk_menu_item_new ();
      commands[i].widget = menu_item;
      gtk_menu_append (GTK_MENU (menu), menu_item);
      gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
			  (GtkSignalFunc) commands[i].command,
			  (gpointer) commands[i].label);
      if (commands[i].label
	  && (commands[i].binding[binding_style].key != 0))
#ifdef GTK_HAVE_ACCEL_GROUP
	gtk_widget_add_accelerator (menu_item,
				    "activate",
				    accelerator_table,
				    commands[i].binding[binding_style].key,
				    commands[i].binding[binding_style].mod,
				    GTK_ACCEL_VISIBLE);
#else
	gtk_widget_install_accelerator (menu_item,
					accelerator_table,
					"activate",
					commands[i].binding[binding_style].key,
					commands[i].binding[binding_style].mod);
#endif      

      gtk_widget_show (menu_item);
      if (! commands[i].label)	/* separator should not focus on */
	gtk_widget_set_sensitive (menu_item, FALSE);
    }
#ifdef GTK_HAVE_ACCEL_GROUP
  gtk_window_add_accel_group (GTK_WINDOW (dlg),accelerator_table);
#else
  gtk_window_add_accelerator_table (GTK_WINDOW (dlg),accelerator_table);
#endif
}

static void
thumbnail_panel_create_menu ()
{
#ifdef GTK_HAVE_ACCEL_GROUP
  GtkAccelGroup	*accelerator_table = gtk_accel_group_new ();
#else
  GtkAcceleratorTable	*accelerator_table = gtk_accelerator_table_new ();
#endif
  gint	modified = FALSE;

  DEBUGBLOCK ("Create menus...");
  if (! thumbnail_panel_root_menu)
    {
      thumbnail_panel_root_menu = gtk_menu_new ();
      thumbnail_panel_create_menu_for (thumbnail_panel_root_menu,
				       image_root_commands,
				       sizeof (image_root_commands)
				       / sizeof (image_root_commands[0]),
				       accelerator_table);
      modified = TRUE;
    }
  if (! thumbnail_panel_selection_menu)
    {
      thumbnail_panel_selection_menu = gtk_menu_new ();
      thumbnail_panel_create_menu_for (thumbnail_panel_selection_menu,
				       image_selection_commands,
				       sizeof (image_selection_commands)
				       / sizeof (image_selection_commands[0]),
				       accelerator_table);
      modified = TRUE;
    }
  if ((! thumbnail_panel_hidden_menu) && hidden_features)
    {
      thumbnail_panel_hidden_menu = gtk_menu_new ();
      thumbnail_panel_create_menu_for (thumbnail_panel_hidden_menu,
				       image_hidden_commands,
				       sizeof (image_hidden_commands)
				       / sizeof (image_hidden_commands[0]),
				       accelerator_table);
      modified = TRUE;
    }
  if (modified)
#ifdef GTK_HAVE_ACCEL_GROUP
    gtk_window_add_accel_group (GTK_WINDOW (dlg),accelerator_table);
#else
    gtk_window_add_accelerator_table (GTK_WINDOW (dlg),accelerator_table);
#endif
  DEBUGEND;
}

static gint
thumbnail_panel_move_focus (gint offset)
{
  GINDEX	index = -1;
  GINDEX	new_index = -1;
  gint	new_page = cwd_cache->display_page;
  cache_entry *cache;

  if (selection_is_active ())
    {
      GList *last = g_list_last (selection_list);

      index = (GINDEX) last->data;
      new_index = index + offset;
    }
  else
    if (0 < cwd_cache->nimage)
      {
	if (0 < offset)
	  new_index = MAX ((cwd_cache->display_page * nthumbnails_in_page),
			   cwd_cache->ndir);
	else
	  new_index = MIN ((1 + cwd_cache->display_page) * nthumbnails_in_page -1,
			   (cwd_cache->ndir + cwd_cache->nimage - 1));
      }
  if ((cwd_cache->ndir <= new_index)
      && (new_index < cwd_cache->ndir + cwd_cache->nimage))
    {
      if (0 < index)
	selection_delete (index);
      new_page = new_index / (ncol_of_thumbnail * nrow_of_thumbnail);
      if (new_page != cwd_cache->display_page)
	{
	  cwd_cache->display_page = new_page;
	  thumbnail_panel_update ();
	}
      selection_add (new_index);
      cache = directory_cache_get_nth (new_index);
      thumbnail_panel_set_info (cache->info);
    }
  else
    thumbnail_panel_set_info (NULL);

  thumbnail_panel_update_sensitive_menu ();
  return new_index;
}

/* purge is
    TRUE: to build w/o current information
    FALSE: only to update panel
    GC: to remove unexisting files from panel
*/
directory_cache *
directory_cache_set_to_cwd (gint purge)
{
  gpointer	ptr = NULL;
#ifdef __USE_GNU
  GCHAR		*dir = get_current_dir_name ();
#else
  GCHAR		dir[LINE_BUF_SIZE];

  getcwd (dir, LINE_BUF_SIZE - 1);
#endif

  if ((ptr = g_hash_table_lookup (directory_cache_table, (gpointer) dir)))
    {
      cwd_cache = (directory_cache *) ptr;

      if ((purge == TRUE) || (cwd_cache->purged))
	{
	  cwd_cache->purged = FALSE;
	  directory_cache_update ();
	}
      else if (purge == GC)
	{
	  directory_cache_garbage_collect ();
	}
      else
	{
	  selection_reset ();
	  thumbnail_panel_update ();
	}
    }
  else
    {
#ifdef STRICT_MM
      if ((0 < directory_cache_table_max_size) &&
	  (directory_cache_table_max_size <= directory_cache_table_size))
	{
	  directory_cache	*tmp = NULL;

	  /* First, set cwd_cache to NULL.
	   */
	  cwd_cache = NULL;
	  /* Second: map recycler.
	   * Recycler set one of the oldest entries to cwd_cache
	   * and set the 1st entry in directory hashtable to tmp.
	   */
	  g_hash_table_foreach (directory_cache_table,
				directory_cache_table_recycler,
				(gpointer) &tmp);
	  if (cwd_cache)	/* recycler worked fine, finding an entry! */
	    {
	      /* Then, delete it from directory_cache_table */
	      g_hash_table_remove (directory_cache_table, cwd_cache->name);
	      /* Finally, set the name as the new key */
	      strcpy (cwd_cache->name, dir);
	      cwd_cache->birth_index = directory_cache_table_max_size;
	    }
	  else			/* should not happen! FAILSAFE path */
	    {
	      if (tmp == NULL)
		{
		  printf ("CATASTROPHIC ERROR: CACHE RECYCLE FAILED!!!\n");
		  cwd_cache = directory_cache_new (dir);
		}
	      else
		{
		  printf ("Warning: cache recycle failed (%s)!\n", tmp->name);
		  cwd_cache = tmp;
		  /* tmp is born again as cwd_cache */
		  cwd_cache->birth_index = directory_cache_table_size;
		}
	    }
	}
      else
	{
	  if (0 < directory_cache_table_max_size)
	    directory_cache_table_size++;

	  cwd_cache = directory_cache_new (dir);
	}
#else
      cwd_cache = directory_cache_new (dir);
#endif
      g_hash_table_insert (directory_cache_table,
			   cwd_cache->name,
			   (gpointer) cwd_cache);

      directory_cache_update ();
    }
  strcpy (VAL.last_dir_name, cwd_cache->name);
  return cwd_cache;
}

#ifdef STRICT_MM
static void
directory_cache_table_recycler (gpointer key,
				gpointer value,
				gpointer user_data)
{
  directory_cache *cache, **fail_safe;

  cache = (directory_cache *) value;
  fail_safe = (directory_cache **)user_data;

  /* This is for fail safe */
  if ((cwd_cache == NULL) && (*fail_safe == NULL))
    *fail_safe = cache;

  /* set the oldest entry to cwd_cache */
  if ((--(cache->birth_index) == 0) && (cwd_cache == NULL))
    cwd_cache = (directory_cache *) value;
}
#endif

directory_cache *
directory_cache_new (GCHAR *name)
{
  GCHAR		*cache_dir;
  directory_cache *p;

  p = (directory_cache *) g_malloc (sizeof (directory_cache));
  strcpy (p->name, name);
#ifdef STRICT_MM
  p->birth_index = directory_cache_table_size;
#endif

  cache_dir = file_build_cache_directory_name (name);
  switch (os_file_kind (cache_dir, TRUE))
    {
    case NOT_EXIST:
      p->savable = UNKNOWN;
      p->filed = FALSE;
      break;
    case DIRECTORY:
      p->savable = TRUE;
      p->filed = TRUE;
      break;
    default:
      p->savable = FALSE;
      p->filed = FALSE;
      break;
    }
  g_free (cache_dir);

  p->max_nimage = 16;
  p->image = (cache_entry *) g_malloc (p->max_nimage * sizeof (cache_entry));
  memset (p->image, 0, p->max_nimage * sizeof (cache_entry));
  p->nimage = 0;

  p->max_ndir = 2;
  p->dir = (cache_entry *) g_malloc (p->max_ndir * sizeof (cache_entry));
  memset (p->dir, 0, p->max_ndir * sizeof (cache_entry));
  p->ndir = 0;

  p->purged = FALSE;
  p->display_page = 0;
  return p;
}

static gint
directory_cache_npage ()
{
  gint	page;

  page = (cwd_cache->ndir + cwd_cache->nimage) / nthumbnails_in_page;
  if ((cwd_cache->ndir + cwd_cache->nimage) % nthumbnails_in_page != 0)
    page++;
  return page;
}

cache_entry *
directory_cache_get_nth (GINDEX index)
{
  if (index < cwd_cache->ndir)
    return (cwd_cache->dir) + index;
  else
    return (cwd_cache->image) + (index - cwd_cache->ndir);
}

static gint
directory_cache_update ()
{
  /* side-effect: selection is reset  */
  guchar	old_fg[3], old_bg[3];
  GCHAR		path[LINE_BUF_SIZE], counter[256], *fname;
  gint		index_to_update;
  gint		index = 0;
  GCHAR		**filename_list;
  gint		nentry;
#ifdef DELETE_CORE
  gint		no_core;
#endif

  gimp_palette_get_foreground (old_fg, old_fg + 1, old_fg + 2);
  gimp_palette_get_background (old_bg, old_bg + 1, old_bg + 2);

  cache_set_to_normal_color ();

  if (VAL.last_dir_name[0] != 0)
    {
      thumbnail_panel_clear ();
      gtkW_preview_force_to_update (thumbnail_panel);
    }
  if (cwd_cache->filed)
    gtk_window_set_title (GTK_WINDOW (dlg),
			  "Guash: scanning with cache files...");
  else
    gtk_window_set_title (GTK_WINDOW (dlg),
			  "Guash: wait a minute...");

  selection_reset ();
  cwd_cache->nimage = cwd_cache->ndir = 0;
  index_to_update = (1 + cwd_cache->display_page) * nthumbnails_in_page;

  guash_assure_scratch_buffer ();
  DEBUGBLOCK ("Register parent directory...");
  directory_cache_add_directory ("..", DIRECTORY);
  DEBUGEND;

  nentry = os_scandir (cwd_cache->name, &filename_list, &os_scandir_selector,
		       VAL.sort_type);
  /* For displaying eagarly, I use two pass scanning. */
  thumbnail_panel_set_info ("Scanning subdirectories...");
  gdk_flush ();

#ifdef DELETE_CORE
  /* before the traverse, check the existance of core */
  DEBUGBLOCK ("Checking core...");
  no_core = (os_file_kind ("core", TRUE) == NOT_EXIST);
  DEBUGEND;
#endif

  /* 1st path: for collecting directories */
  DEBUGBLOCK ("Scanning directory 1st path...");
  for (index = 0; index < nentry; index++)
    {
      gint	f_kind = NOT_EXIST;

      fname = filename_list[index];
      if (fname[0] == '.')
	continue;
      f_kind = os_file_kind (fname, FALSE);

      switch (f_kind)
	{
	case DIRECTORY:
	case SLNKDIR:
	  directory_cache_add_directory (fname, f_kind);
	  break;
	default:
	  break;
	}
    }
  if (index_to_update < cwd_cache->ndir)
    {
      thumbnail_panel_update ();
      gtkW_preview_force_to_update (thumbnail_panel);
      /* stop further update */
      index_to_update = -1;
    }
  DEBUGEND;
  guash_unref_scratch_buffer ();

  /* 2nd path: for loading image files */
  DEBUGBLOCK ("Scanning directory 2nd path...");
  for (index = 0; index < nentry; free (filename_list[index]), index++)
    {
      gint	f_kind = NOT_EXIST;

      sprintf (counter, "Reading the directory (%d/%d)... ", index, nentry);
      thumbnail_panel_set_info (counter);
      gdk_flush ();

      fname = filename_list[index];
      if (fname[0] == '.')
	continue;
      if (file_match_inhibit_suffix (fname))
	continue;

      sprintf (path, "%s/%s", cwd_cache->name, fname);
      f_kind = os_file_kind (path, FALSE);

      switch (f_kind)
	{
	case REGFILE:
	case SLNKFILE:
	  {
	    gint32	i_id;

	    if ((i_id = cache_get_image (fname, f_kind)) < 0)
	      continue;

	    directory_cache_add_image (fname, i_id, f_kind);
	    gimp_image_delete (i_id);
	  }
	  break;
	default:
	  break;
	}
      if (index_to_update == cwd_cache->ndir + cwd_cache->nimage)
	{
	  thumbnail_panel_update ();
	  gtkW_preview_force_to_update (thumbnail_panel);
	  /* if the next entry is non-directory/image, another update is
	     occurred, unless reset index_to_update. */
	  index_to_update = -1;
	}
    }
  free (filename_list);
  DEBUGEND;

#ifdef DELETE_CORE
  if (no_core && (os_file_kind ("core", TRUE) == REGFILE))
    {
      unlink ("core");
    }
#endif

  gimp_palette_set_foreground (old_fg[0], old_fg[1], old_fg[2]);
  gimp_palette_set_background (old_bg[0], old_bg[1], old_bg[2]);

  if (directory_cache_npage () <= cwd_cache->display_page)
    {
      cwd_cache->display_page = 0;
      thumbnail_panel_update ();
    }
  else if (cwd_cache->ndir + cwd_cache->nimage < index_to_update)
    thumbnail_panel_update ();
  else
    {
      /* Then thumbnail_panel doesn't know the true number of dir & image. */
      thumbnail_panel_set_directory_info ();
      thumbnail_panel_set_info_default ();
      thumbnail_panel_update_sensitive_menu ();
      if ((0 < directory_cache_max_size)
	  && (directory_cache_max_size < cwd_cache->ndir + cwd_cache->nimage))
	{
	  GCHAR	buffer[256];

	  sprintf (buffer,
		   "Warning: # of files is beyond the display limit(%d)",
		   directory_cache_max_size);
	  thumbnail_panel_set_info (buffer);
	}
      else
	thumbnail_panel_set_info (NULL);
    }
  {
    gint	flag = FALSE;
    GdkEvent	*e;

    /*
     * Tue May 12 20:49:27 1998
     * gtk+ FAQ uses gtk_events_pending (), but it returns 1 anytime.
     * On the other hand, gdk_events_pending () returns zero if no events!
     * That's what I want!
     */
    DEBUGBLOCK ("directory_cache_update: filtering events...");
    while (gdk_events_pending ())
      {
	if ((e = gdk_event_get ()) != NULL)
	  {
	    switch (e->type)
	      {
	      case GDK_BUTTON_PRESS:
	      case GDK_2BUTTON_PRESS:
	      case GDK_3BUTTON_PRESS:
	      case GDK_KEY_PRESS:
		/*
	      case GDK_BUTTON_RELEASE:
	      case GDK_KEY_RELEASE:
		*/
		flag = TRUE;
		gdk_event_free (e);
		break;
	      case GDK_EXPOSE:
	      case GDK_ENTER_NOTIFY:
	      case GDK_LEAVE_NOTIFY:
	      default:
		gtk_main_iteration ();
		break;
	      }
	  }
	else
	  break;
      }
    if (flag == TRUE)
      thumbnail_panel_set_info ("FYI, events during directory scan were discarded.");

    DEBUGEND;
  }
  return TRUE;
}

static gint
directory_cache_garbage_collect ()
{
  /* side-effect: selection is reset  */
  gint		old_nimage, new_nimage, index;
  cache_entry	*chunk;
  GCHAR		*tmp;

  new_nimage = old_nimage = cwd_cache->nimage;
  chunk = cwd_cache->image;
  index = 0;
  while (index < new_nimage)
    {
      if (chunk[index].deleted == TRUE)
	{
	  new_nimage--;
	  tmp = chunk[index].data;
	  memcpy (chunk + index,
		  chunk + (index + 1),
		  sizeof (cache_entry) * (new_nimage - index));
	  chunk[new_nimage].data = tmp;
	}
      else
	index++;
    }
  cwd_cache->nimage = new_nimage;

  selection_reset ();
  if (directory_cache_npage () <= cwd_cache->display_page)
    cwd_cache->display_page = 0;
  thumbnail_panel_update ();
  return TRUE;
}

static gint
directory_cache_add_directory (GCHAR *name, gint f_kind)
{
  cache_entry	*cache;
  gint	x, y;

  if ((0 < directory_cache_max_size)
      && (directory_cache_max_size < (cwd_cache->ndir + cwd_cache->nimage)))
    return FALSE;

  DEBUGBLOCK ("directory_cache_add_directory...");
  if (cwd_cache->max_ndir <= cwd_cache->ndir)
    {
      cwd_cache->max_ndir *= 2;
      cwd_cache->dir =
	(cache_entry *) g_realloc ((gpointer) cwd_cache->dir,
				   cwd_cache->max_ndir * sizeof (cache_entry));
      memset (cwd_cache->dir + cwd_cache->ndir, 0,
	      (cwd_cache->max_ndir - cwd_cache->ndir) * sizeof (cache_entry));
    }

  cache = &cwd_cache->dir[cwd_cache->ndir++];
  cache->deleted = FALSE;
  if (cache->data == 0)
    cache->data = (GCHAR *) g_malloc (THUMBNAIL_WIDTH * THUMBNAIL_THEIGHT * 3);

  cache->directory_p = TRUE;
  strcpy (cache->name, name);
  if (f_kind == SLNKDIR)
#ifdef VERBOSE_LINK
    sprintf (cache->info, "(link) %s", name);
#else
    strcpy (cache->info, name);
#endif
  else
    strcpy (cache->info, name);
  cache->width = THUMBNAIL_WIDTH;
  cache->height = THUMBNAIL_THEIGHT;

  for (y = 0; y < cache->height; y++)
    for (x = 0; x < cache->width; x++)
      {
	cache->data[(y * THUMBNAIL_WIDTH + x) * 3    ] = 255;
	cache->data[(y * THUMBNAIL_WIDTH + x) * 3 + 1] = 255;
	cache->data[(y * THUMBNAIL_WIDTH + x) * 3 + 2] = 255;
      }
  if ((strlen (cache->name) == 2) && (strcmp ("..", cache->name) == 0))
    cache_render_text (".. (parent)", cache);
  else
    {
      if (f_kind == SLNKDIR)
	cache_set_to_symlink_color ();
      cache_render_text (cache->info, cache);
      if (f_kind == SLNKDIR)
	cache_set_to_normal_color ();
    }
  DEBUGEND;

  return TRUE;
}

static gint
directory_cache_add_image (GCHAR *name, gint i_id, gint f_kind)
{
  gint		x, y;
  gint		i_width, i_height;
  gint		drawable_id;
  GDrawable	*drawable;
  GPixelRgn	src_rgn;
  GCHAR		*src;
  gpointer	pr;
  gint		gap = 0;
  cache_entry	*cache;

  if ((0 < directory_cache_max_size)
      && (directory_cache_max_size < (cwd_cache->ndir + cwd_cache->nimage)))
    return FALSE;

  if (cwd_cache->max_nimage <= cwd_cache->nimage)
    {
      cwd_cache->max_nimage *= 2;
      cwd_cache->image
	= (cache_entry *) g_realloc ((gpointer) cwd_cache->image,
				     cwd_cache->max_nimage * sizeof (cache_entry));
      memset (cwd_cache->image + cwd_cache->nimage, 0,
	      (cwd_cache->max_nimage - cwd_cache->nimage) * sizeof (cache_entry));
    }

  cache = &cwd_cache->image[cwd_cache->nimage++];
  cache->deleted = FALSE;
  if (cache->data == 0)
    cache->data = (GCHAR *) g_malloc (THUMBNAIL_WIDTH * (THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT) * 3);

  drawable_id = gimp_image_get_active_layer (i_id);
  drawable = gimp_drawable_get (drawable_id);
  i_width = gimp_layer_width (drawable_id);
  i_height = gimp_layer_height (drawable_id);

  gap = (gimp_drawable_has_alpha (drawable_id)) ? 1 : 0;
  gimp_pixel_rgn_init (&src_rgn, drawable, 0, 0, i_width, i_height, FALSE, FALSE);
  pr = gimp_pixel_rgns_register (1, &src_rgn);

  for (; pr != NULL; pr = gimp_pixel_rgns_process (pr))
    {
      gint	gx = src_rgn.x;
      gint	gy = src_rgn.y;

      for (y = 0; y < src_rgn.h; y++)
	{
	  src = src_rgn.data + y * src_rgn.rowstride;

	  for (x = 0; x < src_rgn.w; x++)
	    {
	      guchar	*ch = src + (x * (3 + gap));
	      gint	offset = (gy + y) * i_width * 3 + (gx + x) * 3;

	      cache->data[offset    ] = *ch;
	      cache->data[offset + 1] = *(ch + 1);
	      cache->data[offset + 2] = *(ch + 2);
	    }
	}
    }
  gimp_drawable_detach (drawable);

  cache->directory_p = FALSE;
  strcpy (cache->name, name);
  if (f_kind == SLNKFILE)
#ifdef VERBOSE_LINK
    sprintf (cache->info, "(link) %s",
	     gimp_layer_get_name (gimp_image_get_active_layer (i_id)));
#else
    strcpy (cache->info,
	    gimp_layer_get_name (gimp_image_get_active_layer (i_id)));
#endif
  else
    strcpy (cache->info,
	    gimp_layer_get_name (gimp_image_get_active_layer (i_id)));
  cache->width = i_width;
  cache->height = i_height;

  return TRUE;
}

static gint
directory_cache_force_to_purge (GCHAR *name)
{
  gpointer	ptr = NULL;
  GCHAR		cwd[LINE_BUF_SIZE];
  GCHAR		dir[LINE_BUF_SIZE];

  getcwd (cwd, LINE_BUF_SIZE - 1);
  if (chdir (name) == -1)
    return FALSE;
  getcwd (dir, LINE_BUF_SIZE - 1);
  chdir (cwd);

  if ((ptr = g_hash_table_lookup (directory_cache_table, (gpointer) dir)))
    {
      directory_cache	*dcache = (directory_cache *) ptr;

      dcache->purged = TRUE;
      return TRUE;
    }
  else
    return FALSE;
}

static gint
directory_cache_make_cache_directory (directory_cache *dentry)
{
  GCHAR		*cache_dir;
  mode_t	mode = NEW_DIRECTORY_MODE;

  cache_dir = file_build_cache_directory_name (dentry->name);

  if (os_make_directory (cache_dir, mode) == -1)
    {
      switch (errno)
	{
	case EEXIST:
	  dentry->savable = TRUE;
	  dentry->filed = TRUE;
	  break;
	default:
	  dentry->savable = FALSE;
	  dentry->filed = FALSE;
	  break;
	}
      g_free (cache_dir);
      return FALSE;
    }
  else
    {
      dentry->savable = TRUE;
      dentry->filed = TRUE;

      g_free (cache_dir);
      return TRUE;
    }
}

static gint
directory_cache_delete_invalid_cache_files (gint force)
{
  GCHAR		*path;
  DIR		*dir;
  struct dirent *dentry;
  gint		index = 0;

  path = file_build_cache_directory_name (cwd_cache->name);
  dir = opendir (path);
  rewinddir (dir);

  for (; (dentry = readdir (dir)); index++)
    {
      GString	*file;

      file = g_string_new (g_strdup (path));
      g_string_append (file, "/");
      g_string_append (file, dentry->d_name);

      if (os_file_kind (file->str, TRUE) == REGFILE)
	{
#ifdef THUMBNAIL_FORMAT_IS_XCF
	  if (force ||
	      ((cache_file_is_valid (file->str, THUMBNAIL_SUFFIX) == FALSE)
	       && (cache_file_is_valid (file->str, "") == FALSE)))
#else
	  if (force || (cache_file_is_valid (file->str, "") == FALSE))
#endif
	    {
	      DPRINT ("delete cache file: %s\n", file->str);
	      unlink (file->str);
	    }
#ifdef THUMBNAIL_FORMAT_IS_XCF
	  g_string_append (file, THUMBNAIL_SUFFIX);
	  if (os_file_kind (file->str, TRUE) == REGFILE)
	    unlink (file->str);
#endif
	}
      g_string_free (file, TRUE);
    }
  if (force)
    {
      if (rmdir (path) == 0)
	thumbnail_panel_set_info ("Thumbnail directory was deleted.");
      else
	thumbnail_panel_set_info ("Failed to delete thumbnail directory (permission problem?).");
    }

  g_free (path);
  closedir (dir);
  if (force)
    cwd_cache->filed = FALSE;

  if (! force)
    directory_cache_set_to_cwd (TRUE);

  return TRUE;
}

GtkWidget *
directory_cache_create_parents_menu ()
{
  GtkWidget	*menu;
  gint		count, index;
  GCHAR		*cwd;
  GtkWidget	*menuitem;
  GCHAR		*label = NULL;

  menu = gtk_menu_new ();
  cwd = cwd_cache->name;

  label = (GCHAR *) g_malloc (2);
  label[0] = '/';
  label[1] = 0;
  menuitem = gtk_menu_item_new_with_label (label);
  gtk_menu_append (GTK_MENU (menu), menuitem);
  gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
		      (GtkSignalFunc) menu_change_directory_callback,
		      (gpointer) label);
  gtk_widget_show (menuitem);

  for (count = index = 1; index < strlen (cwd); index++)
    if (cwd[index] == '/')
      {
	label = (GCHAR *) g_malloc (index);
	memcpy (label, cwd, index);
	label[index] = 0;

	menuitem = gtk_menu_item_new_with_label (label);
	gtk_menu_prepend (GTK_MENU (menu), menuitem);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    (GtkSignalFunc) menu_change_directory_callback,
			  (gpointer) label);
	gtk_widget_show (menuitem);
    }

  return menu;
}

static void
directory_cache_create_history_menu_foreach (gpointer key,
					     gpointer value,
					     gpointer user_data)
{
  GtkWidget	*menu_item;
  GtkWidget	*menu;

  menu = (GtkWidget *) user_data;

  menu_item = gtk_menu_item_new_with_label ((GCHAR *)key);
  gtk_menu_append (GTK_MENU (menu), menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      (GtkSignalFunc) menu_change_directory_callback,
		      key);
  gtk_widget_show (menu_item);
}

GtkWidget *
directory_cache_create_history_menu ()
{
  GtkWidget *menu;

  menu = gtk_menu_new ();

  g_hash_table_foreach (directory_cache_table,
			directory_cache_create_history_menu_foreach,
			menu);

  return menu;
}

static gint
cache_get_image (GCHAR *name, gint f_kind)
{
  gint	id = -1;

  if (cwd_cache->filed)			/* check cache */
    {
      id = cache_get_image_from_file (name, "", f_kind);
#ifdef THUMBNAIL_FORMAT_IS_XCF
      if (id < 0)
	id = cache_get_image_from_file (name, THUMBNAIL_SUFFIX, f_kind);
#endif
    }
  if (id < 0)
    {
      id = cache_get_image_from_file (name, NULL, f_kind);
    }
  return id;
}

static gint
cache_get_image_from_file (GCHAR *name, GCHAR *suffix, gint f_kind)
{
  GParam*	return_vals;
  GDrawable	*text_layer = NULL;
  gint		retvals;
  gint		xv_thumbnail_p = FALSE;
  gint		f_size = 0;
  gint32	i_id, d_id, width, height, new_width, new_height;
  GString	*path;
  GCHAR		info[256];
  GCHAR		image_type[16];

  image_type[0] = 0;

  if (suffix == NULL)		/* original image file */
    path = g_string_new (cwd_cache->name);
  else				/* thumbnail file */
    path = g_string_new (file_build_cache_directory_name (cwd_cache->name));

  g_string_append (path, "/");
  g_string_append (path, name);
  if ((suffix != NULL) && (0 < strlen (suffix)))
    g_string_append (path, suffix);

  if ((os_file_kind (path->str, TRUE)) == NOT_EXIST)
    {
      g_string_free (path, TRUE);
      return -1;
    }

  if ((suffix != NULL) && (strlen (suffix) == 0))
    {
      /* invoke embbed xvpict loader */
      i_id = load_xvpict_image (path->str);
      if (i_id < 0)
	{
	  g_string_free (path, TRUE);
	  return i_id;
	}
      else
	{
	  xv_thumbnail_p = TRUE;
	  strcpy (info, gimp_layer_get_name (gimp_image_get_active_layer (i_id)));
	}
    }
  else
    {
      return_vals = gimp_run_procedure ("gimp_file_load",
					&retvals,
					PARAM_INT32, RUN_NONINTERACTIVE,
					PARAM_STRING, path->str,
					PARAM_STRING, path->str,
					PARAM_END);
      f_size = os_file_size (path->str);
      if (return_vals[0].data.d_status != STATUS_SUCCESS)
	{
	  g_string_free (path, TRUE);
	  gimp_destroy_params (return_vals, retvals);
	  return -1;
	}
      else
	gimp_destroy_params (return_vals, retvals);
      i_id = return_vals[1].data.d_image;
    }
  g_string_free (path, TRUE);
#if	FALSE
  gimp_display_new (i_id);
#endif
  if (gimp_image_base_type (i_id) != RGB)
    {
      if (gimp_image_base_type (i_id) == INDEXED)
	strcpy (image_type, "Indexed file");
      else
	strcpy (image_type, "Gray file");

      return_vals = gimp_run_procedure ("gimp_convert_rgb",
					&retvals,
					PARAM_IMAGE, i_id,
					PARAM_END);
      gimp_destroy_params (return_vals, retvals);
    }
  else
    strcpy (image_type, "RGB file");

#ifdef THUMBNAIL_FORMAT_IS_XCF
  /* The Image gotten from xvpict or original file requires some
     transformations. */
  if ((suffix != NULL) && (0 < strlen (suffix)))
    {
      return i_id;			/* the case of xcf cache */
    }
#endif
  d_id = gimp_image_get_active_layer (i_id);

  new_width = width = gimp_image_width (i_id);
  new_height = height = gimp_image_height (i_id);

  if (THUMBNAIL_HEIGHT < new_height)
    {
      new_width = new_width * THUMBNAIL_HEIGHT / new_height;
      new_height = THUMBNAIL_HEIGHT;
    }
  if (THUMBNAIL_WIDTH < new_width)
    {
      new_height = new_height * THUMBNAIL_WIDTH / new_width;
      new_width = THUMBNAIL_WIDTH;
    }
  if (new_width < 2) new_width = 2;
  if (new_height < 2) new_height = 2;
  if (! xv_thumbnail_p)
    {
      gimp_image_flatten (i_id);
      d_id = gimp_image_get_active_layer (i_id);
    }
  if ((width != new_width) || (height != new_height))
    gimp_layer_scale (d_id, new_width, new_height, 0);

 /* Now we can save image as an XV thumbnail.*/
  if (! xv_thumbnail_p)
    {
      sprintf (info, "%dx%d %s (%d bytes)", width, height, image_type, f_size);
      gimp_layer_set_name (d_id, info);

      if (cwd_cache->savable == UNKNOWN)
	directory_cache_make_cache_directory (cwd_cache);
      if (cwd_cache->savable == TRUE)
	{
	  if (cache_save_to_file (name, i_id) == TRUE)
	    cwd_cache->filed = TRUE;
	}
    }

  /* OK, let's make the compound thumbnail image! */
  gimp_image_resize (i_id,
		     THUMBNAIL_WIDTH,
		     THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT,
		     0, 0);

  gimp_layer_set_offsets (d_id,
			  (THUMBNAIL_WIDTH - new_width) / 2,
			  (THUMBNAIL_HEIGHT - new_height) / 2);

  /* First make the background layer beneath both of thumbnail layer and the
     text layer for filename.
     WARNING:
     To set to white BG, a new layer should be inserted as the bottom layer.
     But don't copy the current buttom layer by gimp_layer_copy.
     It leaves 60 or 100 tiles internally.
     So gimp quits with a complaint message.  */
  /*
    comment out since 1.0.1 [Sun May 10 06:28:02 1998]
  gimp_layer_add_alpha (d_id);
  {
    gint32	bg_layer;

    bg_layer = gimp_layer_new (i_id, "White BG", THUMBNAIL_WIDTH,
			       THUMBNAIL_HEIGHT + THUMBNAIL_THEIGHT,
			       RGBA_IMAGE, 100, NORMAL_MODE);
    gimp_image_add_layer (i_id, bg_layer, 0);
    gimp_layer_set_offsets (bg_layer, 0, 0);
    gimp_drawable_fill (bg_layer, TRANS_IMAGE_FILL);
  }
  */

  if (f_kind == SLNKFILE)
    cache_set_to_symlink_color ();

  return_vals = gimp_run_procedure ("gimp_text",
				    &retvals,
				    PARAM_IMAGE, i_id,
				    PARAM_DRAWABLE, d_id,
				    PARAM_FLOAT, (gfloat) 0,
				    PARAM_FLOAT, (gfloat) THUMBNAIL_HEIGHT,
				    PARAM_STRING, name,
				    PARAM_INT32, (gint32) 1,
				    PARAM_INT32, (gint32) THUMBNAIL_FONT_ANTIALIAS,
				    PARAM_FLOAT, (gfloat) THUMBNAIL_FONT_SIZE,
				    PARAM_INT32, (gint32) 0,
				    PARAM_STRING, THUMBNAIL_FONT_FOUNDARY,
				    PARAM_STRING, THUMBNAIL_FONT_FAMILY,
				    PARAM_STRING, THUMBNAIL_FONT_WEIGHT,
				    PARAM_STRING, THUMBNAIL_FONT_SLANT,
				    PARAM_STRING, "*", /* set_width */
				    PARAM_STRING, "*", /* spacing */
				    PARAM_END);
  if (return_vals[0].data.d_status == STATUS_SUCCESS)
    {
      text_layer = gimp_drawable_get (return_vals[1].data.d_layer);

      if (text_layer->width < THUMBNAIL_WIDTH)
	gimp_layer_set_offsets (return_vals[1].data.d_layer,
				CLAMP (((THUMBNAIL_WIDTH - text_layer->width) / 2),
				       0, THUMBNAIL_WIDTH),
				THUMBNAIL_HEIGHT);
    }
  else
    {
      thumbnail_panel_set_info ("Error: failed to invoke gimp_text (font not found?)");
      g_warning ("Guash failed to invoke gimp_text. Check the existence of the font selected in config.h.");
    }
  gimp_destroy_params (return_vals, retvals);
  if (f_kind == SLNKFILE)
    cache_set_to_normal_color ();

  gimp_image_flatten (i_id);

  d_id = gimp_image_get_active_layer (i_id);
  if (! xv_thumbnail_p)
    sprintf (info, "%s %dx%d %s (%d bytes)",
	     name, width, height,
	     image_type, f_size);
  gimp_layer_set_name (d_id, info);

  return i_id;
}

static void
cache_set_to_symlink_color ()
{
  gimp_palette_set_foreground (46, 139, 87); /* This is Sea green. */
  gimp_palette_set_background (255, 255, 255);
}

static void
cache_set_to_normal_color ()
{
  gimp_palette_set_foreground (0, 0, 0);
  gimp_palette_set_background (255, 255, 255);
}

static void
cache_render_text (GCHAR *str, cache_entry *cache)
{
  GParam*	return_vals;
  gint		retvals;
  gint		x, y;
  gint		i_width, i_height;
  gint		drawable_id;
  GDrawable	*drawable;
  GPixelRgn	src_rgn;
  GCHAR		*src;
  gpointer	pr;
  gint		gap = 0;

  guash_assure_scratch_buffer ();
  return_vals = gimp_run_procedure ("gimp_selection_none",
				    &retvals,
				    PARAM_IMAGE, text_image,
				    PARAM_END);
  gimp_destroy_params (return_vals, retvals);
  gimp_drawable_fill (text_bg, BG_IMAGE_FILL);
  return_vals = gimp_run_procedure ("gimp_text",
				    &retvals,
				    PARAM_IMAGE, text_image,
				    PARAM_DRAWABLE, text_bg,
				    PARAM_FLOAT, (gfloat) 0,
				    PARAM_FLOAT, (gfloat) 0,
				    PARAM_STRING, str,
				    PARAM_INT32, (gint32) 1,
				    PARAM_INT32, (gint32) THUMBNAIL_FONT_ANTIALIAS,
				    PARAM_FLOAT, (gfloat) THUMBNAIL_FONT_SIZE,
				    PARAM_INT32, (gint32) 0,
				    PARAM_STRING, THUMBNAIL_FONT_FOUNDARY,
				    PARAM_STRING, THUMBNAIL_FONT_FAMILY,
				    PARAM_STRING, THUMBNAIL_FONT_WEIGHT,
				    PARAM_STRING, THUMBNAIL_FONT_SLANT,
				    PARAM_STRING, "*", /* set_width */
				    PARAM_STRING, "*", /* spacing */
				    PARAM_END);
  if (return_vals[0].data.d_status == STATUS_SUCCESS)
    {
      gint	text_layer = return_vals[1].data.d_layer;
      GDrawable	*drw = gimp_drawable_get (text_layer);

      if (drw->width < THUMBNAIL_WIDTH)
	gimp_layer_set_offsets (text_layer,
				(THUMBNAIL_WIDTH - drw->width) / 2,
				0);
      gimp_destroy_params (return_vals, retvals);
      return_vals = gimp_run_procedure ("gimp_floating_sel_anchor",
					&retvals,
					PARAM_LAYER, text_layer,
					PARAM_END);
      gimp_destroy_params (return_vals, retvals);
      gimp_drawable_detach (drw);
    }
  else
    {
      thumbnail_panel_set_info ("Error: failed to invoke gimp_text (font not found?)");
      g_warning ("Guash fails to invoke gimp_text. Check the existence of the font.");
      gimp_destroy_params (return_vals, retvals);
      return;
    }
  text_bg = drawable_id = gimp_image_get_active_layer (text_image);
  drawable = gimp_drawable_get (drawable_id);
  i_width = gimp_layer_width (drawable_id);
  i_height = gimp_layer_height (drawable_id);

  gap = (gimp_drawable_has_alpha (drawable_id)) ? 1 : 0;
  gimp_pixel_rgn_init (&src_rgn, drawable, 0, 0, i_width, i_height, FALSE, FALSE);
  pr = gimp_pixel_rgns_register (1, &src_rgn);
  for (; pr != NULL; pr = gimp_pixel_rgns_process (pr))
    {
      gint	gx = src_rgn.x;
      gint	gy = src_rgn.y;

      for (y = 0; y < src_rgn.h; y++)
	{
	  src = src_rgn.data + y * src_rgn.rowstride;

	  for (x = 0; (x < src_rgn.w) && (gx + x < THUMBNAIL_WIDTH); x++)
	    {
	      guchar	*ch = src + (x * (3 + gap));
	      gint	offset = (gy + y) * THUMBNAIL_WIDTH * 3 + (gx + x) * 3;
	      cache->data[offset    ] = *ch;
	      cache->data[offset + 1] = *(ch + 1);
	      cache->data[offset + 2] = *(ch + 2);
	    }
	}
    }
  /*
  gimp_drawable_delete (drawable);
  gimp_drawable_detach (drawable);
  text_bg = -1;
  */
}

static gint
cache_save_to_file (GCHAR *file_name, gint32 i_id)
{
  GParam	*return_vals = NULL;
  GString	*dir, *s;
  gint	retvals;

  if (cwd_cache->savable == TRUE)
    {
      dir = g_string_new (file_build_cache_directory_name (cwd_cache->name));
      g_string_append (dir, "/");

      s = g_string_new (dir->str);
      g_string_append (s, file_name);
#ifdef THUMBNAIL_FORMAT_IS_XCF
      g_string_append (s, THUMBNAIL_SUFFIX);
#endif
      gimp_image_set_filename (i_id, s->str);
      if (gimp_image_base_type (i_id) != RGB)
	{
	  return_vals = gimp_run_procedure ("gimp_convert_rgb",
					    &retvals,
					    PARAM_IMAGE, i_id,
					    PARAM_END);
	  gimp_destroy_params (return_vals, retvals);
	}
#ifdef THUMBNAIL_FORMAT_IS_XCF
      return_vals = gimp_run_procedure ("gimp_file_save",
					&retvals,
					PARAM_INT32, RUN_INTERACTIVE,
					PARAM_IMAGE, i_id,
					PARAM_DRAWABLE, gimp_image_get_active_layer (i_id),
					PARAM_STRING, s->str,
					PARAM_STRING, s->str,
					PARAM_END);
      gimp_destroy_params (return_vals, retvals);
#else
      save_xvpict_image (s->str, i_id, gimp_image_get_active_layer (i_id));
#endif
      g_string_free (s, TRUE);
      g_string_free (dir, TRUE);
      return TRUE;
    }
  else
    return FALSE;
}

static gint
cache_open_image_file (cache_entry *cache)
{
  GParam*	return_vals;
  gint		retvals;
  gint		i_id;
  GString *path;

  path = g_string_new (cwd_cache->name);
  g_string_append (path, "/");
  g_string_append (path, cache->name);

  return_vals = gimp_run_procedure ("gimp_file_load",
				    &retvals,
				    PARAM_INT32, RUN_NONINTERACTIVE,
				    PARAM_STRING, path->str,
				    PARAM_STRING, path->str,
				    PARAM_END);
  g_string_free (path, TRUE);

  if (return_vals[0].data.d_status != STATUS_SUCCESS)
    {
      gimp_destroy_params (return_vals, retvals);
      return -1;
    }
  else
    gimp_destroy_params (return_vals, retvals);

  i_id = return_vals[1].data.d_image;
  /* the following sequence is copied from app/fileops.c */
  /*  enable & clear all undo steps  */
  gimp_image_enable_undo (i_id);
  /*  set the image to clean  */
  gimp_image_clean_all (i_id);
  /*  display the image */
  gimp_display_new (i_id);
  return i_id;
}

static GCHAR *
file_get_canonical_name (GCHAR *name)
{
  GCHAR	cwd[LINE_BUF_SIZE];
  GCHAR	*dir;

  dir = g_malloc (LINE_BUF_SIZE);

  getcwd (cwd, LINE_BUF_SIZE - 1);
  if (chdir (name) == -1)
    return NULL;
  getcwd (dir, LINE_BUF_SIZE - 1);
  chdir (cwd);

  return dir;
}

static gint
file_get_last_slash_index (GCHAR *pathname)
{
  gint	index;
  gint	pname_len;

  pname_len = strlen (pathname);

  for (index = pname_len - 2; 0 <= index; index--)
    if (pathname[index] == '/') break;

  return index;
}

static gint
file_get_last_period_index (GCHAR *pathname)
{
  gint	index;
  gint	pname_len;

  pname_len = strlen (pathname);

  for (index = pname_len - 2; 0 <= index; index--)
    if (pathname[index] == '.') break;

  return index;
}

static GCHAR *
file_get_parent_directory (GCHAR *pathname)
{
  gint	pname_len, slash_pos;
  GCHAR	*dirname;

  pname_len = strlen (pathname);
  slash_pos = file_get_last_slash_index (pathname);

  if (slash_pos == 0)
    return NULL;

  dirname = (GCHAR *) g_malloc (slash_pos + 1);
  memcpy (dirname, pathname, slash_pos);
  dirname[slash_pos] = 0;

  return dirname;
}

/* foo/bar.baz => bar.baz */
static GCHAR *
file_get_filename (GCHAR *pathname)
{
  gint	pname_len, slash_pos;
  GCHAR	*filename;

  pname_len = strlen (pathname);
  slash_pos = file_get_last_slash_index (pathname);

  filename = (GCHAR *) g_malloc (pname_len - slash_pos);
  strcpy (filename, pathname + (slash_pos + 1));

  return filename;
}

/* foo/bar.baz => foo/bar.baz/.xvpics */
static GCHAR *
file_build_cache_directory_name (GCHAR *dir_name)
{
  gint	dir_len, cd_len, cp_len;
  GCHAR	*cache_path;
  GCHAR	*cache_dir = THUMBNAIL_DIR;

  dir_len = strlen (dir_name);
  cd_len = strlen (THUMBNAIL_DIR);
  cp_len = dir_len + cd_len;
  cache_path = (GCHAR *) g_malloc (cp_len + 1);

  memcpy (cache_path, dir_name, dir_len);
  memcpy (cache_path + dir_len, cache_dir, cd_len);
  cache_path[cp_len] = 0;

  return cache_path;
}

static GCHAR *
file_build_cache_file_name (GCHAR *filename)
{
  gint	index;
  gint	fname_len, cname_len;
  GCHAR	*cache_name;
  GCHAR	*cache_dir = THUMBNAIL_DIR;
#ifdef THUMBNAIL_FORMAT_IS_XCF
  GCHAR	*cache_suffix = THUMBNAIL_SUFFIX;
#endif

  fname_len = strlen (filename);
#ifdef THUMBNAIL_FORMAT_IS_XCF
  cname_len = fname_len + strlen (cache_dir) + strlen (cache_suffix);
#else
  cname_len = fname_len + strlen (cache_dir);
#endif
  index = file_get_last_slash_index (filename);

  cache_name = (GCHAR *) g_malloc (cname_len + 1);
  memcpy (cache_name, filename, index);
  memcpy (cache_name + index, cache_dir, strlen (cache_dir));
  memcpy (cache_name + (index + strlen (cache_dir)),
	  filename + index,
	  fname_len - index);
#ifdef THUMBNAIL_FORMAT_IS_XCF
  memcpy (cache_name + (cname_len - strlen (cache_suffix)),
	  cache_suffix,
	  strlen (cache_suffix));
#endif
  cache_name [cname_len] = 0;
  return cache_name;
}

static gint
file_match_inhibit_suffix (GCHAR *pathname)
{
  gint	index =0;
  gint	pathlen = strlen (pathname);

  for (; index < num_inhibit_suffix; index++)
    {
      GCHAR	*suffix = inhibit_suffix_table[index];
      gint	prelen = strlen (suffix);

      if (pathlen < prelen)
	continue;
      if (strcmp (suffix, pathname + (pathlen - prelen)) == 0)
	return TRUE;
    }
  return FALSE;
}

static gint	os_scandir_selector (GCHAR *filename)
{
  return 1;
}

static gint	os_file_alphasort (GCHAR **a, GCHAR **b)
{
  /* if b has larger name than a, return 1 */
  /* Warning: file existance check is omitted */
#ifdef DEBUG
  if (0 < strcmp (*a, *b))
    printf ("%s < %s\n", *a, *b);
  else
    printf ("%s < %s\n", *b, *a);
#endif
  return 0 < strcmp (*a, *b) ? 1 : 0;
}

static gint	os_file_mtimesort (GCHAR **a, GCHAR **b)
{
  /* if b is newer than a, return 1 */
  struct stat a_stat;
  struct stat b_stat;

  /* Warning: file existance check is omitted */
  stat (*a, &a_stat);
  stat (*b, &b_stat);

  if (a_stat.st_mtime < b_stat.st_mtime)
    return 1;
  else if (a_stat.st_mtime == b_stat.st_mtime)
    return 0;
  else
    return -1;
}

static gint
os_scandir (GCHAR *dir_name,
	    GCHAR ***filename_list,
	    scandir_selector_function selector,
	    gint sort_type)
{
  GCHAR		**list = NULL;
  struct dirent	*dirent_ptr;
  DIR		*directory;
  gint		entry_count = 0;
  gint		i;

  *filename_list = NULL;
  directory = opendir (dir_name);

  if (!directory)
    return 0;

  while ((dirent_ptr = readdir (directory)) != NULL)
    entry_count++;

  list = (GCHAR **) g_malloc (entry_count * sizeof (GCHAR *));
  rewinddir (directory);

  for (i = 0; i < entry_count; i++)
    {
      if ((dirent_ptr = readdir (directory)))
	{
	  list[i] = g_malloc (strlen (dirent_ptr->d_name) + 1);
	  strcpy (list[i], dirent_ptr->d_name);
	}
      else
	{
	  gint	j;

	  for (j = 0; j < i; j++)
	    g_free (list[j]);
	  g_free (list);
	  closedir (directory);
	  return 0;
	}
    }

  qsort (list,
	 entry_count,
	 sizeof (GCHAR *),
	 (sort_compare_function) ((sort_type == SORT_BY_NAME) ? &os_file_alphasort : &os_file_mtimesort));
  closedir (directory);

  *filename_list = list;
  return entry_count;
}

static gint
os_copy_file (GCHAR *filename, GCHAR *newname)
{
  gint		from;
  gint		to;
  gint		size;
  GCHAR		buffer[LINE_BUF_SIZE];
  mode_t	mode = NEW_FILE_MODE;

  if ((from = open (filename, O_RDONLY)) == -1)
    {
      perror ("os_copy_file: can't open source file.");
      return FALSE;
    }
  if ((to = creat (newname, mode)) == -1)
    {
      close (from);
      perror ("os_copy_file: can't create destination file.");
      return FALSE;
    }
  while (0 < (size = read (from, buffer, LINE_BUF_SIZE - 1)))
    write (to, buffer, size);

  close (from);
  close (to);

  return TRUE;
}

static gint
os_file_kind (GCHAR *filename, gint shrink)
{
  struct stat ent_sbuf;
  gint	flag = TRUE;			/*  fail safe */
  gint	symlink = FALSE;
  gint	size = 0;
  GCHAR	fname[LINE_BUF_SIZE], tmp[LINE_BUF_SIZE];

  if (*filename == '/')
    strcpy (fname, filename);
  else
    sprintf (fname, "%s/%s", cwd_cache->name, filename);

  while ((size = readlink (fname, tmp, LINE_BUF_SIZE - 1)) != -1)
    {
      /* fname is a symbolic link. */
      symlink = TRUE;
      tmp[size] = 0;
      if (tmp[0] != '/')
	{
	  GCHAR	*dir = NULL;
	  GCHAR	scratch[LINE_BUF_SIZE];

	  dir = file_get_parent_directory (fname);
	  sprintf (scratch, "%s/%s", dir, tmp);
	  strcpy (fname, scratch);
	  g_free (dir);
	}
      else
	{
	  strcpy (fname, tmp);
	}
    }

  if (stat (fname, &ent_sbuf) == 0)
    {
      /* something exists */
      if (S_ISDIR (ent_sbuf.st_mode))
	flag = DIRECTORY;
      else if (S_ISREG (ent_sbuf.st_mode))
	flag = REGFILE;

      if ((! shrink) && symlink)
	{
	  switch (flag)
	    {
	    case REGFILE:
	      flag = SLNKFILE;
	      break;
	    case DIRECTORY:
	      flag = SLNKDIR;
	      break;
	    default:
	      break;
	    }
	}
    }
  else
    {
      /* error was occured */
      if (errno == ENOENT)
	flag = NOT_EXIST;
    }
  return flag;
}

static gint
os_file_size (GCHAR *filename)
{
  struct stat ent_sbuf;
  gint	size = 0;
  gint	f_size = -1;
  GCHAR	fname[LINE_BUF_SIZE], tmp[LINE_BUF_SIZE];

  strcpy (fname, filename);
  while ((size = readlink (fname, tmp, LINE_BUF_SIZE - 1)) != -1)
    {
      /* fname is a symbolic link. */
      tmp[size] = 0;
      strcpy (fname, tmp);
    }
  if (stat (fname, &ent_sbuf) == 0)
    f_size = ent_sbuf.st_size;

  return f_size;
}

static gint
os_mkdir (GCHAR *pathname)
{
  GCHAR		info[256];
  mode_t	mode = NEW_DIRECTORY_MODE;
  gint		kind;

  kind = os_file_kind (pathname, TRUE);

  if (NOT_EXIST == kind)
    {
      if (os_make_directory (pathname, mode) != -1)
	{
	  GCHAR	*canonical;

	  canonical = file_get_canonical_name (pathname);
	  if (canonical != NULL)
	    {
	      GCHAR	*parent;
	      GCHAR	*filename;

	      parent = file_get_parent_directory (canonical);
	      filename = file_get_filename (canonical);

	      if (strcmp (cwd_cache->name, parent) == 0)
		{
		  guchar	old_fg[3], old_bg[3];

		  gimp_palette_get_foreground (old_fg, old_fg + 1, old_fg + 2);
		  gimp_palette_get_background (old_bg, old_bg + 1, old_bg + 2);
		  guash_assure_scratch_buffer ();
		  directory_cache_add_directory (filename, DIRECTORY);
		  guash_unref_scratch_buffer ();
		  gimp_palette_set_foreground (old_fg[0], old_fg[1], old_fg[2]);
		  gimp_palette_set_background (old_bg[0], old_bg[1], old_bg[2]);
		  thumbnail_panel_update ();
		}
	      else
		directory_cache_force_to_purge (parent);

	      g_free (parent);
	      g_free (filename);
	    }
	  else
	    printf ("os_mkdir: can't move to the directory\n");
	  g_free (canonical);
	}
    }
  else
    {
      sprintf (info, "%s already exists.", pathname);
      thumbnail_panel_set_info (info);
    }
  return (NOT_EXIST == kind);
}

static gint
os_make_directory (GCHAR *pathname, gint mode)
{
  return mkdir (pathname, mode);
}

static gint
os_rename_file (GCHAR *filename, GCHAR *newname)
{
  return rename (filename, newname);
}

static gint
file_confirm_operation (GCHAR *operation_phrase)
{
  gint	flag = TRUE;

  if (! selection_is_active ())
    return flag;

  if (use_confirmor)
    {
      GCHAR buffer [LINE_BUF_SIZE];

      if (selection_length () == 1)
	{
	  cache_entry	*selected;

	  selected = directory_cache_get_nth ((gint) selection_list->data);
	  sprintf (buffer, " %s the selected image: %s? \n\
 Guash is NO WARRANTY program. ",
		   operation_phrase,
		   selected->name);
	}
      else
	{
	  sprintf (buffer, " %s the selected %d images? \n\
 Guash is NO WARRANTY program. ",
		   operation_phrase,
		   selection_length ());
	}

      if (! gtkW_confirmor_dialog (TRUE, buffer, FALSE))
	flag = FALSE;
    }

  return flag;
}

static gint
cache_file_is_valid (GCHAR *cache_name, GCHAR *suffix)
{
  GCHAR		*file_name;
  struct stat	image, cache;
  gint		stripper;
  gint		last_slash, parent_slash;

  stripper = strlen (THUMBNAIL_DIR) + strlen (suffix);
  file_name = (GCHAR *) g_malloc (strlen (cache_name) - stripper + 1);
  last_slash = file_get_last_slash_index (cache_name);
  parent_slash = last_slash - strlen (THUMBNAIL_DIR);
  /* /.../dir/  */
  memcpy (file_name, cache_name, parent_slash);
  /* /.../dir/image.file */
  memcpy (file_name + parent_slash,
	  cache_name + last_slash,
	  strlen (cache_name) - last_slash - strlen (suffix));
  /* /.../dir/image.file + TERMINATOR */
  file_name [strlen (cache_name) - stripper] = 0;

  if (stat (file_name, &image) != 0)
    return FALSE;
  if (stat (cache_name, &cache) != 0)
    return FALSE;

  return (image.st_mtime <= cache.st_mtime);
}

static gint
image_file_copy (GCHAR *filename, GCHAR *newname)
{
  if (os_file_kind (newname, TRUE) == NOT_EXIST)
    {
      GCHAR	*from_cache_name;
      GCHAR	*to_cache_name;
      mode_t	mode = NEW_DIRECTORY_MODE;

      if (os_copy_file (filename, newname) == FALSE)
	return FALSE;

      /* copy image cache */
      from_cache_name = file_build_cache_file_name (filename);
      if (os_file_kind (from_cache_name, TRUE) != REGFILE)
	{
	  g_free (from_cache_name);
	  return TRUE;
	}
      to_cache_name = file_build_cache_file_name (newname);
      {
	GCHAR	*to_dir_name;

	to_dir_name = file_get_parent_directory (to_cache_name);

	if (os_make_directory (to_dir_name, mode) == -1)
	  if (errno != EEXIST)
	    {
	      g_free (to_dir_name);
	      g_free (to_cache_name);
	      g_free (from_cache_name);
	      return TRUE;
	    }
	if (os_file_kind (to_dir_name, TRUE) != DIRECTORY)
	  {
	    g_free (to_dir_name);
	    g_free (to_cache_name);
	    g_free (from_cache_name);
	    return TRUE;
	  }
	g_free (to_dir_name);
      }
      os_copy_file (from_cache_name, to_cache_name);

      g_free (from_cache_name);
      g_free (to_cache_name);
      return TRUE;
    }
  else
    return FALSE;
}

static gint
image_file_delete (GCHAR *filename)
{
  return (unlink (filename) == 0);
}

static gint
image_file_move (GCHAR *filename, GCHAR *newname)
{
  if (os_file_kind (newname, TRUE) == NOT_EXIST)
    {
      if (os_rename_file (filename, newname) == 0)
	{
	  GCHAR		*from_cache_name = NULL;
	  GCHAR		*to_cache_name = NULL;
	  GCHAR		*cache_dir = NULL;
	  mode_t	mode = NEW_DIRECTORY_MODE;

	  from_cache_name = file_build_cache_file_name (filename);

	  if (os_file_kind (from_cache_name, TRUE) != REGFILE)
	    {
	      g_free (from_cache_name);
	      return TRUE;
	    }
	  to_cache_name = file_build_cache_file_name (newname);
	  cache_dir = file_get_parent_directory (to_cache_name);

	  if (os_make_directory (cache_dir, mode) == -1)
	    if (errno != EEXIST)
	      {
		g_free (cache_dir);
		g_free (to_cache_name);
		g_free (from_cache_name);
		return TRUE;
	      }
	  if (os_file_kind (cache_dir, TRUE) != DIRECTORY)
	    {
	      g_free (cache_dir);
	      g_free (to_cache_name);
	      g_free (from_cache_name);
	      return TRUE;
	    }
	  g_free (cache_dir);

	  if (os_rename_file (from_cache_name, to_cache_name) == -1)
	    {
	      perror ("rename cache file");
	      printf ("from %s to %s\n", from_cache_name, to_cache_name);
	    }

	  g_free (from_cache_name);
	  g_free (to_cache_name);
	  return TRUE;
	}
      else
	{
	  if (errno == EXDEV)
	    {
	      if (image_file_copy (filename, newname))
		{
		  image_file_delete (filename);
		  return TRUE;
		}
	      else
		return FALSE;
	    }
	  else
	    {
	      perror ("file_move");
	      return FALSE;
	    }
	}
    }
  else
    {
      GString *message;

      message = g_string_new ("NOP: ");
      g_string_append (message, newname);
      g_string_append (message, " already exists.");
      gtkW_message_dialog (TRUE, message->str);
      g_string_free (message, TRUE);

      return FALSE;
    }
}

static void
fileselector_set_last_value (GCHAR *pathname)
{
  if (os_file_kind (pathname, TRUE) == DIRECTORY)
    strcpy (fileselector_last_pathname, pathname);
  else
    strcpy (fileselector_last_pathname, file_get_parent_directory (pathname));
}

/* dialog stuff */
static int
DIALOG (gint restart)
{
  GtkWidget	*hbox;
  GtkWidget	*ps_box;
  GtkWidget	*thumbnail_panel_frame = NULL;
  GtkWidget	*button;
  gchar		**argv;
  gint		argc;

  DEBUGBLOCK ("Building dialog window...");
  argc = 1;
  argv = g_new (gchar *, 1);
  argv[0] = g_strdup (PLUG_IN_NAME);
  gtk_init (&argc, &argv);
  gtk_rc_parse (gimp_gtkrc ());

  ncol_of_thumbnail = gtkW_parse_gimprc_gint ("guash-ncol",
					      NCOL_OF_THUMBNAIL_DEFAULT);
  ncol_of_thumbnail_default
    = ncol_of_thumbnail
    = CLAMP (ncol_of_thumbnail, NCOL_OF_THUMBNAIL_MIN, 10);
  nrow_of_thumbnail = gtkW_parse_gimprc_gint ("guash-nrow",
					      NROW_OF_THUMBNAIL_DEFAULT);
  nrow_of_thumbnail_default
    = nrow_of_thumbnail
    = CLAMP (nrow_of_thumbnail, NROW_OF_THUMBNAIL_MIN, 10);
  nthumbnails_in_page = ncol_of_thumbnail * nrow_of_thumbnail;
  thumbnail_panel_width = COL2WIDTH (ncol_of_thumbnail);
  thumbnail_panel_height = ROW2HEIGHT (nrow_of_thumbnail);

  dlg = gtk_dialog_new ();
  gtk_window_set_wmclass (GTK_WINDOW (dlg), "Guash", "Gimp");
  gtk_window_set_title (GTK_WINDOW (dlg), "Guash: initializing...");
  gtk_window_position (GTK_WINDOW (dlg), GTK_WIN_POS_MOUSE);
  gtk_signal_connect (GTK_OBJECT (dlg), "destroy",
		      (GtkSignalFunc) gtkW_close_callback, NULL);
  gtk_signal_connect (GTK_OBJECT (dlg), "key_press_event",
		      (GtkSignalFunc) cursor_event_handler,
		      NULL);

  /* Action Area */
  gtk_container_border_width (GTK_CONTAINER (GTK_DIALOG (dlg)->action_area), 0);
#ifdef USE_DND
  widget_for_selecion[0] = button = gtk_button_new_with_label ("Drag for copy");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->action_area), button,
		      TRUE, TRUE, 0);
  gtk_widget_show (button);
  gtk_widget_realize (button);
  gtk_signal_connect (GTK_OBJECT (button),
		      "clicked",
		      GTK_SIGNAL_FUNC (dnd_drag_button_callback),
		      button);
  gtk_signal_connect (GTK_OBJECT (button),
		      "drag_request_event",
		      GTK_SIGNAL_FUNC (dnd_drag_request_callback),
		      button);
  gtk_widget_dnd_drag_set (button, FALSE, possible_dnd_types, 1);
  gtk_widget_set_sensitive (button, 0);
#endif
  button = gtk_button_new_with_label ("Help");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) help_callback, dlg);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->action_area), button,
		      TRUE, TRUE, 0);
  gtk_widget_show (button);

  button = gtk_button_new_with_label ("Close");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     (GtkSignalFunc) gtkW_close_callback,
			     GTK_OBJECT(dlg));
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->action_area), button,
		      TRUE, TRUE, 0);
  gtk_widget_grab_default (button);
  gtk_widget_show (button);

  /* top row */
  hbox = gtkW_hbox_new ((GTK_DIALOG (dlg)->vbox), FALSE, FALSE);

  cwd_label = gtk_label_new ("Gimp Users' Another SHell");
  gtk_widget_set_usize (cwd_label,
			COL2WIDTH(NCOL_OF_THUMBNAIL_MIN) - JUMP_BUTTON_WIDTH, 0);
  gtk_box_pack_start (GTK_BOX (hbox), cwd_label, TRUE, TRUE, 0);
  gtk_widget_show (cwd_label);

  button = gtk_button_new_with_label ("Jump");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (directory_jump_callback),
		      NULL);
  gtk_box_pack_end (GTK_BOX (hbox), button, FALSE, FALSE, 0);

  gtk_widget_set_usize (button, JUMP_BUTTON_WIDTH, 0);
  gtk_widget_show (button);

  gtk_widget_show (hbox);

  /* 2nd row */
  ps_box = gtkW_hbox_new ((GTK_DIALOG (dlg)->vbox), TRUE, TRUE);

  gdk_set_use_xshm (gimp_use_xshm ());
  gtk_preview_set_gamma (gimp_gamma ());
  {
    guchar *color_cube;
    color_cube = gimp_color_cube ();
    gtk_preview_set_color_cube (color_cube[0], color_cube[1],
				color_cube[2], color_cube[3]);
  }
  {
    GtkPreviewInfo *info;

    info = gtk_preview_get_info ();
    if ( info->visual->type != GDK_VISUAL_PSEUDO_COLOR)
      {
	/* rule of thumb: An extension should not share cmap with the gimp. */
	gtk_preview_set_install_cmap (gimp_install_cmap ());
      }
    gtk_widget_set_default_colormap (gtk_preview_get_cmap ());
    gtk_preview_reset ();
    gtk_widget_set_default_visual (gtk_preview_get_visual ());
  }

  thumbnail_panel = gtk_preview_new (GTK_PREVIEW_COLOR);
  gtk_preview_size (GTK_PREVIEW (thumbnail_panel),
		    thumbnail_panel_width,
		    thumbnail_panel_height);
  GTK_WIDGET_CLASS (GTK_OBJECT (thumbnail_panel)->klass)->size_request
    = thumbnail_panel_size_request;
  GTK_WIDGET_CLASS (GTK_OBJECT (thumbnail_panel)->klass)->size_allocate
    = thumbnail_panel_size_allocate;
  gtk_widget_set_events (thumbnail_panel, EVENT_MASK);
  gtk_signal_connect (GTK_OBJECT (thumbnail_panel), "event",
		      (GtkSignalFunc) preview_event_handler,
		      NULL);
  gtk_widget_show (thumbnail_panel);

  thumbnail_panel_frame = gtkW_frame_new (NULL, NULL);
  gtkW_frame_shadow_type = GTK_SHADOW_ETCHED_IN;
  gtk_container_border_width (GTK_CONTAINER (thumbnail_panel_frame), 0);
  gtk_container_add (GTK_CONTAINER (thumbnail_panel_frame), thumbnail_panel);
  gtk_widget_show (thumbnail_panel_frame);

  gtkW_ivscroll_entry_new (&widget_for_scroll[0],
			   &widget_for_scroll[1],
			   (GtkSignalFunc) gtkW_iscroll_update,
			   (GtkSignalFunc) gtkW_ientry_update,
			   &thumbnail_panel_page1,
			   1, 5, 1, &widget_pointer[0]);
  gtk_widget_set_usize (widget_for_scroll[0], SCROLLBAR_WIDTH, 0);
  gtk_widget_set_usize (widget_for_scroll[1], JUMP_BUTTON_WIDTH, 0);
  gtk_widget_show (widget_for_scroll[0]);
  gtk_widget_show (widget_for_scroll[1]);

  gtk_box_pack_start (GTK_BOX (ps_box), thumbnail_panel_frame, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (ps_box), widget_for_scroll[0], FALSE, FALSE, 0);

  gtk_widget_show (ps_box);

#ifdef USE_DND
  /*
  gtk_signal_connect (GTK_OBJECT (thumbnail_panel),
		      "drag_request_event",
		      GTK_SIGNAL_FUNC (dnd_drag_request_callback),
		      thumbnail_panel);
  gtk_widget_dnd_drag_set (thumbnail_panel, TRUE, possible_dnd_types, 1);
  */
  gtk_signal_connect (GTK_OBJECT (thumbnail_panel),
		      "drop_data_available_event",
		      GTK_SIGNAL_FUNC (dnd_drop),
		      thumbnail_panel);
  gtk_widget_dnd_drop_set (thumbnail_panel, TRUE, possible_dnd_types, 1, FALSE);
#endif

  /* 3rd row */
  hbox = gtkW_hbox_new ((GTK_DIALOG (dlg)->vbox), FALSE, FALSE);

  file_property = gtk_label_new ("Gimp Users' Another SHell: initializing...");
  gtk_widget_set_usize (file_property,
			COL2WIDTH(NCOL_OF_THUMBNAIL_MIN) - JUMP_BUTTON_WIDTH, 0);
  gtk_box_pack_start (GTK_BOX (hbox), file_property, TRUE, TRUE, 0);
  gtk_widget_show (file_property);

  gtk_box_pack_end (GTK_BOX (hbox), widget_for_scroll[1], FALSE, FALSE, 0);
  gtk_widget_show (hbox);

  if (VAL.last_dir_name[0] == 0)
    thumbnail_panel_banner ();
  else
    thumbnail_panel_clear ();

  gtk_window_set_title (GTK_WINDOW (dlg), SHORT_NAME);
  gtk_widget_show (dlg);

  gtk_window_set_title (GTK_WINDOW (dlg), SHORT_NAME);
  DEBUGEND;

  DEBUGBLOCK ("Setting timer...");
  gtk_timeout_add (((VAL.last_dir_name[0] == 0) ?
		    INITIALIZATION_SLEEP_PERIOD : STARTUP_SLEEP_PERIOD),
		   timer_initialize_thumbnail_panel,
		   NULL);
  DEBUGEND;
  gtk_main ();
  gdk_flush ();
  return 1;
}

static gint
timer_initialize_thumbnail_panel (gpointer data)
{
  DEBUGBLOCK ("Delayed Initialization...");
  guash_parse_gimprc ();
  thumbnail_panel_create_menu ();
  directory_cache_set_to_cwd (FALSE);
  DEBUGEND;
  return FALSE;
}

static gint
about_dialog ()
{
  GtkWidget	*a_dlg;
  GtkWidget	*button;
  GtkWidget	*frame;
  GtkWidget	*hbox;
  GtkWidget	*table;
  GtkWidget	*hseparator;
  gint		index, i, nhelp;
  gint		align;

  nhelp = sizeof (help_document) / sizeof (help_document[0]);

  a_dlg = gtk_dialog_new ();
  gtk_window_set_wmclass (GTK_WINDOW (a_dlg), "Guash", "Gimp");
  gtk_window_set_title (GTK_WINDOW (a_dlg), "Guash help");
  gtk_window_position (GTK_WINDOW (a_dlg), GTK_WIN_POS_MOUSE);
  /*
  gtk_signal_connect (GTK_OBJECT (a_dlg), "destroy",
		      (GtkSignalFunc) gtkW_close_callback, NULL);
  */
  gtk_container_border_width (GTK_CONTAINER (GTK_DIALOG (a_dlg)->action_area),
			      gtkW_border_width);

  button = gtk_button_new_with_label ("Close");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT(a_dlg));
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (a_dlg)->action_area), button,
		      TRUE, TRUE, 0);
  gtk_widget_grab_default (button);
  gtk_widget_show (button);

  hbox = gtkW_hbox_new ((GTK_DIALOG (a_dlg)->vbox), FALSE, TRUE);
  frame = gtkW_frame_new (hbox, NULL);

  table = gtkW_table_new (frame, nhelp + 2, 3);
  index = 0;
  align = gtkW_align_x;
  gtkW_align_x = GTK_EXPAND|GTK_FILL;

  for (i = 0; i < nhelp; i++, index++)
    {
      if (help_document[i].action)
	{
	  gtkW_table_add_label (table, help_document[i].action,
				0, 1, index, help_document[i].flush_left);
	  if (help_document[i].condition)
	    gtkW_table_add_label (table, help_document[i].condition,
				  1, 2, index, help_document[i].flush_left);
	  gtkW_table_add_label (table, help_document[i].behavior,
				2, 3, index, help_document[i].flush_left);
	}
      else
	{
	  if (help_document[i].condition)
	    {
	      gtkW_table_add_label (table, help_document[i].condition,
				    0, 3, index, FALSE);
	    }
	  else			/* hseparator */
	    {
	      hseparator = gtk_hseparator_new ();
	      gtk_table_attach (GTK_TABLE (table), hseparator,
				0, 3, index, index + 1,
				GTK_FILL, GTK_FILL,
				gtkW_border_width, 2 * gtkW_border_width);
	      gtk_widget_show (hseparator);
	    }
	}
    }
  gtkW_align_x = align;
  gtk_widget_show (a_dlg);

  gdk_flush ();
  return 1;
}

static void
menu_change_directory_callback (GtkWidget *widget, gpointer data)
{
  if (data != NULL)
    {
      chdir ((GCHAR *) data);
      directory_cache_set_to_cwd (FALSE);
    }
}

static void
directory_jump_callback (GtkWidget *widget, gpointer data)
{
  GtkWidget	*jump_menu, *parents_menu, *history_menu, *menu_item;

  jump_menu = gtk_menu_new ();

  parents_menu = gtk_menu_item_new_with_label ("Parents");
  gtk_menu_append (GTK_MENU (jump_menu), parents_menu);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (parents_menu),
			     directory_cache_create_parents_menu ());
  gtk_widget_show (parents_menu);

  history_menu = gtk_menu_item_new_with_label ("History");
  gtk_menu_append (GTK_MENU (jump_menu), history_menu);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (history_menu),
			     directory_cache_create_history_menu ());
  gtk_widget_show (history_menu);

  menu_item = gtk_menu_item_new_with_label ("To ...");
  gtk_menu_append (GTK_MENU (jump_menu), menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      (GtkSignalFunc) fileselector_for_chdir_callback,
		      NULL);
  gtk_widget_show (menu_item);

  menu_item = gtk_menu_item_new_with_label ("Close menu");
  gtk_menu_append (GTK_MENU (jump_menu), menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      (GtkSignalFunc) NULL,
		      NULL);
  gtk_widget_show (menu_item);

  gtk_menu_popup (GTK_MENU (jump_menu), NULL, NULL, NULL, NULL, 1, 0);
}

static void
parent_directory_callback (GtkWidget *widget, gpointer data)
{
  chdir ("..");
  directory_cache_set_to_cwd (FALSE);
}

static void
forward_callback (GtkWidget *widget, gpointer data)
{
  thumbnail_panel_move_focus (1);
}

static void
select_and_forward_callback (GtkWidget *widget, gpointer data)
{
  GINDEX	index = -1;
  cache_entry	*cache;

  if (selection_is_active ())
    {
      GList *last = g_list_last (selection_list);

      index = (GINDEX) last->data;

      if (index < cwd_cache->ndir + cwd_cache->nimage)
	index++;
      else
	index = -1;
    }
  else
    if (0 < cwd_cache->nimage)
      index = cwd_cache->ndir;

  if ((cwd_cache->ndir <= index)
      && (index < cwd_cache->ndir + cwd_cache->nimage))
    {
      selection_add (index);
      cache = directory_cache_get_nth (index);
    thumbnail_panel_set_info (cache->info);
    }
  else
    thumbnail_panel_set_info (NULL);

  thumbnail_panel_update_sensitive_menu ();
}

static void
backward_callback (GtkWidget *widget, gpointer data)
{
  thumbnail_panel_move_focus (-1);
}

static void
next_callback (GtkWidget *widget, gpointer data)
{
  thumbnail_panel_move_focus (ncol_of_thumbnail);
}

static void
prev_callback (GtkWidget *widget, gpointer data)
{
  thumbnail_panel_move_focus (- ncol_of_thumbnail);
}

static void
next_page_callback (GtkWidget *widget, gpointer data)
{
  if (cwd_cache->display_page + 1 < directory_cache_npage ())
    {
      cwd_cache->display_page++;
      thumbnail_panel_update ();
    }
}

static void
prev_page_callback (GtkWidget *widget, gpointer data)
{
  if (0 < cwd_cache->display_page)
    {
      cwd_cache->display_page--;
      thumbnail_panel_update ();
    }
}

static void
open_callback (GtkWidget *widget, gpointer data)
{
  selection_open_files ();
}

static void
update_callback (GtkWidget *widget, gpointer data)
{
  directory_cache_delete_invalid_cache_files (FALSE);
}

static void
toggle_sort_mode_callback (GtkWidget *widget, gpointer data)
{
  VAL.sort_type = (VAL.sort_type == SORT_BY_NAME) ? SORT_BY_DATE : SORT_BY_NAME;
  directory_cache_set_to_cwd (TRUE);
  thumbnail_panel_set_info ((VAL.sort_type == SORT_BY_NAME)
			    ? "Changed to `sort by name' mode"
			    : "Changed to `sort by date' mode");
}

static void
purge_thumbnail_file_callback (GtkWidget *widget, gpointer data)
{
  directory_cache_delete_invalid_cache_files (TRUE);
}

static void
help_callback (GtkWidget *widget, gpointer client_data)
{
  about_dialog ();
}

static void
fileselector_for_copy_callback (GtkWidget *widget, gpointer client_data)
{
  GtkWidget *filesel;

  filesel = gtk_file_selection_new ("Copy selected images to");
  gtk_window_position (GTK_WINDOW (filesel), GTK_WIN_POS_MOUSE);

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
		      "clicked", (GtkSignalFunc) copy_callback,
		      filesel);

  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button),
			     "clicked", (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT (GTK_WINDOW (filesel)));

  if (selection_length ())
    {
      cache_entry	*selected;
      GCHAR		tmp[LINE_BUF_SIZE];

      selected = directory_cache_get_nth ((gint) selection_list->data);

      sprintf (tmp, "%s/%s", cwd_cache->name, selected->name);
      gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel), tmp);
    }
  else if (strlen (VAL.last_dir_name) > 0)
    {
      gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel),
				       VAL.last_dir_name);
    }
  else
    {
      gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel),
				       cwd_cache->name);
    }
  gtk_widget_show (filesel);
}

static void
fileselector_for_move_callback (GtkWidget *widget, gpointer client_data)
{
  GtkWidget *filesel;

  g_return_if_fail (selection_is_active ());
  g_return_if_fail (selection_kind == IMAGE);
  g_return_if_fail (file_confirm_operation ("Move"));

  filesel = gtk_file_selection_new ("Move selected images to");
  gtk_window_position (GTK_WINDOW (filesel), GTK_WIN_POS_MOUSE);

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
		      "clicked", (GtkSignalFunc) move_callback,
		      filesel);

  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button),
			     "clicked", (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT (GTK_WINDOW (filesel)));

  if (fileselector_last_pathname)
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel),
				     fileselector_last_pathname);
  gtk_widget_show (filesel);
}

static void
fileselector_for_chdir_callback (GtkWidget *widget, gpointer client_data)
{
  GtkWidget *filesel;

  filesel = gtk_file_selection_new ("Change directory");
  gtk_window_position (GTK_WINDOW (filesel), GTK_WIN_POS_MOUSE);

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
		      "clicked", (GtkSignalFunc) chdir_callback,
		      filesel);

  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button),
			     "clicked", (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT (GTK_WINDOW (filesel)));
  if (fileselector_last_pathname)
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel),
				     fileselector_last_pathname);

  gtk_widget_show (filesel);
}

static void
fileselector_for_mkdir_callback (GtkWidget *widget, gpointer client_data)
{
  GtkWidget *filesel;

  filesel = gtk_file_selection_new ("New directory");
  gtk_window_position (GTK_WINDOW (filesel), GTK_WIN_POS_MOUSE);

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
		      "clicked", (GtkSignalFunc) mkdir_callback,
		      filesel);

  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button),
			     "clicked", (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT (GTK_WINDOW (filesel)));
  if (fileselector_last_pathname != NULL)
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel),
				     fileselector_last_pathname);

  gtk_widget_show (filesel);
}

static void
copy_callback (GtkWidget *widget, gpointer data)
{
  GCHAR	*pathname, *tmp;

  tmp = gtk_file_selection_get_filename (GTK_FILE_SELECTION (data));
  pathname = (GCHAR *) g_malloc (strlen (tmp) + 1);
  strcpy (pathname, tmp);
  fileselector_set_last_value (pathname);

  gtk_widget_destroy (GTK_WIDGET (data));

  selection_copy_files_to (pathname);

  g_free (pathname);
}

static void
move_callback (GtkWidget *widget, gpointer data)
{
  GCHAR	*pathname, *tmp;

  tmp = gtk_file_selection_get_filename (GTK_FILE_SELECTION (data));
  pathname = (GCHAR *) g_malloc (strlen (tmp) + 1);
  strcpy (pathname, tmp);
  fileselector_set_last_value (pathname);

  gtk_widget_destroy (GTK_WIDGET (data));

  selection_move_files_to (pathname);

  g_free (pathname);
}

static void
delete_callback (GtkWidget *widget, gpointer data)
{
  if (! selection_is_active ())
    return;

  if (use_confirmor)
    {
      GCHAR buffer [LINE_BUF_SIZE];

      if (selection_length () == 1)
	{
	  cache_entry	*selected;

	  selected = directory_cache_get_nth ((gint) selection_list->data);
	  sprintf (buffer, " Do you want to delete the selected image: %s? ",
		   selected->name);
	}
      else
	{
	  sprintf (buffer,  " Do you want to delete the selected %d images? ",
		   selection_length ());
	}
      if (! gtkW_confirmor_dialog (TRUE, buffer, FALSE))
	return;
    }
  selection_delete_files ();
}

static void
select_all_callback (GtkWidget *widget, gpointer data)
{
  GINDEX	index;

  for (index = cwd_cache->ndir;
       index < cwd_cache->ndir + cwd_cache->nimage;
       index++)
    selection_add (index);
  thumbnail_panel_set_info (NULL);
}

static void
select_none_callback (GtkWidget *widget, gpointer data)
{
  selection_reset ();
  thumbnail_panel_set_info (NULL);
}

static void
chdir_callback (GtkWidget *widget, gpointer data)
{
  GCHAR	*pathname, *tmp;
  gint	f_kind;

  tmp = gtk_file_selection_get_filename (GTK_FILE_SELECTION (data));
  pathname = (GCHAR *) g_malloc (strlen (tmp) + 1);
  strcpy (pathname, tmp);
  fileselector_set_last_value (pathname);

  gtk_widget_destroy (GTK_WIDGET (data));

  tmp = pathname;
  f_kind = os_file_kind (tmp, TRUE);
  while ((f_kind == NOT_EXIST) || (f_kind == REGFILE))
    {
      tmp = file_get_parent_directory (tmp);
      f_kind = os_file_kind (tmp, TRUE);
    }

  switch (os_file_kind (tmp, TRUE))
    {
    case DIRECTORY:
      chdir (tmp);
      directory_cache_set_to_cwd (FALSE);
      break;
    default:
      thumbnail_panel_set_info ("Can't move there.");
      break;
    }
  g_free (pathname);
}

static void
mkdir_callback (GtkWidget *widget, gpointer data)
{
  GCHAR	*pathname, *tmp;

  tmp = gtk_file_selection_get_filename (GTK_FILE_SELECTION (data));
  pathname = (GCHAR *) g_malloc (strlen (tmp) + 1);
  strcpy (pathname, tmp);
  gtk_widget_destroy (GTK_WIDGET (data));

  os_mkdir (pathname);
  /* fileselector_set_last_value should be called after making the directory */
  fileselector_set_last_value (pathname);

  if (cwd_cache->purged)
    directory_cache_set_to_cwd (TRUE);

  g_free (pathname);
}

static void
selection_map_script_callback (GtkWidget *widget, gpointer data)
{
  selection_map_script ();
}

static void
selection_map_unix_command_callback (GtkWidget *widget, gpointer data)
{
  selection_map_unix_command ();
}

static void
gtkW_close_callback (GtkWidget *widget,
		     gpointer   data)
{
  gtk_main_quit ();
}

static void
gtkW_message_dialog (gint gtk_was_initialized, GCHAR *message)
{
  GtkWidget	*dlg;
  GtkWidget	*table;
  GtkWidget	*label;
  gchar		**argv;
  gint		argc;

  if (! gtk_was_initialized)
    {
      argc = 1;
      argv = g_new (gchar *, 1);
      argv[0] = g_strdup (PLUG_IN_NAME);
      gtk_init (&argc, &argv);
      gtk_rc_parse (gimp_gtkrc ());
    }

  dlg = gtkW_message_dialog_new (PLUG_IN_NAME);
  gtk_window_set_wmclass (GTK_WINDOW (dlg), "Guash", "Gimp");

  table = gtkW_table_new (GTK_DIALOG (dlg)->vbox, 1, 1);

  label = gtk_label_new (message);
  gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL|GTK_EXPAND,
		    0, 0, 0);

  gtk_widget_show (label);
  gtk_widget_show (dlg);

  gtk_main ();
  gdk_flush ();
}

static GtkWidget *
gtkW_message_dialog_new (GCHAR *name)
{
  GtkWidget *dlg, *button;

  dlg = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (dlg), name);
  gtk_window_position (GTK_WINDOW (dlg), GTK_WIN_POS_MOUSE);
  gtk_signal_connect (GTK_OBJECT (dlg), "destroy",
		      (GtkSignalFunc) gtkW_close_callback, NULL);
  gtk_container_border_width (GTK_CONTAINER (GTK_DIALOG (dlg)->action_area),
			      gtkW_border_width);

  /* Action Area */
  button = gtk_button_new_with_label ("OK");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT (dlg));
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->action_area), button,
		      TRUE, TRUE, 0);
  gtk_widget_grab_default (button);
  gtk_widget_show (button);

  return dlg;
}

static gint
gtkW_confirmor_dialog (gint gtk_was_initialized, GCHAR *message, gint default_value)
{
  GtkWidget	*dlg;
  GtkWidget	*table;
  GtkWidget	*label;
  gtkW_widget_table	wtable;
  gchar		**argv;
  gint		argc;

  if (! gtk_was_initialized)
    {
      argc = 1;
      argv = g_new (gchar *, 1);
      argv[0] = g_strdup (PLUG_IN_NAME);
      gtk_init (&argc, &argv);
      gtk_rc_parse (gimp_gtkrc ());
    }

  dlg = gtkW_confirmor_new (PLUG_IN_NAME, default_value, &wtable);

  table = gtkW_table_new (GTK_DIALOG (dlg)->vbox, 1, 1);

  label = gtk_label_new (message);
  gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL|GTK_EXPAND,
		    0, 0, 0);

  gtk_widget_show (label);
  gtk_widget_show (dlg);

  gtk_main ();
  gdk_flush ();
  return (gint) wtable.value;
}

static GtkWidget *
gtkW_confirmor_new (GCHAR *name, gint default_value, gtkW_widget_table *table)
{
  GtkWidget	*dlg, *button;

  dlg = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (dlg), name);
  gtk_window_position (GTK_WINDOW (dlg), GTK_WIN_POS_MOUSE);
  gtk_signal_connect (GTK_OBJECT (dlg), "destroy",
		      (GtkSignalFunc) gtkW_close_callback, NULL);
  gtk_container_border_width (GTK_CONTAINER (GTK_DIALOG (dlg)->action_area),
			      gtkW_border_width);
  table->widget = dlg;

  /* Action Area */
  button = gtk_button_new_with_label ("Yes");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) gtkW_confirmor_yes,
		      (gpointer) table);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->action_area), button,
		      TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  if (default_value == TRUE)
    {
      gtk_widget_grab_default (button);
    }
  gtk_widget_show (button);

  button = gtk_button_new_with_label ("No");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) gtkW_confirmor_no,
		      (gpointer) table);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->action_area), button,
		      TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  if (default_value == FALSE)
    {
      gtk_widget_grab_default (button);
    }
  gtk_widget_show (button);

  return dlg;
}

static void
gtkW_confirmor_yes (GtkWidget *widget, gpointer data)
{
  gtkW_widget_table *table;

  table = (gtkW_widget_table *)data;
  table->value = (gpointer) TRUE;
  gtk_widget_destroy ((GtkWidget *) (table->widget));
}

static void
gtkW_confirmor_no (GtkWidget *widget, gpointer data)
{
  gtkW_widget_table *table;

  table = (gtkW_widget_table *)data;
  table->value = (gpointer) FALSE;
  gtk_widget_destroy ((GtkWidget *) (table->widget));
}

static GtkWidget *
gtkW_frame_new (GtkWidget *parent, GCHAR *name)
{
  GtkWidget *frame;

  frame = gtk_frame_new (name);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_container_border_width (GTK_CONTAINER (frame), gtkW_border_width);
  if (parent != NULL)
    gtk_box_pack_start (GTK_BOX(parent), frame, TRUE, TRUE, 0);
  gtk_widget_show (frame);

  return frame;
}

static GtkWidget *
gtkW_hbox_new (GtkWidget *parent, gint expand, gint fill)
{
  GtkWidget	*hbox;

  hbox = gtk_hbox_new (FALSE, 2);
  gtk_container_border_width (GTK_CONTAINER (hbox), gtkW_border_width);
  if (parent)
    {
      gtk_box_pack_start (GTK_BOX (parent), hbox, expand, fill,
			  gtkW_border_width);
      /* gtk_container_add (GTK_CONTAINER (parent), hbox); */
    }
  gtk_widget_show (hbox);

  return hbox;
}

static GtkWidget *
gtkW_table_new (GtkWidget *parent, gint col, gint row)
{
  GtkWidget	*table;

  table = gtk_table_new (col,row, FALSE);
  gtk_container_border_width (GTK_CONTAINER (table), gtkW_border_width);
  gtk_container_add (GTK_CONTAINER (parent), table);
  gtk_widget_show (table);

  return table;
}

static gint
preview_event_handler (GtkWidget *widget, GdkEvent *event)
{
  GdkEventButton *bevent;
  gint		x, y, index;

  DEBUGBLOCK ("preview_event_handler...");

  gtk_widget_get_pointer (widget, &x, &y);

  bevent = (GdkEventButton *) event;
  index = (cwd_cache && VALID_POS_P (x, y)) ? (POS_TO_INDEX (x, y)) : -1;

  switch (event->type)
    {
    case GDK_EXPOSE:
      if (thumbnail_panel_resized)
	{
	  thumbnail_panel_resized = FALSE;
	  thumbnail_panel_update ();
	}
      /* from 0.99.5 [Sat Feb 28 06:30:45 1998] gtk_widget_show (dlg); */
      gdk_flush ();
      break;
      /*
    case GDK_2BUTTON_PRESS:
      switch (bevent->button)
	{
	case 1:
	  if ((0 <= index) && (index < cwd_cache->ndir + cwd_cache->nimage))
	    {
	      cache_entry *cache = directory_cache_get_nth (index);

	      if ((! cache->directory_p)
		  && (! bevent->state & GDK_SHIFT_MASK)
		  && (selection_is_active () && selection_member_p (index)))
		selection_open_files ();
	    }
	  break;
	default:
	  break;
	}
      break;
      */
    case GDK_BUTTON_PRESS:
      switch (bevent->button)
	{
	case 1:
	  if ((0 <= index) && (index < cwd_cache->ndir + cwd_cache->nimage))
	    {
	      cache_entry *cache = directory_cache_get_nth (index);

	      if (cache->directory_p)
		{
		  if (selection_is_active ())
		    {
		      if (bevent->state & GDK_CONTROL_MASK)
			{
			  selection_copy_files_to (cache->name);
			}
		      else if (bevent->state & GDK_SHIFT_MASK)
			{
			  if (file_confirm_operation ("Move"))
			    selection_move_files_to (cache->name);
			}
		      else
			{
			  selection_reset ();
			  chdir (cache->name);
			  directory_cache_set_to_cwd (FALSE);
			}
		    }
		  else
		    {
		      selection_reset ();
		      chdir (cache->name);
		      directory_cache_set_to_cwd (FALSE);
		    }
		}
	      else
		{
		  if (bevent->state & GDK_SHIFT_MASK)
		    {
		      selection_reverse_member (index);
		      if (selection_is_active ())
			thumbnail_panel_set_info (cache->info);
		    }
		  else
		    {
		      if (selection_is_active () && selection_member_p (index))
			selection_open_files ();
		      else
			{
			  selection_reset ();
			  selection_add (index);
			  thumbnail_panel_set_info (cache->info);
			}
		    }
		}
	    }
	  else
	    {
	      thumbnail_panel_set_info (NULL);
	    }
	  break;
	case 2:
	  if (bevent->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK))
	    {
	      if (0 < cwd_cache->display_page)
		cwd_cache->display_page--;
	      else
		cwd_cache->display_page = directory_cache_npage () - 1;
	    }
	  else
	    {
	      if (cwd_cache->display_page + 1 < directory_cache_npage ())
		cwd_cache->display_page++;
	      else if (0 < cwd_cache->display_page)
		cwd_cache->display_page = 0;
	    }
	  thumbnail_panel_update ();
	  break;
	case 3:
	  if (bevent->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK))
	    gtk_menu_popup (GTK_MENU (thumbnail_panel_root_menu),
			    NULL, NULL, NULL, NULL, 3, bevent->time);
	  else if (selection_is_active ())
	    gtk_menu_popup (GTK_MENU (thumbnail_panel_selection_menu),
			    NULL, NULL, NULL, NULL, 3, bevent->time);
	  else
	    gtk_menu_popup (GTK_MENU (thumbnail_panel_root_menu),
			    NULL, NULL, NULL, NULL, 3, bevent->time);
	  break;
	default:
	  thumbnail_panel_update ();
	  break;
	}
      break;
    case GDK_BUTTON_RELEASE:
    default:
      break;
    }
  DEBUGEND;

  return FALSE;
}

static gint
cursor_event_handler (GtkWidget *widget, GdkEvent *event)
{
  GdkEventKey	*kevent;

  kevent = (GdkEventKey *) event;

  gtk_widget_grab_focus (thumbnail_panel);
  switch (kevent->keyval)
    {
      /* Please give me the explanation of why the following code does not work!
    case GDK_Up:
      thumbnail_panel_move_focus (- ncol_of_thumbnail);
      gtk_widget_grab_focus (file_property);
      break;
    case GDK_Down:
      thumbnail_panel_move_focus (ncol_of_thumbnail);
      gtk_widget_grab_focus (cwd_label);
      break;
      */
      /* Please explain why the following focus moving is required!
	 I found this code after a long try-and-error, by chance.
      */
    case GDK_Left:
      thumbnail_panel_move_focus (-1);
      gtk_widget_grab_focus (file_property);
      break;
    case GDK_Right:
      thumbnail_panel_move_focus (1);
      gtk_widget_grab_focus (cwd_label);
      break;
    default:
      break;
    }
  return FALSE;
}

static GtkWidget *
gtkW_table_add_label (GtkWidget	*table,
		      GCHAR	*text,
		      gint	x0,
		      gint	x1,
		      gint	y,
		      gint	flush_left)
{
  GtkWidget *label;

  label = gtk_label_new (text);
  if (flush_left)
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 1.0);
  gtk_table_attach (GTK_TABLE(table), label, x0, x1, y, y+1,
		    gtkW_align_x, gtkW_align_y, 5, 0);
  gtk_widget_show (label);

  return label;
}

static void
gtkW_iscroll_entry_change_value (gtkW_widget_table *wtable)
{
  GtkWidget	*entry;
  gchar		buffer[32];
  GtkAdjustment *adjustment = (GtkAdjustment *) (wtable->widget);

  if (! cwd_cache)
    return;
  /*  adustment->value is double, that is not precise to hold long interger. */
  adjustment->value = * (gint *) (wtable->value);
  gtk_signal_emit_by_name (GTK_OBJECT (adjustment), "value_changed");
  entry = gtk_object_get_user_data (GTK_OBJECT (adjustment));
  if (entry)
    {
      sprintf (buffer, "%d", (gint) adjustment->value);
      gtk_entry_set_text (GTK_ENTRY (entry), buffer);
    }
}

static void
gtkW_iscroll_update (GtkAdjustment *adjustment,
		    gpointer       data)
{
  GtkWidget	*entry;
  gchar		buffer[32];
  gint		*val;

  if (! cwd_cache)
    return;
  val = data;
  if (*val != (gint) adjustment->value)
    {
      *val = adjustment->value;

      entry = gtk_object_get_user_data (GTK_OBJECT (adjustment));
      if (entry)
	{
	  sprintf (buffer, "%d", (int) adjustment->value);
	  gtk_entry_set_text (GTK_ENTRY (entry), buffer);
	}
      cwd_cache->display_page = (gint) adjustment->value - 1;
      thumbnail_panel_update ();
    }
}

static void
gtkW_ivscroll_entry_new (GtkWidget	**scroll,
			 GtkWidget	**entry,
			 GtkSignalFunc	scroll_update,
			 GtkSignalFunc	entry_update,
			 gint		*value,
			 gdouble	min,
			 gdouble	max,
			 gdouble	step,
			 gpointer	widget_entry)
{
  GtkObject	*adjustment;
  GCHAR		buffer[GTKW_ENTRY_BUFFER_SIZE];

  adjustment = gtk_adjustment_new (*value, min, max, step, step, step);
  gtk_signal_connect (GTK_OBJECT (adjustment), "value_changed",
		      (GtkSignalFunc) scroll_update, value);

  *scroll = gtk_vscrollbar_new (GTK_ADJUSTMENT (adjustment));
  /*
  gtk_widget_set_usize (*scroll, thumbnail_panel_width - GTKW_ENTRY_WIDTH - 40, 0);
  */
  gtk_range_set_update_policy (GTK_RANGE (*scroll), GTK_UPDATE_CONTINUOUS);

  *entry = gtk_entry_new ();
  gtk_object_set_user_data (GTK_OBJECT (*entry), adjustment);
  gtk_object_set_user_data (GTK_OBJECT (adjustment), *entry);
  gtk_widget_set_usize (*entry, GTKW_ENTRY_WIDTH, 0);

  sprintf (buffer, "%d", *value);
  gtk_entry_set_text (GTK_ENTRY (*entry), buffer);
  gtk_signal_connect (GTK_OBJECT (*entry), "changed",
		      (GtkSignalFunc) entry_update, value);

  if (widget_entry)
    {
      gtkW_widget_table *tentry = (gtkW_widget_table *) widget_entry;

      tentry->widget = (GtkWidget *) adjustment;
      tentry->updater = gtkW_iscroll_entry_change_value;
      tentry->value = value;
    }
}

static void
gtkW_ientry_update (GtkWidget *widget,
		    gpointer   data)
{
  GtkAdjustment *adjustment;
  gint	new_val;
  gint	val;

  if (! cwd_cache)
    return;

  val = cwd_cache->display_page + 1;
  new_val = atoi (gtk_entry_get_text (GTK_ENTRY (widget)));

  if (val != new_val)
    {
      adjustment = gtk_object_get_user_data (GTK_OBJECT (widget));

      if ((new_val >= adjustment->lower) &&
	  (new_val <= adjustment->upper))
	{
	  adjustment->value = new_val;
	  cwd_cache->display_page = (gint) new_val - 1;
	  gtk_signal_emit_by_name (GTK_OBJECT (adjustment), "value_changed");
	}
    }
}

#ifdef USE_DND
static void
dnd_drag_button_callback (GtkWidget *widget, GdkEvent *event)
{
  thumbnail_panel_set_info ("You failed to copy. Don't click but drag the button.");
}

static void
dnd_drag_request_callback (GtkWidget *widget, GdkEvent *event)
{
  GList	*l;
  GString *str;

  if (! selection_is_active ())
    {
      thumbnail_panel_set_info ("No selected images for dragging");
      return;
    }

  str = g_string_new (GUASH_DND_SIGNATURE);
  l = selection_list;
  while (l)
    {
      GINDEX		selected_id = (gint) l->data;
      cache_entry	*selected;

      selected = directory_cache_get_nth (selected_id);
      g_string_append (str, cwd_cache->name);
      g_string_append_c (str, '/');
      g_string_append (str, selected->name);
      g_string_append_c (str, '\n');
      l = l->next;
    }

  gtk_widget_dnd_data_set (widget, event, str->str, strlen (str->str) + 1);
  g_string_free (str, TRUE);
}

static void
dnd_drop (GtkWidget *widget, GdkEvent *event)
{
  GCHAR	*ptr = NULL;
  GCHAR	dest[LINE_BUF_SIZE];
  GCHAR	*subdir_p = NULL;
  gint	x, y, index;

  gtk_widget_get_pointer (widget, &x, &y);

  ptr = (GCHAR *)event->dropdataavailable.data;
  if (strlen (ptr) < strlen (GUASH_DND_SIGNATURE))
    return;
  if (strncmp (ptr, GUASH_DND_SIGNATURE, strlen (GUASH_DND_SIGNATURE)) != 0)
    return;
  ptr += strlen (GUASH_DND_SIGNATURE);
  index = VALID_POS_P (x, y) ? POS_TO_INDEX (x, y): -1;

  if ((0 <= index) && (index < cwd_cache->ndir + cwd_cache->nimage))
    {
      cache_entry	*cache = directory_cache_get_nth (index);

      if (cache->directory_p)
	{
	  sprintf (dest, "%s/%s", cwd_cache->name, cache->name);
	  subdir_p = cache->name;
	}
      else
	strcpy (dest, cwd_cache->name);
    }
  else
    strcpy (dest, cwd_cache->name);

  {
    GCHAR	*message;

    if (subdir_p == NULL)
      message = "current directory";
    else
      message = subdir_p;

    dnd_copy_files_to (ptr, dest, message, (subdir_p == NULL));
  }
}

static gint
dnd_copy_files_to (GCHAR *buffer, GCHAR *pathname, GCHAR *dir, gint cwd_p)
{
  GCHAR	info[256], first_file[LINE_BUF_SIZE];
  GCHAR	*ptr = buffer;
  gint	kind = NOT_EXIST;
  gint	success = 0;

  /* Since relations from id to image are changed, selection should be reset. */
  selection_reset ();

  while (*ptr)
    {
      gint	index = 0;

      kind = os_file_kind (pathname, TRUE);
      while ((ptr[index] != 0) && (ptr[index] != '\n'))
	index++;

      if ((kind == NOT_EXIST) || (kind == DIRECTORY))
	{
	  GCHAR		name[LINE_BUF_SIZE];
	  GCHAR		*file_basename;
	  GString	*new;

	  memcpy (name, ptr, index);
	  name[index] = 0;
	  file_basename = file_get_filename (name);
	  new = g_string_new (pathname);
	  if (kind == DIRECTORY)
	    {
	      if (pathname [strlen (pathname) - 1] != '/')
		g_string_append_c (new, '/');
	      g_string_append (new, file_basename);
	    }
	  /* try to copy `name' to `new->str' */
	  if (os_file_kind (new->str, TRUE) != NOT_EXIST)
	    {
	      sprintf (info, "%s already exists.", new->str);
	      gtkW_message_dialog (TRUE, info);
	    }
	  else if (image_file_copy (name, new->str) == TRUE)
	    {
	      if (success++ == 0)
		strcpy (first_file, file_basename);
	    }

	  g_string_free (new, TRUE);
	  g_free (file_basename);
	}
      else if (kind == REGFILE)
	{
	  sprintf (info, "%s already exists.", pathname);
	  gtkW_message_dialog (TRUE, info);
	}
      if (ptr[index] == 0)
	break;
      else
	ptr += index + 1;
    }
  if ((0 < success) && (kind == DIRECTORY))
    directory_cache_force_to_purge (pathname);

  if (0 < success)
    {
      GCHAR	buffer[LINE_BUF_SIZE];

      /* Since copying by Dnd does not allow filename renaming,
	 We need not to check directory. Thus FALSE is sufficient. */
      if (cwd_p)
	directory_cache_set_to_cwd (FALSE);

      /* then display information on result of dnd-copy */
      if (success == 1)
	sprintf (buffer, "Dropped file %s was copied to %s.", first_file, dir);
      else
	sprintf (buffer, "%d dropped files were copied to %s.", success, dir);
      thumbnail_panel_set_info (buffer);
    }
  else
    thumbnail_panel_set_info ("Failed to copy.");

  return success;
}
#endif

static void
gtkW_query_box (GCHAR *title, GCHAR *message, GCHAR *initial, GCHAR *data)
{
  GtkWidget *qbox;

  qbox = gtkW_query_string_box_new (title, message, initial, (gpointer) data);
  gtk_main ();
  gdk_flush ();
}

static void
gtkW_preview_force_to_update (GtkWidget *widget)
{
  GtkPreview	*preview = GTK_PREVIEW (widget);

  gtk_preview_put (GTK_PREVIEW (widget),
		   widget->window, widget->style->black_gc,
		   (widget->allocation.width - preview->buffer_width) / 2,
		   (widget->allocation.height - preview->buffer_height) / 2,
		   0, 0,
		   widget->allocation.width, widget->allocation.height);
  gdk_flush ();
}

/* copied from gimp/app/interface.c */
/*
 *  A text string query box
 */
typedef struct _QueryBox QueryBox;

struct _QueryBox
{
  GtkWidget *qbox;
  GtkWidget *entry;
  gpointer data;
};

static GtkWidget *
gtkW_query_string_box_new (char        *title,
			   char        *message,
			   char        *initial,
			   gpointer     data)
{
  QueryBox  *query_box;
  GtkWidget *qbox;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *entry;
  GtkWidget *button;

  query_box = (QueryBox *) g_malloc (sizeof (QueryBox));

  qbox = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (qbox), title);
  gtk_window_position (GTK_WINDOW (qbox), GTK_WIN_POS_MOUSE);
  gtk_signal_connect (GTK_OBJECT (qbox), "delete_event",
		      (GtkSignalFunc) gtkW_query_box_delete_callback,
		      query_box);
  gtk_container_border_width (GTK_CONTAINER (GTK_DIALOG (qbox)->action_area),
			      gtkW_border_height);

  button = gtk_button_new_with_label ("OK");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      (GtkSignalFunc) gtkW_query_box_ok_callback,
                      query_box);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (qbox)->action_area), button, TRUE, TRUE, 0);
  gtk_widget_grab_default (button);
  gtk_widget_show (button);

  button = gtk_button_new_with_label ("Cancel");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      (GtkSignalFunc) gtkW_query_box_cancel_callback,
                      query_box);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (qbox)->action_area), button, TRUE, TRUE, 0);
  gtk_widget_show (button);

  vbox = gtk_vbox_new (FALSE, 1);
  gtk_container_border_width (GTK_CONTAINER (vbox), gtkW_border_width);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (qbox)->vbox), vbox);
  gtk_widget_show (vbox);

  label = gtk_label_new (message);
  gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, FALSE, 0);
  gtk_widget_show (label);

  entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (vbox), entry, TRUE, TRUE, 0);
  if (initial)
    gtk_entry_set_text (GTK_ENTRY (entry), initial);
  gtk_widget_show (entry);

  query_box->qbox = qbox;
  query_box->entry = entry;
  query_box->data = data;

  gtk_widget_show (qbox);

  return qbox;
}

static gint
gtkW_query_box_delete_callback (GtkWidget *w,
				GdkEvent  *e,
				gpointer   client_data)
{
  gtkW_query_box_cancel_callback (w, client_data);
  return FALSE;
}

static void
gtkW_query_box_cancel_callback (GtkWidget *w,
				gpointer   client_data)
{
  QueryBox *query_box;

  query_box = (QueryBox *) client_data;

  *((GCHAR *) query_box->data) = 0;
  /*  Destroy the box  */
  gtk_widget_destroy (query_box->qbox);
  g_free (query_box);
  gtk_main_quit ();
}

static void
gtkW_query_box_ok_callback (GtkWidget *w,
			    gpointer   client_data)
{
  QueryBox *query_box;
  char *string;

  query_box = (QueryBox *) client_data;

  /*  Get the entry data  */
  string = g_strdup (gtk_entry_get_text (GTK_ENTRY (query_box->entry)));

  /*  Call the user defined callback  */
  /* (* query_box->callback) (w, query_box->data, (gpointer) string); */
  strcpy ((GCHAR *)query_box->data, string);
  /*  Destroy the box  */
  gtk_widget_destroy (query_box->qbox);
  g_free (query_box);
  gtk_main_quit ();
}

static gint
gtkW_parse_gimprc_gint (GCHAR *name, gint default_value)
{
  GParam	*return_vals;
  gint		nreturn_vals;
  gint		val = default_value;

  return_vals = gimp_run_procedure ("gimp_gimprc_query",
				    &nreturn_vals,
				    PARAM_STRING, name,
				    PARAM_END);
  if (return_vals[0].data.d_status == STATUS_SUCCESS)
    val = atoi (return_vals[1].data.d_string);
  gimp_destroy_params (return_vals, nreturn_vals);

  return val;
}

static GCHAR *
gtkW_parse_gimprc_string (GCHAR *name, GCHAR *result)
{
  GParam	*return_vals;
  gint		nreturn_vals;

  return_vals = gimp_run_procedure ("gimp_gimprc_query",
				    &nreturn_vals,
				    PARAM_STRING, name,
				    PARAM_END);
  if (return_vals[0].data.d_status == STATUS_SUCCESS)
    strcpy (result, return_vals[1].data.d_string);
  else
    *result = 0;
  gimp_destroy_params (return_vals, nreturn_vals);

  return result;
}
/* gtkW ends here */

static gint
save_xvpict_image (char *filename, gint32 image_ID, gint32 drawable_ID)
{
  FILE		*dest;
  GPixelRgn	src_rgn;
  GDrawable	*drawable = NULL;
  gint		i_width;
  gint		i_height;
  gint		gap;
  gint		x, y;
  GCHAR		*buf;

  drawable = gimp_drawable_get (drawable_ID);
  i_width = gimp_layer_width (drawable_ID);
  i_height = gimp_layer_height (drawable_ID);
  gap = (gimp_drawable_has_alpha (drawable_ID)) ? 1 : 0;

  if ((dest = fopen (filename, "w")) == NULL)
    {
      gimp_drawable_detach (drawable);
      return FALSE;
    }
  fprintf (dest, "P7 332\n");
  /* fprintf (dest, "#XVVERSION:Version 3.10a  Rev: 12/29/94\n"); */
  fprintf (dest, "#XVVERSION:Version 3.10a  Rev: 12/29/94 (faked by The GIMP/GUASH 1.0)\n");
  fprintf (dest, "#IMGINFO:%s\n",
	   gimp_layer_get_name (gimp_image_get_active_layer (image_ID)));
  fprintf (dest, "#END_OF_COMMENTS\n");
  fprintf (dest, "%d %d 255\n", i_width, i_height);

  buf = (GCHAR *) g_malloc ((3 + gap) * i_width);

  gimp_pixel_rgn_init (&src_rgn, drawable, 0, 0, i_width, i_height,
		       FALSE, FALSE);

  for (y = 0; y < i_height; y++)
    {
      GCHAR	*ch;
      GCHAR	val;

      gimp_pixel_rgn_get_row (&src_rgn, buf, 0, y, i_width);
      ch = buf;

      for (x = 0; x < i_width; x++, ch += 3)
	{
	  val = (*ch & 0xE0)
	    + ((*(ch + 1) & 0xE0) >> 3)
	    + ((*(ch + 2) & 0xC0) >> 6);
	  fprintf (dest, "%c", val);
	}
    }
  gimp_drawable_detach (drawable);
  fclose (dest);
  g_free (buf);
  return TRUE;
}
/* end of my part of guash.c */
#define EMBEDDED_IN_GUASH	1

static gint32
load_xvpict_image (char *filename)
{
	FILE *fd;
	char *name_buf, w_buf[4], h_buf[4];
	int i, j, w, h;
	unsigned char cmap[3*256];
	gint32 image_ID = -1, layer_ID;
	GPixelRgn pixel_rgn;
	GDrawable *drawable;
	GCHAR *dest;
#ifdef	EMBEDDED_IN_GUASH
	GCHAR comment[LINE_BUF_SIZE];
#endif

#ifdef	EMBEDDED_IN_GUASH
	DEBUGBLOCK ("load_xvpict_image...");
#else
	name_buf = g_malloc(strlen(filename) + 11);
	sprintf(name_buf, "Loading %s:", filename);
	gimp_progress_init(name_buf);
	g_free(name_buf);
	gimp_progress_update(1);
#endif
	fd = fopen(filename, "rb");
	if (!fd) {
#ifdef EMBEDDED_IN_GUASH
	  return -1;
#else
		printf("XVPics: can't open \"%s\"\n", filename);
		/* FIXME: quit the plug-in */
#endif
	}

	/* calculate the rgb 332 8bit palette          */
	/* NOTE: this palette should be pre-calculated */
	for (i = 0, j = 0; i < 256; i++, j+=3) {
		cmap[j+0] = (((i >> 5) & 0x07)*255) / 7;
		cmap[j+1] = (((i >> 2) & 0x07)*255) / 7;
		cmap[j+2] = (((i >> 0) & 0x03)*255) / 3;
	}

	/* skips the first three lines of comment */
	i = 4;
#ifdef	EMBEDDED_IN_GUASH
	/* Third line holds infos about the image. */
	while (--i)
	  if (fgets (comment, LINE_BUF_SIZE - 1, fd) == NULL) return -1;
	i = 1;
	while (i) if (fgetc(fd) == 0xA) i--;
#else
	while (i) if (fgetc(fd) == 0xA) i--;
#endif
	/* read the last line of comment that contains info on image size */
#ifdef	EMBEDDED_IN_GUASH
	if (fscanf (fd, "%s %s %*s\n", w_buf, h_buf) != 2)
	  return -1;
	w = atoi(w_buf);
	h = atoi(h_buf);
#else
	fscanf(fd, "%s %s %*s\n", w_buf, h_buf);
	printf("XVPict %s[%i|%i]\n", filename, w = atoi(w_buf), h = atoi(h_buf));
#endif
	/* create the image */
	image_ID = gimp_image_new(w, h, INDEXED);
	layer_ID = gimp_layer_new(image_ID, "Background", w, h, INDEXED_IMAGE, 100, NORMAL_MODE);
	gimp_image_add_layer(image_ID,layer_ID,0);
#ifdef EMBEDDED_IN_GUASH
	{
	  GCHAR	*basename = file_get_filename (filename);

	  name_buf = (GCHAR *) g_malloc (strlen (basename)
					 + strlen (comment) - 8);
	  strcpy (name_buf, basename);
	  name_buf [strlen (basename)] = ' ';
	  memcpy (name_buf + strlen (basename) + 1,
		  comment + 9,
		  strlen (comment) - 10);
	  name_buf[strlen (basename) + strlen (comment) - 9] = 0;
	  gimp_layer_set_name (layer_ID, name_buf);
	  g_free (basename);
	}
#endif
	drawable = gimp_drawable_get(layer_ID);
	dest = g_malloc(drawable->width*drawable->height);
	fread(dest, drawable->width*drawable->height, 1, fd);
	fclose (fd);
	gimp_image_set_filename(image_ID, filename);
	gimp_pixel_rgn_init(&pixel_rgn, drawable, 0, 0, drawable->width, drawable->height, TRUE, FALSE);
	gimp_pixel_rgn_set_rect(&pixel_rgn, dest, 0, 0, drawable->width, drawable->height);
	gimp_image_set_cmap(image_ID, cmap, 256);
	gimp_drawable_flush(drawable);
	gimp_drawable_detach(drawable);
	g_free(dest);
#ifdef EMBEDDED_IN_GUASH
	DEBUGEND;
#endif
	return image_ID;
}
/* guash.c ends here */
