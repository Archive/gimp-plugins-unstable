#!@prefix@/bin/gimpleproxy
!#
;;;; 	Copyright (C) 1998 Lauri Alanko <la@iki.fi>
;;;; 
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2, or (at your option)
;;;; any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this software; see the file COPYING.  If not, write to
;;;; the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
;;;; Boston, MA 02111-1307 USA
;;;; 
;;;; The GPL is also available at http://www.gnu.org/copyleft/gpl.html

; script-fu-compat.scm

(def-pdb
  "gimple-script-fu"
  'startup
  #f
  (lambda x
  (use-modules (gtk gtk) (gtk gdk) (ice-9 regex))
  (define-macro (enum enumname . args)
    (apply append
           `(begin (define ,enumname (apply append ',args)))
           (map (lambda (x)
                  (do ((a x (cdr a))
                       (b '() (cons `(define ,(car a) ,v) b))
                       (v 0 (+ v 1)))
                      ((null? a) b)))
                args)))
  

  (enum sf-enums
   (RGB_IMAGE RGBA_IMAGE GRAY_IMAGE GRAYA_IMAGE INDEXEDA_IMAGE)
   (NORMAL DISSOLVE BEHIND MULTIPLY SCREEN OVERLAY DIFFERENCE ADDITION
    SUBTRACT DARKEN_ONLY LIGHTEN_ONLY HUE SATURATION COLOR VALUE DIVIDE)
   (FG-BG-RGB FG-BG-HSV FG-TRANS CUSTOM)
   (LINEAR BILINEAR RADIAL SQUARE CONICAL-SYMMETRIC CONICAL-ASYMMETRIC
    SHAPEBURST-ANGULAR SHAPEBURST-SPHERICAL SHAPEBURST-DIMPLED)
   (REPEAT-NONE REPEAT-SAWTOOTH REPEAT-TRIANGULAR)
   (FG-BUCKET-FILL BG-BUCKET-FILL PATTERN-BUCKET-FILL)
   (FG-IMAGE-FILL BG-IMAGE-FILL WHITE-IMAGE-FILL TRANS-IMAGE-FILL
    NO-IMAGE-FILL)
   (RGB GRAY INDEXED)
   (RGB_IMAGE RGBA_IMAGE GRAY_IMAGE GRAYA_IMAGE INDEXED_IMAGE INDEXEDA_IMAGE)
   (RED-CHANNEL GREEN-CHANNEL BLUE-CHANNEL GRAY-CHANNEL INDEXED-CHANNEL)
   (WHITE-MASK BLACK-MASK ALPHA-MASK)
   (APPLY DISCARD)
   (EXPAND-AS-NECESSARY CLIP-TO-IMAGE CLIP-TO-BOTTOM-LAYER)
   (ADD SUB REPLACE INTERSECT)
   (PIXELS POINTS)
   (IMAGE-CLONE PATTERN-CLONE)
   (BLUR SHARPEN)
   (TRUE FALSE)
   (SF-IMAGE SF-DRAWABLE SF-LAYER SF-CHANNEL SF-COLOR SF-TOGGLE SF-VALUE
    SF-STRING SF-ADJUSTMENT SF-FONT)
   (SF-SLIDER SF-SPINNER)
   (FALSE TRUE))

  (define (assert expr)
    (if (not expr)
        (error "Assert error: "))) 

  (define gimplesf-prefix "g-")

  ; This is an alist whose elements are of the form:
  ; (scriptname proc img-based (type desc default) (type desc default) ... ) 

  (define sf-scriptdb '())

  (define pdb-cb list)

  (define (sf-parse-param type desc default)
    (define ti (assq type sf-argtypes))
    (if (not (string? desc))
        (error "Invalid description"))
    (if (not ti)
        (error "Invalid type"))
    (list (cadr ti) (caddr ti) desc ((list-ref ti 4) default)))

  (define (sf-parse-params-iter params parsed)
    (if (null? params)
        (reverse parsed)                ;paramdef
      (sf-parse-params-iter
       (cdr params)
       (cons (apply sf-parse-param (car params)) parsed))))


  (define (group-params-iter params accum)
    (if (null? params)
        (reverse accum)
      (group-params-iter (cdddr params) (cons (list (car params)
						    (cadr params)
						    (caddr params))
					      accum))))
  (define (script-fu-register name
                              path
                              help
                              author
                              copyright
                              date
                              img-types
                              . params)
    (define paramgrp (group-params-iter params '()))
    (define paramdefs (sf-parse-params-iter paramgrp '()))
    (define dbname (string-append gimplesf-prefix name))
    (define menu-path
      (regexp-substitute
       #f (string-match "Script-Fu" path) 'pre "GimpleSF" 'post)) 
    (define imgt (and (> (string-length img-types) 0) img-types))
    (define r-params (if imgt (cddr paramdefs) paramdefs))
    (def-pdb
      :name dbname
      :args r-params
      :path menu-path
      :func (sf-script-wrapper (string->symbol name) (map car paramgrp) imgt)
      :img-types imgt
      :help help
      :author author
      :copyright copyright
      :date date
      :blurb "A script-fu script run by gimple"))


  (define (sf-color->g-color lst)
    (apply make-color lst))
  (define (g-color->sf-color col)
    (list (color-red col) (color-green col) (color-blue col)))


  (define (replace str from to)
    (define newstr (string-copy str))
    (do ((i (string-index newstr from) (string-index newstr from i)))
        ((not i) newstr)
      (string-set! newstr i to)))
  
  (define (sf-drawable->g-drawable x)
    (if (number? x) null-drawable x))

  (define sf-argtypes
    `((,SF-IMAGE ,[image] "Image" ,id ,id)  
      (,SF-DRAWABLE ,[drawable] "Drawable" ,id ,sf-drawable->g-drawable)
      (,SF-LAYER ,[layer] "Layer" ,id ,id)
      (,SF-CHANNEL ,[channel] "Channel" ,id ,id)
      (,SF-COLOR ,[color] "Color" ,g-color->sf-color ,sf-color->g-color)
      (,SF-VALUE ,[string] "Value"
            ,(lambda (s)
               (call-with-input-string
                s
                (lambda (p) (eval-in-module (read p) sf-user-module))))
            ,id)
      (,SF-TOGGLE ,[boolean] "Toggle"
           ,(lambda (b) (if b 1 0))
           ,(lambda (b) (not (zero? b))))
      (,SF-FONT ,[string] "Font" ,id ,id)
      (,SF-STRING ,[string] "String" ,id ,id)
      (,SF-ADJUSTMENT ,[string] "Adjustment"
                      ,(lambda (arg) (call-with-input-string arg read))
                      ,id)))

  (define (g-arg->sf-arg arg type)
    ((cadddr (assq type sf-argtypes)) arg))
  
  (define (sf-arg-type->g-arg-type type)
    (cadr (assq type sf-argtypes)))

  ; these are used to convert between gimple's type system
  ; (which uses vectors for arrays, and color structs) and script-fu's

  (define gparamtype-alist
    `((int32 ,id ,id)
      (int16 ,id ,id)
      (int8 ,id ,id)
      (float ,id ,id)
      (string ,id ,id)
      (stringarray ,vector->list ,list->vector)
      (color ,g-color->sf-color ,sf-color->g-color)
      ; All right, s-f represents these as integers, but no code should rely
      ; on that..
      (drawable ,id ,sf-drawable->g-drawable)
      (image ,id ,id)
      (layer ,id ,id)
      (channel ,id ,id)
      (display ,id ,id)
      (int32array ,vector->list ,list->vector)
      (int16array ,vector->list ,list->vector)
      (int8array ,vector->list ,list->vector)
      (floatarray ,vector->list ,list->vector)
      (stringarray ,vector->list ,list->vector)))

  (define (sf-param->g-param arg paramtype)
    ((caddr (assq paramtype gparamtype-alist)) arg))

  (define (g-param->sf-param arg paramtype)
    ((cadr (assq paramtype gparamtype-alist)) arg))

  (define sf-user-module (resolve-module '(gimp sf-user)))

  (define ((sf-script-wrapper procname sf-types img-based) . g-args)
    (define old-mod (current-module))
    (define real-proc (module-ref sf-user-module procname))
    (define sf-args (map g-arg->sf-arg g-args ((if img-based cddr id) sf-types)))
    (define f-args (append (if img-based
                               (list current-image current-drawable)
                               '())
                           sf-args))
    (set-current-module sf-user-module)
    (apply real-proc f-args)
    (set-current-module old-mod)
    
    ; return zero values
    (values))

  (define (add-procedure procname)
    (define sfname (symbol (replace procname #\_ #\-)))
    (module-define! sf-user-module sfname
                    (sf-pdb-wrapper procname)))

  ; converts "foo:bar:baz" to (foo bar baz)
  (define (listify-path path)
    (listify-path-iter path 0 '()))

  (define (listify-path-iter path idx accum)
    (define i (string-index path #\: idx))
    (if i
        (listify-path-iter path
                           (+ i 1)
                           (cons (substring path idx i) accum))
      (reverse (cons (substring path idx (string-length path)) accum))))
  
  ; read the scripts in the correct module..
  (define (read-scripts dirname)
    (if (access? dirname (logior R_OK X_OK))
        (let ((dir (opendir dirname)))
          (do ((file (readdir dir) (readdir dir)))
              ((eof-object? file) #t)
            (if (string-match "\.scm$" file)
                (catch #t
                  (lambda ()
                    (define mod (current-module))
                    (set-current-module sf-user-module)
                    (load (string-append dirname "/" file))
                    (set-current-module mod))
                  (lambda (key . args)
                    (warn "Error" key args "processing file" file))))))
      #f))
  
  (define (find-scripts)
    (define path (listify-path (gimp-gimprc-query "script-fu-path")))
    (for-each read-scripts path))
  

  (define (sf-pdb-wrapper procname)
    (define paramtypes (pdb-procedure-param-types procname))
    (define rettypes (pdb-procedure-return-types procname))
    (lambda sf-params
      (define g-params (map sf-param->g-param sf-params paramtypes))
      (define g-retvals (values->list
                         (apply pdb-run procname g-params)))
      (define sf-retvals (map g-param->sf-param g-retvals rettypes))
      sf-retvals))
  
  (debug-set! depth 50)
  
  (define (export-user vars)
    (for-each
     (lambda (var)
       (module-define! sf-user-module var (eval-here var)))
     vars))

  (define (sf-init)
    (define allprocs (value-ref (gimp-procedural-db-query
                                   ".*" ".*" ".*" ".*" ".*" ".*" ".*")
                                 1))
    
    (define proclist (vector->list allprocs))
    (for-each add-procedure proclist)

    (export-user (cons 'script-fu-register sf-enums))
    ; do the rest of init stuff now that we have pdb bindings
    ((module-ref sf-user-module 'init)))


  (sf-init)
  (find-scripts)
  ;maybe this should be done earlier.. gimp blocks until this..
  (gimple-extension-ack)

  ;(top-repl)
  (do ()
      (#f)
    (gimple-extension-process 0))))
