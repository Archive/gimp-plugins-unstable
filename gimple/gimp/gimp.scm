
(define-module (gimp gimp)
  :use-module (gimp util)
 ; :use-module (gtk dynlink)
  :use-module (ice-9 regex))

;(merge-compiled-code "gimple_init" "libgimple")


; interface to c code: I-Params are represented as a (type . value) pair,
; where type is a I-ParamType integer, value depends


(define-macro (define-enum name . alts)
  `(define-public ,name ',(list->vector alts)))

(define-enum e-param-type
  int32 int16 int8 float string int32array
  int16array int8array floatarray stringarray color region display image
  layer channel drawable selection boundary path status)

(define-enum e-run-mode
  interactive noninteractive with-last-vals)

(define-enum e-layer-mode
  normal dissolve #f multiply screen overlay difference addition subtract
  darken-only lighten-only hue saturation color value)

(define-enum e-image
  rgb gray indexed)

(define-enum e-drawable
  rgb rgba gray graya indexed indexeda)

(define-enum e-status
  execution-error calling-error pass-through success)

(define-enum e-procedure-type
  #f plug-in extension temporary)


(define (make-i-param type value)
  (cons (enum type e-param-type) value))

(define i-param-value cdr)
(define (i-param-type p) (denum (car p) e-param-type))

; returns a list of return values
(define-public (pdb-procedure-run procname . params)
  (define r (apply gimple-run-pdb procname params))
  (case (denum (i-param-value (car r)) e-status)
    ((success) (cdr r))
    ((calling-error) (throw 'pdb-calling-error procname params))
    ((execution-error) (throw 'pdb-execution-error procname params))
    ; this shouldn't occur in practice..
    ((pass-through) (throw 'pdb-pass-through procname params))))


(define-public (pdb-procedure-param-types pdbname)
  (map pdb-arg-type (vector->list (pdb-info-args (pdb-info pdbname)))))

(define-public (pdb-procedure-return-types pdbname)
  (map pdb-arg-type (vector->list (pdb-info-retvals (pdb-info pdbname)))))


(define-public (pdb-bound? pdbname)
  (if (hash-ref pdb-cache pdbname)
      #t
      (let ()
        (define x (make-i-param 'string ".*"))
        (define ans (pdb-procedure-run "gimp_procedural_db_query"
                                       (make-i-param 'string
                                                    (regexp-quote pdbname))
                                       x x x x x x))
        (define procnames
          (vector->list
           (i-param-value (cadr ans))))
        (not (null? procnames)))))

(define pdb-cache (make-hash-table 233))

(define (reffer i) (lambda (v) (vector-ref v i)))

(multi-define (pdb-info-blurb
               pdb-info-help
               pdb-info-author
               pdb-info-copyright
               pdb-info-date
               pdb-info-proctype
               pdb-info-args
               pdb-info-retvals)
              (do ((i 0 (+ i 1))
                   (a '() (cons (reffer i) a)))
                  ((= i 8) (reverse a))))

(define pdb-arg-type (reffer 0))
(define pdb-arg-name (reffer 1))
(define pdb-arg-desc (reffer 2))

(define (pdb-info pdbname)
  (define pdbarg (make-i-param 'string pdbname))
  (define x (hash-ref pdb-cache pdbname))
  (or x
      (let ()
        (multi-define
         (blurb help author copyright date proctype nargs nretvals)
         (map i-param-value
              (pdb-procedure-run "gimp_procedural_db_proc_info"
                                 pdbarg)))
        (define (paraminfo pdbproc n)
          (let loop ((i n)
                     (a '()))
            (if (zero? i)
                (list->vector a)
                (let ()
                  (multi-define (type name desc)
                                (map i-param-value
                                     (pdb-procedure-run
                                      pdbproc
                                      pdbarg (make-i-param 'int32 (- i 1)))))
                  (loop (- i 1)
                        (cons (vector (denum type e-param-type)
                                      name
                                      desc)
                              a))))))

        (define args (paraminfo "gimp_procedural_db_proc_arg" nargs))
        (define retvals (paraminfo "gimp_procedural_db_proc_val" nretvals))
        (define y (vector blurb help author copyright date
                          (denum proctype e-procedure-type) args retvals))
        (hash-set! pdb-cache pdbname y)
        y)))

(define (opaque-int-type typename)
  (make-record-type typename '(value) (lambda (s p)
                                        (for-each
                                         (lambda (x) (display x p))
                                         `("#<"
                                           ,typename
                                           " "
                                           ,(struct-ref s 0)
                                           ">")))))


(define image-type (opaque-int-type "Image"))
(define layer-type (opaque-int-type "Layer"))
(define channel-type (opaque-int-type "Channel"))
(define drawable-type (opaque-int-type "Drawable"))
(define display-type (opaque-int-type "Display"))
(define color-type (make-record-type "Color" '(r g b)))

(define-public (color->list color)
  (list (color-red color) (color-green color) (color-blue color)))
(define-public (list->color lst)
  (apply make-color lst))

(define-public color-red (record-accessor color-type 'r))
(define-public color-set-red! (record-modifier color-type 'r))
(define-public color-green (record-accessor color-type 'g))
(define-public color-set-green! (record-modifier color-type 'g))
(define-public color-blue (record-accessor color-type 'b))
(define-public color-set-blue! (record-modifier color-type 'b))
(define-public make-color (record-constructor color-type))
(define-public color? (record-predicate color-type))

(define-public image? (record-predicate image-type))
(define-public drawable? (record-predicate drawable-type))
(define-public layer? (record-predicate layer-type))
(define-public channel? (record-predicate channel-type))
(define-public display? (record-predicate display-type))

(define drawable-ref (record-accessor drawable-type 'value))
(define layer-ref (record-accessor layer-type 'value))
(define channel-ref (record-accessor channel-type 'value))
(define display-ref (record-accessor channel-type 'value))
                        

(define (vector-checker pred?)
  (lambda (v)
    (and (vector? v)
         (let loop ((i (vector-length v)))
           (cond ((zero? i) #f)
                 ((pred? (vector-ref v (- i 1)))
                  (loop (- i 1)))
                 (else #f))))))

(define (vector-mapper f)
  (lambda (x)
    (list->vector (map f (vector->list x)))))

(define to-int-arr (vector-mapper inexact->exact))
(define to-float-arr (vector-mapper exact->inexact))
(define real-arr? (vector-checker real?))
(define string-arr? (vector-checker string?))
(define make-drawable (record-constructor drawable-type))
(define make-display (record-constructor display-type))

(define convertors
  `((image ,(record-accessor image-type 'value)
           ,(record-constructor image-type)
           ,image?)
    (color ,(lambda (c)
              (vector (color-red c)
                      (color-green c)
                      (color-blue c)))
           ,(lambda (v)
              (apply make-color (vector->list v)))
           ,color?)
    (string ,id ,id ,string?)
    (int32 ,inexact->exact ,id ,real?)
    (int16 ,inexact->exact ,id ,real?)
    (int8 ,inexact->exact ,id ,real?)
    (float ,exact->inexact ,id ,real?)
    (int32array ,to-int-arr ,id ,real-arr?)
    (int16array ,to-int-arr ,id ,real-arr?)
    (int8array ,to-int-arr ,id ,real-arr?)
    (floatarray ,to-float-arr ,id ,real-arr?)
    (stringarray ,id ,id ,string-arr?)
    (status ,(lambda (x) (enum x e-status))
            ,(lambda (x) (denum x e-status))
            ,(let ((l (vector->list e-status))) (lambda (x) (memq x l))))
    ; Bah, gotta be lenient with these.. lotsa code assumes (rightly)
    ; that the initial drawable argument is a layer...
    (layer ,drawable-ref ,make-drawable ,drawable?)
    (channel ,drawable-ref ,make-drawable ,drawable?)
    (drawable ,drawable-ref ,make-drawable ,drawable?)
    (display ,display-ref ,make-display ,display?)))

(define (convert-record param-type)
  (assq param-type convertors))

(define output-convertor cadr)
(define input-convertor caddr)
(define type-checker cadddr)


(define-public (g-param->i-param arg param-type)
  (define rec (convert-record param-type))
  (or ((type-checker rec) arg) (error "bad type when converting to i-param"
                                      arg param-type))
  (make-i-param param-type ((output-convertor rec) arg)))

(define-public (i-param->g-param iparam)
  (define rec (convert-record (i-param-type iparam)))
  ((input-convertor rec) (i-param-value iparam)))


(define-public null-drawable ((record-constructor drawable-type) -1))

(define-public (pdb-procedure-wrapper pdbname)
  (define paramtypes (pdb-procedure-param-types pdbname))
  (define numparams (length paramtypes))
  (define (wrapper . args)
    (if (not (= (length args) numparams))
        (error pdbname "wrong number of args"))
    (define pdbargs (map g-param->i-param args paramtypes))
    (define pdbretvals (apply pdb-procedure-run pdbname pdbargs))
    (list->values (map i-param->g-param pdbretvals)))
  (set-procedure-property! wrapper 'pdb-name pdbname)
  wrapper)


(define-public (pdb-run pdbname . args)
  (define paramtypes (pdb-procedure-param-types pdbname))
  (define numparams (length paramtypes))
  (if (not (= (length args) numparams))
      (error pdbname "wrong number of args"))
  (define pdbargs (map g-param->i-param args paramtypes))
  (define pdbretvals (apply pdb-procedure-run pdbname pdbargs))
  (list->values (map i-param->g-param pdbretvals)))


(define ((run-wrapper proc) name . i-params)
  (define g-params (map i-param->g-param i-params))
  (define-values (g-retvals ret-types) (apply proc name g-params))
  (define i-retvals (map g-param->i-param g-retvals ret-types))
  i-retvals)

(define-public (gimp-main cmdline query run end)
  (gimple-main cmdline
               query
               (run-wrapper run)
               end))

(define (g-param-def->i-param-def def)
  (cons (enum (car def) e-param-type)
        (cdr def)))

(define-public (install-pdb-procedure              
                pdbname
                blurb
                help
                author
                copyright
                date
                menupath
                imgtypes
                proctype
                paramdefs
                retdefs)
  (gimple-install-proc (enum proctype e-procedure-type)
                       (map g-param-def->i-param-def paramdefs)
                       (map g-param-def->i-param-def retdefs)
                       pdbname
                       blurb
                       help
                       author
                       copyright
                       date
                       menupath
                       imgtypes))
