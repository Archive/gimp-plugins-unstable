(define-module (gimp sf-user)
  :use-module (ice-9 slib))

;;
;; SIOD compatibility stuff
;;

(require 'random)

(define-macro (prog1 first . rest)
  (define tmp (gensym))
  `(begin (define ,tmp ,first)
          ,@rest
          ,tmp))  

(define-macro (while test . body)
  `(do () ((not ,test)) ,@body))

(define (fmod nom den) (- nom (* (truncate (/ nom den)) den)))
(define (nth i l) (list-ref l i))
(define *pi* (* 4 (atan 1)))
(define cons-array make-vector)
(define aset vector-set!)
(define aref vector-ref)
(define pow expt)

;; SIOD's set! is really screwed up. If the variable being set is
;; unbound, it is bound at the top level. Otherwise it is reset. So we
;; have to find out if a variable is bound in a _local_ environment.. thus
;; we create the syntax manually. Also, SIOD set! returns the new value.

;; There would be no prob otherwise, but all scripts use these "features"..

(define set! 
  ; we memoize the correct place for the variable..
  (procedure->memoizing-macro
   (lambda (exp env)
     (define variable (cadr exp))
     (define value (caddr exp))
     `(begin 
        (,@(let loop ((e env))
             (define frame (car e))
             (cond ((procedure? frame)
                    ; here we create the top level binding
                    `(',variable-set! ',(frame variable #t)))
                   ((eq? (car frame) variable)
                    `(set-cdr! ',frame))
                   ((and (list? (car frame)) (list-index (car frame) variable))
                    => (lambda (i)
                         `(',set-car! ',(list-tail (cdr frame) i))))
                   (else (loop (cdr e)))))
         ,value)
        ,variable))))

           

(define gimp-data-dir #f)
(define gimp-plugin-dir #f)

; this can be evaluated only after the bindings have been created
(define (init)
  (set! gimp-data-dir (car (gimp-gimprc-query "gimp_data_dir")))
  (set! gimp-plugin-dir (car (gimp-gimprc-query "gimp_plugin_dir"))))


(define (the-environment) #t)
(define (symbol-bound? var env)
  (defined? var))

(define rand random)

