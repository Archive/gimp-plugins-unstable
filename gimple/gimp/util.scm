(define-module (gimp util)
  :use-module (ice-9 slib))

(require 'macro-by-example)



;; Basic multiple values implementation..
;; this is pretty much the same as in slib, except for the printing..

(define (print-values s p)
  (define v (struct-ref s 0))
  (display "[" p)
  (if (null? v)
      (display "]" p)
      (begin
        (write (car v) p)
        (do ((x (cdr v) (cdr x)))
            ((null? x) (display "]" p))
          (display " " p)
          (write (car x) p)))))

(define values-rtd
  (make-record-type "Values" '(values) print-values))

(define make-values
  (record-constructor values-rtd))

(define access-values
  (record-accessor values-rtd 'values))


(define-public (call-with-current-continuation x)
  (@call-with-current-continuation
   (lambda (c) (x (lambda z (if (= 1 (length z)) (car z) (make-values z)))))))


(define-public values? (record-predicate values-rtd)) 

(define-public values
  (lambda x
    (call-with-current-continuation
     (lambda (c)
       (apply c x)))))
  
(define-public call-with-values
  (lambda (a b)
    (define v (a))
    (if (values? v)
        (apply b (access-values v))
        (b v))))


(define-public (list->values x)
  (apply values x))

(define-syntax initialize
 (syntax-rules
         ()
         ((initialize (a . x))
          (begin
            (initialize a)
            (initialize x)))
         ((initialize ())
          #f)
         ((initialize x)
          (define x #f))))

(define-syntax multi-define
  (syntax-rules
   ()
   ((multi-define x exp)
    (begin
      (initialize x)
      (multi-set! x exp)))))

(define-syntax values->list
  (syntax-rules
   ()
   ((values->list exp)
    (call-with-values (lambda () exp) list))))



(define-syntax multi-set!
  (syntax-rules
   ()
   ((multi-set! (a . x) exp)
    (let ((%value exp))
      (multi-set! a (car %value))
      (multi-set! x (cdr %value))))
   ((multi-set! () exp)
    #f)
   ((multi-set! x exp)
    (set! x exp))))

(define-syntax define-values
  (syntax-rules
   ()
   ((define-values x exp)
    (multi-define x (call-with-values (lambda () exp) list)))))

(define-syntax set-values!
  (syntax-rules
   ()
   ((set-values! x exp)
    (multi-set! x (call-with-values (lambda () exp) list)))))

(define-syntax value-ref
  (syntax-rules
   ()
   ((value-ref exp idx)
    (call-with-values (lambda () exp) (lambda x (list-ref x idx))))))

(define-macro (multi-swap a b)
  (define tmps (map gensym a))
  `(let ,(map list tmps a)
     ,@(map (lambda (x y) `(set! ,x ,y)) a b)
     ,@(map (lambda (x y) `(set! ,x ,y)) b tmps)))

(export-syntax define-values
               set-values!
               value-ref
               multi-define
               multi-set!
               initialize
               values->list
               multi-swap)

;; for #\_ -> #\- conversions

(define-public (string-translate str from to)
  (define newstr (string-copy str))
  (do ((i (string-index str from) (string-index str from i)))
      ((not i) newstr)
    (string-set! str i to)))

; debug thingy

(defmacro-public unquote (x)
  (define tmp (gensym))
  `(with-output-to-port (current-error-port)
     (lambda ()
       (display "Expression: ")
       (write ',x)
       (newline)
       (define ,tmp ,x) 
       (display "Value: ")
       (write ,tmp)
       (newline)
       ,tmp)))

(define-public (->string x)
  (call-with-output-string
   (lambda (p)
     (display x p))))

(define-public (parse-keywords lst template)
  (define l (map cdr template))
  (define (get-index kw)
    (let loop ((i 0)
               (k template))
      (cond ((null? k) #f)
            ((eq? kw (caar k)) i)
            (else (loop (+ i 1) (cdr k))))))
  (let loop ((i 0)
             (r lst))
    (cond ((null? r) l)
          ((keyword? (car r))
           (list-set! l (get-index (car r)) (cadr r))
           (loop i (cddr r)))
          (else
           (list-set! l i (car r))
           (loop (+ i 1) (cdr r))))))

(define-public (named-arg-wrapper func template)
  (lambda x
    func
    template
    (apply func (parse-keywords x template))))


(define local-environment
  (procedure->syntax
   (lambda (exp env)
     env)))

(define-macro (eval-here exp)
  `(local-eval ,exp (local-environment)))

(export-syntax local-environment eval-here)

(define-public (enum val vec)
  (define len (vector-length vec))
  (let loop ((i 0))
    (cond ((= i len) #f)
          ((equal? (vector-ref vec i) val) i)
          (else (loop (+ i 1))))))

(define-public (denum i vec)
  (vector-ref vec i))

