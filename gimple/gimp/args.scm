(define-module (gimp args)
  :use-module (gimp gimp)
  :use-module (gimp util)
  :use-module (gtk gtk))



(define arg-type
  (make-record-type "GimpleArgType" '(widget
                                      gparam-type
                                      input-converter)))



(define-public make-arg-type (record-constructor arg-type))
(define-public arg-widget
  (record-accessor arg-type 'widget))
(define-public arg-gparam-type
  (record-accessor arg-type 'gparam-type))
(define-public arg-input-converter
  (record-accessor arg-type 'input-converter))


;;;
;;; Argtype definitions
;;;

(define gimple-tag
  (list (gensym 'gimple)))

; A string type

(define-public [string]
  (make-arg-type
   (lambda (default)
     (let ((entry (gtk-entry-new)))
       (gtk-entry-set-text entry default)
       (values entry (lambda () (gtk-entry-get-text entry)))))
   'string
   id))

(define-public [boolean]
  (make-arg-type
   (lambda (default)
     (let ((toggle (gtk-check-button-new)))
       (gtk-toggle-button-set-state toggle default)
       (values toggle (lambda () (gtk-toggle-button-active toggle)))))
   'int32
   (lambda (x)
     (not (zero? x)))))

(define-public (make-[enum] lst)
  (let* ((vec (list->vector lst))
         (in (from-enum-wrapper vec))
         (out (to-enum-wrapper vec)))
    (make-arg-type
     (lambda (default)
       (define group #f)
       (define enum-list
         (map (lambda (alt)
                (define button ,(gtk-radio-button-new-with-label
                                group
                                (->string alt)))
                (set! group button)
                button)
              lst))
       (gtk-toggle-button-set-state (list-ref enum-list (out default)) #t)
       (define box ,(gtk-vbox-new #t 0))
       (for-each (lambda (button)
                   (gtk-box-pack-start box button))
                 enum-list)
       (values box
               (lambda ()
                 (do ((widgets enum-list (cdr widgets))
                      (enums lst (cdr enums)))
                     ((gtk-toggle-button-active (car widgets))
                      (car enums))))))
     'int32
     in)))
     
(define-public [integer]
  (make-arg-type
   #f
   'int32
   id))

(define-public [image] 
  (make-arg-type
   #f
   'image
   id))

(define-public [drawable]
  (make-arg-type
   #f
   'drawable
   id))

(define-public [channel]
  (make-arg-type
   #f
   'channel
   id))

(define-public [layer]
  (make-arg-type
   #f
   'layer
   id))

(define-public [color]
  (make-arg-type
   (lambda (default)
     (define adjs (map (lambda (val) (gtk-adjustment-new val 0 255 1 1 0))
                       (color->list default)))
     (define scales (map gtk-hscale-new adjs))
     (define labels (map gtk-label-new '("R" "G" "B")))
     (define table (gtk-table-new 3 2 #f))
     (define i 0)
     (for-each (lambda (label scale)
                 (gtk-table-attach table label 0 1 i (1+ i) '() '())
                 (gtk-widget-show label)
                 (gtk-table-attach table scale 1 2 i (1+ i))
                 (gtk-scale-set-digits scale 0)
                 (gtk-widget-show scale)
                 (set! i (1+ i)))
               labels
               scales)
     (define getter
       (lambda ()
         (list->color
          (map (lambda (adj)
                 (inexact->exact (gtk-adjustment-value adj)))
               adjs))))
     (values table getter))
   'color
   id))
   
