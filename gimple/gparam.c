/* Gimple - Gimp in Guile
 * Copyright (C) 1998 Lauri Alanko <la@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "gimple.h"

typedef struct _TypeInfo TypeInfo;
struct _TypeInfo{
	void (*s2g)(); /* (SCM, paramdata*) */
	SCM (*g2s)(); /* (const paramdata*) */
};

static void s2g_null(SCM s, GParamData* g);
static SCM  g2s_null(GParamData*);
static void s2g_int32(SCM s, gint32* g);
static SCM g2s_int32(gint32* g);
static void s2g_int16(SCM s, gint16* g);
static SCM g2s_int16(gint16* g);
static void s2g_int8(SCM s, gint8* g);
static SCM g2s_int8(gint8* g);
static void s2g_float(SCM s, gdouble* g);
static SCM g2s_float(gdouble* g);
static void s2g_string(SCM arg, gchar** g);
static SCM g2s_string(gchar** d);
static void s2g_color(SCM arg, GParamColor* g);
static SCM g2s_color(GParamColor* g);
static void s2g_stringarray(SCM arg, gchar*** g);
static SCM g2s_stringarray(gchar*** g);
static void s2g_int32array(SCM arg, gint32** g);
static SCM g2s_int32array(gint32** g);
static void s2g_int16array(SCM arg, gint16** g);
static SCM g2s_int16array(gint16** g);
static void s2g_int8array(SCM arg, gint8** g);
static SCM g2s_int8array(gint8** g);
static void s2g_floatarray(SCM arg, gdouble** g);
static SCM g2s_floatarray(gdouble** g);

static TypeInfo typeinfo[PARAM_END];

#define DEFTYPE(i,s,g) \
do{ \
	typeinfo[i].s2g=s; \
	typeinfo[i].g2s=g; \
}while(0)

void gimple_gparam_init(void){
	int i;
	for(i=0;i<PARAM_END;i++)
		DEFTYPE(i, s2g_null, g2s_null);
	DEFTYPE(PARAM_INT32, s2g_int32, g2s_int32);
	DEFTYPE(PARAM_INT16, s2g_int16, g2s_int16);
	DEFTYPE(PARAM_INT8, s2g_int8, g2s_int8);
	DEFTYPE(PARAM_FLOAT, s2g_float, g2s_float);
	DEFTYPE(PARAM_COLOR, s2g_color, g2s_color);
	DEFTYPE(PARAM_STRING, s2g_string, g2s_string);
	DEFTYPE(PARAM_STRINGARRAY, s2g_stringarray, g2s_stringarray);
	DEFTYPE(PARAM_INT32ARRAY, s2g_int32array, g2s_int32array);
	DEFTYPE(PARAM_INT16ARRAY, s2g_int16array, g2s_int16array);
	DEFTYPE(PARAM_INT8ARRAY, s2g_int8array, g2s_int8array);
	DEFTYPE(PARAM_FLOATARRAY, s2g_floatarray, g2s_floatarray);
	DEFTYPE(PARAM_DRAWABLE, s2g_int32, g2s_int32);
	DEFTYPE(PARAM_LAYER, s2g_int32, g2s_int32);
	DEFTYPE(PARAM_IMAGE, s2g_int32, g2s_int32);
	DEFTYPE(PARAM_DISPLAY, s2g_int32, g2s_int32);
	DEFTYPE(PARAM_CHANNEL, s2g_int32, g2s_int32);
	DEFTYPE(PARAM_STATUS, s2g_int32, g2s_int32);
}

static SCM g2s_null(GParamData* g){
	return SCM_UNSPECIFIED;
}

static void s2g_null(SCM s, GParamData* g){
	g->d_int32=0;
}

static void s2g_int32(SCM s, gint32* g){
	*g=gh_scm2long(s);
}

static SCM g2s_int32(gint32* g){
	return gh_long2scm(*g);
}

static void s2g_int16(SCM s, gint16* g){
	*g=gh_scm2int(s);
}

static SCM g2s_int16(gint16* g){
	return gh_int2scm(*g);
}

static void s2g_int8(SCM s, gint8* g){
	*g=gh_scm2int(s);
}

static SCM g2s_int8(gint8* g){
	return gh_int2scm(*g);
}

static void s2g_float(SCM s, gdouble* g){
	*g=gh_scm2double(s);
}

static SCM g2s_float(gdouble* g){
	return gh_double2scm(*g);
}

static void s2g_string(SCM arg, gchar** g){
	if(!gh_scm2bool(arg))
		*g=NULL;
	else{
		char *tmp=gh_scm2newstr(arg, NULL);
		*g=g_strdup(tmp);
		free(tmp);
	}
}

static SCM g2s_string(gchar** d){
	return gh_str02scm(*d);
}

static void s2g_color(SCM arg, GParamColor* g){
	g->red = gh_scm2long(gh_vector_ref(arg, gh_int2scm(0)));
	g->green = gh_scm2long(gh_vector_ref(arg, gh_int2scm(1)));
	g->blue = gh_scm2long(gh_vector_ref(arg, gh_int2scm(2)));
}

static SCM g2s_color(GParamColor* g){
	return gh_vector(gh_list(gh_long2scm(g->red),
				 gh_long2scm(g->green),
				 gh_long2scm(g->blue),
				 SCM_UNDEFINED));
}

static void s2g_stringarray(SCM arg, gchar*** g){
	gint i=gh_vector_length(arg);
	*g=g_new(gchar*, i);
	while(i--)
		s2g_string(gh_vector_ref(arg, gh_int2scm(i)),
			   &((*g)[i]));
}

static SCM g2s_stringarray(gchar*** g){
	/* Nasty! We get the length from the previous GParam */
	gint32 l=*(gint32*)(((gpointer)g)-sizeof(GParam));
	SCM s=gh_make_vector(gh_long2scm(l), SCM_BOOL_F);
	while(l--)
	    gh_vector_set_x(s, gh_int2scm(l),
			    gh_str02scm((*g)[l]));
	return s;
}

static void s2g_int32array(SCM arg, gint32** g){
	gint i=gh_vector_length(arg);
	*g=g_new(gint32, i);
	while(i--)
	    (*g)[i]=gh_scm2long(gh_vector_ref(arg, gh_int2scm(i)));
}

static SCM g2s_int32array(gint32** g){
	gint32 l=*(gint32*)(((gpointer)g)-sizeof(GParam));
	SCM s=gh_make_vector(gh_long2scm(l), SCM_BOOL_F);
	while(l--)
	    gh_vector_set_x(s, gh_int2scm(l),
			    gh_long2scm((*g)[l]));
	return s;
}

static void s2g_int16array(SCM arg, gint16** g){
	gint i=gh_vector_length(arg);
	*g=g_new(gint16, i);
	while(i--)
	    (*g)[i]=gh_scm2int(gh_vector_ref(arg, gh_int2scm(i)));
}

static SCM g2s_int16array(gint16** g){
	gint16 l=*(gint32*)(((gpointer)g)-sizeof(GParam));
	SCM s=gh_make_vector(gh_long2scm(l), SCM_BOOL_F);
	while(l--)
	    gh_vector_set_x(s, gh_int2scm(l),
			    gh_int2scm((*g)[l]));
	return s;
}

static void s2g_int8array(SCM arg, gint8** g){
	gint i=gh_vector_length(arg);
	*g=g_new(gint8, i);
	while(i--)
	    (*g)[i]=gh_scm2int(gh_vector_ref(arg, gh_int2scm(i)));
}

static SCM g2s_int8array(gint8** g){
	gint l=*(gint32*)(((gpointer)g)-sizeof(GParam));
	SCM s=gh_make_vector(gh_long2scm(l), SCM_BOOL_F);
	while(l--)
	    gh_vector_set_x(s, gh_int2scm(l),
			    gh_int2scm((*g)[l]));
	return s;
}

static void s2g_floatarray(SCM arg, gdouble** g){
	gint i=gh_vector_length(arg);
	*g=g_new(gdouble, i);
	while(i--)
	    (*g)[i]=gh_scm2double(gh_vector_ref(arg, gh_int2scm(i)));
}

static SCM g2s_floatarray(gdouble** g){
	gint l=*(gint32*)(((gpointer)g)-sizeof(GParam));
	SCM s=gh_make_vector(gh_long2scm(l), SCM_BOOL_F);
	while(l--)
	    gh_vector_set_x(s, gh_int2scm(l),
			    gh_double2scm((*g)[l]));
	return s;
}


void gimple_lst2gparamarr(SCM lst, GParam** params, gint* nparams){
	gint n=*nparams=gh_length(lst);
	GParam *a=*params=g_new(GParam, n);
	gint i;
	
	for(i=0; i<n; i++, lst=gh_cdr(lst)){
		GParamType t=a[i].type=gh_scm2int(gh_caar(lst));
		typeinfo[t].s2g(gh_cdar(lst), &a[i].data);
	}
}

SCM gimple_gparamarr2lst(GParam* params, gint n){
	SCM lst=NIL;
	while(n--){
		GParamType t=params[n].type;
		lst=gh_cons(gh_cons(gh_int2scm(t),
				    typeinfo[t].g2s(&params[n].data)),
			    lst);
	}
	return lst;
	
}
			 
			 
	
