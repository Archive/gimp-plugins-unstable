#include <glib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <stdlib.h>
#include <signal.h>

static FILE* sockstream;

void chat(const gchar* message, const gchar* reply){
	static gchar buffer[1024];
	
	if(message){
		fprintf(sockstream, "%s\n", message);
		fflush(sockstream);
	}

	fgets(buffer, sizeof(buffer), sockstream);

	if(strncmp(reply, buffer, strlen(reply)))
		g_error("Communication error with daemon.");
}
	   
int main(int argc, char* argv[]){
	gint readfd, writefd;
	gchar* sockpath;
	gchar* scriptname;
	static gchar buffer[1024];
	struct sockaddr_un addr;
	gboolean query;
	gint sock;
	gint i;
	fd_set rfds;
	
	
	if ((argc < 5) || (strcmp (argv[2], "-gimp") != 0))
		g_error ("%s must be run by gimp", argv[0]);
	query=((argc == 6) && (!strcmp(argv[5], "-query")));
	
	scriptname = argv[1];
	readfd = atoi (argv[3]);

	writefd = atoi (argv[4]);
	sockpath = getenv("GIMPLED_SOCKET");
	if(!sockpath)
		sockpath=g_strconcat(g_get_home_dir(),
				     "/.gimp-1.1/gimpled-socket",
				     NULL);

	sock=socket(AF_UNIX, SOCK_STREAM, 0);
	
	g_assert(sock!=-1);

	addr.sun_family=AF_UNIX;
	strcpy(addr.sun_path, sockpath);
	if(connect(sock, &addr, sizeof(addr)))
		g_error("Connection to socket failed.");

	sockstream=fdopen(dup(sock), "r+");
	setvbuf(sockstream, NULL, _IOLBF, 0);

	chat(NULL, "Gimpled here, how may I be of assistance?");
	if(query)
		chat("Hey, check out what sorta script this is, OK?",
		     "All right. Which script are you talking about?" );
	else
		chat("Run this script for me, will ya?",
		     "Can do. What's its name?");
	chat(scriptname,
	     "Okie. Now just handle me the gimpwire, and off we go.");
	chat("Sure thing. Here you are.",
	     "Thanks, my good man.");

	fclose(sockstream);
	
	while(TRUE){
		FD_ZERO(&rfds);
		FD_SET(readfd, &rfds);
		FD_SET(sock, &rfds);
		i=select(MAX(readfd, sock)+1, &rfds, NULL, NULL, NULL);
		if(i==-1)
			g_error("Error upon select");
		if(FD_ISSET(readfd, &rfds)){
			i=read(readfd, buffer, sizeof(buffer));
			write(sock, buffer, i);
		}
		if(FD_ISSET(sock, &rfds)){
			i=read(sock, buffer, sizeof(buffer));
			if(i==0)
				return 0;
			write(writefd, buffer, i);
		}
	}

	return 0;
}
		
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	

	
	
	
	
	
	
	
  
  
