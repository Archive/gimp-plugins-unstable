/* Gimple - Gimp in Guile
 * Copyright (C) 1998 Lauri Alanko <la@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "gimple.h"
#include <guile-gtk.h>

static void
gimple_main (int argc, 
	     char** argv,
	     void* closure){
	gimple_init();
	scm_set_program_arguments(argc-1, argv+1, NULL);
	scm_primitive_load_path(scm_makfrom0str("ice-9/boot-9.scm"));
	scm_primitive_load(scm_makfrom0str(argv[1]));
}

int main(int argc, char *argv[]){
	gh_enter(argc, argv, gimple_main);
	return 0;
}



