#include "gimple.h"


static void query (void);
static void run	(char    *name, 
		 int      nparams, 
		 GParam  *param, 
		 int     *nreturn_vals, 
		 GParam **return_vals);
static void quit(void);
SCM srun;
SCM squery;
SCM squit;


GPlugInInfo PLUG_IN_INFO={
	NULL, /* init_proc is deprecated! */
	quit,
	query,
	run
};



/* Is this legal? gimp_quit calls exit().. guile might not like it */
static SCM scmproc_gimp_quit(void){
	gimp_quit();
	return SCM_UNSPECIFIED;
}

static SCM scmproc_gimp_main(SCM args, SCM querycb, SCM runcb, SCM quitcb){
	gchar** argv;
	gint argc, i;

	argc=gh_length(args);
	argv=g_new(gchar*, argc);
	for(i=0;i<argc;i++){
		argv[i]=g_strdup(SCM_ROCHARS(gh_car(args)));
		args=gh_cdr(args);
	}
	if(querycb==SCM_BOOL_F)
		PLUG_IN_INFO.query_proc=NULL;
	if(runcb==SCM_BOOL_F)
		PLUG_IN_INFO.run_proc=NULL;
	if(quitcb==SCM_BOOL_F)
		PLUG_IN_INFO.quit_proc=NULL;
	
	srun=runcb;
	squery=querycb;
	squit=quitcb;
	gimp_main(argc, argv);
	/* never reached, unfortunately */
	return SCM_UNSPECIFIED;
};

	
static void query(void){
	gh_call0(squery);
}

static void quit(void){
	gh_call0(squit);
}

static void run(char    *name, 
		int      nparams, 
		GParam  *params, 
		int     *nreturn_vals, 
		GParam **return_vals){
	SCM sname, sparams, sreturn_vals;
	sname=gh_str02scm(name);
	sparams=gimple_gparamarr2lst(params, nparams);
	sreturn_vals=gh_apply(srun, gh_cons(sname, sparams));
	gimple_lst2gparamarr(sreturn_vals, return_vals, nreturn_vals);
}

void gimple_init(void){
	gimple_gparam_init();
	gimple_pdb_init();
	gh_new_procedure("gimple-main", scmproc_gimp_main, 4, 0, 0);
	gh_new_procedure("gimple-quit", scmproc_gimp_quit, 0, 0, 0);
}
