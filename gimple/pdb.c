/* Gimple - Gimp in Guile
 * Copyright (C) 1998 Lauri Alanko <la@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "gimple.h"

static SCM scmproc_run_pdb (SCM proc, SCM args);
static SCM scmproc_install_proc(SCM type, SCM params, SCM return_vals, 
				SCM strings);
static SCM scmproc_extension_process(SCM timeout);
static SCM scmproc_extension_ack(void);

static void pdbproc_run_scm(char    *name,
			       int      nparams,
			       GParam  *params,
			       int     *nreturn_vals,
			       GParam **return_vals);

static gchar* scm_to_str(SCM x);


/* Exported */

void gimple_pdb_init(void){
	gh_new_procedure("gimple-run-pdb", scmproc_run_pdb, 1, 0, TRUE);
	gh_new_procedure("gimple-install-proc", scmproc_install_proc, 3, 0, TRUE);
	gh_new_procedure("gimple-extension-process",
			 scmproc_extension_process, 1, 0, FALSE);
	gh_new_procedure("gimple-extension-ack",
			 scmproc_extension_ack, 0, 0, FALSE);
}
		
/* Static */

static gchar* scm_to_str(SCM x){
	if(!gh_scm2bool(x))
		return NULL;
	else
		return gh_scm2newstr(x, NULL);
}

static void lst2paramdefarr(SCM lst, GParamDef** arr, gint *nparams){
	gint n=*nparams=gh_length(lst);
	gint i;
	GParamDef* a=*arr=g_new(GParamDef, n);;
	for(i=0;i<n;i++,lst=gh_cdr(lst)){
		SCM t;
		a[i].type=gh_scm2int(gh_car(t=gh_car(lst)));
		a[i].name=scm_to_str(gh_car(t=gh_cdr(t)));
		a[i].description=scm_to_str(gh_car(t=gh_cdr(t)));
	}		
}

static void freeparamdefarr(GParamDef* arr, gint nparams){
	gint i;
	for(i=0;i<nparams;i++){
		free(arr[i].name);
		free(arr[i].description);
	}		
	g_free(arr);
}

static SCM scmproc_install_proc(SCM type, SCM params, SCM return_vals, 
				SCM strings){
	char** s;
	GParamDef* cparams;
	GParamDef* cretvals;
	gint nparams, nretvals;
	gint i, ctype;

	g_assert(gh_length(strings)==8);
	s=g_new(char*, 8);
	for(i=0;i<8;i++,strings=gh_cdr(strings))
		if(gh_car(strings)==SCM_BOOL_F)
			s[i]=NULL;
		else
			s[i]=scm_to_str(gh_car(strings));
	lst2paramdefarr(params, &cparams, &nparams);
	lst2paramdefarr(return_vals, &cretvals, &nretvals);
	
	ctype=gh_scm2int(type);
	if(ctype==PROC_TEMPORARY)
		gimp_install_temp_proc(s[0], s[1], s[2],
				       s[3], s[4], s[5],
				       s[6], s[7], PROC_TEMPORARY,
				       nparams, nretvals,
				       cparams, cretvals,
				       pdbproc_run_scm);
	else
		gimp_install_procedure(s[0], s[1], s[2],
				       s[3], s[4], s[5],
				       s[6], s[7], ctype,
				       nparams, nretvals,
				       cparams, cretvals);
		
	freeparamdefarr(cparams, nparams);
	freeparamdefarr(cretvals, nretvals);
	for(i=0;i<8;i++)
	    free(s[i]);
	g_free(s);
	return SCM_UNDEFINED;
}

static void pdbproc_run_scm(char    *name,
			    int      nparams,
			    GParam  *params,
			    int     *nreturn_vals,
			    GParam **return_vals){
	SCM sname, sparams, sreturn_vals;

	sname=gh_str02scm(name);
	sparams=gimple_gparamarr2lst(params, nparams);
	
	sreturn_vals=gh_apply(srun, gh_cons(sname, sparams));
	gimple_lst2gparamarr(sreturn_vals, return_vals, nreturn_vals);
}

static SCM scmproc_extension_process(SCM timeout){
	extern void gimp_extension_process (guint timeout);
	gimp_extension_process(gh_scm2int(timeout));
	return SCM_UNSPECIFIED;
}

static SCM scmproc_extension_ack(void){
	extern void gimp_extension_ack(void);
	gh_defer_ints();
	gimp_extension_ack();
	gh_allow_ints();
	
	return SCM_UNSPECIFIED;
}
	
static SCM scmproc_run_pdb (SCM proc, SCM args){
	gchar* proc_name;
	SCM sretval;
	GParam *params, *retvals;
	gint nparams, nretvals;
	

	gimple_lst2gparamarr(args, &params, &nparams);
	proc_name=scm_to_str(proc);
	retvals=gimp_run_procedure2(proc_name, &nretvals,
				    nparams, params);
	free(proc_name);
	
	gimp_destroy_params(params, nparams);
	sretval=gimple_gparamarr2lst(retvals, nretvals);
	gimp_destroy_params(retvals, nretvals);
	return sretval;
}

