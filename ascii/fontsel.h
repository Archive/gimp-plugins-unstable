/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Fontselect-utility for plug-ins. Composed from text_tool.c,
 * build_menu.c, linked.c by Peter Kirchgessner, pkirchg@aol.com
 *
 */
#ifndef __FSELECT_H__
#define __FSELECT_H__

typedef struct _FontSelect FontSelect;
typedef struct
{
  GtkWidget *widget;
  FontSelect *fsel;
} FontSelectDialog;

typedef struct
{
  char *foundry;    /* Foundry (NULL, "*", "adobe", ... */
  char *family;     /* Family (NULL, "*", "courier", ... */
  char *weight;     /* Weight (NULL, "*", "bold", ... */
  char *slant;      /* Slant (NULL, "*", "i", "o", "r" */
  char *set_width;  /* Set Width (NULL, "*", "condensed", ... */
  char *spacing;    /* Spacing (NULL, "*", "c", "m", "p" */
  char *registry;   /* Registry (NULL, "*", "adobe", "dec", ... */
  char *encoding;   /* Encoding (NULL, "*", ... */
  double size;      /* Size */
  int use_pixel;    /* Size flag (0: points, 1: pixels) */
  int border;       /* Size of border around text */
  int antialias;    /* Antialias flag */
  char *text;       /* The given text */

  int hide_registry; /* Use registry ? */
  int hide_encoding; /* Use encoding ? */
  int hide_border;   /* Use border-entry ? */
} FontProperties;

/* Creates a widget with a font selection dialog. */
/* The widget is packed into main_box. */
/* fprop keeps startup font properties/settings */
FontSelectDialog *fontselect_dialog_new (GtkWidget *main_box,
                                         FontProperties *fprop);

/* Reset the FontProperties structure */
void fontselect_properties_reset (FontProperties *fprop);

/* Get the information that has been selected by the dialog */
FontProperties   *fontselect_properties_get (FontSelectDialog *fd);

/* Release memory allocated by fontselect_properties_get () */
void fontselect_properties_free (FontProperties *fprop);

/* Release memory allocated by fontselect_dialog_new () */
void fontselect_dialog_free (FontSelectDialog *fd);

#endif
/* __FSELECT_H__ */
