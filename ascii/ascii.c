/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 * ASCII file plugin
 * Copyright (C) 1997 Peter Kirchgessner
 * e-mail: pkirchg@aol.com, WWW: http://members.aol.com/pkirchg
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/* Event history:
 * V 1.00, PK, 05-Oct-97: Creation
 * V 1.10, PK, 16-Nov-97: Add fontselect dialog
 * V 1.11, PK, 20-Dec-97: Fix problem with leading white space
 * V 1.12, PK, 10-Jan-98: Port for gimp V0.99.17/gtk+-0.99.2
 */
static char dversio[] =                           "v1.12  10-Jan-98";
static char ident[] = "@(#) GIMP ASCII file-plugin v1.12  10-Jan-98";

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gtk/gtk.h"
#include "libgimp/gimp.h"
#include "fontsel.h"

static char *margin_text[] = { "Top margin", "Left margin", "Right margin",
                               "Bottom margin", "Tab stop", "Line distance" };
#define NUM_POS_ENTRIES (sizeof (margin_text) / sizeof (margin_text[0]))

/* Load info  */
typedef struct
{
  gint margin[NUM_POS_ENTRIES];    /* Margins top, left, right, bottom, */
                                   /*  tab stop, line distance */
  guint char_height;               /* Character height */
  guint use_pixel;                 /* 0: points, 1: pixels */
  char foundry[32];
  char family[64];
  char weight[32];
  char slant[8];
  char set_width[32];
  char spacing[8];
  guint antialias;
  unsigned char back_color[3];     /* Background colour */
  unsigned char forg_color[3];     /* Foreground colour */
  guint prv_width, prv_height;     /* Size of previews */
} ASCIILoadVals;

typedef struct
{
  gint  run;  /*  run  */
} ASCIILoadInterface;


/* Declare some local functions.
 */
static void   query      (void);
static void   run        (char    *name,
                          int      nparams,
                          GParam  *param,
                          int     *nreturn_vals,
                          GParam **return_vals);

static gint32 load_image (char *filename);

static gint32 create_new_image (char *filename, guint width, guint height,
                GDrawableType gdtype, gint32 *layer_ID, GDrawable **drawable,
                GPixelRgn *pixel_rgn);

static gint32 load_ascii (char *filename, FILE *ifp);

static void   check_load_vals (void);

static void   subst_tabs (int tabstop, char *in, char *out, int maxout);

static int    get_ascii_size (FILE *ifp, int num_lines, int *width, int *height,
                              int *line_height);

static void   set_pixels (int numpix, unsigned char *dst,
                          unsigned char *rgb);

/* Handling for color selection dialogs */
typedef struct
{
  GtkWidget *activate;   /* The button that activates the color sel. dialog */
  GtkWidget *colselect;  /* The colour selection dialog itself */
  GtkWidget *preview;    /* The colour preview */
  unsigned char color[3];/* The selected colour */
  int idx;               /* The index of the color selection dialog */
} CSEL;

/* Dialog-handling */
typedef struct
{
  GtkWidget *dialog;
  GtkWidget *margin[NUM_POS_ENTRIES];
  CSEL      csel[2];
  FontSelectDialog *fd;
} LoadDialogVals;

static gint   load_dialog              (void);
static void   load_close_callback      (GtkWidget *widget,
                                        gpointer   data);
static void   load_ok_callback         (GtkWidget *widget,
                                        gpointer   data);
static void   color_button_callback    (GtkWidget *widget,
                                        gpointer data);
static void   color_select_ok_callback (GtkWidget *widget,
                                        gpointer data);
static void   color_select_cancel_callback (GtkWidget *widget,
                                        gpointer data);
static void   color_preview_show       (GtkWidget *widget,
                                        unsigned char *color);
static void   add_color_button         (int csel_index,
                                        GtkWidget *table,
                                        LoadDialogVals *vals);
static void   show_message             (char *);


GPlugInInfo PLUG_IN_INFO =
{
  NULL,    /* init_proc */
  NULL,    /* quit_proc */
  query,   /* query_proc */
  run,     /* run_proc */
};

static ASCIILoadVals asciivals =
{
  { 5, 5, 5, 5, 3, 2 }, /* Margins, tab stops, line distance */
  18,                /* Character height */
  1,                 /* Size flag (0: points, 1: pixels) */
  "*",               /* Foundry */
  "courier",         /* Family */
  "*",               /* Weight */
  "r",               /* Slant */
  "*",               /* Set width */
  "*",               /* Spacing */
  0,                 /* Antialias */
  { 255, 255, 255 }, /* Background colour */
  { 0, 0, 0 },       /* Foreground colour */
  50, 20             /* Preview size */
};

static ASCIILoadInterface asciiint =
{
  FALSE     /* run */
};


/* The run mode */
static GRunModeType l_run_mode;

#define STRMCPY(dst,src) { strncpy (dst, src, sizeof(dst)); \
                           dst[sizeof(dst)-1] = '\0'; }


MAIN ()


static void
query (void)

{
  static GParamDef load_args[] =
  {
    { PARAM_INT32, "run_mode", "Interactive, non-interactive" },
    { PARAM_STRING, "filename", "The name of the file to load" },
    { PARAM_STRING, "raw_filename", "The name of the file to load" },
    { PARAM_INT32, "top_margin", "Number of pixels to add on top of picture" },
    { PARAM_INT32, "left_margin", "Number of pixels to add on left side of picture" },
    { PARAM_INT32, "right_margin", "Number of pixels to add on right side of picture" },
    { PARAM_INT32, "bottom_margin", "Number of pixels to add at bottom of picture" },
    { PARAM_INT32, "tab_stops", "tab stops" },
    { PARAM_INT32, "line_distance", "line distance" },
    { PARAM_STRING, "foundry", "foundry of font (\"*\", \"freefont\",...)" },
    { PARAM_STRING, "family", "family of font (\"*\", \"courier\",...)" },
    { PARAM_STRING, "weight", "weight of font (\"*\", \"bold\",...)" },
    { PARAM_STRING, "slant", "slant of font (\"*\", \"i\",...)" },
    { PARAM_STRING, "set_width", "set_width of font (\"*\", \"condensed\",...)" },
    { PARAM_STRING, "spacing", "spacing of font (\"*\", \"c\",...)" },
    { PARAM_FLOAT, "size", "size of font" },
    { PARAM_INT32, "use_pixel", "size flag (0: points, 1: pixels)" },
    { PARAM_INT32, "antialias_flag", "antialias flag (0/1)" },
    { PARAM_COLOR, "back_color", "Background color" },
    { PARAM_COLOR, "fore_color", "Foreground color" }
  };
  static GParamDef load_return_vals[] =
  {
    { PARAM_IMAGE, "image", "Output image" },
  };
  static int nload_args = sizeof (load_args) / sizeof (load_args[0]);
  static int nload_return_vals = sizeof (load_return_vals)
                               / sizeof (load_return_vals[0]);

  gimp_install_procedure ("file_ascii_load",
                          "load ascii file",
                          "load ascii file and generate image that keeps the text",
                          "Peter Kirchgessner (pkirchg@aol.com)",
                          "Peter Kirchgessner",
                          dversio,
                          "<Load>/Ascii",
                          NULL,
                          PROC_PLUG_IN,
                          nload_args, nload_return_vals,
                          load_args, load_return_vals);

  /* Register file plugin by plugin name and handable extensions */
  gimp_register_load_handler ("file_ascii_load", "asc,txt", "");
}


static void
run (char    *name,
     int      nparams,
     GParam  *param,
     int     *nreturn_vals,
     GParam **return_vals)

{
  static GParam values[2];
  GRunModeType run_mode;
  GStatusType status = STATUS_SUCCESS;
  gint32 image_ID = -1;

  l_run_mode = run_mode = param[0].data.d_int32;

  *nreturn_vals = 1;
  *return_vals = values;
  values[0].type = PARAM_STATUS;
  values[0].data.d_status = STATUS_CALLING_ERROR;

  if (strcmp (name, "file_ascii_load") == 0)
    {
      *nreturn_vals = 2;
      values[1].type = PARAM_IMAGE;
      values[1].data.d_image = -1;

      switch (run_mode)
      {
        case RUN_INTERACTIVE:
          /*  Possibly retrieve data  */
          gimp_get_data ("file_ascii_load", &asciivals);

          if (!load_dialog ()) return;
          break;

        case RUN_NONINTERACTIVE:
          if (nparams == 20)   /* All arguments specified ? */
          {
            asciivals.margin[0] = param[3].data.d_int32;
            asciivals.margin[1] = param[4].data.d_int32;
            asciivals.margin[2] = param[5].data.d_int32;
            asciivals.margin[3] = param[6].data.d_int32;
            asciivals.margin[4] = param[7].data.d_int32;
            asciivals.margin[5] = param[8].data.d_int32;
            STRMCPY (asciivals.foundry, param[9].data.d_string);
            STRMCPY (asciivals.family, param[10].data.d_string);
            STRMCPY (asciivals.weight, param[11].data.d_string);
            STRMCPY (asciivals.slant, param[12].data.d_string);
            STRMCPY (asciivals.set_width, param[13].data.d_string);
            STRMCPY (asciivals.spacing, param[14].data.d_string);
            asciivals.char_height = (guint)param[15].data.d_float;
            asciivals.use_pixel = (param[16].data.d_int32 != 0);
            asciivals.antialias = (param[17].data.d_int32 != 0);
            asciivals.back_color[0] = param[18].data.d_color.red;
            asciivals.back_color[1] = param[18].data.d_color.green;
            asciivals.back_color[2] = param[18].data.d_color.blue;
            asciivals.forg_color[0] = param[19].data.d_color.red;
            asciivals.forg_color[1] = param[19].data.d_color.green;
            asciivals.forg_color[2] = param[19].data.d_color.blue;
          }
          else
          {
            status = STATUS_CALLING_ERROR;
          }
          break;

        case RUN_WITH_LAST_VALS:
          /* Possibly retrieve data */
          gimp_get_data ("file_ascii_load", &asciivals);
          break;

        default:
          break;
      }
      if (status == STATUS_SUCCESS)
      {
        check_load_vals ();
        image_ID = load_image (param[1].data.d_string);

        status = (image_ID != -1) ? STATUS_SUCCESS : STATUS_EXECUTION_ERROR;

        if (status == STATUS_SUCCESS)
        {
          if (run_mode == RUN_INTERACTIVE)
          {     /* Save new values */
            gimp_set_data ("file_ascii_load", &asciivals,
                           sizeof (ASCIILoadVals));
          }
          else  /* Clean image */
          {
            gimp_image_clean_all (image_ID);
          }
        }
      }
      values[0].data.d_status = status;
      values[1].data.d_image = image_ID;
    }
}


static gint32
load_image (char *filename)

{
 gint32 image_ID;
 FILE *ifp;

 ifp = fopen (filename, "rb");
 if (!ifp)
 {
   show_message ("can't open file for reading");
   return (-1);
 }

 image_ID = load_ascii (filename, ifp);

 fclose (ifp);

 return (image_ID);
}


static void
check_load_vals (void)

{int j;

 for (j = 0; j < 4; j++)
   if (asciivals.margin[j] < 0) asciivals.margin[j] = 0;
 if (asciivals.margin[4] < 1) asciivals.margin[4] = 1;
 if (asciivals.char_height < 2) asciivals.char_height = 2;
 if (asciivals.foundry[0] == '\0') strcpy (asciivals.foundry, "*");
 if (asciivals.family[0] == '\0') strcpy (asciivals.family, "courier");
 if (asciivals.weight[0] == '\0') strcpy (asciivals.weight, "*");
 if (asciivals.slant[0] == '\0') strcpy (asciivals.slant, "r");
 if (asciivals.set_width[0] == '\0') strcpy (asciivals.set_width, "*");
 if (asciivals.spacing[0] == '\0') strcpy (asciivals.spacing, "*");
}


/* Replace tabs by blanks */
static void
subst_tabs (int tabstop,
            char *in,
            char *out,
            int maxout)

{int count;

 count = 0;
 maxout--;     /* decrease maxout to have room for terminating char. */

 while (*in != '\0')
 {
   if (count >= maxout) break;
   if (*in != '\t')
   {
     *(out++) = *(in++);
     count++;
     continue;
   }
   if ((count % tabstop) == 0)
   {
     in++;
   }
   else
   {
     *(out++) = ' ';
     count++;
   }
 }
 *out = '\0';
}


/* Get extent of image (returns 0 on success, -1 on failure) */
static int
get_ascii_size (FILE *ifp,
                int num_lines,
                int *width,
                int *height,
                int *line_height)


{char line[1024], no_tab[1024];
 int line_count, length, start_x, end_x, start_y;
 GParam *params;
 gint nreturn_vals;

 start_x = end_x = asciivals.margin[1];
 line_count = 0;
 *line_height = 0;

 while (fgets (line, sizeof (line), ifp) != NULL)
 {
   length = strlen (line);
   if ((length > 0) && ((line[length-1] == '\r') || (line[length-1] == '\n')))
     line[--length] = '\0';
   if ((length > 0) && ((line[length-1] == '\r') || (line[length-1] == '\n')))
     line[--length] = '\0';

   if (length <= 0) strcpy (line, " ");
   subst_tabs (asciivals.margin[4], line, no_tab, sizeof (no_tab));

   params = gimp_run_procedure ("gimp_text_get_extents", &nreturn_vals,
                 PARAM_STRING, no_tab,
                 PARAM_FLOAT, (gfloat)asciivals.char_height,
                 PARAM_INT32, (gint32)(asciivals.use_pixel == 0),
                 PARAM_STRING, asciivals.foundry,
                 PARAM_STRING, asciivals.family,
                 PARAM_STRING, asciivals.weight,
                 PARAM_STRING, asciivals.slant,
                 PARAM_STRING, asciivals.set_width,
                 PARAM_STRING, asciivals.spacing,
                 PARAM_END);

   if (params[0].data.d_status != STATUS_SUCCESS)
   {
     printf ("ascii: It seems that font %s is not available for height %d\n",
             asciivals.family, (int)asciivals.char_height);
     return (-1);
   }

                                /* Check for maximum width */
   if (params[1].data.d_int32 + start_x > end_x)
     end_x = params[1].data.d_int32 + start_x;

   if (params[3].data.d_int32 + params[4].data.d_int32 > *line_height)
     *line_height = params[3].data.d_int32 + params[4].data.d_int32;

   if (l_run_mode != RUN_NONINTERACTIVE)
     gimp_progress_update (((double)(line_count+1))/(double)(2*num_lines));
   line_count++;
 }

 *width = end_x + asciivals.margin[2];

 if (asciivals.margin[5] < (-(*line_height))) /* Bottom to top ? */
 {
   start_y = asciivals.margin[3] + *line_height;
   start_y += (line_count-1)*(-(*line_height) - asciivals.margin[5]);
   *height = start_y + asciivals.margin[0];
 }
 else   /* Top to bottom */
 {
   start_y = asciivals.margin[0];
   start_y += (line_count * *line_height) + (line_count-1)*asciivals.margin[5];
   *height = start_y + asciivals.margin[3];
 }

 return (0);
}


/* Assigns numpix pixels starting at dst with color r,g,b */
static void
set_pixels (int numpix,
            unsigned char *dst,
            unsigned char *rgb)

{register int k;
 register unsigned char ur, ug, ub, *udest;

 if ((rgb[0] == rgb[1]) && (rgb[1] == rgb[2]))
 {
   memset (dst, (int)rgb[0], numpix*3);
   return;
 }

 ur = rgb[0];
 ug = rgb[1];
 ub = rgb[2];
 k = numpix;
 udest = dst;
 while (k-- > 0)
 {
   *(udest++) = ur;
   *(udest++) = ug;
   *(udest++) = ub;
 }
}


/* Create an image. Sets layer_ID, drawable and rgn. Returns image_ID */
static gint32
create_new_image (char *filename,
                  guint width,
                  guint height,
                  GDrawableType gdtype,
                  gint32 *layer_ID,
                  GDrawable **drawable,
                  GPixelRgn *pixel_rgn)

{gint32 image_ID;
 GImageType gitype;

 if ((gdtype == GRAY_IMAGE) || (gdtype == GRAYA_IMAGE))
   gitype = GRAY;
 else if ((gdtype == INDEXED_IMAGE) || (gdtype == INDEXEDA_IMAGE))
   gitype = INDEXED;
 else
   gitype = RGB;

 image_ID = gimp_image_new (width, height, gitype);
 gimp_image_set_filename (image_ID, filename);

 *layer_ID = gimp_layer_new (image_ID, "Background", width, height,
                             gdtype, 100, NORMAL_MODE);
 gimp_image_add_layer (image_ID, *layer_ID, 0);

 if (drawable != NULL)
 {
   *drawable = gimp_drawable_get (*layer_ID);
   if (pixel_rgn != NULL)
     gimp_pixel_rgn_init (pixel_rgn, *drawable, 0, 0, (*drawable)->width,
                          (*drawable)->height, TRUE, FALSE);
 }

 return (image_ID);
}


/* Load ASCII file */
static gint32
load_ascii (char *filename,
            FILE *ifp)

{int width, height, line_height, start_x, start_y, line_count, num_lines;
 int i, length, tile_height, scan_lines;
 char line[1024], no_tab[1024];
 unsigned char *dst = (unsigned char *)ident;
 guchar f_red, f_green, f_blue;
 gint nreturn_vals;
 gint32 layer_ID, image_ID;
 GPixelRgn pixel_rgn;
 GDrawable *drawable;
 GParam *params;
 int bottom_to_top, err = 0;

 if (l_run_mode != RUN_NONINTERACTIVE)
 {
   /* At first count the lines for displaying the progress bar */
   num_lines = 0;
   while (fgets (line, sizeof (line), ifp) != NULL)
     num_lines++;
   if (fseek (ifp, 0, SEEK_SET) != 0)
     return (-1);

   gimp_progress_init ("Reading ASCII text...");
 }
 else
 {
   num_lines = 1;
 }

 if (get_ascii_size (ifp, num_lines, &width, &height, &line_height) < 0)
   return (-1);
 if (fseek (ifp, 0, SEEK_SET) != 0)
   return (-1);

 bottom_to_top = (asciivals.margin[5] < (-line_height));

 image_ID = create_new_image (filename, width, height, RGB_IMAGE,
                              &layer_ID, &drawable, &pixel_rgn);

 tile_height = gimp_tile_height ();
 dst = (unsigned char *)g_malloc (width * tile_height * 3);
 if (dst == NULL) return (-1);

                                    /* Save foreground colour */
 gimp_palette_get_foreground (&f_red, &f_green, &f_blue);
                                    /* Set new foreground colour */
 gimp_palette_set_foreground (asciivals.forg_color[0],
                              asciivals.forg_color[1],
                              asciivals.forg_color[2]);

 /* Fill background */
 i = 0;
 while (i < height)
 {
   scan_lines = (i+tile_height-1 < height) ? tile_height : (height-i);

   set_pixels (width*scan_lines, dst, asciivals.back_color);

   gimp_pixel_rgn_set_rect (&pixel_rgn, dst, 0, i, width, scan_lines);
   i += scan_lines;
 }
 g_free (dst);

 if (bottom_to_top)
 {
   start_x = asciivals.margin[1];
   start_y = height-1-asciivals.margin[3]-line_height;
 }
 else
 {
   start_x = asciivals.margin[1];
   start_y = asciivals.margin[0];
 }

 /* Now create the text */
 line_count = 0;
 while (fgets (line, sizeof (line), ifp) != NULL)
 {
   length = strlen (line);    /* Cut off newline character */
   if ((length > 0) && ((line[length-1] == '\r') || (line[length-1] == '\n')))
     line[--length] = '\0';
   if ((length > 0) && ((line[length-1] == '\r') || (line[length-1] == '\n')))
     line[--length] = '\0';

   if (length > 0)
   {
     subst_tabs (asciivals.margin[4], line, no_tab, sizeof (no_tab));

     if (no_tab[0] != '\0')
     {
       params = gimp_run_procedure ("gimp_text", &nreturn_vals,
                   PARAM_IMAGE, image_ID,
                   PARAM_DRAWABLE, layer_ID,
                   PARAM_FLOAT, (gfloat)start_x,
                   PARAM_FLOAT, (gfloat)start_y,
                   PARAM_STRING, no_tab,
                   PARAM_INT32, (gint32)-1,
                   PARAM_INT32, (gint32)asciivals.antialias,
                   PARAM_FLOAT, (gfloat)asciivals.char_height,
                   PARAM_INT32, (gint32)(asciivals.use_pixel == 0),
                   PARAM_STRING, asciivals.foundry,
                   PARAM_STRING, asciivals.family,
                   PARAM_STRING, asciivals.weight,
                   PARAM_STRING, asciivals.slant,
                   PARAM_STRING, asciivals.set_width,
                   PARAM_STRING, asciivals.spacing,
                   PARAM_END);

       if (params[0].data.d_status != STATUS_SUCCESS)
       {
         printf ("ascii: Error drawing text%s\n", asciivals.antialias ?
                 " (try without anti-alias)" : "");
         err = 1;
         goto clean_up;
       }
     }
   }
   start_y += line_height + asciivals.margin[5];

   if (l_run_mode != RUN_NONINTERACTIVE)
     gimp_progress_update (((double)(num_lines+line_count+1))
                           /(double)(2*num_lines));
   line_count++;
 }

 /* Drawing text leaves us with a floating selection. Stop it */
 gimp_run_procedure ("gimp_floating_sel_anchor", &nreturn_vals,
                     PARAM_LAYER, gimp_image_floating_selection (image_ID),
                     PARAM_END);
clean_up:
                                  /* Restore foreground */
 gimp_palette_set_foreground (f_red, f_green, f_blue);

 gimp_drawable_flush (drawable);

 return (err ? -1 : image_ID);
}


/*  Load interface functions  */

static gint
load_dialog (void)

{
  LoadDialogVals vals;
  GtkWidget *dlg;
  GtkWidget *button;
  GtkWidget *frame;
  GtkWidget *vbox, *hbox, *left_vbox;
  GtkWidget *table;
  GtkWidget *label;
  FontSelectDialog *fseldialog;
  FontProperties fpset;
  guchar *color_cube;
  gchar **argv;
  gint argc;
  char tmp[80];
  int j;

  argc = 1;
  argv = g_new (gchar *, 1);
  argv[0] = g_strdup ("load");

  gtk_init (&argc, &argv);
  gtk_rc_parse (gimp_gtkrc ());

  gdk_set_use_xshm(gimp_use_xshm());

  gtk_preview_set_gamma(gimp_gamma());
  gtk_preview_set_install_cmap(gimp_install_cmap());
  color_cube = gimp_color_cube();
  gtk_preview_set_color_cube(color_cube[0], color_cube[1], color_cube[2],
                             color_cube[3]);
  gtk_widget_set_default_visual(gtk_preview_get_visual());
  gtk_widget_set_default_colormap(gtk_preview_get_cmap());

  vals.dialog = dlg = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (dlg), "Load ASCII");
  gtk_window_position (GTK_WINDOW (dlg), GTK_WIN_POS_MOUSE);
  gtk_signal_connect (GTK_OBJECT (dlg), "destroy",
                      (GtkSignalFunc) load_close_callback,
                      NULL);

  /*  Action area  */
  button = gtk_button_new_with_label ("OK");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      (GtkSignalFunc) load_ok_callback,
                      &vals);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->action_area), button,
                      TRUE, TRUE, 0);
  gtk_widget_grab_default (button);
  gtk_widget_show (button);

  button = gtk_button_new_with_label ("Cancel");
  GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                             (GtkSignalFunc) gtk_widget_destroy,
                             GTK_OBJECT (dlg));
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->action_area), button,
                      TRUE, TRUE, 0);
  gtk_widget_show (button);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_container_border_width (GTK_CONTAINER (hbox), 0);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), hbox,
                      TRUE, TRUE, 0);

  left_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_border_width (GTK_CONTAINER (left_vbox), 0);
  gtk_box_pack_start (GTK_BOX (hbox), left_vbox, TRUE, TRUE, 0);

  /* Margins */
  frame = gtk_frame_new ("Positioning");
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_container_border_width (GTK_CONTAINER (frame), 10);
  gtk_box_pack_start (GTK_BOX (left_vbox), frame, FALSE, TRUE, 0);
  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  /* Margin Labels and entries */
  table = gtk_table_new (NUM_POS_ENTRIES, 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (table), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table), 5);
  gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);
  gtk_widget_show (table);

  for (j = 0; j < NUM_POS_ENTRIES; j++)   /* Labels */
  {
    label = gtk_label_new (margin_text[j]);
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach (GTK_TABLE (table), label, 0, 1, j, j+1,
                      GTK_FILL, GTK_FILL, 0, 0);
    gtk_widget_show (label);
  }

  /* Margins/Tabstop */
  for (j = 0; j < NUM_POS_ENTRIES; j++)
  {
    vals.margin[j] = gtk_entry_new ();
    gtk_widget_set_usize (vals.margin[j], 35, 0);
    sprintf (tmp, "%d", (int)asciivals.margin[j]);
    gtk_entry_set_text (GTK_ENTRY (vals.margin[j]), tmp);
    gtk_table_attach (GTK_TABLE (table), vals.margin[j], 1, 2, j, j+1,
                      GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
    gtk_widget_show (vals.margin[j]);
  }
  gtk_widget_show (vbox);
  gtk_widget_show (frame);

  /* Colour */
  frame = gtk_frame_new ("Colour");
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_container_border_width (GTK_CONTAINER (frame), 10);
  gtk_box_pack_start (GTK_BOX (left_vbox), frame, FALSE, TRUE, 0);
  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  table = gtk_table_new (2, 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (table), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table), 5);
  gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);
  gtk_widget_show (table);

  add_color_button (0, table, &vals);
  add_color_button (1, table, &vals);

  gtk_widget_show (vbox);
  gtk_widget_show (frame);
  gtk_widget_show (left_vbox);

  /* Font */
  frame = gtk_frame_new ("Font");
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_container_border_width (GTK_CONTAINER (frame), 10);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, TRUE, 0);
  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  fontselect_properties_reset (&fpset);

  fpset.foundry = asciivals.foundry;
  fpset.family = asciivals.family;
  fpset.weight = asciivals.weight;
  fpset.slant = asciivals.slant;
  fpset.set_width = asciivals.set_width;
  fpset.spacing = asciivals.spacing;
  fpset.size = asciivals.char_height;
  fpset.use_pixel = asciivals.use_pixel;
  /* Currently "gimp_text" does not support registry/encoding */
  fpset.hide_registry = 1;
  fpset.hide_encoding = 1;
  fpset.hide_border = 1;   /* We also dont want the border. Use margins */
  fpset.antialias = asciivals.antialias;
  fpset.text = "ABCabc123";

  vals.fd = fseldialog = fontselect_dialog_new (vbox, &fpset);
  gtk_widget_show (fseldialog->widget);

  gtk_widget_show (vbox);
  gtk_widget_show (frame);

  gtk_widget_show (hbox);
  gtk_widget_show (dlg);

  gtk_main ();
  gdk_flush ();

  return asciiint.run;
}


static void
load_close_callback (GtkWidget *widget,
                     gpointer   data)

{
  gtk_main_quit ();
}


static void
load_ok_callback (GtkWidget *widget,
                  gpointer   data)

{int j, n, i_data;
 LoadDialogVals *vals = (LoadDialogVals *)data;
 FontProperties *fprop;

  /* Read margins/tabstop/linedistance */
  for (j = 0; j < NUM_POS_ENTRIES; j++)
  {
    n = sscanf (gtk_entry_get_text (GTK_ENTRY (vals->margin[j])), "%d", &i_data);
    if (n == 1) asciivals.margin[j] = i_data;
  }

  fprop = fontselect_properties_get (vals->fd);
  if (fprop != NULL)
  {
    STRMCPY (asciivals.foundry, fprop->foundry);
    STRMCPY (asciivals.family, fprop->family);
    STRMCPY (asciivals.weight, fprop->weight);
    STRMCPY (asciivals.slant, fprop->slant);
    STRMCPY (asciivals.set_width, fprop->set_width);
    STRMCPY (asciivals.spacing, fprop->spacing);
    asciivals.char_height = fprop->size;
    asciivals.use_pixel = fprop->use_pixel;
    asciivals.antialias = fprop->antialias;

    fontselect_properties_free (fprop);
  }

  memcpy ((char *)&(asciivals.forg_color[0]),(char *)&(vals->csel[0].color[0]),3);
  memcpy ((char *)&(asciivals.back_color[0]),(char *)&(vals->csel[1].color[0]),3);

  asciiint.run = TRUE;
  gtk_widget_destroy (GTK_WIDGET (vals->dialog));
}


static void
color_button_callback (GtkWidget *widget,
                       gpointer data)

{int idx, j;
 GtkColorSelectionDialog *csd;
 GtkWidget *dialog;
 gdouble colour[3];
 CSEL *csel;

 csel = (CSEL *)data;
 idx = csel->idx;

 /* Is the colour selection dialog already running ? */
 if (csel->colselect != NULL) return;

 for (j = 0; j < 3; j++)
   colour[j] = csel->color[j] / 255.0;

 dialog = csel->colselect = gtk_color_selection_dialog_new (
           (idx == 0) ? "Load ASCII foreground color"
                      : "Load ASCII background color");
 csd = GTK_COLOR_SELECTION_DIALOG (dialog);

 gtk_widget_destroy (csd->help_button);

 gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
                      (GtkSignalFunc) color_select_cancel_callback, data);
 gtk_signal_connect (GTK_OBJECT (csd->ok_button), "clicked",
                     (GtkSignalFunc) color_select_ok_callback, data);
 gtk_signal_connect (GTK_OBJECT (csd->cancel_button), "clicked",
                     (GtkSignalFunc) color_select_cancel_callback, data);

 gtk_color_selection_set_color (GTK_COLOR_SELECTION (csd->colorsel), colour);

 gtk_window_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);
 gtk_widget_show (dialog);
}


static void
color_select_ok_callback (GtkWidget *widget,
                          gpointer data)

{gdouble color[3];
 int idx, j;
 GtkWidget *dialog;
 CSEL *csel;

 csel = (CSEL *)data;
 idx = csel->idx;
 if ((dialog = csel->colselect) == NULL) return;

 gtk_color_selection_get_color (
   GTK_COLOR_SELECTION (GTK_COLOR_SELECTION_DIALOG (dialog)->colorsel),
   color);

 for (j = 0; j < 3; j++)
   csel->color[j] = (unsigned char)(color[j]*255.0);

 color_preview_show (csel->preview, &(csel->color[0]));

 csel->colselect = NULL;
 gtk_widget_destroy (dialog);
}


static void
color_select_cancel_callback (GtkWidget *widget,
                              gpointer data)

{int idx;
 GtkWidget *dialog;
 CSEL *csel;

 csel = (CSEL *)data;
 idx = csel->idx;
 if ((dialog = csel->colselect) == NULL) return;

 csel->colselect = NULL;
 gtk_widget_destroy (dialog);
}


static void
color_preview_show (GtkWidget *widget,
                    unsigned char *rgb)

{guchar *buf, *bp;
 int j, width, height;

 width = asciivals.prv_width;
 height = asciivals.prv_height;

 bp = buf = g_malloc (width*3);
 if (buf == NULL) return;

 for (j = 0; j < width; j++)
 {
   *(bp++) = rgb[0];
   *(bp++) = rgb[1];
   *(bp++) = rgb[2];
 }
 for (j = 0; j < height; j++)
   gtk_preview_draw_row (GTK_PREVIEW (widget), buf, 0, j, width);

 gtk_widget_draw (widget, NULL);

 g_free (buf);
}

static void
add_color_button (int csel_index,
                  GtkWidget *table,
                  LoadDialogVals *vals)

{
 GtkWidget *label;
 GtkWidget *button;
 GtkWidget *preview;
 static char *label_text[] = { "Foreground", "Background" };

 label = gtk_label_new (label_text[csel_index]);
 gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
 gtk_table_attach (GTK_TABLE (table), label, 0, 1, csel_index, csel_index+1,
                   GTK_FILL, GTK_FILL, 0, 0);
 gtk_widget_show (label);

 vals->csel[csel_index].colselect = NULL;
 vals->csel[csel_index].idx = csel_index;

 button = vals->csel[csel_index].activate = gtk_button_new ();

 memcpy (&(vals->csel[csel_index].color[0]),
         (csel_index == 0) ? asciivals.forg_color : asciivals.back_color, 3);

 preview = vals->csel[csel_index].preview = gtk_preview_new(GTK_PREVIEW_COLOR);
 gtk_preview_size (GTK_PREVIEW (preview), asciivals.prv_width,
                   asciivals.prv_height);
 gtk_container_add (GTK_CONTAINER (button), preview);
 gtk_widget_show (preview);

 color_preview_show (preview, &(vals->csel[csel_index].color[0]));

 gtk_signal_connect (GTK_OBJECT (button), "clicked",
                     (GtkSignalFunc) color_button_callback,
                     (gpointer)&(vals->csel[csel_index]));

 gtk_table_attach (GTK_TABLE (table), button, 1, 2, csel_index, csel_index+1,
                   0, 0, 0, 0);
 gtk_widget_show (button);
}


/* Show a message. Where to show it, depends on the runmode */
static void show_message (char *message)

{
#ifdef Simple_Message_Box_Available
 /* If there would be a simple message box like the one */
 /* used in ../app/interface.h, I would like to use it. */
 if (l_run_mode == RUN_INTERACTIVE)
   gtk_message_box (message);
 else
#endif
   fprintf (stderr, "ASCII: %s\n", message);
}
