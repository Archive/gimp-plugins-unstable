/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * This is the Fontselect-utility for plug-ins. Composed from text_tool.c,
 * build_menu.c, linked.c by Peter Kirchgessner, pkirchg@aol.com
 *
 */

/* Event history:
 * V 1.00, PK, 16-Nov-97: Creation
 */
static char ident[] = "@(#) GIMP fontselect widget v1.00  16-Nov-97";

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "gdk/gdkx.h"
#include "gdk/gdkkeysyms.h"
#include "gtk/gtk.h"
#include "libgimp/gimp.h"
#include "fontsel.h"

#define FONT_LIST_WIDTH  125
#define FONT_LIST_HEIGHT 200

#define PIXELS 0
#define POINTS 1

#define FOUNDRY   0
#define FAMILY    1
#define WEIGHT    2
#define SLANT     3
#define SET_WIDTH 4
#define SPACING   10
#define REGISTRY  12
#define ENCODING  13

#define N_MENUS   7

/* The following part has been taken from app/linked.h */

/* Begin linked.h */

typedef struct _link
{
  void *data;
  struct _link *next;
} *link_ptr;

static link_ptr alloc_list (void);
static link_ptr free_list (link_ptr);
static link_ptr add_to_list (link_ptr, void *);
static link_ptr remove_from_list (link_ptr, void *);
static link_ptr next_item (link_ptr);
static link_ptr nth_item (link_ptr, int);
static int      list_length (link_ptr);

/* End linked.h */


typedef struct _FontInfo FontInfo;
struct _FontInfo
{
  char *family;         /* The font family this info struct describes. */
  int *foundries;       /* An array of valid foundries. */
  int *weights;         /* An array of valid weights. */
  int *slants;          /* An array of valid slants. */
  int *set_widths;      /* An array of valid set widths. */
  int *spacings;        /* An array of valid spacings */
  int *registries;      /* An array of valid registries */
  int *encodings;       /* An array of valid encodings */
  int **combos;         /* An array of valid combinations of the above 7 items */
  int ncombos;          /* The number of elements in the "combos" array */
  link_ptr fontnames;   /* A list of valid fontnames.
                         * This is used to make sure a family/foundry/weight/slant/set_width
                         *  combination is valid.
                         */
};

typedef struct _FontSelectIndex FontSelectIndex;

struct _FontSelectIndex
{ int index;         /* Passing integer and pointer data to a signal- */
  FontSelect *fsel;  /* function requires this structure */
};


struct _FontSelect
{
  GtkWidget *main_vbox;
  GtkWidget *font_list;
  GtkWidget *size_menu;
  GtkWidget *size_text;
  GtkWidget *border_text;
  GtkWidget *text_frame;
  GtkWidget *the_text;
  GtkWidget *menus[N_MENUS];
  GtkWidget *option_menus[N_MENUS];
  GtkWidget **foundry_items;
  GtkWidget **weight_items;
  GtkWidget **slant_items;
  GtkWidget **set_width_items;
  GtkWidget **spacing_items;
  GtkWidget **registry_items;
  GtkWidget **encoding_items;
  GtkWidget *antialias_toggle;
  FontSelectIndex *itemindex;
  FontSelectIndex *fontindex;
  GdkFont *font;
  char *save_text;
  int hide_registry;
  int hide_encoding;
  int hide_border;
  int keep_old;
  int font_index;
  int size_type;
  int antialias;
  int foundry;
  int weight;
  int slant;
  int set_width;
  int spacing;
  int registry;
  int encoding;
};


typedef void (*MenuItemCallback) (GtkWidget *widget,
                                  gpointer   user_data);

typedef struct
{
  char *label;
  MenuItemCallback callback;
  gpointer user_data;
  GtkWidget *widget;
} SimpleMenuItem;


static FontInfo **font_info;
static int nfonts = -1;

static link_ptr foundries = NULL;
static link_ptr weights = NULL;
static link_ptr slants = NULL;
static link_ptr set_widths = NULL;
static link_ptr spacings = NULL;
static link_ptr registries = NULL;
static link_ptr encodings = NULL;

static char **foundry_array = NULL;
static char **weight_array = NULL;
static char **slant_array = NULL;
static char **set_width_array = NULL;
static char **spacing_array = NULL;
static char **registry_array = NULL;
static char **encoding_array = NULL;

static int nfoundries = 0;
static int nweights = 0;
static int nslants = 0;
static int nset_widths = 0;
static int nspacings = 0;
static int nregistries = 0;
static int nencodings = 0;

static FontSelect *FontSelect_alloc (void);
static void        FontSelect_free (FontSelect *);

static void       text_validate_combo     (FontSelect *, int);

static void       text_get_fonts          (void);
static void       text_insert_font        (FontInfo **, int *, char *);
static link_ptr   text_insert_field       (link_ptr, char *, int);
static char*      text_get_field          (char *, int);
static int        text_field_to_index     (char **, int, char *);
static int        text_is_xlfd_font_name  (char *);
static int        text_get_xlfd           (double, int, char *, char *, char *,
                                           char *, char *, char *, char *,
                                           char *, char *);
static int        text_load_font          (FontSelect *);
static void       text_pixels_callback    (GtkWidget *, gpointer);
static void       text_points_callback    (GtkWidget *, gpointer);
static void       text_foundry_callback   (GtkWidget *, gpointer);
static void       text_weight_callback    (GtkWidget *, gpointer);
static void       text_slant_callback     (GtkWidget *, gpointer);
static void       text_set_width_callback (GtkWidget *, gpointer);
static void       text_spacing_callback   (GtkWidget *, gpointer);
static void       text_registry_callback  (GtkWidget *, gpointer);
static void       text_encoding_callback  (GtkWidget *, gpointer);

static gint       text_size_key_function  (GtkWidget *, GdkEventKey *, gpointer);
static void       text_antialias_update   (GtkWidget *, gpointer);
static void       text_font_item_update   (GtkWidget *, gpointer);
static void       text_resize_text_widget (FontSelect *);

static GtkWidget *build_simple_menu       (SimpleMenuItem *items);


static SimpleMenuItem size_metric_items[] =
{
  { "Pixels", text_pixels_callback, (gpointer) PIXELS },
  { "Points", text_points_callback, (gpointer) POINTS },
  { NULL, NULL, NULL }
};

#define fs_free(m) { if (m) { g_free (m); m = NULL; }}


/* The following part has been taken from linked.c */

/* Begin linked.c */

#include <glib.h>


static GMemChunk *list_mem_chunk = NULL;
static link_ptr free_list_list = NULL;


static link_ptr
alloc_list (void)
{
  link_ptr new_list;

  if (!list_mem_chunk)
    list_mem_chunk = g_mem_chunk_new ("gimp list mem chunk",
				      sizeof (struct _link),
				      1024, G_ALLOC_ONLY);

  if (free_list_list)
    {
      new_list = free_list_list;
      free_list_list = free_list_list->next;
    }
  else
    {
      new_list = g_mem_chunk_alloc (list_mem_chunk);
    }

  new_list->data = NULL;
  new_list->next = NULL;

  return new_list;
}

static link_ptr
free_list (link_ptr l)

{
  link_ptr temp_list;

  if (!l)
    return NULL;

  temp_list = l;
  while (temp_list->next)
    temp_list = temp_list->next;
  temp_list->next = free_list_list;
  free_list_list = l;

  return NULL;
}

static link_ptr
add_to_list (link_ptr list,
              void *data)

{
  link_ptr new_link;

  new_link = alloc_list ();

  new_link->data = data;
  new_link->next = list;
  return new_link;
}

static link_ptr
remove_from_list (link_ptr list,
                  void *data)

{
  link_ptr tmp;

  if (!list)
    return NULL;

  if (list->data == data)
    {
      tmp = list->next;
      list->next = NULL;
      free_list (list);
      return tmp;
    }
  else
    list->next = remove_from_list (list->next, data);

  return list;
}

static link_ptr
next_item (link_ptr list)

{
  if (!list)
    return NULL;
  else
    return list->next;
}

static link_ptr
nth_item (link_ptr list,
          int n)

{
  if (list && (n > 0))
    return nth_item (next_item (list), n - 1);

  return list;
}

static int
list_length (link_ptr list)

{
  int n;

  n = 0;
  while (list)
    {
      n += 1;
      list = list->next;
    }

  return n;
}
/* End linked.c */


static FontSelect *
FontSelect_alloc (void)

{FontSelect *fsel;

 fsel = (FontSelect *)g_malloc (sizeof (FontSelect));
 if (fsel == NULL) return (NULL);

 memset (fsel, 0, sizeof (FontSelect));
 return (fsel);
}


static void
FontSelect_free (FontSelect *fsel)

{
 if (fsel == NULL) return;

 fs_free (fsel->fontindex);
 fs_free (fsel->foundry_items);
 fs_free (fsel->weight_items);
 fs_free (fsel->slant_items);
 fs_free (fsel->set_width_items);
 fs_free (fsel->spacing_items);
 fs_free (fsel->registry_items);
 fs_free (fsel->encoding_items);
 fs_free (fsel->itemindex);
 fs_free (fsel->save_text);
}


FontSelectDialog *
fontselect_dialog_new (GtkWidget *main_box,
                       FontProperties *fprop)

{
  GtkWidget *top_hbox;
  GtkWidget *list_box;
  GtkWidget *list_item;
  GtkWidget *right_vbox;
  GtkWidget *text_hbox;
  GtkWidget *size_opt_menu;
  GtkWidget *menu_table;
  GtkWidget *menu_label;
  GtkWidget *border_label;
  GtkWidget *alignment;
  GtkWidget **menu_items[N_MENUS];
  MenuItemCallback menu_callbacks[N_MENUS];
  FontSelect *fsel;
  FontSelectIndex *fselindex;
  FontSelectDialog *fseldialog;
  int i, j, max_items, hide;
  int cu_font, cu_foundry, cu_weight, cu_slant, cu_set_width, cu_spacing;
  int cu_registry, cu_encoding;
  int nmenu_items[N_MENUS];
  char *menu_strs[N_MENUS];
  char **menu_item_strs[N_MENUS];
  int *font_infos[N_MENUS];
  char ctmp[32];

  fseldialog = (FontSelectDialog *)g_malloc (sizeof (FontSelectDialog));
  fseldialog->fsel = fsel = FontSelect_alloc ();

  /* Difference to text-tool: When selecting an font which is not available, */
  /* we clear the text-entry to show that it is not available. Set keep_old  */
  /* to one, to get a behaviour like the text-tool */
  fsel->keep_old = 0;

  fseldialog->widget = fsel->main_vbox = gtk_vbox_new (FALSE, 1);
  gtk_box_pack_start (GTK_BOX (main_box), fsel->main_vbox, TRUE, TRUE, 0);

  top_hbox = gtk_hbox_new (FALSE, 1);
  gtk_box_pack_start (GTK_BOX (fsel->main_vbox), top_hbox, TRUE, TRUE, 0);

  /* Create the font listbox  */
  list_box = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_set_usize (list_box, FONT_LIST_WIDTH, FONT_LIST_HEIGHT);
  gtk_box_pack_start (GTK_BOX (top_hbox), list_box, TRUE, TRUE, 0);

  fsel->font_list = gtk_list_new ();
  gtk_container_add (GTK_CONTAINER (list_box), fsel->font_list);
  gtk_list_set_selection_mode (GTK_LIST (fsel->font_list), GTK_SELECTION_BROWSE);

  if (nfonts == -1)
    text_get_fonts ();

  /* Fill in the given values */
  cu_foundry = 0;
  if ((fprop != NULL) && (fprop->foundry != NULL))
    cu_foundry = text_field_to_index (foundry_array, nfoundries, fprop->foundry);

  cu_weight = 0;
  if ((fprop != NULL) && (fprop->weight != NULL))
    cu_weight = text_field_to_index (weight_array, nweights, fprop->weight);

  cu_slant = 0;
  if ((fprop != NULL) && (fprop->slant != NULL))
    cu_slant = text_field_to_index (slant_array, nslants, fprop->slant);

  cu_set_width = 0;
  if ((fprop != NULL) && (fprop->set_width != NULL))
    cu_set_width = text_field_to_index (set_width_array, nset_widths, fprop->set_width);

  cu_spacing = 0;
  if ((fprop != NULL) && (fprop->spacing != NULL))
    cu_spacing = text_field_to_index (spacing_array, nspacings, fprop->spacing);

  cu_registry = 0;
  if ((fprop != NULL) && (fprop->registry != NULL))
    cu_registry = text_field_to_index (registry_array, nregistries, fprop->registry);

  cu_encoding = 0;
  if ((fprop != NULL) && (fprop->encoding != NULL))
    cu_encoding = text_field_to_index (encoding_array, nencodings, fprop->encoding);

  fsel->hide_registry = fprop->hide_registry;
  fsel->hide_encoding = fprop->hide_encoding;
  fsel->hide_border = fprop->hide_border;

  fsel->size_type = PIXELS;
  if (fprop != NULL) fsel->size_type = fprop->use_pixel ? PIXELS : POINTS;

  fsel->antialias = 1;
  if (fprop!= NULL) fsel->antialias = (fprop->antialias != 0);

  /* We want to pass integer data and pointer data to a signal function. */
  /* Pack them into a structure and pass a pointer to the structure. */
  fsel->fontindex = fselindex =
    (FontSelectIndex *)g_malloc ((nfonts + 1)*sizeof (FontSelectIndex));

  for (i = 0; i < nfonts; i++)
  {
    fselindex[i].index = i;
    fselindex[i].fsel = fsel;
  }
  fselindex[nfonts].index = -1;
  fselindex[nfonts].fsel = NULL;

  cu_font = 0;
  if ((fprop != NULL) && (fprop->family != NULL))
  {
    for (i = 0; i < nfonts; i++)
    {
      if (strcmp (fprop->family, font_info[i]->family) == 0)
      {
        cu_font = i;
        break;
      }
    }
  }
  for (i = 0; i < nfonts; i++)
    {
      list_item = gtk_list_item_new_with_label (font_info[i]->family);
      gtk_container_add (GTK_CONTAINER (fsel->font_list), list_item);
      gtk_signal_connect (GTK_OBJECT (list_item), "select",
                          (GtkSignalFunc) text_font_item_update,
                          (gpointer) (fselindex+i));
      gtk_widget_show (list_item);
    }
  gtk_widget_show (fsel->font_list);

  /* Create the box to hold options  */
  right_vbox = gtk_vbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (top_hbox), right_vbox, TRUE, TRUE, 2);

  /* Create the text hbox, size text, and fonts size metric option menu */
  text_hbox = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (right_vbox), text_hbox, FALSE, FALSE, 0);
  fsel->size_text = gtk_entry_new ();
  gtk_widget_set_usize (fsel->size_text, 75, 0);
  i = (int)((fprop != NULL) ? fprop->size : 50);
  sprintf (ctmp, "%d", (i > 0) ? i : 50);
  gtk_entry_set_text (GTK_ENTRY (fsel->size_text), ctmp);
  gtk_signal_connect (GTK_OBJECT (fsel->size_text), "key_press_event",
                      (GtkSignalFunc) text_size_key_function,
                      fsel);
  gtk_box_pack_start (GTK_BOX (text_hbox), fsel->size_text, TRUE, TRUE, 0);

  /* Create the size menu */
  size_metric_items[0].user_data = fsel;
  size_metric_items[1].user_data = fsel;
  fsel->size_menu = build_simple_menu (size_metric_items);
  size_opt_menu = gtk_option_menu_new ();
  gtk_box_pack_start (GTK_BOX (text_hbox), size_opt_menu, FALSE, FALSE, 0);

  /* create the text entry widget */
  fsel->text_frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (fsel->text_frame), GTK_SHADOW_NONE);
  gtk_box_pack_start (GTK_BOX (fsel->main_vbox), fsel->text_frame, FALSE, FALSE, 2);
  fsel->the_text = gtk_entry_new ();
  gtk_container_add (GTK_CONTAINER (fsel->text_frame), fsel->the_text);
  gtk_entry_set_text (GTK_ENTRY (fsel->the_text), fprop->text ? fprop->text : "");

  /* create the antialiasing toggle button  */
  fsel->antialias_toggle = gtk_check_button_new_with_label ("Antialiasing");
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (fsel->antialias_toggle),
                               fsel->antialias);
  gtk_box_pack_start (GTK_BOX (right_vbox), fsel->antialias_toggle, FALSE, FALSE, 0);
  gtk_signal_connect (GTK_OBJECT (fsel->antialias_toggle), "toggled",
                      (GtkSignalFunc) text_antialias_update,
                      fsel);

  /* Allocate the arrays for the foundry, weight, slant, set_width and spacing menu items */
  fsel->foundry_items = (GtkWidget **) g_malloc (sizeof (GtkWidget *) * nfoundries);
  fsel->weight_items = (GtkWidget **) g_malloc (sizeof (GtkWidget *) * nweights);
  fsel->slant_items = (GtkWidget **) g_malloc (sizeof (GtkWidget *) * nslants);
  fsel->set_width_items = (GtkWidget **) g_malloc (sizeof (GtkWidget *) * nset_widths);
  fsel->spacing_items = (GtkWidget **) g_malloc (sizeof (GtkWidget *) * nspacings);
  fsel->registry_items = (GtkWidget **) g_malloc (sizeof (GtkWidget *) * nregistries);
  fsel->encoding_items = (GtkWidget **) g_malloc (sizeof (GtkWidget *) * nencodings);

  /* We want to pass integer data and pointer data to a signal function. */
  /* Pack them into a structure and pass a pointer to the structure. */
  max_items = nfoundries;
  if (nweights > max_items) max_items = nweights;
  if (nslants > max_items) max_items = nslants;
  if (nset_widths > max_items) max_items = nset_widths;
  if (nspacings > max_items) max_items = nspacings;
  if (nregistries > max_items) max_items = nregistries;
  if (nencodings > max_items) max_items = nencodings;
  fsel->itemindex = fselindex =
    (FontSelectIndex *)g_malloc ((max_items + 1)*sizeof (FontSelectIndex));
  for (i = 0; i < max_items; i++)
  {
    fselindex[i].index = i;
    fselindex[i].fsel = fsel;
  }
  fselindex[max_items].index = -1;
  fselindex[max_items].fsel = NULL;

  menu_items[0] = fsel->foundry_items;
  menu_items[1] = fsel->weight_items;
  menu_items[2] = fsel->slant_items;
  menu_items[3] = fsel->set_width_items;
  menu_items[4] = fsel->spacing_items;
  menu_items[5] = fsel->registry_items;
  menu_items[6] = fsel->encoding_items;

  nmenu_items[0] = nfoundries;
  nmenu_items[1] = nweights;
  nmenu_items[2] = nslants;
  nmenu_items[3] = nset_widths;
  nmenu_items[4] = nspacings;
  nmenu_items[5] = nregistries;
  nmenu_items[6] = nencodings;

  menu_strs[0] = "Foundry";
  menu_strs[1] = "Weight";
  menu_strs[2] = "Slant";
  menu_strs[3] = "Set width";
  menu_strs[4] = "Spacing";
  menu_strs[5] = "Registry";
  menu_strs[6] = "Encoding";

  menu_item_strs[0] = foundry_array;
  menu_item_strs[1] = weight_array;
  menu_item_strs[2] = slant_array;
  menu_item_strs[3] = set_width_array;
  menu_item_strs[4] = spacing_array;
  menu_item_strs[5] = registry_array;
  menu_item_strs[6] = encoding_array;

  menu_callbacks[0] = text_foundry_callback;
  menu_callbacks[1] = text_weight_callback;
  menu_callbacks[2] = text_slant_callback;
  menu_callbacks[3] = text_set_width_callback;
  menu_callbacks[4] = text_spacing_callback;
  menu_callbacks[5] = text_registry_callback;
  menu_callbacks[6] = text_encoding_callback;

  font_infos[0] = font_info[0]->foundries;
  font_infos[1] = font_info[0]->weights;
  font_infos[2] = font_info[0]->slants;
  font_infos[3] = font_info[0]->set_widths;
  font_infos[4] = font_info[0]->spacings;
  font_infos[5] = font_info[0]->registries;
  font_infos[6] = font_info[0]->encodings;

  menu_table = gtk_table_new (N_MENUS+1, 2, FALSE);
  gtk_box_pack_start (GTK_BOX (right_vbox), menu_table, TRUE, TRUE, 0);

  /* Create the other menus */
  for (i = 0; i < N_MENUS; i++)
    {
      menu_label = gtk_label_new (menu_strs[i]);
      gtk_misc_set_alignment (GTK_MISC (menu_label), 0.0, 0.5);
      gtk_table_attach (GTK_TABLE (menu_table), menu_label, 0, 1, i, i + 1,
                        GTK_SHRINK | GTK_FILL, GTK_SHRINK, 0, 1);

      alignment = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
      gtk_table_attach (GTK_TABLE (menu_table), alignment, 1, 2, i, i + 1,
                        GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK, 1, 1);
      fsel->menus[i] = gtk_menu_new ();

      for (j = 0; j < nmenu_items[i]; j++)
       {
	  menu_items[i][j] = gtk_menu_item_new_with_label (menu_item_strs[i][j]);
	  gtk_widget_set_sensitive (menu_items[i][j], font_infos[i][j]);

	  gtk_container_add (GTK_CONTAINER (fsel->menus[i]), menu_items[i][j]);
	  gtk_signal_connect (GTK_OBJECT (menu_items[i][j]), "activate",
			      (GtkSignalFunc) menu_callbacks[i],
			      (gpointer) (fselindex+j));
	  gtk_widget_show (menu_items[i][j]);
	}

      fsel->option_menus[i] = gtk_option_menu_new ();
      gtk_container_add (GTK_CONTAINER (alignment), fsel->option_menus[i]);

      hide = (   ((i == 5) && fsel->hide_registry)
              || ((i == 6) && fsel->hide_encoding));
      if (!hide)
      {
        gtk_widget_show (menu_label);
        gtk_widget_show (fsel->option_menus[i]);
        gtk_widget_show (alignment);
      }

      gtk_option_menu_set_menu (GTK_OPTION_MENU (fsel->option_menus[i]), fsel->menus[i]);
    }

  /* Create the border text hbox, border text, and label  */
  border_label = gtk_label_new ("Border");
  gtk_misc_set_alignment (GTK_MISC (border_label), 0.0, 0.5);
  gtk_table_attach (GTK_TABLE (menu_table), border_label, 0, 1, i, i + 1,
                    GTK_SHRINK | GTK_FILL, GTK_SHRINK, 0, 1);
  alignment = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
  gtk_table_attach (GTK_TABLE (menu_table), alignment, 1, 2, i, i + 1,
                    GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK, 1, 1);
  fsel->border_text = gtk_entry_new ();
  gtk_widget_set_usize (fsel->border_text, 75, 25);
  i = (int)((fprop != NULL) ? fprop->border : 0);
  sprintf (ctmp, "%d", (i >= 0) ? i : 0);
  gtk_entry_set_text (GTK_ENTRY (fsel->border_text), ctmp);
  gtk_container_add (GTK_CONTAINER (alignment), fsel->border_text);
  gtk_widget_show (alignment);

  /* Show the widgets */
  gtk_widget_show (menu_table);
  gtk_widget_show (fsel->antialias_toggle);
  gtk_widget_show (fsel->size_text);
  if (!fsel->hide_border)
  {
    gtk_widget_show (border_label);
    gtk_widget_show (fsel->border_text);
  }
  gtk_widget_show (size_opt_menu);
  gtk_widget_show (text_hbox);
  gtk_widget_show (list_box);
  gtk_widget_show (right_vbox);
  gtk_widget_show (fsel->the_text);
  gtk_widget_show (fsel->text_frame);

  /* Post initialization */
  gtk_option_menu_set_menu (GTK_OPTION_MENU (size_opt_menu), fsel->size_menu);
  gtk_option_menu_set_history (GTK_OPTION_MENU (size_opt_menu), fsel->size_type);
  gtk_list_select_item (GTK_LIST (fsel->font_list), cu_font);

  /* Now it would be nice if we could position the vertical scollbar */
  /* of the scrolled window list_box in a way, that the selected item */
  /* cu_font of the list fsel->font_list is at its top. Any idea ? */

  gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[0]),
                               fsel->foundry = cu_foundry);
  gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[1]),
                               fsel->weight = cu_weight);
  gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[2]),
                               fsel->slant = cu_slant);
  gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[3]),
                               fsel->set_width = cu_set_width);
  gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[4]),
                               fsel->spacing = cu_spacing);
  gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[5]),
                               fsel->registry = cu_registry);
  gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[6]),
                               fsel->encoding = cu_encoding);

  if (nfonts)
    text_load_font (fsel);

  gtk_widget_show (top_hbox);
  return (fseldialog);
}

void
fontselect_properties_reset (FontProperties *fprop)

{
  fprop->foundry = NULL;
  fprop->family = "courier";
  fprop->weight = NULL;
  fprop->slant = "r";
  fprop->set_width = NULL;
  fprop->spacing = NULL;
  fprop->registry = NULL;
  fprop->encoding = NULL;
  fprop->size = 24;
  fprop->use_pixel = 1;
  fprop->border = 5;
  fprop->antialias = 1;
  fprop->text = "ABCabc123";

  fprop->hide_registry = 0;
  fprop->hide_encoding = 0;
  fprop->hide_border = 0;
}

FontProperties *
fontselect_properties_get (FontSelectDialog *fd)

{
  FontSelect *fsel = fd->fsel;
  FontProperties *fprop;
  char *ctmp = ident; /* Just to satisfy gcc/lint */

  fprop = (FontProperties *)g_malloc (sizeof (FontProperties));

  /* Get foundry */
  fprop->foundry = foundry_array[fsel->foundry];
  if (strcmp (fprop->foundry, "(nil)") == 0) fprop->foundry = "";

  /* Get family */
  fprop->family = font_info[fsel->font_index]->family;

  /* Get weight */
  fprop->weight = weight_array[fsel->weight];
  if (strcmp (fprop->weight, "(nil)") == 0) fprop->weight = "";

  /* Get slant */
  fprop->slant = slant_array[fsel->slant];
  if (strcmp (fprop->slant, "(nil)") == 0) fprop->slant = "";

  /* Get set_width */
  fprop->set_width = set_width_array[fsel->set_width];
  if (strcmp (fprop->set_width, "(nil)") == 0) fprop->set_width = "";

  /* Get spacing */
  fprop->spacing = spacing_array[fsel->spacing];
  if (strcmp (fprop->spacing, "(nil)") == 0) fprop->spacing = "";

  /* Get registry */
  fprop->hide_registry = fsel->hide_registry;
  if (fsel->hide_registry)
    fprop->registry = registry_array[0];
  else
    fprop->registry = registry_array[fsel->registry];
  if (strcmp (fprop->registry, "(nil)") == 0) fprop->registry = "";

  /* Get encoding */
  fprop->hide_encoding = fsel->hide_encoding;
  if (fsel->hide_encoding)
    fprop->encoding = encoding_array[0];
  else
    fprop->encoding = encoding_array[fsel->encoding];
  if (strcmp (fprop->encoding, "(nil)") == 0) fprop->encoding = "";

  /* Get text size */
  ctmp = gtk_entry_get_text (GTK_ENTRY (fsel->size_text));
  fprop->size = atof (ctmp);

  /* Get size flag */
  fprop->use_pixel = (fsel->size_type == PIXELS);

  /* Get border width */
  fprop->hide_border = fsel->hide_border;
  if (fsel->hide_border)
  {
    fprop->border = 0;
  }
  else
  {
    ctmp = gtk_entry_get_text (GTK_ENTRY (fsel->border_text));
    fprop->border = atoi (ctmp);
  }

  /* Get antialias flag */
  fprop->antialias = fsel->antialias;

  /* Get text */
  ctmp = gtk_entry_get_text (GTK_ENTRY (fsel->the_text));
  fprop->text = g_strdup (ctmp);

  return (fprop);
}


void
fontselect_properties_free (FontProperties *fprop)

{
 if (fprop == NULL) return;

 g_free (fprop->text); fprop->text = NULL;
 g_free (fprop);
}


void
fontselect_dialog_free (FontSelectDialog *fd)

{
 if (fd == NULL) return;
 if (fd->fsel == NULL) return;
 FontSelect_free (fd->fsel);
 fd->fsel = NULL;
}


static void
text_validate_combo (FontSelect *fsel,
		     int       which)
{
  FontInfo *font;
  int which_val;
  int new_combo[N_MENUS];
  int best_combo[N_MENUS];
  int best_matches;
  int matches;
  int i;

  font = font_info[fsel->font_index];

  switch (which)
    {
    case 0:
      which_val = fsel->foundry;
      break;
    case 1:
      which_val = fsel->weight;
      break;
    case 2:
      which_val = fsel->slant;
      break;
    case 3:
      which_val = fsel->set_width;
      break;
    case 4:
      which_val = fsel->spacing;
      break;
    case 5:
      which_val = fsel->registry;
      break;
    case 6:
      which_val = fsel->encoding;
      break;
    default:
      which_val = 0;
      break;
    }

  best_matches = -1;
  best_combo[0] = 0;
  best_combo[1] = 0;
  best_combo[2] = 0;
  best_combo[3] = 0;
  best_combo[4] = 0;
  best_combo[5] = 0;
  best_combo[6] = 0;

  for (i = 0; i < font->ncombos; i++)
    {
      /* we must match the which field */
      if (font->combos[i][which] == which_val)
	{
	  matches = 0;
	  new_combo[0] = 0;
	  new_combo[1] = 0;
	  new_combo[2] = 0;
	  new_combo[3] = 0;
	  new_combo[4] = 0;
	  new_combo[5] = 0;
	  new_combo[6] = 0;

	  if ((fsel->foundry == 0) || (fsel->foundry == font->combos[i][0]))
	    {
	      matches++;
	      if (fsel->foundry)
		new_combo[0] = font->combos[i][0];
	    }
	  if ((fsel->weight == 0) || (fsel->weight == font->combos[i][1]))
	    {
	      matches++;
	      if (fsel->weight)
		new_combo[1] = font->combos[i][1];
	    }
	  if ((fsel->slant == 0) || (fsel->slant == font->combos[i][2]))
	    {
	      matches++;
	      if (fsel->slant)
		new_combo[2] = font->combos[i][2];
	    }
	  if ((fsel->set_width == 0) || (fsel->set_width == font->combos[i][3]))
	    {
	      matches++;
	      if (fsel->set_width)
		new_combo[3] = font->combos[i][3];
	    }
	  if ((fsel->spacing == 0) || (fsel->spacing == font->combos[i][4]))
	    {
	      matches++;
	      if (fsel->spacing)
		new_combo[4] = font->combos[i][4];
	    }
	  if ((fsel->registry == 0) || (fsel->registry == font->combos[i][5]))
	    {
	      matches++;
	      if (fsel->registry)
		new_combo[5] = font->combos[i][5];
	    }
	  if ((fsel->encoding == 0) || (fsel->encoding == font->combos[i][6]))
	    {
	      matches++;
	      if (fsel->encoding)
		new_combo[6] = font->combos[i][6];
	    }

	  /* if we get all 7 matches simply return */
	  if (matches == N_MENUS)
	    return;

	  if (matches > best_matches)
	    {
	      best_matches = matches;
	      best_combo[0] = new_combo[0];
	      best_combo[1] = new_combo[1];
	      best_combo[2] = new_combo[2];
	      best_combo[3] = new_combo[3];
	      best_combo[4] = new_combo[4];
	      best_combo[5] = new_combo[5];
	      best_combo[6] = new_combo[6];
	    }
	}
    }

  if (best_matches > -1)
    {
      if (fsel->foundry != best_combo[0])
	{
	  fsel->foundry = best_combo[0];
	  if (which != 0)
	    gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[0]), fsel->foundry);
	}
      if (fsel->weight != best_combo[1])
	{
	  fsel->weight = best_combo[1];
	  if (which != 1)
	    gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[1]), fsel->weight);
	}
      if (fsel->slant != best_combo[2])
	{
	  fsel->slant = best_combo[2];
	  if (which != 2)
	    gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[2]), fsel->slant);
	}
      if (fsel->set_width != best_combo[3])
	{
	  fsel->set_width = best_combo[3];
	  if (which != 3)
	    gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[3]), fsel->set_width);
	}
      if (fsel->spacing != best_combo[4])
	{
	  fsel->spacing = best_combo[4];
	  if (which != 4)
	    gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[4]), fsel->spacing);
	}
      if (fsel->registry != best_combo[5])
	{
	  fsel->registry = best_combo[5];
	  if (which != 5)
	    gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[5]), fsel->spacing);
	}
      if (fsel->encoding != best_combo[6])
	{
	  fsel->encoding = best_combo[6];
	  if (which != 6)
	    gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[6]), fsel->spacing);
	}
    }
}

static void
text_get_fonts ()
{
  char **fontnames;
  char *fontname;
  char *field;
  link_ptr temp_list;
  int num_fonts;
  int index;
  int i, j;

  /* construct a valid font pattern */

  fontnames = XListFonts ((Display *)GDK_DISPLAY(),
                          "-*-*-*-*-*-*-0-0-75-75-*-0-*-*", 32767,
                          &num_fonts);

  /* the maximum size of the table is the number of font names returned */
  font_info = g_malloc (sizeof (FontInfo**) * num_fonts);

  /* insert the fontnames into a table */
  nfonts = 0;
  for (i = 0; i < num_fonts; i++)
    if (text_is_xlfd_font_name (fontnames[i]))
      {
	text_insert_font (font_info, &nfonts, fontnames[i]);

	foundries = text_insert_field (foundries, fontnames[i], FOUNDRY);
	weights = text_insert_field (weights, fontnames[i], WEIGHT);
	slants = text_insert_field (slants, fontnames[i], SLANT);
	set_widths = text_insert_field (set_widths, fontnames[i], SET_WIDTH);
	spacings = text_insert_field (spacings, fontnames[i], SPACING);
	registries = text_insert_field (registries, fontnames[i], REGISTRY);
	encodings = text_insert_field (encodings, fontnames[i], ENCODING);
      }

  XFreeFontNames (fontnames);

  nfoundries = list_length (foundries) + 1;
  nweights = list_length (weights) + 1;
  nslants = list_length (slants) + 1;
  nset_widths = list_length (set_widths) + 1;
  nspacings = list_length (spacings) + 1;
  nregistries = list_length (registries) + 1;
  nencodings = list_length (encodings) + 1;

  foundry_array = g_malloc (sizeof (char*) * nfoundries);
  weight_array = g_malloc (sizeof (char*) * nweights);
  slant_array = g_malloc (sizeof (char*) * nslants);
  set_width_array = g_malloc (sizeof (char*) * nset_widths);
  spacing_array = g_malloc (sizeof (char*) * nspacings);
  registry_array = g_malloc (sizeof (char*) * nregistries);
  encoding_array = g_malloc (sizeof (char*) * nencodings);

  i = 1;
  temp_list = foundries;
  while (temp_list)
    {
      foundry_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }

  i = 1;
  temp_list = weights;
  while (temp_list)
    {
      weight_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }

  i = 1;
  temp_list = slants;
  while (temp_list)
    {
      slant_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }

  i = 1;
  temp_list = set_widths;
  while (temp_list)
    {
      set_width_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }

  i = 1;
  temp_list = spacings;
  while (temp_list)
    {
      spacing_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }
  i = 1;
  temp_list = registries;
  while (temp_list)
    {
      registry_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }
  i = 1;
  temp_list = encodings;
  while (temp_list)
    {
      encoding_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }

  foundry_array[0] = "*";
  weight_array[0] = "*";
  slant_array[0] = "*";
  set_width_array[0] = "*";
  spacing_array[0] = "*";
  registry_array[0] = "*";
  encoding_array[0] = "*";

  for (i = 0; i < nfonts; i++)
    {
      font_info[i]->foundries = g_malloc (sizeof (int) * nfoundries);
      font_info[i]->weights = g_malloc (sizeof (int) * nweights);
      font_info[i]->slants = g_malloc (sizeof (int) * nslants);
      font_info[i]->set_widths = g_malloc (sizeof (int) * nset_widths);
      font_info[i]->spacings = g_malloc (sizeof (int) * nspacings);
      font_info[i]->registries = g_malloc (sizeof (int) * nregistries);
      font_info[i]->encodings = g_malloc (sizeof (int) * nencodings);
      font_info[i]->ncombos = list_length (font_info[i]->fontnames);
      font_info[i]->combos = g_malloc (sizeof (int*) * font_info[i]->ncombos);

      for (j = 0; j < nfoundries; j++)
	font_info[i]->foundries[j] = 0;
      for (j = 0; j < nweights; j++)
	font_info[i]->weights[j] = 0;
      for (j = 0; j < nslants; j++)
	font_info[i]->slants[j] = 0;
      for (j = 0; j < nset_widths; j++)
	font_info[i]->set_widths[j] = 0;
      for (j = 0; j < nspacings; j++)
	font_info[i]->spacings[j] = 0;
      for (j = 0; j < nregistries; j++)
	font_info[i]->registries[j] = 0;
      for (j = 0; j < nencodings; j++)
	font_info[i]->encodings[j] = 0;

      font_info[i]->foundries[0] = 1;
      font_info[i]->weights[0] = 1;
      font_info[i]->slants[0] = 1;
      font_info[i]->set_widths[0] = 1;
      font_info[i]->spacings[0] = 1;
      font_info[i]->registries[0] = 1;
      font_info[i]->encodings[0] = 1;

      j = 0;
      temp_list = font_info[i]->fontnames;
      while (temp_list)
	{
	  fontname = temp_list->data;
	  temp_list = temp_list->next;

	  font_info[i]->combos[j] = g_malloc (sizeof (int) * N_MENUS);

	  field = text_get_field (fontname, FOUNDRY);
	  index = text_field_to_index (foundry_array, nfoundries, field);
	  font_info[i]->foundries[index] = 1;
	  font_info[i]->combos[j][0] = index;
	  free (field);

	  field = text_get_field (fontname, WEIGHT);
	  index = text_field_to_index (weight_array, nweights, field);
	  font_info[i]->weights[index] = 1;
	  font_info[i]->combos[j][1] = index;
	  free (field);

	  field = text_get_field (fontname, SLANT);
	  index = text_field_to_index (slant_array, nslants, field);
	  font_info[i]->slants[index] = 1;
	  font_info[i]->combos[j][2] = index;
	  free (field);

	  field = text_get_field (fontname, SET_WIDTH);
	  index = text_field_to_index (set_width_array, nset_widths, field);
	  font_info[i]->set_widths[index] = 1;
	  font_info[i]->combos[j][3] = index;
	  free (field);

	  field = text_get_field (fontname, SPACING);
	  index = text_field_to_index (spacing_array, nspacings, field);
	  font_info[i]->spacings[index] = 1;
	  font_info[i]->combos[j][4] = index;
	  free (field);

	  field = text_get_field (fontname, REGISTRY);
	  index = text_field_to_index (registry_array, nregistries, field);
	  font_info[i]->registries[index] = 1;
	  font_info[i]->combos[j][5] = index;
	  free (field);

	  field = text_get_field (fontname, ENCODING);
	  index = text_field_to_index (encoding_array, nencodings, field);
	  font_info[i]->encodings[index] = 1;
	  font_info[i]->combos[j][6] = index;
	  free (field);

	  j += 1;
	}
    }
}


static void
text_insert_font (FontInfo **table,
		  int       *ntable,
		  char      *fontname)
{
  FontInfo *temp_info;
  char *family;
  int lower, upper;
  int middle, cmp;

  /* insert a fontname into a table */
  family = text_get_field (fontname, FAMILY);
  if (!family)
    return;

  lower = 0;
  if (*ntable > 0)
    {
      /* Do a binary search to determine if we have already encountered
       *  a font from this family.
       */
      upper = *ntable;
      while (lower < upper)
	{
	  middle = (lower + upper) >> 1;

	  cmp = strcmp (family, table[middle]->family);
	  if (cmp == 0)
	    {
	      table[middle]->fontnames = add_to_list (table[middle]->fontnames, g_strdup (fontname));
	      return;
	    }
	  else if (cmp < 0)
	    upper = middle;
	  else if (cmp > 0)
	    lower = middle+1;
	}
    }

  /* Add another entry to the table for this new font family */
  table[*ntable] = g_malloc (sizeof (FontInfo));
  table[*ntable]->family = family;
  table[*ntable]->foundries = NULL;
  table[*ntable]->weights = NULL;
  table[*ntable]->slants = NULL;
  table[*ntable]->set_widths = NULL;
  table[*ntable]->fontnames = NULL;
  table[*ntable]->fontnames = add_to_list (table[*ntable]->fontnames, g_strdup (fontname));
  (*ntable)++;

  /* Quickly insert the entry into the table in sorted order
   *  using a modification of insertion sort and the knowledge
   *  that the entries proper position in the table was determined
   *  above in the binary search and is contained in the "lower"
   *  variable.
   */
  if (*ntable > 1)
    {
      temp_info = table[*ntable - 1];

      upper = *ntable - 1;
      while (lower != upper)
	{
	  table[upper] = table[upper-1];
	  upper -= 1;
	}

      table[lower] = temp_info;
    }
}

static link_ptr
text_insert_field (link_ptr  list,
		   char     *fontname,
		   int       field_num)
{
  link_ptr temp_list;
  link_ptr prev_list;
  link_ptr new_list;
  char *field;
  int cmp;

  field = text_get_field (fontname, field_num);
  if (!field)
    return list;

  temp_list = list;
  prev_list = NULL;

  while (temp_list)
    {
      cmp = strcmp (field, temp_list->data);
      if (cmp == 0)
	{
	  free (field);
	  return list;
	}
      else if (cmp < 0)
	{
	  new_list = alloc_list ();
	  new_list->data = field;
	  new_list->next = temp_list;
	  if (prev_list)
	    {
	      prev_list->next = new_list;
	      return list;
	    }
	  else
	    return new_list;
	}
      else
	{
	  prev_list = temp_list;
	  temp_list = temp_list->next;
	}
    }

  new_list = alloc_list ();
  new_list->data = field;
  new_list->next = NULL;
  if (prev_list)
    {
      prev_list->next = new_list;
      return list;
    }
  else
    return new_list;
}

static char*
text_get_field (char *fontname,
		int   field_num)
{
  char *t1, *t2;
  char *field;

  /* we assume this is a valid fontname...that is, it has 14 fields */

  t1 = fontname;
  while (*t1 && (field_num >= 0))
    if (*t1++ == '-')
      field_num--;

  t2 = t1;
  while (*t2 && (*t2 != '-'))
    t2++;

  if (t1 != t2)
    {
      field = g_malloc (1 + (long) t2 - (long) t1);
      strncpy (field, t1, (long) t2 - (long) t1);
      field[(long) t2 - (long) t1] = 0;
      return field;
    }

  return g_strdup ("(nil)");
}

static int
text_field_to_index (char **table,
		     int    ntable,
		     char  *field)
{
  int i;

  for (i = 0; i < ntable; i++)
    if (strcmp (field, table[i]) == 0)
      return i;

  return -1;
}

static int
text_is_xlfd_font_name (char *fontname)
{
  int i;

  i = 0;
  while (*fontname)
    if (*fontname++ == '-')
      i++;
  return (i == 14);
}

static int
text_get_xlfd (double  size,
	       int     size_type,
	       char   *foundry,
	       char   *family,
	       char   *weight,
	       char   *slant,
	       char   *set_width,
	       char   *spacing,
	       char   *registry,
	       char   *encoding,
	       char   *fontname)
{
  char pixel_size[12], point_size[12];

  if (size > 0)
    {
      switch (size_type)
	{
	case PIXELS:
	  sprintf (pixel_size, "%d", (int) size);
	  sprintf (point_size, "*");
	  break;
	case POINTS:
	  sprintf (pixel_size, "*");
	  sprintf (point_size, "%d", (int) (size * 10));
	  break;
	}

      /* create the fontname */
      sprintf (fontname, "-%s-%s-%s-%s-%s-*-%s-%s-75-75-%s-*-%s-%s",
	       foundry,
	       family,
	       weight,
	       slant,
	       set_width,
	       pixel_size, point_size,
	       spacing,
	       registry,
	       encoding);
      return TRUE;
    }
  else
    return FALSE;
}


static int
text_load_font (FontSelect *fsel)
{
  GdkFont *font;
  char fontname[2048];
  double size;
  char *size_text;
  char *foundry_str;
  char *family_str;
  char *weight_str;
  char *slant_str;
  char *set_width_str;
  char *spacing_str;
  char *registry_str;
  char *encoding_str;

  size_text = gtk_entry_get_text (GTK_ENTRY (fsel->size_text));
  size = atof (size_text);

  foundry_str = foundry_array[fsel->foundry];
  if (strcmp (foundry_str, "(nil)") == 0)
    foundry_str = "";
  family_str = font_info[fsel->font_index]->family;
  weight_str = weight_array[fsel->weight];
  if (strcmp (weight_str, "(nil)") == 0)
    weight_str = "";
  slant_str = slant_array[fsel->slant];
  if (strcmp (slant_str, "(nil)") == 0)
    slant_str = "";
  set_width_str = set_width_array[fsel->set_width];
  if (strcmp (set_width_str, "(nil)") == 0)
    set_width_str = "";
  spacing_str = spacing_array[fsel->spacing];
  if (strcmp (spacing_str, "(nil)") == 0)
    spacing_str = "";
  registry_str = registry_array[fsel->registry];
  if (strcmp (registry_str, "(nil)") == 0)
    registry_str = "";
  encoding_str = encoding_array[fsel->encoding];
  if (strcmp (encoding_str, "(nil)") == 0)
    encoding_str = "";

  if (text_get_xlfd (size, fsel->size_type, foundry_str, family_str,
		     weight_str, slant_str, set_width_str, spacing_str,
		     registry_str, encoding_str, fontname))
    {
      font = gdk_font_load (fontname);
      if (font)
	{
	  if (fsel->font)
	    gdk_font_unref (fsel->font);
	  fsel->font = font;
	  text_resize_text_widget (fsel);

	  return TRUE;
	}
    }

  /* Clear the entry if we dont have the font */
  if (!fsel->keep_old)
  {char *ctmp;

    ctmp = gtk_entry_get_text (GTK_ENTRY (fsel->the_text));
    /* Is there a need to save the text ? */
    if ((*ctmp != '\0') && (fsel->save_text != NULL))
    {
      g_free (fsel->save_text);
      fsel->save_text = NULL;
    }
    if (fsel->save_text == NULL)
    {
      fsel->save_text = g_strdup (gtk_entry_get_text (GTK_ENTRY (fsel->the_text)));
      gtk_entry_set_text (GTK_ENTRY (fsel->the_text), "");
    }
  }

  return FALSE;
}

static void
text_font_item_update (GtkWidget *w,
		       gpointer   data)
{
  FontInfo *font;
  int old_index;
  int i, index, retry = 0;
  FontSelectIndex *fselindex;
  FontSelect *fsel;

  /*  Is this font being selected?  */
  if (w->state != GTK_STATE_SELECTED)
    return;

  fselindex = (FontSelectIndex *)data;
  fsel = fselindex->fsel;
  index = fselindex->index;

  old_index = fsel->font_index;
  fsel->font_index = index;

  if (!text_load_font (fsel))
   {
      if (fsel->keep_old)
      {
         fsel->font_index = old_index;
         return;
      }
    }

  font = font_info[fsel->font_index];

  if (fsel->foundry && !font->foundries[fsel->foundry])
    {
      fsel->foundry = 0;
      retry = 1;
      gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[0]), 0);
    }
  if (fsel->weight && !font->weights[fsel->weight])
    {
      fsel->weight = 0;
      retry = 1;
      gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[1]), 0);
    }
  if (fsel->slant && !font->slants[fsel->slant])
    {
      fsel->slant = 0;
      retry = 1;
      gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[2]), 0);
    }
  if (fsel->set_width && !font->set_widths[fsel->set_width])
    {
      fsel->set_width = 0;
      retry = 1;
      gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[3]), 0);
    }
  if (fsel->spacing && !font->spacings[fsel->spacing])
    {
      fsel->spacing = 0;
      retry = 1;
      gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[4]), 0);
    }
  if (fsel->registry && !font->registries[fsel->registry])
    {
      fsel->registry = 0;
      retry = 1;
      gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[5]), 0);
    }
  if (fsel->encoding && !font->encodings[fsel->encoding])
    {
      fsel->encoding = 0;
      retry = 1;
      gtk_option_menu_set_history (GTK_OPTION_MENU (fsel->option_menus[6]), 0);
    }

  for (i = 0; i < nfoundries; i++)
    gtk_widget_set_sensitive (fsel->foundry_items[i], font->foundries[i]);
  for (i = 0; i < nweights; i++)
    gtk_widget_set_sensitive (fsel->weight_items[i], font->weights[i]);
  for (i = 0; i < nslants; i++)
    gtk_widget_set_sensitive (fsel->slant_items[i], font->slants[i]);
  for (i = 0; i < nset_widths; i++)
    gtk_widget_set_sensitive (fsel->set_width_items[i], font->set_widths[i]);
  for (i = 0; i < nspacings; i++)
    gtk_widget_set_sensitive (fsel->spacing_items[i], font->spacings[i]);
  for (i = 0; i < nregistries; i++)
    gtk_widget_set_sensitive (fsel->registry_items[i], font->registries[i]);
  for (i = 0; i < nencodings; i++)
    gtk_widget_set_sensitive (fsel->encoding_items[i], font->encodings[i]);

  if (retry) text_load_font (fsel);
}

static void
text_pixels_callback (GtkWidget *w,
                      gpointer   client_data)
{
  FontSelect *fsel;
  int old_value;

  fsel = (FontSelect *) client_data;

  old_value = fsel->size_type;
  fsel->size_type = PIXELS;

  if (!text_load_font (fsel))
    if (fsel->keep_old) fsel->size_type = old_value;
}

static void
text_points_callback (GtkWidget *w,
                      gpointer   client_data)
{
  FontSelect *fsel;
  int old_value;

  fsel = (FontSelect *) client_data;

  old_value = fsel->size_type;
  fsel->size_type = POINTS;

  if (!text_load_font (fsel))
    if (fsel->keep_old) fsel->size_type = old_value;
}

static void
text_foundry_callback (GtkWidget *w,
		       gpointer   client_data)
{
  int old_value;
  FontSelectIndex *fselindex = (FontSelectIndex *)client_data;
  FontSelect *fsel = fselindex->fsel;

  old_value = fsel->foundry;
  fsel->foundry = fselindex->index;
  text_validate_combo (fsel, 0);

  if (!text_load_font (fsel))
    if (fsel->keep_old) fsel->foundry = old_value;
}

static void
text_weight_callback (GtkWidget *w,
		      gpointer   client_data)
{
  int old_value;
  FontSelectIndex *fselindex = (FontSelectIndex *)client_data;
  FontSelect *fsel = fselindex->fsel;

  old_value = fsel->weight;
  fsel->weight = fselindex->index;
  text_validate_combo (fsel, 1);

  if (!text_load_font (fsel))
    if (fsel->keep_old) fsel->weight = old_value;
}

static void
text_slant_callback (GtkWidget *w,
		     gpointer   client_data)
{
  int old_value;
  FontSelectIndex *fselindex = (FontSelectIndex *)client_data;
  FontSelect *fsel = fselindex->fsel;

  old_value = fsel->slant;
  fsel->slant = fselindex->index;
  text_validate_combo (fsel, 2);

  if (!text_load_font (fsel))
    if (fsel->keep_old) fsel->slant = old_value;
}

static void
text_set_width_callback (GtkWidget *w,
			 gpointer   client_data)
{
  int old_value;
  FontSelectIndex *fselindex = (FontSelectIndex *)client_data;
  FontSelect *fsel = fselindex->fsel;

  old_value = fsel->set_width;
  fsel->set_width = fselindex->index;
  text_validate_combo (fsel, 3);

  if (!text_load_font (fsel))
    if (fsel->keep_old) fsel->set_width = old_value;
}

static void
text_spacing_callback (GtkWidget *w,
		       gpointer   client_data)
{
  int old_value;
  FontSelectIndex *fselindex = (FontSelectIndex *)client_data;
  FontSelect *fsel = fselindex->fsel;

  old_value = fsel->spacing;
  fsel->spacing = fselindex->index;
  text_validate_combo (fsel, 4);

  if (!text_load_font (fsel))
    if (fsel->keep_old) fsel->spacing = old_value;
}

static void
text_registry_callback (GtkWidget *w,
		       gpointer   client_data)
{
  int old_value;
  FontSelectIndex *fselindex = (FontSelectIndex *)client_data;
  FontSelect *fsel = fselindex->fsel;

  old_value = fsel->registry;
  fsel->registry = fselindex->index;
  text_validate_combo (fsel, 5);

  if (!text_load_font (fsel))
    if (fsel->keep_old) fsel->registry = old_value;
}

static void
text_encoding_callback (GtkWidget *w,
		       gpointer   client_data)
{
  int old_value;
  FontSelectIndex *fselindex = (FontSelectIndex *)client_data;
  FontSelect *fsel = fselindex->fsel;

  old_value = fsel->encoding;
  fsel->encoding = fselindex->index;
  text_validate_combo (fsel, 6);

  if (!text_load_font (fsel))
    if (fsel->keep_old) fsel->encoding = old_value;
}

static void
text_resize_text_widget (FontSelect *fsel)
{
  GtkStyle *style;

  style = gtk_style_new ();
  gdk_font_unref (style->font);
  style->font = fsel->font;
  gdk_font_ref (style->font);

  gtk_widget_set_style (fsel->the_text, style);

  if (!fsel->keep_old)
  {
    if (fsel->save_text) /* Need to restore a text ? */
    {
      gtk_entry_set_text (GTK_ENTRY (fsel->the_text), fsel->save_text);
      g_free (fsel->save_text);
      fsel->save_text = NULL;
    }
  }
}

static gint
text_size_key_function (GtkWidget   *w,
			GdkEventKey *event,
			gpointer     data)
{
  FontSelect *fsel;
  char buffer[16];
  int old_value;

  fsel = (FontSelect *) data;

  if (event->keyval == GDK_Return)
    {
      gtk_signal_emit_stop_by_name (GTK_OBJECT (w), "key_press_event");

      old_value = atoi (gtk_entry_get_text (GTK_ENTRY (fsel->size_text)));
      if (!text_load_font (fsel))
        if (fsel->keep_old)
	{
	  sprintf (buffer, "%d", old_value);
	  gtk_entry_set_text (GTK_ENTRY (fsel->size_text), buffer);
	}
      return TRUE;
    }

  return FALSE;
}

static void
text_antialias_update (GtkWidget *w,
                       gpointer   data)
{
  FontSelect *fsel;

  fsel = (FontSelect *) data;

  if (GTK_TOGGLE_BUTTON (w)->active)
    fsel->antialias = TRUE;
  else
    fsel->antialias = FALSE;
}

static GtkWidget *
build_simple_menu (SimpleMenuItem *items)

{
  GtkWidget *menu;
  GtkWidget *menu_item;

  menu = gtk_menu_new ();

  while (items->label)
    {
      if (items->label[0] == '-')
	{
	  menu_item = gtk_menu_item_new ();
	  gtk_container_add (GTK_CONTAINER (menu), menu_item);
	}
      else
	{
	  menu_item = gtk_menu_item_new_with_label (items->label);
	  gtk_container_add (GTK_CONTAINER (menu), menu_item);
	}

      if (items->callback)
	gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
			    (GtkSignalFunc) items->callback,
			    items->user_data);

      gtk_widget_show (menu_item);
      items->widget = menu_item;

      items++;
    }

  return menu;
}
