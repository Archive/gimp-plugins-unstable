/* janitor.c
 * janitor: an extension for the GIMP that does some stuff.
 * http://www.poboxes.com/kevint/gimp/janitor.html
 * by Kevin Turner <kevint@poboxes.com>
 */

/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* Code blatantly stolen from Sven's screenshot.c */

#include "gtk/gtk.h"
#include "libgimp/gimp.h"
#include "libgimp/gimpui.h"

/* Defines */
#define PLUG_IN_NAME        "extension_janitor"
#define PLUG_IN_PRINT_NAME  "Janitor"
#define PLUG_IN_VERSION     "0.0.0"
#define PLUG_IN_MENU_PATH   "<Toolbox>/Xtns/Janitor"
#define PLUG_IN_AUTHOR      "Kevin Turner (kevint@poboxes.com)"
#define PLUG_IN_COPYRIGHT   "Kevin Turner"
#define PLUG_IN_DESCRIBTION "Allows adding displays and layers to images through a semi-useable interface."
#define PLUG_IN_HELP        "FIXME: Write help text."

#define NUMBER_IN_ARGS 1
#define IN_ARGS { PARAM_INT32,    "run_mode", "Interactive, non-interactive" }

#define NUMBER_OUT_ARGS 0
/* #define OUT_ARGS { PARAM_IMAGE,   "image", "Output image" } */

typedef struct {
  gint run;
} FunkDatInterface;

static FunkDatInterface shootint =
{
  FALSE	    /* run */
};

typedef struct {
  GtkWidget *image_menu;
  GtkWidget *drawable_menu;
  GtkWidget *image_entry;
  GtkWidget *drawable_entry;
} SomeWidgets;

static void  query (void);
static void  run (gchar *name,
		  gint nparams,	          /* number of parameters passed in */
		  GParam * param,	  /* parameters passed in */
		  gint *nreturn_vals,     /* number of parameters returned */
		  GParam ** return_vals); /* parameters to be returned */
static gint  janitor_dialog (void);
static void  dialog_close_callback (GtkWidget *widget,
				   gpointer   data);
static void  menu_callback (gint32 id,
			    gpointer data);     
static void  add_display_callback (GtkWidget *widget,
				   gpointer   data);
static void  add_layer_callback (GtkWidget *widget,
				 gpointer   data);
static void  refresh_callback (GtkWidget *widget, 
			       gpointer data);
static gint  return_true(gint32 foo,gint32 bar, gpointer baz);

/* Global Variables */
GPlugInInfo PLUG_IN_INFO =
{
  NULL,   /* init_proc  */
  NULL,	  /* quit_proc  */
  query,  /* query_proc */
  run     /* run_proc   */
};

/* Functions */

MAIN ()

static void query (void)
{

static GParamDef args[] = { IN_ARGS };
static gint nargs = NUMBER_IN_ARGS;
/* static GParamDef return_vals[] = { NULL };  */
static gint nreturn_vals = NUMBER_OUT_ARGS; 

/* the actual installation of the plugin */
gimp_install_procedure (PLUG_IN_NAME,
			PLUG_IN_DESCRIBTION,
			PLUG_IN_HELP,
			PLUG_IN_AUTHOR,
			PLUG_IN_COPYRIGHT,
			PLUG_IN_VERSION,
			PLUG_IN_MENU_PATH,
			NULL,
			PROC_EXTENSION,		
			nargs,
			nreturn_vals,
			args,
		        NULL /*return_vals */);
}

static void 
run (gchar *name,		/* name of plugin */
     gint nparams,		/* number of in-paramters */
     GParam * param,		/* in-parameters */
     gint *nreturn_vals,	/* number of out-parameters */
     GParam ** return_vals)	/* out-parameters */
{

  /* Get the runmode from the in-parameters */
  GRunModeType run_mode = param[0].data.d_int32;	
  
  /* status variable, use it to check for errors in invocation usualy only 
     during non-interactive calling */	
  GStatusType status = STATUS_SUCCESS;	 
  
  /*always return at least the status to the caller. */
  static GParam values[1];
  
  /* initialize the return of the status */ 	
  values[0].type = PARAM_STATUS;
  values[0].data.d_status = status;
  *nreturn_vals = 1;
  *return_vals = values;
  
  /*how are we running today? */
  switch (run_mode)
  {
    case RUN_INTERACTIVE:
      /* Get information from the dialog */
      if (!janitor_dialog())
	return;
      break;

    case RUN_NONINTERACTIVE:
      /* right now, I don't know how to handle non-interactive calls */
      status = STATUS_CALLING_ERROR;
      break;
      
    default:
      break;
  } /* switch */

  /* The dialog is *it*, hun.  We can go now. */

  values[0].data.d_status = status; 
}

static gint 
janitor_dialog (void)
{
  GtkWidget *dialog;
  GtkWidget *done_button, *refresh_button;
 
  GtkWidget *hbox;

  SomeWidgets p;

  GtkWidget *image_frame, *image_vbox;
  GtkWidget *drawable_frame, *drawable_vbox;

  GtkWidget *new_disp_button;
  GtkWidget *add_layer_button;

  gint argc = 1;
  gchar **argv = g_new (gchar *, 1);
  argv[0] = g_strdup ("Janitor");

  /* Init GTK  */
  gtk_init (&argc, &argv);
  gtk_rc_parse (gimp_gtkrc ());
  gdk_set_use_xshm (gimp_use_xshm ());

  /* Main Dialog */
  dialog = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (dialog), PLUG_IN_PRINT_NAME);
  gtk_window_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);
  gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
		      (GtkSignalFunc) dialog_close_callback,
		      NULL);
  /*  Action area  */
  done_button = gtk_button_new_with_label ("Done");
  GTK_WIDGET_SET_FLAGS (done_button, GTK_CAN_DEFAULT);
  gtk_signal_connect_object (GTK_OBJECT (done_button), "clicked",
			     (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT (dialog));
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area), 
		      done_button, TRUE, TRUE, 0);
  gtk_widget_grab_default (done_button);
  gtk_widget_show (done_button);

  refresh_button = gtk_button_new_with_label ("Refresh");
  GTK_WIDGET_SET_FLAGS (refresh_button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (refresh_button), "clicked",
		      (GtkSignalFunc) refresh_callback, &p);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area), 
		      refresh_button, TRUE, TRUE, 0);
  gtk_widget_grab_default (refresh_button);
  gtk_widget_show (refresh_button);

#ifdef JANITOR_DEBUG
#if 0
  g_print("janitor: waiting... (pid %d)\n", getpid());
  kill(getpid(), 19);
#endif
#endif           
  
  /* Cute and fluffy area. */
  
  hbox=gtk_hbox_new(FALSE,3);
  gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox),hbox);
  gtk_widget_show(hbox);

  /* Image menu stuff. */

  image_frame = gtk_frame_new("Image");
  gtk_container_add(GTK_CONTAINER(hbox),image_frame);
  gtk_widget_show(image_frame);

  image_vbox = gtk_vbox_new(FALSE,3);
  gtk_container_add(GTK_CONTAINER(image_frame),image_vbox);
  gtk_widget_show(image_vbox);

  p.image_entry = gtk_entry_new();

  p.image_menu = gtk_option_menu_new();

  gtk_option_menu_set_menu(GTK_OPTION_MENU(p.image_menu),
			   gimp_image_menu_new(return_true, /* Constraint function*/
					       menu_callback,
					       p.image_entry,   /* callback data */
					       0 /* active image */));

  gtk_container_add(GTK_CONTAINER(image_vbox),p.image_menu);
  gtk_widget_show(p.image_menu);

  gtk_container_add(GTK_CONTAINER(image_vbox),p.image_entry);
  gtk_widget_show(p.image_entry);
 
  new_disp_button = gtk_button_new_with_label ("Add Display");
  gtk_signal_connect_object (GTK_OBJECT (new_disp_button), "clicked",
			     (GtkSignalFunc) add_display_callback, 
			     GTK_OBJECT (p.image_entry));

  gtk_container_add(GTK_CONTAINER(image_vbox),new_disp_button);
  gtk_widget_show(new_disp_button);

  /* Drawable menu stuff. */

  drawable_frame = gtk_frame_new("Drawables");
  gtk_container_add(GTK_CONTAINER(hbox),drawable_frame);
  gtk_widget_show(drawable_frame);

  drawable_vbox = gtk_vbox_new(FALSE,3);
  gtk_container_add(GTK_CONTAINER(drawable_frame),drawable_vbox);
  gtk_widget_show(drawable_vbox);

  p.drawable_entry = gtk_entry_new();

  p.drawable_menu = gtk_option_menu_new();

  gtk_option_menu_set_menu(GTK_OPTION_MENU(p.drawable_menu),
			   gimp_drawable_menu_new(return_true, /* Constraint function*/
						  menu_callback,
						  p.drawable_entry,  /* callback data */
						  0 /* active image */));

  gtk_container_add(GTK_CONTAINER(drawable_vbox),p.drawable_menu);
  gtk_widget_show(p.drawable_menu);

  gtk_container_add(GTK_CONTAINER(drawable_vbox),p.drawable_entry);
  gtk_widget_show(p.drawable_entry);
 
  add_layer_button = gtk_button_new_with_label ("Add to image");
  gtk_signal_connect_object (GTK_OBJECT (add_layer_button), "clicked",
			     (GtkSignalFunc) add_layer_callback, 
			     GTK_OBJECT (p.drawable_entry));

  gtk_container_add(GTK_CONTAINER(drawable_vbox),add_layer_button);
  gtk_widget_show(add_layer_button);

  gtk_widget_show(dialog);

  gtk_main ();
  gdk_flush ();

  return shootint.run;
}

/* Interface functions  */

static void
dialog_close_callback (GtkWidget *widget,
		       gpointer   data)
{
  gtk_main_quit ();
}

static void
menu_callback (gint32 image_id,
		     gpointer data)
{
     gchar buf[16];
     GtkWidget *image_entry=data;
     
     sprintf(buf,"%d", image_id);
     gtk_entry_set_text(GTK_ENTRY(image_entry),buf);
}

static void
refresh_callback (GtkWidget *widget, gpointer data)
{
     SomeWidgets degimp=*((SomeWidgets *)data);     
     GtkWidget *dmenu, *imenu;

     imenu=gtk_option_menu_get_menu(GTK_OPTION_MENU(degimp.image_menu));
     gtk_option_menu_remove_menu(GTK_OPTION_MENU(degimp.image_menu));
     gtk_object_unref(GTK_OBJECT(imenu));

     gtk_option_menu_set_menu(GTK_OPTION_MENU(degimp.image_menu),
			      gimp_image_menu_new(return_true, /* Constraint function*/
						  menu_callback,
						  degimp.image_entry,/* data */
						  0 /* active image */));

     dmenu=gtk_option_menu_get_menu(GTK_OPTION_MENU(degimp.drawable_menu));
     gtk_option_menu_remove_menu(GTK_OPTION_MENU(degimp.drawable_menu));
     gtk_widget_destroy(dmenu);

     gtk_option_menu_set_menu(GTK_OPTION_MENU(degimp.drawable_menu),
			      gimp_drawable_menu_new(return_true, /* Constraint function*/
						     menu_callback,
						     degimp.drawable_entry,  /* data */
						     0 /* active image */));
}

static void
add_display_callback (GtkWidget *widget,
		       gpointer   data)
{
     GtkWidget *img_entry=data;

     gimp_display_new(atoi(gtk_entry_get_text(GTK_ENTRY(img_entry))));
}

static void
add_layer_callback (GtkWidget *widget,
		    gpointer   data)
{
     GtkWidget *drawable_entry=data;
     gint32 img_id, drawable_id;

     drawable_id=atoi(gtk_entry_get_text(GTK_ENTRY(drawable_entry)));

     img_id = gimp_drawable_image_id(drawable_id);

     gimp_image_add_layer(img_id,drawable_id,0 /* position */);
}

static gint
return_true (gint32 foo, gint32 bar, gpointer baz)
{
     return TRUE;
}
