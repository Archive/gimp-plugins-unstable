/* 
   callbacks.h
   for L-System Plug-In
   Michal Gomulinski
*/
/* 	$Id$	 */
#ifndef CALLBACKS_H
#define CALLBACKS_H

#include<gtk/gtk.h>
#include"lsystem.h"

void     ls_ok_callback(GtkWidget *, gpointer *);
void     ls_cancel_callback(GtkWidget *, gpointer *);
void     ls_about_callback(GtkWidget *, gpointer *);
void     ls_reread_callback(GtkWidget *, gpointer);
void     ls_new_callback(GtkWidget *, gpointer);
void     ls_copy_callback(GtkWidget *, gpointer);
void     ls_rename_callback(GtkWidget *, gpointer);
void     ls_delete_callback(GtkWidget *, gpointer);
void     ls_preview_callback(GtkWidget *, gpointer); 
void     ls_add_rule_callback(GtkWidget *, gpointer);
void     ls_remove_rule_callback(GtkWidget *, gpointer);
void     ls_add_srule_callback(GtkWidget *, gpointer);
void     ls_remove_srule_callback(GtkWidget *, gpointer);
void     ls_add_lrule_callback(GtkWidget *, gpointer);
void     ls_remove_lrule_callback(GtkWidget *, gpointer);
void     ls_new_seed_callback(GtkWidget *, gpointer);
void     ls_univ_close(GtkWidget *, GtkWidget *);
void     ls_edit_rule_callback(GtkWidget *, gpointer);
void     ls_edit_lrule_callback(GtkWidget *, gpointer);
void     ls_edit_srule_callback(GtkWidget *, gpointer);
void     ls_update_main_dialog_callback(GtkWidget *, gpointer);
void ls_toggle_widget(GtkWidget *, GtkWidget *);
void ls_autoscale_toggle(GtkWidget *, gpointer);

#endif
