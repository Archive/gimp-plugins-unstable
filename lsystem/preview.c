/*
  (c) by Michal Gomulinski
  1998 Warsaw

  mailto:M.Gomulinski@writeme.com
*/

#include<values.h>
#include<gtk/gtk.h>
#include<gdk/gdk.h>
#include"lsystem.h"
#include"gdebug.h"

gint prv_wid;
gint prv_hei;

static gint ls_prv_configure_event_handler(GtkWidget *, GdkEventConfigure *);
static gint ls_prv_expose_event_handler(GtkWidget *, GdkEventExpose *);

/* Off-screen buffer */
static GdkPixmap *pixmap=NULL;
static GdkGC *draw_gc=NULL;
static GdkGC *white_gc=NULL;

GtkWidget *ls_preview_new()
{
  GtkWidget *ret;
  gint pw, ph;
  gdouble k, kx, ky;
  
  ret=gtk_drawing_area_new();
  kx=(gdouble)image_wid/150.0;
  ky=(gdouble)image_hei/150.0;
  k=(kx > ky) ? kx: ky;
  pw=image_wid/k;
  ph=image_hei/k;
  gtk_drawing_area_size(GTK_DRAWING_AREA(ret), pw, ph);
  gtk_signal_connect(GTK_OBJECT(ret), "configure_event", 
		     GTK_SIGNAL_FUNC(ls_prv_configure_event_handler), NULL);
  gtk_signal_connect(GTK_OBJECT(ret), "expose_event", 
		     GTK_SIGNAL_FUNC(ls_prv_expose_event_handler), NULL);
  gtk_widget_set_events(ret, GDK_EXPOSURE_MASK);
  return ret;
}

inline void prv_flush_path (Path * path)
{
  GdkSegment *points;
  gint i;
  gdouble *pts_ptr;

  if (path->pathlen > 2) { /* Paths shorter than 2 points are useless */
    points=g_malloc(sizeof(GdkSegment) * path->pathlen/2);
    pts_ptr=path->pts;
    for (i=0; i<path->pathlen /2 - 1; i++) {
      points[i].x1=(gint16) *(pts_ptr++);
      points[i].y1=(gint16) *(pts_ptr++);
      points[i].x2=(gint16) *(pts_ptr);
      points[i].y2=(gint16) *(pts_ptr+1);
    }
    gdk_draw_segments(pixmap, draw_gc, points, path->pathlen/2 -1);
    g_free(points);
  }
  g_free (path->pts);
  g_free (path);
}

int ls_on_screen_preview (gchar * s, Rparams * p)
{
  register gchar *sptr;
  Position pos, keep_pos;
  Special **slut;
  Special *spec, **akt;
  Path *path;
  gint scanned, i, flushed;
  gfloat slen;

  if (p->slut != NULL)
    slut = (p->slut);
  else {
    p->slut = (Special **) g_malloc0 (256 * sizeof (Special *));
    slut = (p->slut);
    for (i = 0; i < p->srulnum; i++) {
      akt = &slut[(guint) p->srules[i][0]];
      (*akt)=ls_parse_srule(p->srules[i]);
    }
  }

  sptr = s;
  pos=p->initpos;

  path = new_path (pos);
  while ((*sptr != 0) && (*sptr != ')')) {
    switch (*sptr) {
    case 'F':			/* Forward move */
      if (p->acu_step_random)
	slen = p->steplen += rand_norm (p->step_random)
	  * p->steplen;
      else
	slen = p->steplen + rand_norm (p->step_random)
	  * p->steplen;
      pos.x += (gfloat) slen *cos (PI * pos.angle / 180.0);
      pos.y += (gfloat) slen *sin (PI * pos.angle / 180.0);
      path = add_point (path, pos);
      break;
    case 'f':			/* Forward jump */
      if (p->acu_step_random)
	slen = p->steplen += rand_norm (p->step_random)
	  * p->steplen;
      else
	slen = p->steplen + rand_norm (p->step_random)
	  * p->steplen;
      prv_flush_path (path);
      pos.x += (gfloat) slen *cos (PI * pos.angle / 180.0);
      pos.y += (gfloat) slen *sin (PI * pos.angle / 180.0);
      path = new_path (pos);
      break;
    case '(':			/* Begining of a "sub l-system" */
      p->steplen *= p->stepfactor;
      keep_pos=p->initpos;
      p->initpos = pos;
      scanned = ls_on_screen_preview (sptr + 1, p);
      p->steplen /= p->stepfactor;
      p->initpos=keep_pos;
      sptr += 1 + scanned;
      break;
    default:			/* Special cases or nothing. */
      if (slut[(guchar) *sptr] != NULL) {
	if ((guchar) *sptr > 127) g_print("A to co : %c (%d)\n", *sptr, (guchar)*sptr);
	flushed = 0;
	spec = slut[(guchar) *sptr];
	pos.angle += spec->turn_angle +
	  rand_norm (p->turn_random) * spec->turn_angle;
      }
    }				/* End of switch(...) */
    sptr++;
  }				/* End of while(...) */
  if (path) prv_flush_path(path);
  return (sptr - s);
}

void ls_draw_preview()
{
  gchar *compiled;
  gdouble keep_step, keep_x, keep_y;
  gdouble kx, ky, k;
  GdkRectangle rect;

  if (!selected_lsystem) return;
  if (!pixmap) return;
  
  gdk_draw_rectangle(pixmap, white_gc, TRUE, 0, 0, prv_wid, prv_hei);  

  compiled=lsystem_create_string(&selected_lsystem->def);
  keep_step=selected_lsystem->prm.steplen;
  keep_x=selected_lsystem->prm.initpos.x;
  keep_y=selected_lsystem->prm.initpos.y;
  selected_lsystem->prm.initpos.x=0.0;
  selected_lsystem->prm.initpos.y=0.0;
  selected_lsystem->prm.steplen=1.0;
  maxpos.x=-MAXFLOAT;
  maxpos.y=-MAXFLOAT;
  minpos.x=MAXFLOAT;
  minpos.y=MAXFLOAT;

  srand(selected_lsystem->prm.seed);
  lsystem_preview(compiled, &selected_lsystem->prm);
  free_slut(&selected_lsystem->prm);
  
  kx=(gdouble)prv_wid / (maxpos.x - minpos.x);
  ky=(gdouble)prv_hei / (maxpos.y - minpos.y);
  k=kx < ky ? kx : ky;
  k*=0.95;
  k*=selected_lsystem->prm.as_correction; /* Adjusting scale according 
					     to user-specified correction */
  selected_lsystem->prm.steplen *= k;
  if (gimp_debug()) 
    g_print("Screen scaling factor=%f, steplen=%f\n", k, selected_lsystem->prm.steplen);
  selected_lsystem->prm.initpos.x -= minpos.x;
  selected_lsystem->prm.initpos.y -= minpos.y;
  selected_lsystem->prm.initpos.x *= k;
  selected_lsystem->prm.initpos.y *= k;
  /* Centering the result on the selection/image */
  selected_lsystem->prm.initpos.x+= (prv_wid - (maxpos.x-minpos.x)*k)/2; 
  selected_lsystem->prm.initpos.y+= (prv_hei - (maxpos.y-minpos.y)*k)/2; 

  srand(selected_lsystem->prm.seed);
  ls_on_screen_preview(compiled, &selected_lsystem->prm);
  free_slut(&selected_lsystem->prm);

  selected_lsystem->prm.initpos.x=keep_x;
  selected_lsystem->prm.initpos.y=keep_y;
  selected_lsystem->prm.steplen=keep_step;
  g_free(compiled);

  /* Updating X image */
  rect.x=0;
  rect.y=0;
  rect.width=prv_wid;
  rect.height=prv_hei;
  gtk_widget_draw(lsui.prv, &rect);
}

void ls_preview_clear()
{
  GdkRectangle rect;

  if (!pixmap) return;
  
  gdk_draw_rectangle(pixmap, white_gc, TRUE, 0, 0, prv_wid, prv_hei);  
  rect.x=0;
  rect.y=0;
  rect.width=prv_wid;
  rect.height=prv_hei;
  gtk_widget_draw(lsui.prv, &rect);
}


static gint ls_prv_configure_event_handler(GtkWidget *w, GdkEventConfigure *ev)
{
  if (pixmap) {
    gdk_pixmap_unref(pixmap);
    gdk_gc_unref(draw_gc);
    gdk_gc_unref(white_gc);
  }
  draw_gc=gdk_gc_ref(w->style->fg_gc[GTK_STATE_NORMAL]);
  white_gc=gdk_gc_ref(w->style->white_gc);

  pixmap=gdk_pixmap_new(w->window, w->allocation.width, w->allocation.height, -1);
  gdk_draw_rectangle(pixmap, w->style->white_gc, TRUE, 0, 0, 
		     w->allocation.width, w->allocation.height);
  prv_wid=w->allocation.width;
  prv_hei=w->allocation.height;
  if (gimp_debug()) g_print("Preview configured.\n");
  return TRUE;
}

static gint ls_prv_expose_event_handler(GtkWidget *w, GdkEventExpose *ev)
{
  gdk_draw_pixmap(w->window, w->style->fg_gc[GTK_STATE_NORMAL], pixmap, 
		  ev->area.x, ev->area.y, ev->area.x, ev->area.y, 
		  ev->area.width, ev->area.height);

  if (gimp_debug()) g_print("Preview exposed and redrawn.\n");
  return FALSE;
}
