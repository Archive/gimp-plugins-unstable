
This is REDME for the fourth public relase (v 0.9) of Lsystems plugin for GIMP

Be sure to visit lsystems plugin homepage at:
http://www.ia.pw.edu.pl/~s146028/fractal/lsyst

0) Any differences from previous relase?

Yes! These include:
- Automatic scaling of the l-system acording to current selection/image with 
  additional zoom factor
- Preview is working!
- Available L-systems list uses a tree widget
- Randomized l-systems are repeatable (seed is set before rendering)
- Only changed l-systems are written to disk on exit (these are indicated with '##')


1) What it is?

My plug-in allows you to create, edit and draw Lsystems on your GIMP 
images. Each Lsystem can be selected by name and its definition 
consists of several different parts. 

If you haven't heard about Lsystems you will probably find it 
quite difficult to learn what they are and why they are so interesting
just from a few examples that go along with the plug-in, but stay 
calm and wait for more extensive help which I am going to write
(if time permits :)). Check the page at address 
http://www.ia.pw.edu.pl/~s146028/fractal/lsyst


2) What you can do?

If you know what Lsystems are that paragraph is for you. With my 
plug-in you can:
- edit the set of rules which form the main part of Lsystem,
- supply it with an initial string (axiom),
- create and edit LAST rules, the ones which are applied after
  the last iteration and can be used to translate some higher
  level objects into renderable things (for example you assign
  a meaning of "trunk" to the letter 't' and write your Lsystems
  in the way that it will simulate tree growth, in the last rules
  you should change "t" into "F", what will make it visible on the
  image).
- create and edit SPECIAL rules which govern rendering turtle
  turns and its painting device (brush, color, opacity and spacing).
- adjust some other rendering parameters


3) Installation

You must do the following: 
- unpack the archive
- look through the Makefile and possibly adjust it to 
  suit your gimp configuration (paths and so on)
- compile the plug-in typing "make"
- copy the resulting "lsystem" file to your ~/.gimp/plug-ins
  directory
- copy "lsystems" subdirectory with its contents to the ~/.gimp
  directory
- update ~/.gimp/gimprc file adding the following line:
	(lsystem-path "${gimp_dir}/lsystems")
  Note, there is a file gimprc.add in plugin distribution.
- update ~/.gimp/gtkrc file adding the following lines:
	style "lsystem_rules"
	{
	  font = "-*-courier-medium-r-normal--*-100-100-100-m-*-*-*"
	}
	widget "*lsrules*" style "lsystem_rules"
  Note, there is a file gtkrc.add in plugin distribution.
- enjoy! 


4) Bugs ? Yes of course... :)

Please report them to me: 

M.Gomulinski@writeme.com 
or if the above won't work here (the next one goes straight to my mailbox):
M.Gomulinski@elka.pw.edu.pl


5) Please help.

If you design or just find and enter any interesting Lsystem let me
know, as I will certainly try to maintain a sort of database of 
interesting and nice Lsystems.
