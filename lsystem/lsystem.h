/* (c) Micha� Gomuli�ski */

#ifndef LSYSTEM_H
#define LSYSTEM_H

#define USE_CLIST

#ifndef M_PI
#define M_PI (gdouble)3.14159265358979323846
#endif

#ifdef __STRICT_ANSI__
#define inline static
#endif

#include<gtk/gtk.h>

/* 
   Version identification string: 
   -$Id$- 
*/

#undef CLIST_HAS_WIDGET

#define PLUGIN_VERSION "0.9.1"
#define PLUG_IN_NAME "plugin_lsystem"

#define MAX_LINE_LENGTH 128

#define PI M_PI

typedef struct _Lsystem Lsystem;
typedef struct _Position Position;
typedef struct _Rparams Rparams;    /* Rendering parameters */
typedef struct _LSinfo LSinfo;
typedef struct _Special Special;
typedef struct _Path Path;
typedef struct _Context Context;
typedef struct _LS_Ui_Controls LS_Ui_Controls;

struct _LS_Ui_Controls {
  GtkWidget *prv;
  GtkWidget *lst;
  GtkWidget *ntb;
  GtkWidget *rules;
  GtkEntry *axiom;
  GtkWidget *iters;
  GtkWidget *lrules;
  GtkWidget *srules;
  GtkEntry *startx;
  GtkEntry *starty;
  GtkEntry *startangle;
  GtkEntry *steplen;
  GtkEntry *stepfactor;
  GtkEntry *rounding;
  GtkEntry *turnrandom;
  GtkEntry *steprandom;
  GtkWidget *autoscale;
  GtkEntry *auto_cor;
  GtkWidget *ruleadd;
  GtkWidget *ruleedit;
  GtkWidget *rulerem;
  GtkWidget *lruleadd;
  GtkWidget *lruleedit;
  GtkWidget *lrulerem;
  GtkWidget *sruleadd;  
  GtkWidget *sruleedit;  
  GtkWidget *srulerem;
  GtkWidget *lsrm;
  GtkWidget *lscp;
  GtkWidget *lsmv;
  GtkWidget *auto_box;
  GtkWidget *manual_box;
}; 

struct _Position {
  gdouble   x;
  gdouble   y;
  gdouble angle;
};

struct _Lsystem {
  gchar * name;
  gchar * fname;
  gchar * axiom;
  gchar * *rules;   /* Rules string format: "%c:%s" */
  gchar * *lrules;  /* Rules applied after last iteration */
  gint    rulnum;
  gint    lrulnum;
  gint    iters;
};

struct _Rparams {
  Position         initpos;
  gdouble          steplen;
  gint             autoscale;   /* True if automatic scaling is ON */
  gdouble          as_correction; 
  gdouble          stepfactor;  /* how much should step
				   be decreased in subsystem */
  gchar *          *srules;     /* Special rules --- turns,
				   color, brush changes */
  gint             srulnum;
  Special          **slut;
  gdouble          rounding_amount; 
  gdouble          turn_random; /* Randomness in turn 
				   angles (N(0,turn_random)) */
  gdouble          step_random; /* Randomness in step 
				   length (N(0,step_random)) */
  gint             acu_step_random;
  gint             seed;        /* Seed of random number generator */
  /*  gint             bbox_wid;  */  /* Dimensions of the bounding box */
  /*gint             bbox_hei;   */ /* in which the l-system fitted */
};

struct _LSinfo {
  Lsystem          def;
  Rparams          prm;
  gint             dirty;  /* Flag which tells whether the specific L-System has
			      been modified recently (during this session). */
  /*  gint             needs_recalculation; */
  GtkLabel         *lab;
};

/* Brush name that means `do not change brush' */

#define CURRENT_BRUSH "<<CURRENT>>"

struct _Special {
  gchar            lefthand;
  gdouble          turn_angle;
  gchar *          brush_name;
  gint             brush_spc;
  gint             opacity;
  gint             color_change;  /* If color values are important */
  guchar           color[3];
};

struct _Context {
  gchar *          brush_name;
  gint             brush_spc;
  gint             opacity;
  guchar           color[3];
};

struct _Path {
  gint             pathlen;
  gint             pavail;
  gdouble          *pts;
};

/* 
srules syntax:
%c:<angle>:<brush_name>:<bspacing>:<bopacity>:<rgb_triplet>

It is advisable to stick to some common symbol definitions:
+:angle::::
-:-angle::::     <- turns to the right and to the left 

and rather not to mess turns with other things as you may soon
not know what's going on in your l-system
*/

/* Global variables */
extern GList *loaded_lsystems;
extern LS_Ui_Controls lsui;
extern GList *brushes;
extern int do_draw;
extern LSinfo *selected_lsystem;
extern LSinfo *default_lsystem;
extern guint image_wid, image_hei;
extern gchar last_ls_name[256];
extern Position maxpos;
extern Position minpos;

gchar   *lsystem_create_string( Lsystem * );


gchar *ls_make_full_filename(gchar *);
gint     lsystem_write_to_file( LSinfo * );
void ls_write_all();
GList *ls_load_all(GList *);
LSinfo  *lsystem_read_from_file( gchar* );
LSinfo *ls_lsystem_copy(LSinfo *, LSinfo *);
LSinfo *ls_lsystem_new();
void    lsinfo_free(LSinfo *);
Special *ls_parse_srule(gchar *);
gdouble ls_atod(gchar *);
gint ls_atoi(gchar *);

gint lscmp(LSinfo *, LSinfo *);
/*
gint     lsystem_do_preview();
*/

gint lsystem_render(gchar *, Rparams *);
gint lsystem_preview (gchar *, Rparams *);
void free_slut (Rparams *);

Path *new_path(Position);
Path *add_point(Path *, Position);

gdouble rand_norm(gdouble);


#define WINDOW_NAME "lsplugin"
#define RULES_NAME "lsrules"
#define LEFT_ENTRY "l_ent"
#define RIGHT_ENTRY "r_ent"
#define SPACING "spacing"
#define OPACITY "opacity"
#define BRUSH "brush"
#define RULE "rule_addr"
#define ANGLE "angle"
#define COLOR_ENABLED "color?"
#define OPACITY_ENABLED "opacity?"
#define SPC_ENABLED "spc?"

#define ENTRY1 "first"
#define ENTRY2 "second"

GtkWidget *ls_main_new();
void ls_create_lsystems_list();
void ls_update_main_dialog();

extern gchar *ls_data_key;   /* used in topleft list */
extern gchar *ls_rules_key;

#endif














