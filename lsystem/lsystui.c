/* 
   L-Systems plug-in 
   (c) by Michal Gomulinski
   1997, 1998 Warszawa

   mailto:M.Gomulinski@writeme.com

*/

#include<string.h>
#include<gtk/gtk.h>
#include"preview.h"
#include"scales.h"
#include"utils.h"
#include"callbacks.h"
#include"lsystem.h"
#include"lsystui.h"
#include"gdebug.h"

/* 
   Version identification string: 
   -$Id$- 
*/

#ifndef lint
static char vcid[] = "$Id$";
#endif /* lint */


LS_Ui_Controls lsui;

LSinfo *selected_lsystem;
LSinfo *default_lsystem;

gdouble steplen, stepfactor, rounding, turnrandom, steprandom, 
  brspacing, bropacity, turn_angle, initx, inity, initangle, auto_cor;

gint rules_changed;
/*
gdouble color[3];
gint edit_rule_dialog=FALSE;
gint edit_lrule_dialog=FALSE;
gint edit_srule_dialog=FALSE;
*/

void ls_rules_list_fill(gchar *[] , guint , GtkCList *);
void ls_rules_list_create(Lsystem *);
void ls_lrules_list_create(Lsystem *);
void ls_srules_list_create();
LSinfo *ls_get_selected_lsystem();
void ls_update_rules_dialog();
void ls_grab_values();


GtkWidget *ls_main_new()
{
   GtkWidget *window,
	     *bx1,
	     *bx2,
	     *bx3,
	     *bx4,
	     *hbox,
	     *vbox,
	     *tbl,
	     *frm,
	     *bCancel,
	     *ntb,
	     *pg1,
	     *pg2,
	     *pg3,
	     *pg4,
             *pg5,
	     *lbl,
	     *wgt,
             *p5vbox,
	     *bOk;
   GtkTooltips 
	     *ts3;
   gchar * rules_titles[]= {"   ", " -> ", "Substituting string"};
   gchar * special_titles[]={"   ", " -> ", "Angle", "Brush", 
			     "Spacing", "Opacity", "Color"};
   gchar *buf=g_malloc0(256*sizeof(gchar));
   GtkAdjustment *adjust;

   (void)vcid; /* To avoid compilator warning. */
/* Window stuff */
   window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
   gtk_container_border_width( GTK_CONTAINER(window), 10);
   gtk_window_set_title( GTK_WINDOW(window), "GIMP:L-Systems" );
   gtk_signal_connect_object(GTK_OBJECT(window), "destroy", 
			     GTK_SIGNAL_FUNC(gtk_widget_hide), 
			     GTK_OBJECT(window));
   gtk_widget_set_name(window, WINDOW_NAME);

   bx1=gtk_vbox_new(FALSE,10);
   bx2=gtk_hbox_new(TRUE,0);

/* Buttons (bottom of window) */
   bOk=gtk_button_new_with_label("Draw");
   GTK_WIDGET_SET_FLAGS( bOk, GTK_CAN_DEFAULT );
   gtk_signal_connect(GTK_OBJECT(bOk), "clicked", 
		      GTK_SIGNAL_FUNC(ls_ok_callback), NULL);
   gtk_box_pack_start(GTK_BOX(bx2), bOk, TRUE, TRUE, 10);
   gtk_widget_show(bOk);
   bCancel=gtk_button_new_with_label("Cancel");
   GTK_WIDGET_SET_FLAGS( bCancel, GTK_CAN_DEFAULT );
   gtk_signal_connect(GTK_OBJECT(bCancel), "clicked",
			 GTK_SIGNAL_FUNC(ls_cancel_callback), NULL);
   gtk_box_pack_start(GTK_BOX(bx2), bCancel, TRUE, TRUE, 10);
   gtk_widget_show(bCancel);
   wgt=gtk_button_new_with_label("About");
   GTK_WIDGET_SET_FLAGS( wgt, GTK_CAN_DEFAULT );
   gtk_box_pack_start(GTK_BOX(bx2), wgt, TRUE, TRUE, 10);
   gtk_widget_show(wgt);
   gtk_signal_connect(GTK_OBJECT(wgt), "clicked",
			 GTK_SIGNAL_FUNC(ls_about_callback), NULL);
   gtk_box_pack_end(GTK_BOX(bx1), bx2, FALSE, FALSE, 0); 

/* Main frame */
   frm=gtk_frame_new("Parameter Settings");
   gtk_frame_set_shadow_type(GTK_FRAME(frm), GTK_SHADOW_ETCHED_IN);
   gtk_container_border_width(GTK_CONTAINER(frm), 5);
   gtk_widget_show(frm);
   gtk_box_pack_start(GTK_BOX(bx1), frm, TRUE, TRUE, 0);
   tbl=gtk_table_new(2,2, FALSE);
   gtk_container_border_width(GTK_CONTAINER(tbl), 5);
   gtk_container_add(GTK_CONTAINER(frm), tbl);
   gtk_widget_show(tbl);

/* Preview frame */
   frm=gtk_frame_new("Preview");
   gtk_frame_set_shadow_type(GTK_FRAME(frm), GTK_SHADOW_ETCHED_IN);
   gtk_container_border_width(GTK_CONTAINER(frm), 0);
   gtk_widget_show(frm);
   gtk_table_attach(GTK_TABLE(tbl), frm, 1,2,0,1, 
			   0, 0, 5, 0);
   bx3=gtk_vbox_new(FALSE,0); gtk_widget_show(bx3);
   gtk_container_border_width(GTK_CONTAINER(bx3), 5);
   pg1=gtk_frame_new(NULL);
   gtk_frame_set_shadow_type(GTK_FRAME(pg1), GTK_SHADOW_IN);
   gtk_container_border_width(GTK_CONTAINER(pg1), 5);
   gtk_widget_show(pg1);
   gtk_box_pack_start(GTK_BOX(bx3), pg1, 0, TRUE, 0);
   wgt=gtk_button_new_with_label("Preview"); gtk_widget_show(wgt);
   gtk_signal_connect(GTK_OBJECT(wgt), "clicked", GTK_SIGNAL_FUNC(ls_preview_callback), NULL);
   gtk_box_pack_start(GTK_BOX(bx3), wgt, 0, TRUE, 0);
   gtk_container_add(GTK_CONTAINER(frm), bx3);

   /* Until I actually make the preview usefull, let's give the
      user some hint that it's only a "wish for a preview" */
   /* gtk_widget_set_sensitive(frm, FALSE); */

   lsui.prv=ls_preview_new();
   gtk_container_add(GTK_CONTAINER(pg1), lsui.prv); 
   gtk_widget_show(lsui.prv);

/* List of available L-systems */
   frm=gtk_frame_new("Available L-Systems"); gtk_widget_show(frm);
   bx3=gtk_vbox_new(FALSE, 5); gtk_widget_show(bx3);
   gtk_container_border_width(GTK_CONTAINER(bx3), 5);
   gtk_container_add(GTK_CONTAINER(frm), bx3);
   bx4=gtk_hbox_new(TRUE, 5); gtk_widget_show(bx4);
   gtk_box_pack_end(GTK_BOX(bx3), bx4, FALSE, FALSE, 0);
   wgt=gtk_button_new_with_label("Copy"); 
   lsui.lscp=wgt;
   
   ts3=gtk_tooltips_new(); 
   gtk_tooltips_set_tip(ts3, wgt, 
		   "Create a copy of current L-system", NULL );
   add_button(bx4, wgt, GTK_SIGNAL_FUNC(ls_copy_callback));
   wgt=gtk_button_new_with_label("New"); 
   
   ts3=gtk_tooltips_new(); 
   gtk_tooltips_set_tip(ts3, wgt, 
		   "Create a new L-System from scratch", NULL);
   add_button(bx4, wgt, GTK_SIGNAL_FUNC(ls_new_callback));
   wgt=gtk_button_new_with_label("Rename"); 
   lsui.lsmv=wgt;
   
   ts3=gtk_tooltips_new(); 
   gtk_tooltips_set_tip(ts3, wgt, 
		   "Rename current L-system", NULL);
   add_button(bx4, wgt, GTK_SIGNAL_FUNC(ls_rename_callback));
   wgt=gtk_button_new_with_label("Delete"); 
   lsui.lsrm=wgt;
   
   ts3=gtk_tooltips_new(); 
   gtk_tooltips_set_tip(ts3, wgt, 
		   "Delete current L-system", NULL);
   add_button(bx4, wgt, GTK_SIGNAL_FUNC(ls_delete_callback));

   /*   
   wgt=gtk_button_new_with_label("Refresh"); 
   ts4=gtk_tooltips_new();
   gtk_tooltips_set_tip(ts4, wgt, 
		   "Re-read L-Systems database", NULL );
   add_button(bx4, wgt, GTK_SIGNAL_FUNC(ls_reread_callback));
   */

   gtk_table_attach(GTK_TABLE(tbl), frm, 0,1, 0,1, 
			   GTK_EXPAND|GTK_FILL, GTK_FILL, 5, 0);


/* Available L-Systems Tree */
   pg1=gtk_scrolled_window_new(NULL, NULL);
   gtk_container_border_width(GTK_CONTAINER(pg1), 5);
   gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(pg1), 
			   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
   gtk_widget_set_usize(pg1, 250, 150);
   gtk_box_pack_start(GTK_BOX(bx3), pg1, TRUE, TRUE, 0);
   gtk_widget_show(pg1);
   lsui.lst=ls_create_lsystem_subtree("L-Systems", "", 0);
   gtk_tree_set_selection_mode(GTK_TREE(lsui.lst), GTK_SELECTION_BROWSE);
   gtk_signal_connect(GTK_OBJECT(lsui.lst), "selection_changed", 
		      GTK_SIGNAL_FUNC(ls_update_main_dialog_callback), NULL);

   gtk_tree_set_view_lines(GTK_TREE(lsui.lst), 1);
   gtk_tree_set_view_mode(GTK_TREE(lsui.lst), 0);
   gtk_container_add(GTK_CONTAINER(pg1), lsui.lst);
   gtk_widget_show(lsui.lst);

/* Bottom frame */
   frm=gtk_frame_new(NULL);
   gtk_frame_set_shadow_type(GTK_FRAME(frm), GTK_SHADOW_ETCHED_IN);
   gtk_widget_show(frm);
   gtk_table_attach(GTK_TABLE(tbl), frm, 0,2,1,2, 
			   GTK_EXPAND|GTK_FILL, GTK_EXPAND|GTK_FILL, 5, 10);

/* Notebook with options */   
   ntb=gtk_notebook_new();
   lsui.ntb=ntb;
   gtk_container_border_width(GTK_CONTAINER(ntb), 5);
   gtk_notebook_set_tab_pos(GTK_NOTEBOOK(ntb), GTK_POS_TOP);
   pg1=gtk_frame_new("Iterative Rules");
   pg2=gtk_frame_new("Last Rules");
   pg3=gtk_frame_new("Rendering Rules");
   pg5=gtk_frame_new("Positioning the result on the image");
   pg4=gtk_frame_new("Rendering Options");
   gtk_widget_show(pg1);
   gtk_widget_show(pg2);
   gtk_widget_show(pg3);
   gtk_widget_show(pg4);
   gtk_widget_show(pg5);

   /* Notebook: first page */
   lbl=gtk_label_new("Main"); 
   gtk_widget_set_usize(lbl, 60, 0);
   gtk_widget_show(lbl);
   gtk_notebook_append_page(GTK_NOTEBOOK(ntb), pg1, lbl);

   tbl=gtk_table_new(7,2,FALSE); gtk_widget_show(tbl);

   gtk_container_add(GTK_CONTAINER(pg1), tbl);
   wgt=gtk_label_new("Axiom:"); gtk_widget_show(wgt);
   gtk_table_attach(GTK_TABLE(tbl), wgt, 0, 1, 0, 1,
                         GTK_FILL, GTK_FILL, 4, 0);
   lsui.axiom=GTK_ENTRY(gtk_entry_new()); 
   gtk_widget_show(GTK_WIDGET(lsui.axiom));
   gtk_widget_set_usize(GTK_WIDGET(lsui.axiom), 200, 0);
   wgt=gtk_hbox_new(FALSE, 0); gtk_widget_show(wgt);
   gtk_box_pack_start(GTK_BOX(wgt), GTK_WIDGET(lsui.axiom), FALSE, FALSE, 0); 
   gtk_table_attach(GTK_TABLE(tbl), wgt, 1, 2, 0, 1,
                         GTK_FILL|GTK_EXPAND, GTK_FILL, 4, 0);
   wgt=gtk_label_new("Iterations:"); gtk_widget_show(wgt);
   gtk_table_attach(GTK_TABLE(tbl), wgt, 0, 1, 1, 2,
                         GTK_FILL, GTK_FILL, 5, 5);
   adjust=GTK_ADJUSTMENT(gtk_adjustment_new(1, 1, 50, 1, 1, 0.0));
   lsui.iters=gtk_spin_button_new(GTK_ADJUSTMENT(adjust), 0, 0);
   gtk_widget_show(lsui.iters);

   wgt=gtk_hbox_new(FALSE, 0); gtk_widget_show(wgt);
   gtk_box_pack_start(GTK_BOX(wgt), GTK_WIDGET(lsui.iters), FALSE, FALSE, 0); 
   gtk_table_attach(GTK_TABLE(tbl), wgt, 1, 2, 1, 2,
                         GTK_FILL|GTK_EXPAND, GTK_FILL, 5, 5);
   wgt=gtk_hseparator_new(); gtk_widget_show(wgt);
   gtk_table_attach(GTK_TABLE(tbl), wgt, 0, 2, 3, 4,
                         GTK_FILL | GTK_EXPAND, 0, 5, 5);
   wgt=gtk_label_new("L-System definition rules:"); 
   gtk_widget_show(wgt);
   gtk_table_attach(GTK_TABLE(tbl), wgt, 0,1,4,5,
                 GTK_FILL, GTK_FILL, 4, 0);

   lsui.rules=gtk_clist_new_with_titles(3, rules_titles);
   gtk_clist_column_titles_hide(GTK_CLIST(lsui.rules));
   gtk_clist_set_selection_mode(GTK_CLIST(lsui.rules), GTK_SELECTION_BROWSE);
   gtk_clist_set_policy(GTK_CLIST(lsui.rules), GTK_POLICY_ALWAYS, 
			GTK_POLICY_AUTOMATIC);
   /* gtk_clist_column_titles_passive(GTK_CLIST(lsui.rules)); */
   gtk_clist_set_column_justification( GTK_CLIST(lsui.rules), 0, GTK_JUSTIFY_CENTER );
   gtk_clist_set_column_justification( GTK_CLIST(lsui.rules), 1, GTK_JUSTIFY_CENTER );
   gtk_clist_set_column_justification( GTK_CLIST(lsui.rules), 2, GTK_JUSTIFY_LEFT );
   gtk_clist_set_column_width( GTK_CLIST(lsui.rules), 0, 20);
   gtk_clist_set_column_width( GTK_CLIST(lsui.rules), 1, 30);
   gtk_table_attach(GTK_TABLE(tbl), lsui.rules, 0,2,5,6,
                 GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 5, 5);
   gtk_widget_show(lsui.rules);

   hbox=gtk_hbox_new(0,0); gtk_widget_show(hbox); 
   wgt=gtk_button_new_with_label("  Edit ");
   
   add_button(hbox, wgt, GTK_SIGNAL_FUNC(ls_edit_rule_callback)); 
   lsui.ruleedit=wgt;
   wgt=gtk_button_new_with_label("  Add  ");
   
   add_button(hbox, wgt, GTK_SIGNAL_FUNC(ls_add_rule_callback)); 
   lsui.ruleadd=wgt;
   wgt=gtk_button_new_with_label(" Remove ");
   
   add_button(hbox, wgt, GTK_SIGNAL_FUNC(ls_remove_rule_callback)); 
   lsui.rulerem=wgt;
   gtk_table_attach(GTK_TABLE(tbl), hbox, 0,2,6,7,
                 GTK_FILL | GTK_EXPAND, 0, 5, 5);
  
 
   /* Notebook - second page */

   lbl=gtk_label_new("Last"); 
   gtk_widget_set_usize(lbl, 60, 0);
   gtk_widget_show(lbl);
   gtk_notebook_append_page(GTK_NOTEBOOK(ntb), pg2, lbl);
   vbox=gtk_vbox_new(0,5); gtk_widget_show(vbox);
   gtk_container_border_width(GTK_CONTAINER(vbox), 5);
   gtk_container_add(GTK_CONTAINER(pg2), vbox);
   wgt=gtk_label_new("Rules applied after last iteration:"); 
   gtk_widget_show(wgt);
   gtk_box_pack_start(GTK_BOX(vbox), wgt, FALSE, TRUE, 0);
   hbox=gtk_hbox_new(0,0); gtk_widget_show(hbox);

   lsui.lrules=gtk_clist_new_with_titles(3, rules_titles);
   gtk_clist_set_selection_mode(GTK_CLIST(lsui.lrules), GTK_SELECTION_BROWSE);
   gtk_clist_set_policy(GTK_CLIST(lsui.lrules), GTK_POLICY_ALWAYS, 
			GTK_POLICY_AUTOMATIC);
   gtk_clist_column_titles_passive(GTK_CLIST(lsui.lrules));
   gtk_clist_column_titles_show(GTK_CLIST(lsui.lrules));
   gtk_clist_set_column_justification( GTK_CLIST(lsui.lrules), 0, GTK_JUSTIFY_CENTER );
   gtk_clist_set_column_justification( GTK_CLIST(lsui.lrules), 1, GTK_JUSTIFY_CENTER );
   gtk_clist_set_column_justification( GTK_CLIST(lsui.lrules), 2, GTK_JUSTIFY_LEFT );
   gtk_clist_set_column_width( GTK_CLIST(lsui.lrules), 0, 20);
   gtk_clist_set_column_width( GTK_CLIST(lsui.lrules), 1, 30);
   gtk_box_pack_start(GTK_BOX(vbox), lsui.lrules, TRUE, TRUE, 0);
   gtk_widget_show(lsui.lrules);

   hbox=gtk_hbox_new(0,0); gtk_widget_show(hbox); 
   wgt=gtk_button_new_with_label("  Edit ");
   
   add_button(hbox, wgt, GTK_SIGNAL_FUNC(ls_edit_lrule_callback)); 
   lsui.lruleedit=wgt;
   wgt=gtk_button_new_with_label("  Add  ");
   
   add_button(hbox, wgt, GTK_SIGNAL_FUNC(ls_add_lrule_callback)); 
   lsui.lruleadd=wgt;
   wgt=gtk_button_new_with_label(" Remove ");
   
   add_button(hbox, wgt, GTK_SIGNAL_FUNC(ls_remove_lrule_callback)); 
   lsui.lrulerem=wgt;
   gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);


   /* Notebook - third page */

   lbl=gtk_label_new("Special"); 
   gtk_widget_set_usize(lbl, 60, 0);
   gtk_widget_show(lbl);
   gtk_notebook_append_page(GTK_NOTEBOOK(ntb), pg3, lbl);
   vbox=gtk_vbox_new(0,5); gtk_widget_show(vbox);
   gtk_container_border_width(GTK_CONTAINER(vbox), 5);
   gtk_container_add(GTK_CONTAINER(pg3), vbox);
   wgt=gtk_label_new("Rules that govern L-System rendering"); 
   gtk_widget_show(wgt);
   gtk_box_pack_start(GTK_BOX(vbox), wgt, FALSE, TRUE, 0);
#ifndef USE_CLIST
   scrl=gtk_scrolled_window_new(NULL, NULL);
   gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrl), 
			   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
   gtk_widget_show(scrl);
   lsui.srules=gtk_list_new();
   gtk_widget_set_name(lsui.srules, RULES_NAME);
   GTK_LIST(lsui.srules)->selection_mode=GTK_SELECTION_BROWSE;
   gtk_container_add(GTK_CONTAINER(scrl), lsui.srules);
   gtk_box_pack_start(GTK_BOX(vbox), scrl, TRUE, TRUE, 0);
   gtk_widget_show(lsui.srules);
#else
   lsui.srules=gtk_clist_new_with_titles(7, special_titles);
   gtk_clist_set_selection_mode(GTK_CLIST(lsui.srules), GTK_SELECTION_BROWSE);
   gtk_clist_set_policy(GTK_CLIST(lsui.srules), GTK_POLICY_ALWAYS, 
			GTK_POLICY_AUTOMATIC);
   gtk_clist_column_titles_passive(GTK_CLIST(lsui.srules));
   gtk_clist_column_titles_show(GTK_CLIST(lsui.srules));
   gtk_clist_set_column_justification( GTK_CLIST(lsui.srules), 0, GTK_JUSTIFY_CENTER );
   gtk_clist_set_column_justification( GTK_CLIST(lsui.srules), 1, GTK_JUSTIFY_CENTER );
   gtk_clist_set_column_justification( GTK_CLIST(lsui.srules), 2, GTK_JUSTIFY_RIGHT );
   gtk_clist_set_column_justification( GTK_CLIST(lsui.srules), 3, GTK_JUSTIFY_RIGHT );
   gtk_clist_set_column_justification( GTK_CLIST(lsui.srules), 4, GTK_JUSTIFY_RIGHT );
   gtk_clist_set_column_justification( GTK_CLIST(lsui.srules), 5, GTK_JUSTIFY_RIGHT );
   gtk_clist_set_column_justification( GTK_CLIST(lsui.srules), 6, GTK_JUSTIFY_RIGHT );
   gtk_clist_set_column_width( GTK_CLIST(lsui.srules), 0, 20);
   gtk_clist_set_column_width( GTK_CLIST(lsui.srules), 1, 30);
   gtk_clist_set_column_width( GTK_CLIST(lsui.srules), 2, 40);
   gtk_clist_set_column_width( GTK_CLIST(lsui.srules), 3, 165);
   gtk_clist_set_column_width( GTK_CLIST(lsui.srules), 4, 50);
   gtk_clist_set_column_width( GTK_CLIST(lsui.srules), 5, 50);
   gtk_clist_set_column_width( GTK_CLIST(lsui.srules), 6, 50);
   gtk_box_pack_start(GTK_BOX(vbox), lsui.srules, TRUE, TRUE, 0);
   gtk_widget_show(lsui.srules);
#endif
   hbox=gtk_hbox_new(0,0); gtk_widget_show(hbox); 
   wgt=gtk_button_new_with_label("  Edit ");
   
   add_button(hbox, wgt, GTK_SIGNAL_FUNC(ls_edit_srule_callback)); 
   lsui.sruleedit=wgt;
   wgt=gtk_button_new_with_label("  Add  ");
   
   add_button(hbox, wgt, GTK_SIGNAL_FUNC(ls_add_srule_callback)); 
   lsui.sruleadd=wgt;
   wgt=gtk_button_new_with_label(" Remove ");
   
   add_button(hbox, wgt, GTK_SIGNAL_FUNC(ls_remove_srule_callback)); 
   lsui.srulerem=wgt;
   gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);


   /* Notebook -- fourth page */

   lbl=gtk_label_new("Position"); 
   gtk_widget_set_usize(lbl, 60, 0);
   gtk_widget_show(lbl);
   gtk_notebook_append_page(GTK_NOTEBOOK(ntb), pg5, lbl);
   p5vbox=gtk_vbox_new(FALSE, 5);
   gtk_container_add(GTK_CONTAINER(pg5), p5vbox);
   gtk_widget_show(p5vbox);
   
   sprintf(buf, "Current selection/image dimensions are: %d x %d", image_wid, image_hei);
   lbl=gtk_label_new(buf);
   gtk_box_pack_start(GTK_BOX(p5vbox), lbl, FALSE, FALSE, 5);
   gtk_widget_show(lbl);
   /*
   wgt=gtk_hseparator_new();
   gtk_box_pack_start(GTK_BOX(p5vbox), wgt, FALSE, FALSE, 5);
   gtk_widget_show(wgt);
   */
   
   lsui.manual_box=gtk_frame_new(NULL); gtk_widget_show(lsui.manual_box);
   gtk_box_pack_start(GTK_BOX(p5vbox), lsui.manual_box, FALSE, FALSE, 0);
   tbl=gtk_table_new(3,4,FALSE); gtk_widget_show(tbl);
   gtk_container_add(GTK_CONTAINER(lsui.manual_box), tbl);

   lsui.startx=dialog_create_value("Start X", 
		       GTK_TABLE(tbl), 0, &initx, 0, image_wid, 0.5, 70, 100);
   lsui.starty=dialog_create_value("Start Y", 
		       GTK_TABLE(tbl), 1, &inity, 0, image_hei, 0.5, 70, 100);
   lsui.steplen=dialog_create_value("Step length", 
		       GTK_TABLE(tbl), 2, &steplen, 0, 100, 0.5, 70, 100);
   gtk_widget_set_sensitive(lsui.manual_box, FALSE);
   lsui.autoscale=
     gtk_check_button_new_with_label("Fit current selection/image");
   gtk_widget_show(lsui.autoscale);
   gtk_box_pack_start(GTK_BOX(p5vbox), lsui.autoscale, FALSE, FALSE, 5);

   lsui.auto_box=gtk_frame_new(NULL); gtk_widget_show(lsui.auto_box);
   gtk_box_pack_start(GTK_BOX(p5vbox), lsui.auto_box, FALSE, FALSE, 0);
   tbl=gtk_table_new(1,4,FALSE); gtk_widget_show(tbl);
   gtk_container_add(GTK_CONTAINER(lsui.auto_box), tbl);
   /*   gtk_widget_set_sensitive(lsui.auto_box, FALSE); */
   lsui.auto_cor=dialog_create_value("Scaling", 
		       GTK_TABLE(tbl), 0, &auto_cor, 0, 300, 0.5, 70, 100);

   gtk_signal_connect(GTK_OBJECT(lsui.autoscale), "toggled", 
		      GTK_SIGNAL_FUNC(ls_autoscale_toggle), NULL);
   gtk_signal_connect(GTK_OBJECT(lsui.autoscale), "toggled", 
		      GTK_SIGNAL_FUNC(ls_toggle_widget), lsui.manual_box);
   gtk_signal_connect(GTK_OBJECT(lsui.autoscale), "toggled", 
		      GTK_SIGNAL_FUNC(ls_toggle_widget), lsui.auto_box);


   /* Notebook - fifth page */

   lbl=gtk_label_new(" Render "); 
   gtk_widget_set_usize(lbl, 60, 0);
   gtk_widget_show(lbl);
   gtk_notebook_append_page(GTK_NOTEBOOK(ntb), pg4, lbl);
   tbl=gtk_table_new(9,4,FALSE); gtk_widget_show(tbl);
   gtk_container_add(GTK_CONTAINER(pg4), GTK_WIDGET(tbl));
   lsui.startangle=dialog_create_value("Start heading", 
		       GTK_TABLE(tbl), 4, &initangle, -180, 180, 0.5, 70, 100);
   lsui.stepfactor=dialog_create_value("Contraction factor (%)",
		       GTK_TABLE(tbl), 5, &stepfactor, 0, 100, 0.5, 70, 100);
   lsui.rounding=dialog_create_value("Rounding (not working!)",
		       GTK_TABLE(tbl), 6, &rounding, 0, 100, 0.5, 70, 100);
   lsui.turnrandom=dialog_create_value("Turn randomness (%)",
		       GTK_TABLE(tbl), 7, &turnrandom, 0, 100, 0.2, 70, 100);
   lsui.steprandom=dialog_create_value("Step randomness (%)",
		       GTK_TABLE(tbl), 8, &steprandom, 0, 100, 0.2, 70, 100);
   wgt=gtk_hseparator_new(); gtk_widget_show(wgt);
   gtk_table_attach(GTK_TABLE(tbl), wgt, 0, 4, 9, 10, GTK_FILL, GTK_FILL, 0, 5);
   wgt=gtk_button_new_with_label("New seed");
   gtk_widget_set_usize(wgt, 100, 0);
   gtk_widget_show(wgt);
   gtk_table_attach(GTK_TABLE(tbl), wgt, 0, 4, 10, 11, 0, GTK_FILL, 0, 0);
   gtk_signal_connect(GTK_OBJECT(wgt), "clicked", GTK_SIGNAL_FUNC(ls_new_seed_callback), NULL);

   gtk_container_add(GTK_CONTAINER(frm), ntb);
   gtk_widget_show(ntb);

   gtk_container_add(GTK_CONTAINER(window), bx1);
   gtk_widget_show(bx2);
   gtk_widget_show(bx1);
   gtk_widget_grab_default(bOk);
   gtk_widget_show(window);
   ls_update_main_dialog_callback(NULL, NULL);
   selected_lsystem=default_lsystem;
   
   g_free(buf);
   return window;
} 

#define SAVE_IF_DIFF_INT(a,b)  if (a!=b) { b=a; changed=TRUE; }
#define SAVE_IF_DIFF_FLOAT(a,b) if (fabs(a-b) > 1.0e-6 ) { b=a; changed=TRUE; } 

void ls_grab_values()
{
  LSinfo *l=selected_lsystem;
  gchar *newaxiom;
  gint changed=FALSE;
  gint ival;
  gdouble dval;
  gchar *labtext, *newlabel;

  newaxiom=g_strdup(gtk_entry_get_text(GTK_ENTRY(lsui.axiom)));
  if (l->def.axiom==NULL || strcmp(newaxiom, l->def.axiom)!=0) {
    g_free(l->def.axiom);
    l->def.axiom=newaxiom;
    changed=TRUE;
  }
  ival=gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(lsui.iters));
  SAVE_IF_DIFF_INT(ival, l->def.iters);
  dval=auto_cor/100.0;
  SAVE_IF_DIFF_FLOAT(dval, l->prm.as_correction);  
  dval=steplen;
  SAVE_IF_DIFF_FLOAT(dval, l->prm.steplen);
  dval=stepfactor/100.0;
  SAVE_IF_DIFF_FLOAT(dval, l->prm.stepfactor);
  dval=rounding/100.0;
  SAVE_IF_DIFF_FLOAT(dval, l->prm.rounding_amount);
  dval=turnrandom/100.0;
  SAVE_IF_DIFF_FLOAT(dval, l->prm.turn_random);
  dval=steprandom/100.0;
  SAVE_IF_DIFF_FLOAT(dval, l->prm.step_random);
  dval=initx;
  SAVE_IF_DIFF_FLOAT(dval, l->prm.initpos.x);
  dval=inity;
  SAVE_IF_DIFF_FLOAT(dval, l->prm.initpos.y);
  dval=initangle;
  SAVE_IF_DIFF_FLOAT(dval, l->prm.initpos.angle);

  if (changed || rules_changed) {
    l->dirty=TRUE;
    if (gimp_debug()) g_print("Dirty flag set.\n");
    gtk_label_get(selected_lsystem->lab, &labtext);
    if (labtext[ strlen(labtext) - 1] != '#') {
      newlabel=g_malloc(strlen(labtext) + 3);
      sprintf(newlabel, "%s-##", labtext);
      gtk_label_set(selected_lsystem->lab, newlabel);
    }
  }
}

void ls_clear_rules_dialog()
{
  gtk_clist_clear(GTK_CLIST(lsui.rules));
  gtk_clist_clear(GTK_CLIST(lsui.lrules));
  gtk_clist_clear(GTK_CLIST(lsui.srules));
  gtk_entry_set_text(lsui.axiom,"");
  gtk_entry_set_text(GTK_ENTRY(lsui.iters), "");
  gtk_entry_set_text(lsui.steplen, "");
  gtk_entry_set_text(lsui.stepfactor, "");
  gtk_entry_set_text(lsui.rounding, "");
  gtk_entry_set_text(lsui.turnrandom, "");
  gtk_entry_set_text(lsui.steprandom, "");
  gtk_entry_set_text(lsui.startx, "");
  gtk_entry_set_text(lsui.starty, "");
  gtk_entry_set_text(lsui.startangle, "");
  ls_preview_clear();
}

void ls_update_rules_dialog()
{
  LSinfo *l;
  static gint warned=0;

  l=ls_get_selected_lsystem();
  if (!l) {
    if (!warned && (loaded_lsystems==NULL || g_list_length(loaded_lsystems)==0)) {
      ls_box_error("No l-system to select!\n"
		   "Plug-in may be UNSTABLE.\n"
		   "Have you installed it properly?, Including example L-Systems?");
      warned=1;
    }
    ls_clear_rules_dialog();
    return;
  }
  ls_rules_list_create(&l->def);
  ls_srules_list_create(&l->prm);
  ls_lrules_list_create(&l->def);
  if (l->def.axiom) gtk_entry_set_text(lsui.axiom,l->def.axiom);
  else gtk_entry_set_text(lsui.axiom, "");
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(lsui.iters), l->def.iters);
  gtk_widget_set_sensitive(lsui.manual_box, ! l->prm.autoscale);
  gtk_widget_set_sensitive(lsui.auto_box, l->prm.autoscale);

  gtk_signal_handler_block_by_data(GTK_OBJECT(lsui.autoscale), NULL);
  gtk_signal_handler_block_by_data(GTK_OBJECT(lsui.autoscale), lsui.manual_box);
  gtk_signal_handler_block_by_data(GTK_OBJECT(lsui.autoscale), lsui.auto_box);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(lsui.autoscale), l->prm.autoscale);
  gtk_signal_handler_unblock_by_data(GTK_OBJECT(lsui.autoscale), NULL);
  gtk_signal_handler_unblock_by_data(GTK_OBJECT(lsui.autoscale), lsui.manual_box);
  gtk_signal_handler_unblock_by_data(GTK_OBJECT(lsui.autoscale), lsui.auto_box);

  gtk_entry_set_double(lsui.auto_cor, l->prm.as_correction*100.0);
  gtk_entry_set_double(lsui.steplen, l->prm.steplen);
  gtk_entry_set_double(lsui.stepfactor, l->prm.stepfactor*100);
  gtk_entry_set_double(lsui.rounding, l->prm.rounding_amount*100);
  gtk_entry_set_double(lsui.turnrandom, l->prm.turn_random*100);
  gtk_entry_set_double(lsui.steprandom, l->prm.step_random*100);
  gtk_entry_set_double(lsui.startx, l->prm.initpos.x);
  gtk_entry_set_double(lsui.starty, l->prm.initpos.y);
  gtk_entry_set_double(lsui.startangle, l->prm.initpos.angle);
  rules_changed=FALSE;
  ls_draw_preview();
}

LSinfo *ls_get_selected_lsystem()
{
  GtkTreeItem *item;
  GList *selected;

  selected=GTK_TREE(lsui.lst)->selection;
  if (selected==NULL) return NULL;
						
  item=GTK_TREE_ITEM(selected->data);
  if (item->subtree != NULL) return NULL;
  return gtk_object_get_user_data(GTK_OBJECT(item));
}

void ls_rules_list_create( Lsystem *what )
{
  Lsystem *l;

  l=(!what) ? &selected_lsystem->def : what; 
  if (!l) return;
  ls_rules_list_fill( l->rules, l->rulnum, GTK_CLIST(lsui.rules) );
}

void ls_lrules_list_create( Lsystem *what )
{
  Lsystem *l;

  l=(!what) ? &selected_lsystem->def : what; 
  if (!l) return;
  ls_rules_list_fill( l->lrules, l->lrulnum, GTK_CLIST(lsui.lrules) );
}

void ls_rules_list_fill(gchar * rules[], guint rulnum, GtkCList *list)
{
  gint i;
  gchar *row[3]={ NULL, "->", NULL};
  gchar *emptyrow[3]={"--", "--", "--"};
  gint row_no;

  row[0]=g_malloc0(2);
  gtk_clist_freeze(list);

  gtk_clist_clear(list);
  for (i=0; i<rulnum; i++) {
    if (rules[i]) {
      row[0][0]=rules[i][0];
      row[2]= rules[i]+2;
      row_no=gtk_clist_append(list, row);
    } else row_no=gtk_clist_append(list, emptyrow);
    gtk_clist_set_row_data(list, row_no, (gpointer)i);
  }

  gtk_clist_thaw(list);
  g_free(row[0]);
}

void ls_srules_list_create(Rparams *what)
{
  gint i, rowno;
  Rparams *l=(!what)? &selected_lsystem->prm : what;
  Special *vals=NULL;
  gchar *texts[SRULES_ENTRIES];
#ifdef CLIST_HAS_WIDGET
  gint pi;
  guchar buf[PRV_WID];
  GtkWidget *color_prv;
#endif

  for (i=0; i<SRULES_ENTRIES; i++) 
    texts[i]=g_malloc0(MAX_LINE_LENGTH);

  gtk_clist_freeze(GTK_CLIST(lsui.srules));
  gtk_clist_clear(GTK_CLIST(lsui.srules));
  for (i=0; i<l->srulnum; i++) {
    if (l->srules[i]) {
      vals=ls_parse_srule(l->srules[i]);
      texts[0][0]=*(l->srules[i]);
      strcpy(texts[1], "->");
      sprintf(texts[2], "%6.2f", vals->turn_angle);
      sprintf(texts[3], "%s", vals->brush_name);
      if (vals->brush_spc >= 0) /* -1 denotes "no change in spacing" */
	sprintf(texts[4], "%d", vals->brush_spc);
      else 
	sprintf(texts[4], "--");
      if (vals->opacity >=0 )   /* -1 denotes "no change in opacity */
	sprintf(texts[5], "%d%%", vals->opacity);
      else
	sprintf(texts[5], "--");
      if (vals->color_change) 
	sprintf(texts[6], "(%2X,%2X,%2X)", vals->color[0], vals->color[1], vals->color[2]);
      else 
	sprintf(texts[6], "--");
    } else {
      strcpy( texts[0], "--" );
      strcpy( texts[1], "--" );
      strcpy( texts[2], "--" );
      strcpy( texts[3], "--" );
      strcpy( texts[4], "--" );
      strcpy( texts[5], "--" );
    }
    rowno=gtk_clist_append(GTK_CLIST(lsui.srules), texts);
#ifdef CLIST_HAS_WIDGET
    if (vals->color_change) {
      color_prv=gtk_preview_new(GTK_PREVIEW_COLOR);
      gtk_preview_size(GTK_PREVIEW(color_prv), PRV_WID, PRV_HEI);
      for (pi=0; pi<PRV_WID; pi+=3) {
	buf[pi]=vals->color[0];
	buf[pi+1]=vals->color[1];
	buf[pi+2]=vals->color[2];
      }
      for (pi=0; pi<PRV_HEI; pi++)
	gtk_preview_draw_row(GTK_PREVIEW(color_prv), buf, 0, pi, PRV_WID);
      gtk_widget_show(color_prv);
      gtk_clist_set_widget(GTK_CLIST(lsui.srules), rowno, 5, color_prv);
    }
#endif
    gtk_clist_set_row_data(GTK_CLIST(lsui.srules), rowno, (gpointer)i);
    g_free(vals->brush_name);
    g_free(vals);
  }
  gtk_clist_thaw(GTK_CLIST(lsui.srules));
}

void dialog_update()  /* For use with dialog_create_values() */
{
}



