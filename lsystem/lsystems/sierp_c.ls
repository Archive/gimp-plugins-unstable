LSname:Fills/Sierpinski's carpet
axiom:F+XF+F+XF
<RULES>:1
rule:0:X:XF-F+F-XF+F+XF-F+F-X
</RULES>
<LRULES>:0
</LRULES>
iters:4
initpos:30.500000,108.940002,45.000000
steplen:5.223000
stepfactor:1.000000
<SRULES>:2
srule:0:+: 90:<<CURRENT>>:-1:-1:0:108,101,46
srule:1:-:-90:<<CURRENT>>:-1:-1:0:108,101,46
</SRULES>
rounding:0.000000
turn_random:0.000000
step_random:0.000000
acu_step_random:0
random_seed:0
autoscale:1
as_correction:1.000000
END.
