LSname:Fills/Sierpinski's triangle
axiom:YF
<RULES>:2
rule:0:X:YF+XF+Y
rule:1:Y:XF-YF-X
</RULES>
<LRULES>:0
</LRULES>
iters:5
initpos:20.700001,201.529999,0.000000
steplen:8.127000
stepfactor:1.000000
<SRULES>:2
srule:0:+: 60:<<CURRENT>>:-1:-1:0:108,101,46
srule:1:-:-60:<<CURRENT>>:-1:-1:0:108,101,46
</SRULES>
rounding:0.000000
turn_random:0.000000
step_random:0.000000
acu_step_random:0
random_seed:0
autoscale:1
as_correction:1.000000
END.
