/* Header file for gdebug.c */

#ifndef _GDEBUG_H
#define _GDEBUG_H

extern gint gimp_debug_get_state(gchar *);

#define gimp_debug() gimp_debug_get_state(NULL)

#endif
