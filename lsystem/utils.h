/* 
   utils.h
   for L-Systems Plug-In
   Michal Gomulinski
	$Id$	 
*/

#ifndef UTILS_H
#define UTILS_H

#include<gtk/gtk.h>
#include"lsystem.h"

void ls_clist_add_empty_rule(GtkCList *, gint);
void ls_clist_remove_selected(GtkCList *);
void ls_clist_update_selected_rule(GtkCList *, gchar *);
void ls_clist_update_selected_srule(GtkCList *, Special *);
gpointer ls_clist_get_selected_row_data(GtkCList *);
void add_button(GtkWidget *, GtkWidget *, GtkSignalFunc);
void add_button_d(GtkWidget *, GtkWidget *, GtkSignalFunc, gpointer);

void ls_box_error(gchar *);
gint ls_check_lsystem_name(gchar *);
gint ls_check_lsystem_filename(gchar *);

GtkWidget *ls_create_lsystem_subtree(gchar *, gchar *, gint);
void ls_tree_add_lsystem(LSinfo *);

#define DUPLICATE 0
#define OK 1

#endif
