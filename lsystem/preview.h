/* (c) 1998 by Michal Gomulinski */
/* Routines connected with preview manipulation */
#ifndef PREVIEW_H
#define PREVIEW_H

/* $Id$ */

extern gint prv_wid;
extern gint prv_hei;

GtkWidget *ls_preview_new();
void ls_draw_preview();
void ls_preview_clear();

#endif
