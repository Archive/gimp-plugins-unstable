/*
  L-System PLug-in
  GUI Callbacks
  Michal Gomulinski
*/

/* 	$Id$	 */

#ifndef lint
static char vcid[] = "$Id$";
#endif /* lint */


#include<gtk/gtk.h>
#include<string.h>
#include<libgimp/gimp.h>
#include<megawidget/megawidget.h>
#include<time.h>
#include<unistd.h>
#include"preview.h"
#include"utils.h"
#include"lsystem.h"
#include"lsystui.h"
#include"scales.h"
#include"callbacks.h"
#include"gdebug.h"

gdouble color[3];
gint edit_rule_dialog=FALSE;
gint edit_lrule_dialog=FALSE;
gint edit_srule_dialog=FALSE;


static GtkWidget *ls_rule_edit_new(gchar **);
static GtkWidget *ls_newname_new(LSinfo *);
static void     ls_rule_edit_ok_callback(GtkWidget *, gpointer);
static void     ls_rule_edit_cancel_callback(GtkWidget *, gpointer);
static GtkWidget *ls_lrule_edit_new(gchar **);
static void     ls_lrule_edit_ok_callback(GtkWidget *, gpointer);
static void     ls_lrule_edit_cancel_callback(GtkWidget *, gpointer);
static GtkWidget *ls_srule_edit_new(gchar **);
static void     ls_srule_edit_ok_callback(GtkWidget *, gpointer);
static void     ls_srule_edit_cancel_callback(GtkWidget *, gpointer);
static void     ls_newname_ok_callback(GtkWidget *, LSinfo *);
static void     ls_newname_cancel_callback(GtkWidget *, LSinfo *);

/* Dummy function to disable warning about vcid */
void callbacks_no_warning()
{
  (void)vcid;
}

void ls_update_main_dialog_callback(GtkWidget *w, gpointer p)
{
  if (selected_lsystem) {
    ls_grab_values();
  }
  selected_lsystem=ls_get_selected_lsystem();
  ls_update_rules_dialog();
  if (!selected_lsystem) {
    gtk_widget_set_sensitive(lsui.ntb, FALSE);
    gtk_widget_set_sensitive(lsui.lsrm, FALSE);
    gtk_widget_set_sensitive(lsui.lscp, FALSE);
    gtk_widget_set_sensitive(lsui.lsmv, FALSE);
  } else {
    gtk_widget_set_sensitive(lsui.ntb, TRUE);
    gtk_widget_set_sensitive(lsui.lsrm, TRUE);
    gtk_widget_set_sensitive(lsui.lscp, TRUE);
    gtk_widget_set_sensitive(lsui.lsmv, TRUE);
  }
}


/*
***************************************************************
**************** L-Systems operations
*/

void ls_copy_callback(GtkWidget *w, gpointer o)
{
  LSinfo *src, *trg;
  
  if (!selected_lsystem) return;
  trg=g_malloc0(sizeof(LSinfo));
  src=selected_lsystem;
  ls_lsystem_copy(trg, src);
  selected_lsystem=trg;
  ls_newname_new(trg);
}

void ls_new_callback(GtkWidget *w, gpointer o)
{
  LSinfo *new_lsystem=ls_lsystem_new();
  GtkWidget *dlg;
  gchar *path_end, *tmp;
  gint path_len;

  if (selected_lsystem) {
    path_end=strrchr(selected_lsystem->def.name, '/');
    path_len=path_end - selected_lsystem->def.name + 1;
    tmp=g_malloc(sizeof(gchar)*(path_len + 1));
    strncpy(tmp, selected_lsystem->def.name, path_len);
    tmp[path_len]=0;
    if (new_lsystem->def.name) g_free(new_lsystem->def.name);
    new_lsystem->def.name=tmp;
  }

  selected_lsystem=new_lsystem;

  dlg=ls_newname_new(new_lsystem);
}

void ls_rename_callback(GtkWidget *w, gpointer o)
{
  /*  LSinfo *new; */

  if (!selected_lsystem) return;
  ls_newname_new( selected_lsystem );
}

static GtkWidget *ls_newname_new(LSinfo* lsystem)
{
  GtkDialog *wind;
  GtkWidget *wgt, *e1, *e2, *b1;
  GtkTable *tbl;
  gchar **oldname=&lsystem->def.name, 
    **oldfname=&lsystem->def.fname;

  wind=GTK_DIALOG(gtk_dialog_new());
  gtk_window_set_title(&wind->window, "New Name");
  tbl=GTK_TABLE(gtk_table_new(2,2, FALSE));
  gtk_widget_show(GTK_WIDGET(tbl));
  gtk_box_pack_start(GTK_BOX(wind->vbox), GTK_WIDGET(tbl), TRUE, TRUE, 0);
  wgt=gtk_label_new("Enter new L-System name:"); gtk_widget_show(wgt);
  gtk_table_attach(tbl, wgt, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 4);
  e1=gtk_entry_new(); gtk_widget_show(e1);
  gtk_table_attach(tbl, e1, 1, 2, 0, 1, GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 4);
  if (*oldname) gtk_entry_set_text(GTK_ENTRY(e1), *oldname);
  wgt=gtk_label_new("File name:"); gtk_widget_show(wgt);
  gtk_table_attach(tbl, wgt, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 4);
  e2=gtk_entry_new(); gtk_widget_show(e2);
  gtk_table_attach(tbl, e2, 1, 2, 1, 2, GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 4);
  if (*oldfname) gtk_entry_set_text(GTK_ENTRY(e2), *oldfname);
  gtk_object_set_data(GTK_OBJECT(wind), ENTRY1, e1);
  gtk_object_set_data(GTK_OBJECT(wind), ENTRY2, e2);
  gtk_object_set_user_data(GTK_OBJECT(e1), oldname);
  gtk_object_set_user_data(GTK_OBJECT(e2), oldfname);
  b1=gtk_button_new_with_label("OK"); 
  
  gtk_object_set_user_data(GTK_OBJECT(b1), wind);
  GTK_WIDGET_SET_FLAGS( b1, GTK_CAN_DEFAULT );
  add_button_d(wind->action_area, b1, GTK_SIGNAL_FUNC(ls_newname_ok_callback), lsystem);
  wgt=gtk_button_new_with_label("Cancel"); 
  
  gtk_object_set_user_data(GTK_OBJECT(wgt), wind);
  GTK_WIDGET_SET_FLAGS( wgt, GTK_CAN_DEFAULT );
  add_button_d(wind->action_area, wgt, GTK_SIGNAL_FUNC(ls_newname_cancel_callback), lsystem);
  gtk_signal_connect(GTK_OBJECT(wind), "destroy", GTK_SIGNAL_FUNC(ls_univ_close), wind);
  gtk_widget_grab_default(b1);
  gtk_widget_show(GTK_WIDGET(wind));
  return GTK_WIDGET(wind);
}

static void ls_newname_ok_callback(GtkWidget *w, LSinfo *lsystem)
{
  GtkWidget *wind,
    *entry1, *entry2;
  gchar **name, **fname;
  GtkWidget *tree, *treeitem;

  if (!w || !lsystem) return;
  wind=gtk_object_get_user_data(GTK_OBJECT(w));
  if (!wind) return;
  entry1=gtk_object_get_data(GTK_OBJECT(wind), ENTRY1);
  entry2=gtk_object_get_data(GTK_OBJECT(wind), ENTRY2);
  if (!entry1 || !entry2) return;
  if (ls_check_lsystem_name( gtk_entry_get_text(GTK_ENTRY(entry1)) )==DUPLICATE) {
    ls_box_error("The name you have just entered \n"
		 "is already used for some other L-System.");
    return; 
  }
  if (!lsystem->lab &&  /* If renaming this doesn't matter */
      ls_check_lsystem_filename( gtk_entry_get_text(GTK_ENTRY(entry2)) )==DUPLICATE) {
    ls_box_error("The file name you have just enetered\n"
		 "is already used for storing some other L-System.\n"
		 "Please select different one.");
    return;
  }
  name=gtk_object_get_user_data(GTK_OBJECT(entry1));
  fname=gtk_object_get_user_data(GTK_OBJECT(entry2));
  if (*name) g_free(*name);
  if (*fname) g_free(*fname);
  (*name)=g_strdup(gtk_entry_get_text(GTK_ENTRY(entry1)));
  (*fname)=g_strdup(gtk_entry_get_text(GTK_ENTRY(entry2)));

  (*fname)=ls_make_full_filename(*fname);
  if (**name== 0) {
    ls_box_error("You have forgotten to fill the NAME entry.\n"
		 "Please try again.");
    return; 
  }
  if (**fname == 0) {
    ls_box_error("You have forgotten to fill the FILE NAME entry.\n"
		 "Please try again.");
    return;
  }
  
  loaded_lsystems=g_list_insert_sorted(loaded_lsystems, lsystem, (GCompareFunc)lscmp);
  if (lsystem->lab) { /* If renaming ... */
    treeitem=GTK_WIDGET(lsystem->lab)->parent;
    tree=GTK_WIDGET(treeitem)->parent;
    gtk_container_remove(GTK_CONTAINER(tree), treeitem);
  }
  lsystem->dirty=TRUE;
  ls_tree_add_lsystem(lsystem);
  gtk_widget_hide(wind);
  gtk_widget_destroy(wind);
}

static void ls_newname_cancel_callback(GtkWidget *w, LSinfo *lsystem)
{
  GtkWidget *window;

  if (!w) return;
  window=gtk_object_get_user_data(GTK_OBJECT(w));
  if (window) {
    gtk_widget_hide(window);
    gtk_widget_destroy(window);
  }
  if (lsystem) lsinfo_free(lsystem);
  /* ls_create_lsystems_list(); */
}

void ls_reread_callback(GtkWidget *w, gpointer o)
{
}

void ls_delete_callback(GtkWidget *w, gpointer o)
{
  GtkWidget *tree, *treeitem;
  LSinfo *for_deletion=selected_lsystem;

  if (!selected_lsystem) return;
  loaded_lsystems=g_list_remove(loaded_lsystems, selected_lsystem);
  treeitem=GTK_WIDGET(selected_lsystem->lab)->parent;
  tree=GTK_WIDGET(treeitem)->parent;
  gtk_container_remove(GTK_CONTAINER(tree), treeitem);
  unlink(for_deletion->def.fname);

  lsinfo_free(for_deletion);
  /* ls_create_lsystems_list(); */
}


/*
********************************************************************
****************** Generic rule list manipulations
*/

static GtkWidget *ls_generic_rule_edit_new(gchar **rstr, GtkSignalFunc ok, GtkSignalFunc cancel)
{
  GtkDialog *ret;
  GtkWidget *wgt, *box, *e1, *e2, *b1;
  GtkTable *tbl;
  gchar buf[2]="";


  gtk_widget_set_sensitive(lsui.lst, FALSE);
  ret=GTK_DIALOG(gtk_dialog_new());
  gtk_object_set_data(GTK_OBJECT(ret), RULE, rstr);
  gtk_object_set_user_data(GTK_OBJECT(ret), ret);
  gtk_window_set_title(&ret->window, "Edit rule");
  tbl=GTK_TABLE(gtk_table_new(2,2, FALSE));
  gtk_widget_show(GTK_WIDGET(tbl));
  gtk_box_pack_start(GTK_BOX(ret->vbox), GTK_WIDGET(tbl), TRUE, TRUE, 0);
  wgt=gtk_label_new("Substituted character:"); gtk_widget_show(wgt);
  gtk_table_attach(tbl, wgt, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 4);
  e1=gtk_entry_new(); gtk_widget_show(e1);
  gtk_widget_set_usize(e1, 30, 0);
  box=gtk_hbox_new(FALSE, 0); gtk_widget_show(box);
  gtk_box_pack_start(GTK_BOX(box), e1, FALSE, FALSE, 0);
  gtk_table_attach(tbl, box, 1, 2, 0, 1, GTK_FILL | GTK_EXPAND, GTK_FILL, 0, 4);
  gtk_object_set_data(GTK_OBJECT(ret), LEFT_ENTRY, e1);
  wgt=gtk_label_new("Substituting string:"); gtk_widget_show(wgt);
  gtk_table_attach(tbl, wgt, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 4);
  e2=gtk_entry_new(); gtk_widget_show(e2);
  gtk_widget_set_usize(e2, 200, 0);
  box=gtk_hbox_new(FALSE, 0); gtk_widget_show(box);
  gtk_box_pack_start(GTK_BOX(box), e2, TRUE, TRUE, 0);
  gtk_table_attach(tbl, box, 1, 2, 1, 2, GTK_FILL | GTK_EXPAND, GTK_FILL, 0, 4);
  gtk_object_set_data(GTK_OBJECT(ret), RIGHT_ENTRY, e2);

  if (rstr && (*rstr)) {
    buf[0]=(*rstr)[0];
    buf[1]=0;
    gtk_entry_set_text(GTK_ENTRY(e1), buf);
    gtk_entry_set_text(GTK_ENTRY(e2), &(*rstr)[2]);
  }
  
  wgt=gtk_button_new_with_label("OK"); 
  
  gtk_object_set_user_data(GTK_OBJECT(wgt), ret);
  GTK_WIDGET_SET_FLAGS( wgt, GTK_CAN_DEFAULT );
  add_button(ret->action_area, wgt, ok);
  b1=wgt;
  wgt=gtk_button_new_with_label("Cancel"); 
  
  GTK_WIDGET_SET_FLAGS( wgt, GTK_CAN_DEFAULT );
  gtk_object_set_user_data(GTK_OBJECT(wgt), ret);
  add_button(ret->action_area, wgt, cancel);
  gtk_signal_connect(GTK_OBJECT(ret), "destroy", cancel, NULL);
  gtk_widget_grab_default(b1);
  return GTK_WIDGET(ret);
}

static void ls_generic_rule_edit_ok_callback(GtkWidget *w, gchar **rules, 
				      gint *rulcount, GtkWidget *rullist)
{
  GtkWidget *edit_dialog=gtk_object_get_user_data(GTK_OBJECT(w));
  GtkEntry *chr=gtk_object_get_data(GTK_OBJECT(edit_dialog), LEFT_ENTRY);
  GtkEntry *str=gtk_object_get_data(GTK_OBJECT(edit_dialog), RIGHT_ENTRY);
  gchar **rule=gtk_object_get_data(GTK_OBJECT(edit_dialog), RULE);
  gchar *left=gtk_entry_get_text(chr),
        *right=gtk_entry_get_text(str);
  gchar *buf=g_malloc0(3+strlen(right));

  gtk_widget_set_sensitive(lsui.lst, TRUE);
  if (strlen(left)==0) {
    if ( (*rule) == NULL) { /* Remove rule which was about to be added */
      (*rulcount)--; /* Rules added on the end of the list */
      rules=g_realloc(rules, (*rulcount) * sizeof(gchar *));
    }
    gtk_widget_hide(edit_dialog);
    ls_clist_remove_selected(GTK_CLIST(rullist));
    gtk_widget_set_sensitive(rullist, TRUE);
    return;
  }
  buf[0]=left[0];
  buf[1]=':';
  strcat(buf, right);
  if (*rule) {
    g_free(*rule);
    (*rule)=buf;
  } else (*rule)=buf;
  rules_changed=TRUE;

  gtk_widget_hide(edit_dialog);
  /*  gtk_widget_destroy(edit_dialog); */
  ls_clist_update_selected_rule(GTK_CLIST(rullist), *rule);
  gtk_widget_set_sensitive(rullist, TRUE);
}

static void ls_generic_rule_edit_cancel_callback(GtkWidget *w, gchar **rules, 
				  gint *rulcount, GtkWidget *rullist)
{
  GtkWidget *edit_dialog=gtk_object_get_user_data(GTK_OBJECT(w));
  gchar **rule=gtk_object_get_data(GTK_OBJECT(edit_dialog), RULE);


  if (! *rule) { /* Remove rule which was about to be added */
    (*rulcount)--; /* Rules added on the end of the list */
    ls_clist_remove_selected(GTK_CLIST(rullist));
  }
  gtk_widget_hide(edit_dialog);
  gtk_widget_set_sensitive(rullist, TRUE);
  gtk_widget_set_sensitive(lsui.lst, TRUE);
}

/*
********************************************************************
******************RULES callbacks
*/

void ls_add_rule_callback(GtkWidget *w, gpointer o)
{
  GtkWidget *wgt;

  if (edit_rule_dialog) return;
  if (!selected_lsystem) return;
  selected_lsystem->def.rules=g_realloc(selected_lsystem->def.rules, 
				 (selected_lsystem->def.rulnum+1)*sizeof(gchar *));
  selected_lsystem->def.rules[(selected_lsystem->def.rulnum)++]=NULL;
  ls_clist_add_empty_rule(GTK_CLIST(lsui.rules), selected_lsystem->def.rulnum-1);
  gtk_widget_set_sensitive(lsui.rules, FALSE);
  wgt=ls_rule_edit_new( &selected_lsystem->def.rules[selected_lsystem->def.rulnum-1] );
  gtk_widget_show(wgt);
}

void ls_edit_rule_callback(GtkWidget *w, gpointer o)
{
  GtkWidget *wgt;
  gchar **cur_rule;
  gint cur_rule_no;

  if (edit_rule_dialog) return;
  if (GTK_CLIST(lsui.rules)->selection==NULL) return;
  cur_rule_no=(gint) ls_clist_get_selected_row_data( GTK_CLIST(lsui.rules) );
  cur_rule= &selected_lsystem->def.rules[cur_rule_no];
  gtk_widget_set_sensitive(lsui.rules, FALSE);
  wgt=ls_rule_edit_new( cur_rule );
  gtk_widget_show(wgt);
}

void ls_remove_rule_callback(GtkWidget *w, gpointer o)
{
  gint i;
  gchar **rules=selected_lsystem->def.rules;
  gint cur_rule_no;

  if (edit_rule_dialog) return;
  if (GTK_CLIST(lsui.rules)->selection == NULL) return;
  cur_rule_no=(gint) ls_clist_get_selected_row_data( GTK_CLIST(lsui.rules) );

  for (i=cur_rule_no+1; i<selected_lsystem->def.rulnum; i++)
    rules[i-1]=rules[i];
  selected_lsystem->def.rulnum--;
  ls_clist_remove_selected(GTK_CLIST(lsui.rules));
  rules_changed=TRUE;
}

static GtkWidget *ls_rule_edit_new(gchar **rstr)
{
  edit_rule_dialog=TRUE;
  gtk_widget_set_sensitive(lsui.ruleadd, FALSE);
  gtk_widget_set_sensitive(lsui.ruleedit, FALSE);
  gtk_widget_set_sensitive(lsui.rulerem, FALSE);  
  return ls_generic_rule_edit_new( rstr, GTK_SIGNAL_FUNC(ls_rule_edit_ok_callback), 
				   GTK_SIGNAL_FUNC(ls_rule_edit_cancel_callback));
} 

static void ls_rule_edit_ok_callback(GtkWidget *w, gpointer o)
{
  edit_rule_dialog=FALSE;
  gtk_widget_set_sensitive(lsui.ruleadd, TRUE);
  gtk_widget_set_sensitive(lsui.ruleedit, TRUE);
  gtk_widget_set_sensitive(lsui.rulerem, TRUE);
  ls_generic_rule_edit_ok_callback(w, selected_lsystem->def.rules, 
				   &selected_lsystem->def.rulnum,
				   lsui.rules);
}

static void ls_rule_edit_cancel_callback(GtkWidget *w, gpointer o)
{
  edit_rule_dialog=FALSE;
  gtk_widget_set_sensitive(lsui.ruleadd, TRUE);
  gtk_widget_set_sensitive(lsui.ruleedit, TRUE);
  gtk_widget_set_sensitive(lsui.rulerem, TRUE);
  ls_generic_rule_edit_cancel_callback(w, selected_lsystem->def.rules, 
				       &selected_lsystem->def.rulnum,
				       lsui.rules);
}


/*
*********************************************************************
************************ LRULES callbacks
*/

void ls_add_lrule_callback(GtkWidget *w, gpointer o)
{
  GtkWidget *wgt;

  if (edit_lrule_dialog) return;
  if (!selected_lsystem) return;
  selected_lsystem->def.lrules=g_realloc(selected_lsystem->def.lrules, 
				 (selected_lsystem->def.lrulnum+1)*sizeof(gchar *));
  selected_lsystem->def.lrules[(selected_lsystem->def.lrulnum)++]=NULL;
  ls_clist_add_empty_rule(GTK_CLIST(lsui.lrules), selected_lsystem->def.lrulnum-1);
  gtk_widget_set_sensitive(lsui.lrules, FALSE);
  wgt=ls_lrule_edit_new(&selected_lsystem->def.lrules[selected_lsystem->def.lrulnum-1] );
  gtk_widget_show(wgt);
}

void ls_edit_lrule_callback(GtkWidget *w, gpointer o)
{
  GtkWidget *wgt;
  gchar **cur_rule;
  gint cur_rule_no;

  if (edit_lrule_dialog) return;
  if (GTK_CLIST(lsui.lrules)->selection == NULL) return;
  cur_rule_no=(gint) ls_clist_get_selected_row_data(GTK_CLIST(lsui.lrules));
  cur_rule= &selected_lsystem->def.lrules[cur_rule_no];
  gtk_widget_set_sensitive(lsui.lrules, FALSE);
  wgt=ls_lrule_edit_new( cur_rule );
  gtk_widget_show(wgt);
}

void ls_remove_lrule_callback(GtkWidget *w, gpointer o)
{
  gint i;
  gchar **rules=selected_lsystem->def.lrules;
  gint cur_rule_no;

  if (edit_lrule_dialog) return;
  if (GTK_CLIST(lsui.lrules)->selection == NULL) return;
  cur_rule_no=(gint) ls_clist_get_selected_row_data(GTK_CLIST(lsui.lrules));
  for (i=cur_rule_no+1; i<selected_lsystem->def.lrulnum; i++)
    rules[i-1]=rules[i];
  selected_lsystem->def.lrulnum--;
  ls_clist_remove_selected(GTK_CLIST(lsui.lrules));
  rules_changed=TRUE;
}

static GtkWidget *ls_lrule_edit_new(gchar **rstr)
{
  edit_lrule_dialog=TRUE;
  gtk_widget_set_sensitive(lsui.lruleadd, FALSE);
  gtk_widget_set_sensitive(lsui.lruleedit, FALSE);
  gtk_widget_set_sensitive(lsui.lrulerem, FALSE);  
  return ls_generic_rule_edit_new( rstr, GTK_SIGNAL_FUNC(ls_lrule_edit_ok_callback), 
				   GTK_SIGNAL_FUNC(ls_lrule_edit_cancel_callback));
} 

static void ls_lrule_edit_ok_callback(GtkWidget *w, gpointer o)
{
  edit_lrule_dialog=FALSE;
  gtk_widget_set_sensitive(lsui.lruleadd, TRUE);
  gtk_widget_set_sensitive(lsui.lruleedit, TRUE);
  gtk_widget_set_sensitive(lsui.lrulerem, TRUE);
  ls_generic_rule_edit_ok_callback(w, selected_lsystem->def.lrules, 
				   &selected_lsystem->def.lrulnum,
				   lsui.lrules);
}

static void ls_lrule_edit_cancel_callback(GtkWidget *w, gpointer o)
{
  edit_lrule_dialog=FALSE;
  gtk_widget_set_sensitive(lsui.lruleadd, TRUE);
  gtk_widget_set_sensitive(lsui.lruleedit, TRUE);
  gtk_widget_set_sensitive(lsui.lrulerem, TRUE);
  ls_generic_rule_edit_cancel_callback(w, selected_lsystem->def.lrules, 
				       &selected_lsystem->def.lrulnum,
				       lsui.lrules);
}

/*
*******************************************************************
************************** SRULES callbacks
*/

void ls_add_srule_callback(GtkWidget *w, gpointer o)
{  
  GtkWidget *wgt;
  if (edit_srule_dialog) return;

  selected_lsystem->prm.srules=g_realloc(selected_lsystem->prm.srules, 
				 (selected_lsystem->prm.srulnum+1)*sizeof(gchar *));
  selected_lsystem->prm.srules[(selected_lsystem->prm.srulnum)++]=NULL;
  ls_clist_add_empty_rule(GTK_CLIST(lsui.srules), selected_lsystem->prm.srulnum-1);
  wgt=ls_srule_edit_new(&selected_lsystem->prm.srules[selected_lsystem->prm.srulnum-1] );
  gtk_widget_show(wgt);
}

void ls_remove_srule_callback(GtkWidget *w, gpointer o)
{
  gint i;
  gchar **rules=selected_lsystem->prm.srules;
  gint cur_rule_no;

  if (edit_srule_dialog) return;
  if (GTK_CLIST(lsui.srules)->selection == NULL) return;
  cur_rule_no=(gint)ls_clist_get_selected_row_data( GTK_CLIST(lsui.srules) );
  for (i=cur_rule_no+1; i<selected_lsystem->prm.srulnum; i++)
    rules[i-1]=rules[i];
  selected_lsystem->prm.srulnum--;
  ls_clist_remove_selected(GTK_CLIST(lsui.srules));
  rules_changed=TRUE;
}

void ls_edit_srule_callback(GtkWidget *w, gpointer o)
{
  GtkWidget *wgt;
  gchar **srule;
  gint srule_no;

  if (edit_srule_dialog) return;
  srule_no=(gint)ls_clist_get_selected_row_data(GTK_CLIST(lsui.srules));
  srule=selected_lsystem->prm.srules+srule_no;
  wgt=ls_srule_edit_new(srule);
  gtk_widget_show(wgt);
}


static GtkWidget *ls_srule_edit_new(gchar **srulestr)
{
  GtkDialog *ret;
  GtkTable *tbl;
  GtkWidget *wgt, *box, *brmenu, *mitem, *omenu, 
    *tbl2, *b1, *b2, *selbrush, 
    *encolor, *enspc, *enopacity,
    *tspc, *topacity, *tcolor;
  GtkEntry *e1, *e2, *e3, *e4;
  GList *br;
  Special *vals=NULL;

  edit_srule_dialog=TRUE;
  gtk_widget_set_sensitive(lsui.sruleadd, FALSE);
  gtk_widget_set_sensitive(lsui.sruleedit, FALSE);
  gtk_widget_set_sensitive(lsui.srulerem, FALSE);
  gtk_widget_set_sensitive(lsui.srules, FALSE);
  gtk_widget_set_sensitive(lsui.lst, FALSE);
  ret=GTK_DIALOG(gtk_dialog_new());
  gtk_window_set_title(&ret->window, "Edit a SPECIAL rule");
  tbl=GTK_TABLE(gtk_table_new(7,2, FALSE));
  gtk_widget_show(GTK_WIDGET(tbl));
  gtk_container_border_width(GTK_CONTAINER(&ret->window), 5);
  gtk_box_pack_start(GTK_BOX(ret->vbox), GTK_WIDGET(tbl), TRUE, TRUE, 0);
  wgt=gtk_label_new("Active character:"); gtk_widget_show(wgt);
  gtk_table_attach(tbl, wgt, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 4);
  e1=GTK_ENTRY(gtk_entry_new()); gtk_widget_show(GTK_WIDGET(e1));
  gtk_widget_set_usize(GTK_WIDGET(e1), 30, 0);
  box=gtk_hbox_new(FALSE, 0); gtk_widget_show(box);
  gtk_box_pack_start(GTK_BOX(box), GTK_WIDGET(e1), FALSE, FALSE, 0);
  gtk_table_attach(tbl, box, 1, 2, 0, 1, GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 4);

  if (*srulestr) vals=ls_parse_srule(*srulestr);

  brmenu=gtk_list_new(); gtk_widget_show(brmenu);
  br=brushes;
  selbrush=NULL;
  while(br) {
    mitem=gtk_list_item_new_with_label(br->data);
    gtk_widget_show(mitem);
    gtk_object_set_user_data(GTK_OBJECT(mitem), br->data);
    gtk_container_add(GTK_CONTAINER(brmenu), mitem);
    if (*srulestr) 
      if (strcmp(br->data, vals->brush_name)==0) selbrush=mitem;
    br=br->next;
  }
  wgt=gtk_label_new("Brush:"); gtk_widget_show(wgt);
  gtk_table_attach(tbl, wgt, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 4);
  omenu=gtk_scrolled_window_new(NULL, NULL); gtk_widget_show(omenu);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(omenu), 
		   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_widget_set_usize(omenu, 200, 100);
  gtk_container_add(GTK_CONTAINER(omenu), brmenu);
  gtk_table_attach(tbl, omenu, 1, 2, 1, 2, GTK_FILL | GTK_EXPAND, 
		   GTK_FILL|GTK_EXPAND, 0, 4);      


  tbl2=gtk_table_new(3,5, FALSE); gtk_widget_show(tbl2);
  gtk_container_border_width(GTK_CONTAINER(tbl2), 5);
  gtk_table_attach(tbl, tbl2, 0, 2, 2, 5, GTK_FILL | GTK_EXPAND, 
		   GTK_FILL, 0, 4);

  tcolor=gtk_table_new(1, 3, TRUE); gtk_widget_show(tcolor);
  gtk_table_attach(GTK_TABLE(tbl2), tcolor, 0, 3, 0, 1, GTK_FILL|GTK_EXPAND, GTK_FILL, 0 ,0); 
  wgt=gtk_label_new("Drawing color:"); gtk_widget_show(wgt);
  gtk_table_attach(GTK_TABLE(tcolor), wgt, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);  
  if (*srulestr) {
      color[0]=(double)vals->color[0]/255.999;
      color[1]=(double)vals->color[1]/255.999;
      color[2]=(double)vals->color[2]/255.999;
      bropacity=vals->opacity;
  } else bropacity=0;
  wgt=gtk_hbox_new(FALSE, 0); gtk_widget_show(wgt);
  gtk_table_attach(GTK_TABLE(tcolor), wgt, 1, 2, 0, 1, GTK_FILL, GTK_FILL, 10, 0);
  mw_color_select_button_create(wgt, "Drawing color", color, 0);
  encolor=gtk_check_button_new();
  gtk_widget_set_sensitive(tcolor, FALSE);
  gtk_widget_show(encolor);
  gtk_table_attach(GTK_TABLE(tbl2), encolor, 3, 4, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
  gtk_signal_connect(GTK_OBJECT(encolor), "toggled", 
		     GTK_SIGNAL_FUNC(ls_toggle_widget), tcolor);

  /* Every input slider/entry pair should go in its own table
     this is to allow enabling/disabling */

  tspc=gtk_table_new(1, 3, TRUE); gtk_widget_show(tspc);
  e2=dialog_create_value("Brush spacing:",
			 GTK_TABLE(tspc), 0, &brspacing, 0, 1000, 1, 70, 100);
  gtk_table_attach(GTK_TABLE(tbl2), tspc, 0, 3, 1, 2, GTK_FILL|GTK_EXPAND, GTK_FILL, 0 ,0); 
  enspc=gtk_check_button_new();  gtk_widget_show(enspc);
  gtk_widget_set_sensitive(tspc, FALSE);
  gtk_table_attach(GTK_TABLE(tbl2), enspc, 3, 4, 1, 2, GTK_FILL, GTK_FILL, 0 ,0); 
  gtk_signal_connect(GTK_OBJECT(enspc), "toggled", 
		     GTK_SIGNAL_FUNC(ls_toggle_widget), tspc);

  topacity=gtk_table_new(1, 3, TRUE); gtk_widget_show(topacity);
  e3=dialog_create_value("Opacity:",
			 GTK_TABLE(topacity), 0, &bropacity, 0, 100, 1, 70, 100);
  gtk_table_attach(GTK_TABLE(tbl2), topacity, 0, 3, 2, 3, GTK_FILL|GTK_EXPAND, GTK_FILL, 0 ,0); 
  enopacity=gtk_check_button_new(); gtk_widget_show(enopacity);
  gtk_widget_set_sensitive(topacity, FALSE);
  gtk_table_attach(GTK_TABLE(tbl2), enopacity, 3, 4, 2, 3, GTK_FILL, GTK_FILL, 0 ,0); 
  gtk_signal_connect(GTK_OBJECT(enopacity), "toggled", 
		     GTK_SIGNAL_FUNC(ls_toggle_widget), topacity);

  wgt=gtk_table_new(1, 3, TRUE); gtk_widget_show(wgt);
  e4=dialog_create_value("Turn angle:",
			 GTK_TABLE(wgt), 0, &turn_angle, 
			 -180, 180, 1, 70, 100);
  gtk_table_attach(GTK_TABLE(tbl2), wgt, 0, 3, 3, 4, GTK_FILL|GTK_EXPAND, GTK_FILL, 0 ,0); 

  b1=gtk_button_new_with_label("OK");
  GTK_WIDGET_SET_FLAGS(b1, GTK_CAN_DEFAULT);
  gtk_object_set_user_data(GTK_OBJECT(b1), ret);
  add_button(ret->action_area, b1, GTK_SIGNAL_FUNC(ls_srule_edit_ok_callback));
  b2=gtk_button_new_with_label("Cancel");
  GTK_WIDGET_SET_FLAGS(b2, GTK_CAN_DEFAULT);
  gtk_object_set_user_data(GTK_OBJECT(b2), ret);
  add_button(ret->action_area, b2, 
	     GTK_SIGNAL_FUNC(ls_srule_edit_cancel_callback));
  gtk_widget_grab_default(b1);
  if (*srulestr) {
    (*srulestr)[1]=0;
    gtk_entry_set_text(e1, (*srulestr));
    (*srulestr)[1]=':';
    if (vals->brush_spc >= 0) {
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(enspc), TRUE);
      gtk_entry_set_int(e2, vals->brush_spc);
    } else
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(enspc), FALSE);
    if (vals->opacity >=0) {
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(enopacity), TRUE);
      gtk_entry_set_int(e3, vals->opacity);
    } else
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(enopacity), FALSE);
    gtk_entry_set_int(e4, vals->turn_angle);
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(encolor), 
				vals->color_change);
    if (selbrush) gtk_list_select_child(GTK_LIST(brmenu), selbrush);
    g_free(vals->brush_name);
    g_free(vals);
  }
  gtk_object_set_data(GTK_OBJECT(ret), LEFT_ENTRY, e1);
  gtk_object_set_data(GTK_OBJECT(ret), ANGLE, e4);
  gtk_object_set_data(GTK_OBJECT(ret), SPACING, e2);
  gtk_object_set_data(GTK_OBJECT(ret), OPACITY, e3);
  gtk_object_set_data(GTK_OBJECT(ret), BRUSH, brmenu);
  gtk_object_set_data(GTK_OBJECT(ret), RULE, srulestr);
  gtk_object_set_data(GTK_OBJECT(ret), COLOR_ENABLED, encolor);
  gtk_object_set_data(GTK_OBJECT(ret), SPC_ENABLED, enspc);
  gtk_object_set_data(GTK_OBJECT(ret), OPACITY_ENABLED, enopacity);

  return GTK_WIDGET(ret);
}

static void ls_srule_edit_ok_callback(GtkWidget *w, gpointer o)
{
  GtkWidget *wind, *chrentry, *spcentry, *opcentry, 
    *brchoice, *encolor, *enopacity, *enspc;
  gchar **srule;
  GtkObject *obj;
  gchar *s;
  gchar buffer[MAX_LINE_LENGTH];
  gint clr_change;
  Special *parsed_srule;
  
  edit_srule_dialog=FALSE;
  gtk_widget_set_sensitive(lsui.sruleadd, TRUE);
  gtk_widget_set_sensitive(lsui.sruleedit, TRUE);
  gtk_widget_set_sensitive(lsui.srulerem, TRUE);
  if (!w) return;
  wind=gtk_object_get_user_data(GTK_OBJECT(w));
  if (!wind) return;
  srule=gtk_object_get_data(GTK_OBJECT(wind), RULE);
  chrentry=gtk_object_get_data(GTK_OBJECT(wind), LEFT_ENTRY);
  spcentry=gtk_object_get_data(GTK_OBJECT(wind), SPACING);
  opcentry=gtk_object_get_data(GTK_OBJECT(wind), OPACITY);
  brchoice=gtk_object_get_data(GTK_OBJECT(wind), BRUSH);
  encolor=gtk_object_get_data(GTK_OBJECT(wind), COLOR_ENABLED);
  enspc=gtk_object_get_data(GTK_OBJECT(wind), SPC_ENABLED);
  enopacity=gtk_object_get_data(GTK_OBJECT(wind), OPACITY_ENABLED);

  s=gtk_entry_get_text(GTK_ENTRY(chrentry));
  if (s[0] == 0) { /* User hasn't filled in the active character field */
    selected_lsystem->prm.srulnum--;
    ls_clist_remove_selected(GTK_CLIST(lsui.srules));
  } else { /* Everything important is provided */
    buffer[0]=s[0];
    if (GTK_LIST(brchoice)->selection) {
      obj=GTK_LIST(brchoice)->selection->data;
      s=gtk_object_get_user_data(obj);
    } else s=CURRENT_BRUSH;
    clr_change=GTK_TOGGLE_BUTTON(encolor)->active;
    if (! GTK_TOGGLE_BUTTON(enspc)->active) brspacing=-1;
    if (! GTK_TOGGLE_BUTTON(enopacity)->active) bropacity=-1;
    sprintf(&buffer[1], ":%3d:%s:%d:%d:%d:%d,%d,%d", 
	    (gint)turn_angle, s, (gint)brspacing, (gint)bropacity,
	    (gint)clr_change,
	    (gint)(color[0]*255.999), (gint)(color[1]*255.999), 
	    (gint)(color[2]*255.999));  
    if (*srule != NULL)  /* Inserting a new rule */
      g_free(*srule);
    (*srule)=g_strdup(buffer);
    parsed_srule=ls_parse_srule( (*srule));
    ls_clist_update_selected_srule( GTK_CLIST(lsui.srules), parsed_srule );
    g_free(parsed_srule->brush_name);
    g_free(parsed_srule);
  }
  gtk_widget_hide(wind);
  gtk_widget_set_sensitive(lsui.srules, TRUE);
  gtk_widget_set_sensitive(lsui.lst, TRUE);
  rules_changed=TRUE;
}

static void ls_srule_edit_cancel_callback(GtkWidget *w, gpointer o)
{
  GtkWidget *wind;
  gchar **srule;

  edit_srule_dialog=FALSE;
  gtk_widget_set_sensitive(lsui.sruleadd, TRUE);
  gtk_widget_set_sensitive(lsui.sruleedit, TRUE);
  gtk_widget_set_sensitive(lsui.srulerem, TRUE);
  if (!w) return;
  wind=gtk_object_get_user_data(GTK_OBJECT(w));
  if (!wind) return;
  srule=gtk_object_get_data(GTK_OBJECT(wind), RULE);
  if (! *srule) { /* Remove an empty (newly created) rule */
    selected_lsystem->prm.srulnum--;
    ls_clist_remove_selected(GTK_CLIST(lsui.srules));
  }
  gtk_widget_hide(wind);
  gtk_widget_set_sensitive(lsui.srules, TRUE);
  gtk_widget_set_sensitive(lsui.lst, TRUE);
}


/* Various other callbacks */


/* Sensitivity toggler */
void ls_toggle_widget(GtkWidget *tbutton, GtkWidget *what)
{
  g_return_if_fail(what);

  if (GTK_WIDGET_IS_SENSITIVE(what))
    gtk_widget_set_sensitive(what, FALSE);
  else
    gtk_widget_set_sensitive(what, TRUE);
}

void ls_autoscale_toggle(GtkWidget *x, gpointer a)
{
  if (GTK_WIDGET_IS_SENSITIVE(GTK_WIDGET(lsui.startx))) {
    if (selected_lsystem) selected_lsystem->prm.autoscale=TRUE;
  } else {
    if (selected_lsystem) selected_lsystem->prm.autoscale=FALSE;
  }
  rules_changed=TRUE;
}

void ls_univ_close(GtkWidget *w, GtkWidget *what)
{
   if (what) { 
       gtk_widget_hide( what ); 
       gtk_widget_destroy( what ); 
   }
}

void     ls_new_seed_callback(GtkWidget *w, gpointer data)
{
  if (!selected_lsystem) return;

  rules_changed=TRUE;
  selected_lsystem->prm.seed=time(NULL);
  ls_grab_values();
  ls_draw_preview();
}

/*
******************************************************
********* Main window Callbacks
*/

void ls_preview_callback(GtkWidget *w, gpointer data)
{
  if (!selected_lsystem) return;
  if (gimp_debug()) g_print("Previewing...\n");
  ls_grab_values();
  ls_draw_preview();
}

void ls_cancel_callback(GtkWidget *w, gpointer *data)
{
  do_draw=FALSE;
  gtk_main_quit();
}

void ls_ok_callback(GtkWidget *w, gpointer *data)
{
  do_draw=TRUE;
  ls_grab_values();
  gtk_main_quit();
}

void ls_about_callback(GtkWidget *w, gpointer *data)
{
   GtkWidget *about,
             *lab;
   gchar buf[100];

   about=gtk_dialog_new();
   gtk_window_set_title(GTK_WINDOW(about), "GIMP:L-Systems:*About*");
   gtk_container_border_width(GTK_CONTAINER(about), 20);
   gtk_signal_connect(GTK_OBJECT(about), "destroy", 
                      GTK_SIGNAL_FUNC(ls_univ_close), about);
   sprintf( buf, "L-System Plug-in for GIMP\nversion %s\n\n"
	    "(c) 1998 Michal Gomulinski\n"
	    "M.Gomulinski@writeme.com\n", PLUGIN_VERSION);
   lab=gtk_label_new( buf );
   gtk_widget_show(lab);
   gtk_box_pack_start(GTK_BOX( GTK_DIALOG(about)->vbox ), lab, 
                          TRUE, TRUE, 5);
   lab=gtk_hseparator_new(); gtk_widget_show(lab);
   gtk_box_pack_start(GTK_BOX( GTK_DIALOG(about)->vbox ), lab, 
                          TRUE, TRUE, 0);
   lab=gtk_button_new_with_label("Ok");
   GTK_WIDGET_SET_FLAGS( lab, GTK_CAN_DEFAULT );
   gtk_signal_connect(GTK_OBJECT(lab), "clicked", 
                      GTK_SIGNAL_FUNC(ls_univ_close), about);
   gtk_box_pack_start(GTK_BOX( GTK_DIALOG(about)->action_area ), lab, 
                          TRUE, TRUE, 10);
   gtk_widget_show(lab);
   gtk_widget_grab_default(lab);
   gtk_widget_show(about);
}
