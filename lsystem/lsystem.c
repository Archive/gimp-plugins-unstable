/* 
   L-systems PLUG-IN for
   GIMP - General Image Manipulation Program, v. 0.99.10
   (c) Michal Gomulinski

   mailto:M.Gomulinski@writeme.com

   Warsaw, June 1997
   Birmingham, October, November 1997
   Warsaw, Mai 1998
 */

#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<string.h>
#include<math.h>
#include<ctype.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<unistd.h>
#include<dirent.h>
#include<glib.h>
#include<libgimp/gimp.h>
#include<values.h>
#include"lsystem.h"
#include"gdebug.h"


/* Global variables */
GList *loaded_lsystems=NULL;
GList *brushes;

double test;
gint do_draw;
gint32 image;
gint32 drawable;
guint image_wid;
guint image_hei;
guint image_ox;
guint image_oy;
gchar *lsystems_home_path;
gchar last_ls_name[256];

Position maxpos, minpos;

/* 
   Version identification string: 
   -$Id$- 
*/

#ifndef lint
static char vcid[] = "$Id$";
#endif /* lint */


static void flush_path (Path *);

static Context *get_drawing_context ();
static void set_drawing_context (Context *);

static void change_brush (gchar *);
static void change_spacing (gint);
static void change_opacity (gint);
static void change_color (guchar[3]);
GList *ls_load_all (GList * list);
GList *parse_lsystem_path ();


static void run ();
static void query ();
static gint ls_display_dialog ();
void ls_get_brushes();

GPlugInInfo PLUG_IN_INFO =
{
  NULL,
  NULL,
  query,
  run
};

MAIN ()

static void query ()
{
  static GParamDef args[] =  {
    {PARAM_INT32, "run_mode", "Interactive, Non-interactive"},
    {PARAM_IMAGE, "image", "Image to be used"},
    {PARAM_DRAWABLE, "drawable", "Drawable to draw on"},
    {PARAM_STRING, "name", "Lsystem name"}};
  static GParamDef *return_vals = NULL;
  static gint nargs = sizeof (args) / sizeof (args[0]);
  static gint nreturn_vals = 0;

  gimp_install_procedure ("plug_in_lsystem",
			  "Lsystems creation and rendering",
		  "This plug-in creates given lsystem from several sets of "
	       "rules given by the user and then renders it on your image. "
		   "Several (possibly) useful options are available. These "
		  "include: color and brush changes, separate set of rules "
			  "applied just before rendering and others."
			  "Setting a 'gimp_debug' variable in gimprc file  "
			  "(to a 'on') enables lots of debug information. ",
			  "Michal Gomulinski",
			  "Michal Gomulinski",
			  "1997,1998",
			  "<Image>/Filters/Render/L-systems",
			  "RGB*, GRAY*, INDEXED*",
			  PROC_PLUG_IN,
			  nargs, nreturn_vals,
			  args, return_vals);

}

void run (gchar * name,
	  gint nparams,
	  GParam * param,
	  gint * nreturn_vals,
	  GParam ** return_vals)
{
  static GParam values[1];
  GRunModeType run_mode;
  GStatusType status = STATUS_SUCCESS;
  gchar *compiled;
  GParam *rets;
  gint nrets;
  gdouble kx, ky, k, keep_x=0, keep_y=0;
  gchar *ls_to_draw;
  GList *p=NULL;

  (void)vcid;
  if (gimp_debug_get_state("lsystem")) {
    g_print("--Debug mode-- Press any key after starting debugger.\n");
    getchar();
  }

  run_mode = param[0].data.d_int32;
  image=param[1].data.d_image;
  drawable=param[2].data.d_drawable;
  ls_to_draw=param[3].data.d_string;

  *nreturn_vals = 1;
  *return_vals = values;

  values[0].type = PARAM_STATUS;
  values[0].data.d_status = status;

  ls_get_brushes();
  rets=gimp_run_procedure("gimp_selection_bounds", &nrets,
			  PARAM_IMAGE, image,
			  PARAM_END);
  if ((rets[0].data.d_status == STATUS_SUCCESS) && (rets[1].data.d_int32 > 0)) {
    image_ox=rets[2].data.d_int32;
    image_oy=rets[3].data.d_int32;
    image_wid=rets[4].data.d_int32 - image_ox;
    image_hei=rets[5].data.d_int32 - image_oy;
  } else {
    image_ox=0;
    image_oy=0;
    image_wid=gimp_drawable_width(drawable);
    image_hei=gimp_drawable_height(drawable);
  }
  gimp_destroy_params(rets, nrets);

  switch (run_mode) {
  case RUN_INTERACTIVE:
    gimp_get_data(PLUG_IN_NAME, &last_ls_name);
    if (!ls_display_dialog ())
      status=STATUS_EXECUTION_ERROR;
    break;
  case RUN_NONINTERACTIVE:
    strcpy(last_ls_name, ls_to_draw);
    p=parse_lsystem_path();
    loaded_lsystems=ls_load_all(p);
    if (!default_lsystem) {
      status=STATUS_CALLING_ERROR;
      g_print("No lsystem named '%s' found.\n", last_ls_name);
    }
    selected_lsystem=default_lsystem;
    break;
  case RUN_WITH_LAST_VALS:
    gimp_get_data(PLUG_IN_NAME, &last_ls_name);
    p=parse_lsystem_path();
    loaded_lsystems=ls_load_all(p);
    if (!default_lsystem) {
      status=STATUS_CALLING_ERROR;
      g_print("Strange, no lsystem named '%s' found.\n", last_ls_name);
    }
    selected_lsystem=default_lsystem;
    break;
  default:
    break;
    return;
  }

  if (status == STATUS_SUCCESS) {
    rets=gimp_run_procedure("gimp_undo_push_group_start", &nrets,
			    PARAM_IMAGE, image,
			    PARAM_END);
    if (rets[0].data.d_status != STATUS_SUCCESS) 
      g_print("No UNDO will be available.\n");
    gimp_destroy_params(rets, nrets);
    
    /*
    rets=gimp_run_procedure("gimp_image_disable_undo", &nrets,
			    PARAM_IMAGE, image,
			    PARAM_END);
    if (rets[0].data.d_status != STATUS_SUCCESS) 
      g_print("No UNDO will be available.\n");
    gimp_destroy_params(rets, nrets);
    */

    compiled=lsystem_create_string(&selected_lsystem->def);
    
    if (selected_lsystem->prm.autoscale) {
      selected_lsystem->prm.steplen=1.0;
      keep_x=selected_lsystem->prm.initpos.x;
      keep_y=selected_lsystem->prm.initpos.y;
      selected_lsystem->prm.initpos.x=0.0;
      selected_lsystem->prm.initpos.y=0.0;
      maxpos.x=-MAXFLOAT;
      maxpos.y=-MAXFLOAT;
      minpos.x=MAXFLOAT;
      minpos.y=MAXFLOAT;

      srand(selected_lsystem->prm.seed);
      lsystem_preview(compiled, &selected_lsystem->prm);
      free_slut(&selected_lsystem->prm);

      kx=(gdouble)image_wid / (maxpos.x - minpos.x);
      ky=(gdouble)image_hei / (maxpos.y - minpos.y);
      k=kx < ky ? kx : ky;
      k*=selected_lsystem->prm.as_correction; /* Adjusting scale according 
						 to user-specified correction */
      selected_lsystem->prm.steplen *= k;
      if (gimp_debug()) 
	g_print("Scaling factor=%f, steplen=%f\n", k, selected_lsystem->prm.steplen);

      selected_lsystem->prm.initpos.x -= minpos.x;
      selected_lsystem->prm.initpos.y -= minpos.y;
      selected_lsystem->prm.initpos.x *= k;
      selected_lsystem->prm.initpos.y *= k;
      /* Centering the result on the selection/image */
      selected_lsystem->prm.initpos.x+= (image_wid - (maxpos.x-minpos.x)*k)/2; 
      selected_lsystem->prm.initpos.y+= (image_hei - (maxpos.y-minpos.y)*k)/2; 
      /* Adjusting to the origin of selection */
      selected_lsystem->prm.initpos.x += image_ox;    
      selected_lsystem->prm.initpos.y += image_oy;
    }

    sprintf(last_ls_name, "Rendering %s", selected_lsystem->def.name);
    gimp_progress_init(last_ls_name);
    srand(selected_lsystem->prm.seed);
    lsystem_render(compiled, &selected_lsystem->prm);
    gimp_progress_update(1.0);

    if (selected_lsystem->prm.autoscale) {
      selected_lsystem->prm.initpos.x=keep_x;
      selected_lsystem->prm.initpos.y=keep_y; 
    }   

    if (run_mode == RUN_INTERACTIVE) {
      strncpy(last_ls_name, selected_lsystem->def.name, 255);
      gimp_set_data(PLUG_IN_NAME, &last_ls_name, sizeof(last_ls_name));
      ls_write_all();
    }

    rets=gimp_run_procedure("gimp_undo_push_group_end", &nrets,
			    PARAM_IMAGE, image,
			    PARAM_END);
    gimp_destroy_params(rets, nrets);

    /*
    rets=gimp_run_procedure("gimp_image_enable_undo", &nrets,
			    PARAM_IMAGE, image,
			    PARAM_END);
    gimp_destroy_params(rets, nrets);
    */

    if (run_mode != RUN_NONINTERACTIVE)
      gimp_displays_flush ();
  }
  values[0].data.d_status = status;
}

void ls_get_brushes()
{
  GParam *ret_vals;
  gint nretvals, nbr, i;
  gchar **bnames;

  ret_vals=gimp_run_procedure("gimp_brushes_list", &nretvals, PARAM_END);
  if (ret_vals[0].data.d_status != STATUS_SUCCESS) {
    return;
  }
  nbr=ret_vals[1].data.d_int32;
  bnames=ret_vals[2].data.d_stringarray;
  for (i=0; i<nbr; i++) 
    brushes=g_list_append(brushes, g_strdup(bnames[i]));
  gimp_destroy_params(ret_vals, nretvals);
}

gint ls_display_dialog ()
{
  GList *p = NULL;
  GtkWidget *window;
  gint argc;
  gchar **argv;
  guchar *color_cube;

  argc = 2;
  argv = g_new (gchar *, 2);
  argv[0] = g_strdup ("lsystem");
  argv[1] = g_strdup ("--show-events");

  gtk_init (&argc, &argv);
  gtk_rc_parse (gimp_gtkrc ());

  gdk_set_use_xshm (gimp_use_xshm ());
  gtk_preview_set_gamma (gimp_gamma ());
  gtk_preview_set_install_cmap (gimp_install_cmap ());
  color_cube = gimp_color_cube ();
  gtk_preview_set_color_cube (color_cube[0], color_cube[1],
			      color_cube[2], color_cube[3]);

  gtk_widget_set_default_visual (gtk_preview_get_visual ());
  gtk_widget_set_default_colormap (gtk_preview_get_cmap ());

  p=parse_lsystem_path();
  loaded_lsystems = ls_load_all (p);
  window = ls_main_new ();
  gtk_main ();
  return do_draw;
}

LSinfo *ls_lsystem_copy(LSinfo *trg, LSinfo *src)
{
  gint i;

  memcpy(trg, src, sizeof(LSinfo));
  trg->def.name=g_strdup(src->def.name);
  trg->def.fname=g_strdup(src->def.fname);
  trg->def.axiom=g_strdup(src->def.axiom);
  trg->def.rules=g_malloc(src->def.rulnum * sizeof(gchar *));
  for (i=0; i< trg->def.rulnum; i++) 
    trg->def.rules[i]=g_strdup(src->def.rules[i]);
  trg->def.lrules=g_malloc(src->def.lrulnum * sizeof(gchar *));
  for (i=0; i< trg->def.lrulnum; i++) 
    trg->def.lrules[i]=g_strdup(src->def.lrules[i]);
  trg->prm.srules=g_malloc(src->prm.srulnum * sizeof(gchar *));
  for (i=0; i< trg->prm.srulnum; i++) 
    trg->prm.srules[i]=g_strdup(src->prm.srules[i]);
  return trg;
}

LSinfo *ls_lsystem_new()
{
  LSinfo *ret=g_malloc0(sizeof(LSinfo));
  Rparams *prm=&ret->prm;
  Lsystem *def=&ret->def;

  prm->srulnum=2;
  prm->srules=g_malloc(prm->srulnum * sizeof(gchar*));
  prm->srules[0]=g_strdup("+:30:" CURRENT_BRUSH ":-1:-1:0:");
  prm->srules[1]=g_strdup("-:-30:" CURRENT_BRUSH ":-1:-1:0:");
  prm->steplen=5;
  prm->stepfactor=50;
  prm->autoscale=TRUE;
  prm->as_correction=1.0;
  def->iters=4;
  return ret;
}

gchar *ls_make_full_filename(gchar *fn)
{
  gchar *tmp=fn, 
    *ext;

  if (fn[0] != '/') { /* File name of new lsystem. 
				   Path will be supplied */
    if (!lsystems_home_path) {
      g_print("No _your_ lsystems directory defined in gimprc.\n"
	      "Lsystem files will be written in your home directory.\n");
      lsystems_home_path=getenv("HOME");
    }
    tmp=g_malloc(sizeof(gchar)* (strlen(fn)+ strlen(lsystems_home_path)+2));
    sprintf(tmp, "%s%s", lsystems_home_path, fn);
    g_free(fn);
    fn=tmp;
  }

  /* Checking file extension */
  ext=strrchr(fn, '.');
  if (!ext || strcmp(ext, ".ls")!=0 ) {
    tmp=g_malloc(sizeof(gchar)*(4+strlen(fn)));
    sprintf(tmp, "%s.ls", fn);
    g_free(fn);
    fn=tmp;
  }

  return fn;
}

gint lsystem_write_to_file (LSinfo * s)
{
  FILE *f=NULL;
  gint i;
  gchar *tmp, *ext;

  if (!s->dirty) {
    if (gimp_debug()) g_print("%s is clean, not writing.\n", s->def.name);
    return 1;
  }
  if (s->def.fname[0] != '/') { /* File name of new lsystem. 
				   Path will be supplied */
    if (!lsystems_home_path) {
      g_print("No _your_ lsystems directory defined in gimprc.\n"
	      "Lsystem files will be written in your home directory.\n");
      lsystems_home_path=getenv("HOME");
    }
    tmp=g_malloc(sizeof(gchar)* (strlen(s->def.fname)+
				 strlen(lsystems_home_path)+2));
    sprintf(tmp, "%s%s", lsystems_home_path, s->def.fname);
    g_free(s->def.fname);
    s->def.fname=tmp;
  }

  /* Checking file extension */
  ext=strrchr(s->def.fname, '.');
  if (!ext || strcmp(ext, ".ls")!=0 ) {
    tmp=g_malloc(sizeof(gchar)*(4+strlen(s->def.fname)));
    sprintf(tmp, "%s.ls", s->def.fname);
    g_free(s->def.fname);
    s->def.fname=tmp;
  }

  if (gimp_debug())
    g_print("Writing to file: '%s'\n", s->def.fname);
  f=fopen(s->def.fname, "w");
  if (!f) {
    g_print ("Cannot open L-System file '%s' for writing!\n", s->def.fname);
    return 0;
  }
  fprintf (f, "LSname:%s\n", s->def.name);
  fprintf (f, "axiom:%s\n", s->def.axiom);
  fprintf (f, "<RULES>:%d\n", s->def.rulnum);
  for (i = 0; i < s->def.rulnum; i++)
    fprintf (f, "rule:%d:%s\n", i, s->def.rules[i]);
  fprintf (f, "</RULES>\n");
  fprintf (f, "<LRULES>:%d\n", s->def.lrulnum);
  for (i = 0; i < s->def.lrulnum; i++)
    fprintf (f, "lrule:%d:%s\n", i, s->def.lrules[i]);
  fprintf (f, "</LRULES>\n");
  fprintf (f, "iters:%d\n", s->def.iters);
  fprintf (f, "initpos:%f,%f,%f\n", s->prm.initpos.x, 
	   s->prm.initpos.y, s->prm.initpos.angle);
  fprintf (f, "steplen:%f\n", s->prm.steplen);
  fprintf (f, "stepfactor:%f\n", s->prm.stepfactor);
  fprintf (f, "<SRULES>:%d\n", s->prm.srulnum);
  for (i = 0; i < s->prm.srulnum; i++)
    fprintf (f, "srule:%d:%s\n", i, s->prm.srules[i]);
  fprintf (f, "</SRULES>\n");
  fprintf (f, "rounding:%f\n", s->prm.rounding_amount);
  fprintf (f, "turn_random:%f\n", s->prm.turn_random);
  fprintf (f, "step_random:%f\n", s->prm.step_random);
  fprintf (f, "acu_step_random:%d\n", s->prm.acu_step_random);
  fprintf (f, "random_seed:%d\n", s->prm.seed);
  fprintf (f, "autoscale:%d\n", s->prm.autoscale ? 1 : 0 );
  fprintf (f, "as_correction:%f\n", s->prm.as_correction);
  fprintf (f, "END.\n");
  fclose (f);
  return 1;
}

void lsinfo_free (LSinfo * l)
{
  gint i;

  if (l->def.name)
    g_free (l->def.name);
  if (l->def.fname)
    g_free (l->def.fname);
  if (l->def.rulnum)
    for (i = 0; i < l->def.rulnum; i++)
      if (l->def.rules[i])
	g_free (l->def.rules[i]);
  if (l->def.lrulnum)
    for (i = 0; i < l->def.lrulnum; i++)
      if (l->def.lrules[i])
	g_free (l->def.lrules[i]);
  if (l->prm.srulnum)
    for (i = 0; i < l->prm.srulnum; i++)
      g_free (l->prm.srules[i]);
  if (l->prm.slut)
    free_slut (&l->prm);
  g_free (l);
}


LSinfo *lsystem_read_from_file (gchar * name)
{
  LSinfo *ret = (LSinfo *) g_malloc0 (sizeof (LSinfo));
  FILE *f = fopen (name, "r");
  gchar *line = (gchar *) g_malloc (MAX_LINE_LENGTH);
  gchar *wrd;
  gint i;

  if (gimp_debug())
    g_print("Opening file '%s'.\n", name);
  if (!f) {
    g_warning ("Can't open L-System file %s\n", name);
    g_free (ret);
    g_free (line);
    return NULL;
  }
  ret->def.fname = g_strdup (name);
  ret->prm.autoscale=TRUE;
  ret->prm.as_correction=1.0;
  while (!feof (f)) {
    fgets (line, MAX_LINE_LENGTH, f);
    wrd = strtok (line, ":\n");
    if (wrd==NULL) goto error;
    if (strcmp (wrd, "LSname") == 0) {
      ret->def.name = g_strdup (strtok (NULL, ":\n"));
      continue;
    }
    if (strcmp (wrd, "axiom") == 0) {
      ret->def.axiom = g_strdup (strtok (NULL, ":\n"));
      continue;
    }
    if (strcmp (wrd, "<RULES>") == 0) {
      ret->def.rulnum = atoi (strtok (NULL, ":\n"));
      ret->def.rules = (gchar **) g_malloc0 (sizeof (gchar *)
					     * ret->def.rulnum);
      for (i = 0; i < ret->def.rulnum; i++) {
	fgets (line, MAX_LINE_LENGTH, f);
	wrd = strtok (line, ":");
	if (strcmp (wrd, "rule") != 0)
	  goto error;
	wrd = strtok (NULL, ":");
	wrd = strtok (NULL, "\n");
	ret->def.rules[i] = g_strdup (wrd);
      }
      fgets (line, MAX_LINE_LENGTH, f);
      wrd = strtok (line, "\n");
      if (strcmp (wrd, "</RULES>") != 0)
	goto error;
      continue;
    }
    if (strcmp (wrd, "<LRULES>") == 0) {
      ret->def.lrulnum = atoi (strtok (NULL, ":\n"));
      ret->def.lrules = (gchar **) g_malloc0 (sizeof (gchar *) *
					      ret->def.lrulnum);
      for (i = 0; i < ret->def.lrulnum; i++) {
	fgets (line, MAX_LINE_LENGTH, f);
	wrd = strtok (line, ":");
	if (strcmp (wrd, "lrule") != 0)
	  goto error;
	wrd = strtok (NULL, ":");
	wrd = strtok (NULL, "\n");
	ret->def.lrules[i] = g_strdup (wrd);
      }
      fgets (line, MAX_LINE_LENGTH, f);
      wrd = strtok (line, "\n");
      if (strcmp (wrd, "</LRULES>") != 0)
	goto error;
      continue;
    }
    if (strcmp (wrd, "iters") == 0) {
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->def.iters = atoi (wrd);
      continue;
    }
    if (strcmp (wrd, "initpos") == 0) {
      wrd = strtok (NULL, ",");
      if (!wrd)
	goto error;
      ret->prm.initpos.x = atof (wrd);
      wrd = strtok (NULL, ",");
      if (!wrd)
	goto error;
      ret->prm.initpos.y = atof (wrd);
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->prm.initpos.angle = atof (wrd);
      continue;
    }
    if (strcmp (wrd, "steplen") == 0) {
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->prm.steplen = atof (wrd);
      continue;
    }
    if (strcmp (wrd, "stepfactor") == 0) {
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->prm.stepfactor = atof (wrd);
      continue;
    }
    if (strcmp (wrd, "<SRULES>") == 0) {
      ret->prm.srulnum = atoi (strtok (NULL, ":\n"));
      ret->prm.srules = (gchar **) g_malloc0 (sizeof (gchar *) *
					      ret->prm.srulnum);
      for (i = 0; i < ret->prm.srulnum; i++) {
	fgets (line, MAX_LINE_LENGTH, f);
	wrd = strtok (line, ":");
	if (strcmp (wrd, "srule") != 0)
	  goto error;
	wrd = strtok (NULL, ":");
	wrd = strtok (NULL, "\n");
	ret->prm.srules[i] = g_strdup (wrd);
      }
      fgets (line, MAX_LINE_LENGTH, f);
      wrd = strtok (line, "\n");
      if (strcmp (wrd, "</SRULES>") != 0)
	goto error;
      continue;
    }
    if (strcmp (wrd, "rounding") == 0) {
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->prm.rounding_amount = atof (wrd);
      continue;
    }
    if (strcmp (wrd, "turn_random") == 0) {
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->prm.turn_random = atof (wrd);
      continue;
    }
    if (strcmp (wrd, "step_random") == 0) {
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->prm.step_random = atof (wrd);
      continue;
    }
    if (strcmp (wrd, "acu_step_random") == 0) {
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->prm.acu_step_random = atoi (wrd);
      continue;
    }
    if (strcmp (wrd, "random_seed") == 0) {
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->prm.seed = atol (wrd);
      continue;
    }
    if (strcmp (wrd, "autoscale") == 0) {
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->prm.autoscale = atoi (wrd);
      continue;
    }
    if (strcmp (wrd, "as_correction") == 0) {
      wrd = strtok (NULL, "\n");
      if (!wrd)
	goto error;
      ret->prm.as_correction = atof (wrd);
      continue;
    }
    if (strcmp (wrd, "END.") == 0) {
      break;
    }
  error:			/* Default */
    g_warning ("LSystem file %s is corrupted. Giving up\n", name);
    lsinfo_free (ret);
    g_free (line);
    fclose (f);
    return NULL;
  }
  g_free (line);
  fclose (f);
  return ret;
}

/* Code based on gflare.c */

/*
 *    Query gimprc for gflare-path, and parse it.
 *      This code is based on script_fu_find_scripts ()
 */

GList *parse_lsystem_path ()
{
  GParam *return_vals;
  gint nreturn_vals;
  gchar *path_string;
  gchar *home;
  gchar *path;
  gchar *token;
  struct stat filestat;
  gint err;
  GList *path_list;
  gint home_path_now=0;

  path_list = g_list_alloc ();

  return_vals = gimp_run_procedure ("gimp_gimprc_query",
				    &nreturn_vals,
				    PARAM_STRING, "lsystem-path",
				    PARAM_END);

  if (return_vals[0].data.d_status != STATUS_SUCCESS
      || return_vals[1].data.d_string == NULL) {
    gimp_destroy_params (return_vals, nreturn_vals);
    g_list_free (path_list);
    g_print("Empty \"lsystem-path\" parameter in your gimprc\n");
    g_print("Please refer to the README file shipped along with the source code\n"
	    "or have a look at some instructions on plug-in's homepage:'n"
	    "http://www.ia.pw.edu.pl/~s146028/fractal/lsyst\n");
    exit(0);
    return NULL;
  }
  path_string = g_strdup (return_vals[1].data.d_string);
  gimp_destroy_params (return_vals, nreturn_vals);

  /* Set local path to contain temp_path, where (supposedly)
   * there may be working files.
   */
  home = getenv ("HOME");

  /* Search through all directories in the  path */

  token = strtok (path_string, ":");

  while (token) {
    home_path_now=0;
    if (*token == '\0') {
      token = strtok (NULL, ":");
      continue;
    }
    if (*token == '~') {
      path = g_malloc (strlen (home) + strlen (token) + 2);
      sprintf (path, "%s%s", home, token + 1);
    } else {
      path = g_malloc (strlen (token) + 2);
      strcpy (path, token);
    }				/* else */

    /* Check if directory exists */
    err = stat (path, &filestat);
    
    if (!err && ( (S_IWUSR & filestat.st_mode) != 0 ))
      home_path_now=1;

    if (!err && S_ISDIR (filestat.st_mode)) {
      if (path[strlen (path) - 1] != '/')
	strcat (path, "/");

      path_list = g_list_append (path_list, path);
      if (home_path_now && !lsystems_home_path) 
	lsystems_home_path=g_strdup(path);
    } else {
      g_free (path);
    }

    token = strtok (NULL, ":");
  }
  g_free (path_string);
  return path_list;
}

gint lscmp(LSinfo *a, LSinfo *b)
{
  if (!a) return -1;
  if (!b) return 1;
  return strcmp(a->def.name, b->def.name);
}


/*
   Load all lsystems, which are found in lsystems-path-list. 
 */

GList *ls_load_all (GList * list)
{
  LSinfo *lsystem;
  GList *lslist;
  gchar *path;
  gchar *filename;
  DIR *dir;
  struct dirent *dir_ent;
  struct stat filestat;
  gint err;
  gboolean supply_slash;

  if (!list) return NULL;
  lslist = g_list_alloc ();
  list = list->next;
  while (list) {
    path = list->data;
    list = list->next;
    supply_slash = (path[strlen (path) - 1] != '/');

    /* Open directory */
    dir = opendir (path);

    if (!dir)
      g_warning ("error reading LSystem directory \"%s\"", path);
    else {
      while ((dir_ent = readdir (dir))) {
	filename = g_malloc (strlen (path) + strlen (dir_ent->d_name) + 1);
	if (supply_slash)
	  sprintf (filename, "%s/%s", path, dir_ent->d_name);
	else
	  sprintf (filename, "%s%s", path, dir_ent->d_name);

	/* Check for ".ls" extension */
	if (strcmp (&filename[strlen (filename) - 3], ".ls") != 0) {
	  g_free (filename);
	  continue;
	}
	/* Check the file and see that it is not a sub-directory */
	err = stat (filename, &filestat);

	if (!err && S_ISREG (filestat.st_mode)) {
	  lsystem = lsystem_read_from_file (filename);
	  if (lsystem) {
	    lslist=g_list_insert_sorted (lslist, lsystem, (GCompareFunc) lscmp);
	    if (lsystem->def.name && (strcmp(lsystem->def.name, last_ls_name)==0))
	      default_lsystem=lsystem;
	  }
	}
	g_free (filename);
      }				/* while */
      closedir (dir);
    }				/* else */
  }
  loaded_lsystems=lslist;
  return (lslist);
}

void ls_write_all() 
{
  GList *all;

  all=loaded_lsystems;
  while(all) {
    if (all->data) lsystem_write_to_file((LSinfo*)all->data);
    all=all->next;
  }
}

static Context *get_drawing_context ()
{
  Context *ret=g_malloc0(sizeof(Context));
  GParam *rets;
  gint nrets;
  
  /* Saving COLOR */
  gimp_palette_get_foreground(&ret->color[0], &ret->color[1], &ret->color[2]);

  /* Saving BRUSH name */
  rets=gimp_run_procedure("gimp_brushes_get_brush", &nrets,
			  PARAM_END);
  if (rets[0].data.d_status == STATUS_SUCCESS) 
    ret->brush_name=g_strdup(rets[1].data.d_string);
  else
    ret->brush_name=NULL;
  gimp_destroy_params (rets, nrets);

  /* Saving OPACITY */
  rets=gimp_run_procedure("gimp_brushes_get_opacity", &nrets,
			  PARAM_END);
  if (rets[0].data.d_status == STATUS_SUCCESS) 
    ret->opacity=rets[1].data.d_float;
  else
    ret->opacity=-1;
  gimp_destroy_params (rets, nrets);

  /* Saving SPACING */
  rets=gimp_run_procedure("gimp_brushes_get_spacing", &nrets,
			  PARAM_END);
  if (rets[0].data.d_status == STATUS_SUCCESS) 
    ret->brush_spc=rets[1].data.d_int32;
  else
    ret->brush_spc=-1;
  gimp_destroy_params (rets, nrets);

  return ret;
}

static void set_drawing_context (Context * c)
{
  change_color(c->color);
  if (c->brush_spc >= 0) change_spacing(c->brush_spc);
  if (c->opacity >= 0) change_opacity(c->opacity);
  if (c->brush_name) change_brush(c->brush_name);
  g_free(c->brush_name);
  g_free(c);
}

static void change_brush (gchar * s)
{
  GParam *rets;
  gint nrets;

  if (!s || (strcmp(s, CURRENT_BRUSH)==0)) return;
  rets=gimp_run_procedure("gimp_brushes_set_brush", &nrets, 
			  PARAM_STRING, s,
			  PARAM_END);
  gimp_destroy_params(rets, nrets);
}

static void change_spacing (gint i)
{
  GParam *rets;
  gint nrets;

  if (i>1000) i=1000;
  rets=gimp_run_procedure("gimp_brushes_set_spacing", &nrets, 
			  PARAM_INT32, (gint32)i,
			  PARAM_END);
  gimp_destroy_params(rets, nrets);
}

static void change_opacity (gint i)
{
  GParam *rets;
  gint nrets;

  if (i>100) i=100;
  rets=gimp_run_procedure("gimp_brushes_set_opacity", &nrets, 
			  PARAM_FLOAT, (gdouble)i,
			  PARAM_END);
  gimp_destroy_params(rets, nrets);
}

static void change_color (guchar r[3])
{
  gimp_palette_set_foreground(r[0], r[1], r[2]);
}

Path *new_path (Position p)
{
  Path *path = (Path *) g_malloc (sizeof (Path));

  path->pts = (gdouble *) g_malloc (sizeof (gdouble));
  path->pathlen = 0;
  path->pavail = 0;
  return add_point (path, p);
}

Path *add_point (Path * path, Position p)
{
  static gint delta = 50;
  gdouble *tail;

  if (path->pavail < 2) {
    path->pts = (gdouble *) g_realloc (path->pts,
				 sizeof (gdouble) * (path->pathlen + delta));
    path->pavail = delta;
  }
  tail = &path->pts[path->pathlen];
  *tail = p.x;
  *(tail + 1) = p.y;
  path->pathlen += 2;
  path->pavail -= 2;
  return path;
}

static void flush_path (Path * path)
{
  GParam *rets;
  gint nrets;

  if (path->pathlen > 2) { /* Paths shorter than 2 points are useless */
    rets=gimp_run_procedure("gimp_paintbrush", &nrets, 
			    PARAM_IMAGE, image,
			    PARAM_DRAWABLE, drawable,
			    PARAM_FLOAT, 0.0, 
			    PARAM_INT32, path->pathlen,
			    PARAM_FLOATARRAY, path->pts,
			    PARAM_END);
    gimp_destroy_params(rets, nrets);
  }
  g_free (path->pts);
  g_free (path);
}


/* L-system previewing (for BBox calculations */

inline Path *prv_add_point (Path * path, Position p)
{
  if (p.x < minpos.x) minpos.x=p.x;
  if (p.y < minpos.y) minpos.y=p.y;
  if (p.x > maxpos.x) maxpos.x=p.x;
  if (p.y > maxpos.y) maxpos.y=p.y;

  return NULL;
}

inline Path *prv_new_path (Position p)
{
  return prv_add_point(NULL, p);
}

static void prv_flush_path (Path * path)
{
  g_assert(path==NULL);
}


gchar *
  lsystem_create_string (Lsystem * s)
{
  gint i, j;
  gchar *p;
  gchar *r;
  register gint rlen;
  register gint ravail;
  gint delta = 4;
  gchar *addstr;
  gint addlen;
  gchar *pptr;
  gchar *lut[256];

  memset (lut, 0, sizeof (lut));
  for (i = 0; i < s->rulnum; i++) {
    lut[(guint) s->rules[i][0]] = &s->rules[i][2];
  }				/* Endfor */
  p = g_strdup (s->axiom);
  r = (gchar *) g_malloc (1);
  r[0] = 0;
  rlen = 0;
  ravail = 0;
  for (i = 0; i < s->iters; i++) {
    pptr = p;
    for (j = 0; j < strlen (p); j++) {
      if (lut[(guint) *pptr] == NULL) {
	if (ravail < 2) {
	  r = (gchar *) g_realloc (r, rlen + delta + 1);
	  ravail = delta + 1;
	  delta *= 1.5;
	}			/* Endif */
	r[rlen++] = *pptr;
	r[rlen] = 0;
	ravail--;
      } else {
	addstr = lut[(guint) *pptr];
	addlen = strlen (addstr);
	if (ravail < addlen+1) {
	  r = (gchar *) g_realloc (r, rlen + delta + addlen);
	  ravail = delta + addlen;
	  delta *= 1.5;
	}			/* Endif */
	strcat (&r[rlen], addstr);
	rlen += addlen;
	ravail -= addlen;
      }				/* Endifelse */
      pptr++;
    }				/* Endfor */
    g_free (p);
    p = r;
    r = (gchar *) g_malloc (1);
    r[0] = 0;
    /*
    g_print("ITER %d, rlen=%d, ravail=%d, delta=%d\n", i, rlen, ravail, delta);
    fflush (stdout);
    */
    rlen = 0;
    ravail = 0;
  }				/* Endfor */

  if (s->lrulnum > 0) {
    /* Preparing Look Up Table (LUT) for application of LAST rules */
    memset (lut, 0, sizeof (lut));
    for (i = 0; i < s->lrulnum; i++) {
      lut[(guint) s->lrules[i][0]] = &s->lrules[i][2];
    }				/* Endfor */
    
    /* Applying a set of LAST rules */
    pptr = p;
    for (j = 0; j < strlen (p); j++) {
      if (lut[(guint) *pptr] == NULL) { /* Simple copying of character */
	if (ravail < 2) {
	  r = (gchar *) g_realloc (r, rlen + delta + 1);
	  ravail = delta + 1;
	  delta *= 1.5;
	}			/* Endif */
	r[rlen] = *pptr;
	r[++rlen] = 0;
	ravail--;
      } else { /* Inserting text replacing the character */
	addstr = lut[(guint) *pptr];
	addlen = strlen (addstr);
	if (ravail < addlen + 1) {
	  r = (gchar *) g_realloc (r, rlen + delta + addlen);
	  ravail = delta + addlen;
	  delta *= 1.5;
	}			/* Endif */
	strcat (&r[rlen], addstr);
	rlen += addlen;
	ravail -= addlen;
      }				/* Endifelse */
      pptr++;
    }				/* Endfor */
    g_free(p);
    p=g_realloc(r, rlen);
  }
  return p;
}

gint ls_atoi(gchar *str)
{
  gint val;
  gchar *end;

  if (!str) return 0;
  val=strtol(str, &end, 10);
  if (end==str) val=0;
  return val;
}

gdouble ls_atod(gchar *str)
{
  gdouble val;
  gchar *end;

  if (!str) return 0;
  val=strtod(str, &end);
  if (end==str) val=0;
  return val;
}

Special *ls_parse_srule(gchar *srule)
{
  Special *akt;
  gchar *tstr;

  akt = (Special *) g_malloc (sizeof (Special));
  akt->lefthand=srule[0];
  tstr = g_strdup (srule + 2);
  akt->turn_angle = ls_atod (strtok (tstr, ":"));
  akt->brush_name = g_strdup(strtok (NULL, ":"));
  akt->brush_spc = ls_atoi (strtok (NULL, ":"));
  akt->opacity = ls_atoi (strtok (NULL, ":"));
  akt->color_change= ls_atoi (strtok(NULL, ":"));
  if (akt->color_change) {
    akt->color[0] = ls_atoi( strtok(NULL, ","));
    akt->color[1] = ls_atoi( strtok(NULL, ","));
    akt->color[2] = ls_atoi( strtok(NULL, ","));
  }
  g_free (tstr);

  return akt;
}

int lsystem_render (gchar * s, Rparams * p)
{
  register gchar *sptr;
  Position pos, keep_pos;
  Special **slut;
  Special *spec, **akt;
  Path *path;
  gint scanned, i, flushed;
  Context *saved_context;
  gfloat slen;
  static gchar *real_begin=NULL;
  static gulong real_length=0;
  static gint every_some=0;
  static gint every_count=0;
  static gdouble increment=0.0;

  if (!real_begin) { 
    real_begin=s;
    real_length=strlen(real_begin);
    every_count=real_length/30;
    increment=1.0/real_length;
  }
  if (p->slut != NULL)
    slut = (p->slut);
  else {
    p->slut = (Special **) g_malloc0 (256 * sizeof (Special *));
    slut = (p->slut);
    for (i = 0; i < p->srulnum; i++) {
      akt = &slut[(guint) p->srules[i][0]];
      (*akt)=ls_parse_srule(p->srules[i]);
    }
  }

  saved_context=get_drawing_context();
  sptr = s;
  pos = p->initpos;
  path = new_path (pos);
  while ((*sptr != 0) && (*sptr != ')')) {
    if (every_some++>every_count) {
      gimp_progress_update((gdouble)(sptr-real_begin)*increment);
      every_some=0;
    }
    switch (*sptr) {
    case 'F':			/* Forward move */
      if (p->acu_step_random)
	slen = p->steplen += rand_norm (p->step_random)
	  * p->steplen;
      else
	slen = p->steplen + rand_norm (p->step_random)
	  * p->steplen;
      pos.x += (gfloat) slen *cos (PI * pos.angle / 180.0);
      pos.y += (gfloat) slen *sin (PI * pos.angle / 180.0);
      path = add_point (path, pos);
      break;
    case 'f':			/* Forward jump */
      if (p->acu_step_random)
	slen = p->steplen += rand_norm (p->step_random)
	  * p->steplen;
      else
	slen = p->steplen + rand_norm (p->step_random)
	  * p->steplen;
      flush_path (path);
      pos.x += (gfloat) slen *cos (PI * pos.angle / 180.0);
      pos.y += (gfloat) slen *sin (PI * pos.angle / 180.0);
      path = new_path (pos);
      break;
    case '(':			/* Begining of a "sub l-system" */
      p->steplen *= p->stepfactor;
      keep_pos=p->initpos;
      p->initpos = pos;
      scanned = lsystem_render (sptr + 1, p);
      p->initpos=keep_pos;
      p->steplen /= p->stepfactor;
      sptr += 1 + scanned;
      break;
    default:			/* Special cases or nothing. */
      if (slut[(guchar) *sptr] != NULL) {
	flushed = 0;
	spec = slut[(guchar) *sptr];
	pos.angle += spec->turn_angle +
	  rand_norm (p->turn_random) * spec->turn_angle;
	if (spec->brush_name || spec->brush_spc >= 0 ||
	    spec->opacity >= 0 || spec->color_change) {
	  flush_path (path);
	  flushed = 1;
	}
	if (flushed)
	  path = new_path (pos);
	if (spec->brush_name)
	  change_brush (spec->brush_name);
	if (spec->brush_spc >= 0)
	  change_spacing (spec->brush_spc);
	if (spec->opacity >= 0 )
	  change_opacity (spec->opacity);
	if (spec->color_change)
	  change_color (spec->color);
      }
    }				/* End of switch(...) */
    sptr++;
  }				/* End of while(...) */
  if (path)
    flush_path (path);
  
  set_drawing_context (saved_context);

  return (sptr - s);

}

int lsystem_preview (gchar * s, Rparams * p)
{
  register gchar *sptr;
  Position pos, keep_pos;
  Special **slut;
  Special *spec, **akt;
  Path *path;
  gint scanned, i, flushed;
  gfloat slen;

  if (p->slut != NULL)
    slut = (p->slut);
  else {
    p->slut = (Special **) g_malloc0 (256 * sizeof (Special *));
    slut = (p->slut);
    for (i = 0; i < p->srulnum; i++) {
      akt = &slut[(guint) p->srules[i][0]];
      (*akt)=ls_parse_srule(p->srules[i]);
    }
  }

  sptr = s;
  pos=p->initpos;

  path = prv_new_path (pos);
  while ((*sptr != 0) && (*sptr != ')')) {
    switch (*sptr) {
    case 'F':			/* Forward move */
      if (p->acu_step_random)
	slen = p->steplen += rand_norm (p->step_random)
	  * p->steplen;
      else
	slen = p->steplen + rand_norm (p->step_random)
	  * p->steplen;
      pos.x += (gfloat) slen *cos (PI * pos.angle / 180.0);
      pos.y += (gfloat) slen *sin (PI * pos.angle / 180.0);
      path = prv_add_point (path, pos);
      break;
    case 'f':			/* Forward jump */
      if (p->acu_step_random)
	slen = p->steplen += rand_norm (p->step_random)
	  * p->steplen;
      else
	slen = p->steplen + rand_norm (p->step_random)
	  * p->steplen;
      prv_flush_path (path);
      pos.x += (gfloat) slen *cos (PI * pos.angle / 180.0);
      pos.y += (gfloat) slen *sin (PI * pos.angle / 180.0);
      path = prv_new_path (pos);
      break;
    case '(':			/* Begining of a "sub l-system" */
      p->steplen *= p->stepfactor;
      keep_pos=p->initpos;
      p->initpos = pos;
      scanned = lsystem_preview (sptr + 1, p);
      p->steplen /= p->stepfactor;
      p->initpos=keep_pos;
      sptr += 1 + scanned;
      break;
    default:			/* Special cases or nothing. */
      if (slut[(guchar) *sptr] != NULL) {
	flushed = 0;
	spec = slut[(guchar) *sptr];
	pos.angle += spec->turn_angle +
	  rand_norm (p->turn_random) * spec->turn_angle;
      }
    }				/* End of switch(...) */
    sptr++;
  }				/* End of while(...) */
  if (path) prv_flush_path(path);
  return (sptr - s);
}


void free_slut (Rparams * p)
{
  gint i;
  Special **slut;

  slut = (p->slut);
  if (slut) {
    for (i = 0; i < 256; i++) {
      if (slut[i]) 
	g_free (slut[i]);
    }				/* Endfor */
  }				/* Endif  */
  p->slut=NULL;
}


/* Gaussian random number generator */

static gfloat roufast ()
{
  gint akc = 1;
  gfloat u, x;
  static gfloat okrdou = RAND_MAX;

  do {
    u = rand();
    u = (u + 1.0) / okrdou;
    x = rand();
    x = 2.0 * sqrt (2.0 / exp (1.0)) * (x + 1.0) / okrdou - sqrt (2.0 / exp (1.0));
    x = x / u;
    if (x * x <= 2.0 * (3.0 - u * (4.0 + sqrt (u))))
      akc = 0;
    else if (x * x <= 2.0 / u - 2.0 * u)
      if (x * x <= -4.0 * log (u))
	akc = 0;
  }
  while (akc);
  return (x);
}

gdouble rand_norm (gdouble sigma)
{
  return roufast () * sigma;
}
