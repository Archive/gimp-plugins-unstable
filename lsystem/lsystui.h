
#include<glib.h>
#include<gtk/gtk.h>
#include"lsystem.h"

#define SRULES_ENTRIES 7
#define PRV_WID 20
#define PRV_HEI 10

extern gdouble steplen, 
  stepfactor, 
  rounding, 
  turnrandom, 
  steprandom, 
  brspacing, 
  bropacity, 
  turn_angle, 
  initx, 
  inity, 
  initangle;

extern gint rules_changed;

void ls_rules_list_fill(gchar *[] , guint , GtkCList *);
void ls_rules_list_create(Lsystem *);
void ls_lrules_list_create(Lsystem *);
void ls_srules_list_create();
LSinfo *ls_get_selected_lsystem();
void ls_update_rules_dialog();
void ls_grab_values();
