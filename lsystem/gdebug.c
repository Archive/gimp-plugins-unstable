/* 
   Plug-in interface to general Gimp 
   debug switch 
   (c) Michal Gomulinski
   M.Gomulinski@writeme.com
*/

#include<stdlib.h>
#include<glib.h>
#include<libgimp/gimp.h>

extern char *strstr(const char*, const char *);

#define NOT_READY -1

/* 
   Query gimprc for token "debug"
   if it contains string "on" or "all" or keyword passed as an argument
   the return value will be TRUE. If not or if the keyword is NULL and
   debug string doesn't contain "on" or "all" string the return will be FALSE.

   The query process is done once only. Following calls to |gimp_debug_get_state()|
   or |gimp_debug()| will return the value prepared during the first call.
*/

gint gimp_debug_get_state(gchar *keyword)
{
  static gint state=NOT_READY;
  GParam *rets;
  gint nrets;
  gchar *debugstr;

  if (state != NOT_READY) 
    return state;
  else {               /* We are called for the first time */
    rets = gimp_run_procedure ("gimp_gimprc_query",
			       &nrets,
			       PARAM_STRING, "debug",
			       PARAM_END);

    if (rets[0].data.d_status != STATUS_SUCCESS ||
	rets[1].data.d_string == NULL ) state=FALSE;
    else {
      /* Non-empty "debug" debug entry in gimprc */
      debugstr=rets[1].data.d_string;
      if (strcmp(debugstr, "all") == 0 ||
	  strcmp(debugstr, "on") == 0) state=TRUE;
      if (state==NOT_READY) {
	if (!keyword) state=FALSE;
	else {
	  if (strstr(debugstr, keyword) != NULL) state=TRUE;
	  else state=FALSE;
	}
      }
    }
    if (state==NOT_READY) {
      g_print("Something is wrong in 'gimp_debug_get_state' routine.\n");
      state=TRUE;
    }
    gimp_destroy_params(rets, nrets);
  }
  return state;
}
