

GtkEntry *dialog_create_value(char *title,       
			 GtkTable *table,   
			 int row,           /* Table row */
			 gdouble *value,    /* pointer to variable */
			 double left,       /* bounds */
			 double right, 
			 double step,       
			 int entry_width,   /* text_entry width in pixels */
			 int scale_width);  /* scale width in pixels */

void gtk_entry_set_double(GtkEntry *, gdouble);

void gtk_entry_set_int(GtkEntry *, gint);

void dialog_scale_update(GtkAdjustment *adjustment, gdouble *value);

void dialog_entry_update(GtkWidget *widget, gdouble *value);
