/* L-Systems plug-in
   Utilities fuctions
   Michal Gomulinski 	
   $Id$	 
*/


#include<stdlib.h>
#include<string.h>
#include<gtk/gtk.h>
#include"lsystui.h"
#include"lsystem.h"
#include"callbacks.h"
#include"utils.h"
#include"gdebug.h"

#ifndef lint
static char vcid[] = "$Id$";
#endif /* lint */

/* Dummy function to disable warning about vcid */
void utils_no_warning()
{
  (void)vcid;
}

void ls_clist_add_empty_rule(GtkCList *list, gint ruleno)
{
  gint rowno;
  gchar *emptyrow[7]={ "--", "--", "--", "--", "--", "--", "--" };

  gtk_clist_freeze(list);
  rowno=gtk_clist_append(list, emptyrow);
  gtk_clist_set_row_data(list, rowno, (gpointer) ruleno);
  gtk_clist_select_row(list, rowno, 0);
  gtk_clist_moveto(list, rowno, 0, 0.0, 0.0);
  gtk_clist_thaw(list);
}

void ls_clist_update_selected_rule(GtkCList *list, gchar *rule)
{
  gint rowno;
  gpointer saved;
    
  rowno=(gint) list->selection->data;
  gtk_clist_freeze(list);
  saved=gtk_clist_get_row_data(list, rowno);

  rule[1]=0;
  gtk_clist_set_text(list, rowno, 0, rule);
  gtk_clist_set_text(list, rowno, 1, "->");
  gtk_clist_set_text(list, rowno, 2, rule+2);
  rule[1]=':';

  gtk_clist_set_row_data(list, rowno, saved);
  gtk_clist_select_row(list, rowno, 0);
  gtk_clist_moveto(list, rowno, 0, 0.0, 0.0);
  gtk_clist_thaw(list);
}

void ls_clist_update_selected_srule(GtkCList *list, Special *srule)
{
  gchar buf[MAX_LINE_LENGTH];
  gint rowno;
#ifdef CLIST_HAS_WIDGET
  GtkWidget *color_prv;
  gint pi;
  gchar pbuf[PRV_WID];
#endif

  if (!srule) return;
  gtk_clist_freeze(list);
  rowno=(gint)list->selection->data;
  memset(buf, 0, sizeof(buf));
  buf[0]=srule->lefthand; gtk_clist_set_text(list, rowno, 0, buf);
  gtk_clist_set_text(list, rowno, 1, "->");
  sprintf(buf, "%6.2f", srule->turn_angle); gtk_clist_set_text(list, rowno, 2, buf);
  gtk_clist_set_text(list, rowno, 3, srule->brush_name); 
  if (srule->brush_spc >=0)
    sprintf(buf, "%d", srule->brush_spc); 
  else
    sprintf(buf, "--");
  gtk_clist_set_text(list, rowno, 4, buf);  
  if (srule->opacity >= 0)
    sprintf(buf, "%d%%", srule->opacity); 
  else
    sprintf(buf, "--");
  gtk_clist_set_text(list, rowno, 5, buf);  
  if (srule->color_change) {
#ifdef CLIST_HAS_WIDGET
    color_prv=gtk_preview_new(GTK_PREVIEW_COLOR);
    gtk_preview_size(GTK_PREVIEW(color_prv), PRV_WID, PRV_HEI);
    for (pi=0; pi<PRV_WID; pi+=3) {
      pbuf[pi]=vals->color[0];
      pbuf[pi+1]=vals->color[1];
      pbuf[pi+2]=vals->color[2];
    }
    for (pi=0; pi<PRV_HEI; pi++)
      gtk_preview_draw_row(GTK_PREVIEW(color_prv), pbuf, 0, pi, PRV_WID);
    gtk_widget_show(color_prv);
    gtk_clist_set_widget(GTK_CLIST(lsui.srules), rowno, 5, color_prv);
#else
    sprintf(buf, "(%2X,%2X,%2X)", srule->color[0], srule->color[1], srule->color[2]); 
    gtk_clist_set_text(list, rowno, 6, buf);
#endif
  } else gtk_clist_set_text(list, rowno, 6, "--");
  gtk_clist_thaw(list);
}

gpointer ls_clist_get_selected_row_data(GtkCList *list)
{
  gint rowno;

  if (!list) return NULL;
  if (!list->selection) return NULL;

  rowno=(gint) list->selection->data;
  return gtk_clist_get_row_data(list, rowno);
}

void ls_clist_remove_selected(GtkCList *list)
{
  gint rowno;
  gint ruleno;

  if (!list) return;
  if (!list->selection) return;

  gtk_clist_freeze(list);
  rowno=(gint) list->selection->data;
  ruleno=(gint)gtk_clist_get_row_data(list, rowno);
  gtk_clist_remove(list, rowno);
  gtk_clist_thaw(list);
}

void add_button( GtkWidget *box, GtkWidget *button, 
			GtkSignalFunc h)
{
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(box), button, TRUE, TRUE, 5);
  gtk_signal_connect(GTK_OBJECT(button), "clicked", h, NULL);
}

void add_button_d( GtkWidget *box, GtkWidget *button, 
			GtkSignalFunc h, gpointer d)
{
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(box), button, TRUE, TRUE, 5);
  gtk_signal_connect(GTK_OBJECT(button), "clicked", h, d);
}


void ls_box_error(gchar *text)
{
  GtkWidget *wind, *label, *button;
  
  wind=gtk_dialog_new();
  gtk_window_set_title(GTK_WINDOW(wind), "Warning/Error");
  gtk_container_border_width(GTK_CONTAINER(wind), 20);
  gtk_signal_connect(GTK_OBJECT(wind), "destroy", 
		     GTK_SIGNAL_FUNC(ls_univ_close), wind);
  label=gtk_label_new(text);
  gtk_box_pack_start(GTK_BOX( GTK_DIALOG(wind)->vbox ), label, 
		     TRUE, TRUE, 5);
  gtk_widget_show(label);

  button=gtk_button_new_with_label("Ok");
  GTK_WIDGET_SET_FLAGS( button, GTK_CAN_DEFAULT );
  gtk_signal_connect(GTK_OBJECT(button), "clicked", 
		     GTK_SIGNAL_FUNC(ls_univ_close), wind);
  gtk_box_pack_start(GTK_BOX( GTK_DIALOG(wind)->action_area ), button, 
		     TRUE, TRUE, 10);
  gtk_widget_show(button);
  gtk_widget_grab_default(button);
  gtk_widget_show(wind);
}


gint ls_check_lsystem_name(gchar *n)
{
  GList *lss=loaded_lsystems->next;
  gint ret=OK;
  gchar *lsname;

  while (lss->next) {
    lsname=((LSinfo*)lss->data)->def.name;
    if (strcmp( lsname, n) == 0 ) {
      ret=DUPLICATE;
      break;
    }
    lss=lss->next;
  }
  return ret;
}

gint ls_check_lsystem_filename(gchar *fn)
{
  GList *lss=loaded_lsystems->next;
  gint ret=OK;
  gchar *lsfname;

  while (lss->next) {
    lsfname=((LSinfo *)lss->data)->def.fname;
    if (!lsfname) {
      lss=lss->next;
      continue;
    }
    if (strcmp( lsfname, fn)==0 ) {
      ret=DUPLICATE;
      break;
    }
    lss=lss->next;
  }
  return ret;
}

GtkWidget *ls_create_lsystem_subtree(gchar *name, gchar *prefix, gint level)
{
  GtkWidget *ret;
  GtkWidget *treeitem;
  GtkWidget *subtree;
  GtkWidget *item4exp;
  static GtkWidget *item4sel=NULL;
  static GtkWidget *tree4sel=NULL;
  GList *cur;
  LSinfo *clsys=NULL;
  gchar *lsname=NULL, 
    *tailname=NULL, 
    *tailend=NULL, 
    *newprefix=NULL, 
    *newname=NULL;
  gchar *indent;
  gint prefix_len=0,
    newprefixlen=0;
  gint i, skipped;
  static gint counter=0;
  
  g_return_val_if_fail(prefix != NULL, NULL);
  g_return_val_if_fail(name != NULL, NULL);

  counter++;
  indent=g_malloc(2*level+1);
  for (i=0; i<2*level; i++) indent[i]=' ';
  indent[2*level]=0;
  ret=gtk_tree_new();
  GTK_TREE(ret)->root_tree=GTK_TREE(ret);
  prefix_len=strlen(prefix);
  cur=loaded_lsystems->next;
  while (cur) {
    skipped=FALSE;
    clsys=(LSinfo *)cur->data;
    lsname=clsys->def.name;
    if (strncmp (prefix, lsname, prefix_len) == 0) {
      /* Only L-Systems with names starting with |prefix| interest us */
      tailname=lsname+prefix_len;
      if (*tailname == '/') tailname++;
      tailend=strchr(tailname, '/');
      if (tailend!=NULL) {
	  /* 
	     We must create another level deeper if:
	     * we are not at the leaf
	     * we are at the first element of the list
	     * we haven't already created that subtree
	  */
	  (*tailend)=0; /* |tailname| finishes at the first occurance of |'/'| for the moment */
	  newprefix=g_strdup(lsname); /* |lsname| is finished at the |tailend|. */
	  newprefixlen=strlen(newprefix);
	  newname=g_strdup(tailname);
	  if (gimp_debug()) 
	    g_print("%sPreparing subtree named '%s' to be put in '%s'\n", 
		    indent, newname, name);
	  (*tailend)='/';
	  subtree=ls_create_lsystem_subtree(newname, newprefix, level+1);
	  treeitem=gtk_tree_item_new_with_label(newname); gtk_widget_show(treeitem);
	  gtk_object_set_user_data(GTK_OBJECT(treeitem), newname);
	  gtk_tree_append(GTK_TREE(ret), treeitem);
	  gtk_tree_item_set_subtree(GTK_TREE_ITEM(treeitem), subtree);
	  if (gimp_debug())
	    g_print("%sInserted subtree '%s'\n", indent, newname);

	  /* Skip all the names we have just added (by recursion) */
	  if (gimp_debug())
	    g_print("%sSkipping all l-systems begining with '%s'\n", indent, newprefix);
	  while (strncmp(lsname, newprefix, newprefixlen)==0) {
	    if (!cur->next) { 
	      skipped=FALSE; 
	      break; 
	    }
	    cur=cur->next;
	    lsname=((LSinfo *)(cur->data))->def.name;
	    skipped=TRUE;
	  }
	  g_free(newprefix);	  
      } else {
	/* We've finally reached a leaf of the tree */
	/*	label=gtk_label_new(tailname);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_widget_show(label); */

	treeitem=gtk_tree_item_new_with_label(tailname);
	/*	clsys->lab=GTK_LABEL(label); */
	clsys->lab=GTK_LABEL(GTK_BIN(treeitem)->child);
	/* gtk_container_add(GTK_CONTAINER(treeitem), label); */
	if (clsys==default_lsystem) {
	  item4sel=treeitem;
	  tree4sel=ret;
	}
	gtk_object_set_user_data(GTK_OBJECT(treeitem), (gpointer)clsys);
	gtk_widget_show(treeitem);
	gtk_tree_append(GTK_TREE(ret), treeitem);
	if (gimp_debug()) 
	  g_print("%sInserted item '%s' into subtree named '%s'\n", 
		  indent, tailname, name);
      }
    } 
    if (!skipped) cur=cur->next;
  }
  counter--;
  if (counter==0 && item4sel) {
    if (gimp_debug()) g_print("Selecting default..\n");
    gtk_tree_set_selection_mode(GTK_TREE(ret), GTK_SELECTION_BROWSE);
    /* Parttial fix for erratic gtk_tree_item_set_subtree */
    
    GTK_TREE(tree4sel)->root_tree=GTK_TREE(ret);
    
    /* End of fix */
    item4exp=GTK_TREE(tree4sel)->tree_owner;
    gtk_tree_select_child(GTK_TREE(tree4sel), item4sel);
    while (item4exp) {
      gtk_tree_item_expand(GTK_TREE_ITEM(item4exp));
      item4exp=GTK_TREE( GTK_WIDGET( item4exp )->parent)->tree_owner;
    }
  }
  gtk_widget_show(ret);
  g_free(indent);
  return ret;
}

void ls_tree_add_lsystem(LSinfo *lsystem)
{
  GtkTree *lstree=GTK_TREE(lsui.lst);
  GtkTree *thistree=lstree;
  GtkWidget *treeitem, *subtree;
  GList *list;
  gchar *subname, *thisname;
  gchar *tmpname=g_strdup(lsystem->def.name);
  gchar *lastpart=strrchr(tmpname, '/')+1;
  gchar *buf=g_malloc(256 * sizeof(gchar));

  list=gtk_container_children(GTK_CONTAINER(lstree));
  subname=strtok(tmpname, "/");
  if (gimp_debug()) g_print("subname=%s\n", subname);
  while (list) {
    if (GTK_IS_TREE_ITEM(list->data) && GTK_TREE_ITEM(list->data)->subtree) {
      thisname=gtk_object_get_user_data(GTK_OBJECT(list->data));
      if (strcmp(subname, thisname)==0) {
	/* We have found the appropriate subtree, search it! */
	subname=strtok(NULL, "/");
	thistree=GTK_TREE( GTK_TREE_ITEM(list->data)->subtree );
	list=gtk_container_children(GTK_CONTAINER( GTK_TREE_ITEM(list->data)->subtree ));
	if (gimp_debug()) g_print("Searching deeper... subname=%s\n", subname);
	continue;
      }
    } else if (gimp_debug()) g_print("Skiping tree item\n");
    list=list->next;
  }

  g_assert(list==NULL);
  if (gimp_debug()) g_print("Almost finished...\n");

  if (subname==lastpart) {
    /* We have found all subtrees and are creating just an entry in 
       the |thistree| tree. */
    if (lsystem->dirty) sprintf(buf, "%s-##", subname);
    else strcpy(buf, subname);
    treeitem=gtk_tree_item_new_with_label(buf);
    lsystem->lab=GTK_LABEL(GTK_BIN(treeitem)->child);
    gtk_object_set_user_data(GTK_OBJECT(treeitem), lsystem);
    gtk_tree_append(thistree, treeitem);
    gtk_tree_select_child(thistree, treeitem);
    gtk_widget_show(treeitem);
  } else {
    /* We must create subtrees from |thistree| (named |thisname|)*/
    while(subname!=lastpart) {
      treeitem=gtk_tree_item_new_with_label(subname);
      gtk_object_set_user_data(GTK_OBJECT(treeitem), g_strdup(subname));
      gtk_tree_append(thistree, treeitem);
      subtree=gtk_tree_new();
      gtk_tree_item_set_subtree(GTK_TREE_ITEM(treeitem), subtree);
      subname=strtok(NULL, "/");
      gtk_widget_show(subtree);
      gtk_widget_show(treeitem);
      gtk_tree_item_expand(GTK_TREE_ITEM(treeitem));
      thistree=GTK_TREE(subtree);
    }
    treeitem=gtk_tree_item_new_with_label(subname);
    gtk_object_set_user_data(GTK_OBJECT(treeitem), lsystem);
    gtk_tree_append(thistree, treeitem);
    gtk_widget_show(treeitem);
  }
  g_free(tmpname);
  g_free(buf);
}
